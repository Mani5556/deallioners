package com.example.deallionaires.adapters

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.DealStatus
import kotlinx.android.synthetic.main.list_completed_deal_item.view.*

class CompletedDealsAdapter(
    val context: Context,
    private val dealsStatus: ArrayList<DealStatus>,
    private val itemClick: (DealStatus, Boolean) -> Unit
) : RecyclerView.Adapter<CompletedDealsAdapter.DealsStatusViewHolder>() {

    class DealsStatusViewHolder(
        private val view: View,
        val itemClick: (DealStatus, Boolean) -> Unit
    ) :
        RecyclerView.ViewHolder(view) {
        fun bindFavourites(
            context: Context,
            deal: DealStatus
        ) {
            with(deal) {
                Glide.with(context).load(profile_image)
                    .placeholder(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.ivCompetedDealImage)
                itemView.tvCompletedDealBusinessName.text = business_name
                itemView.tvCompletedDealBusinessSubCategory.text = title
                itemView.tvCompletedDealsDeallions.text = total_deallions?.toString()?: "0"
                itemView.tv_dealliaons_earned.text = " : $deallionsEarned"
                itemView.tv_deallioans_used.text = " : $deallionsUsed"

                itemView.tvCompletedDealPurchaseAmount.text = " : " +purchase_amount.toString()
                itemView.tvCompletedDealSavedAmount.text = " : " +saving_amount.toString()

                if(deallions_offered == null || deallions_offered == 0){
                    itemView.tvCompletedDealDeallionPointsRedeemed.text = " : " + "0"
                }else {
                    itemView.tvCompletedDealDeallionPointsRedeemed.text = " : " +deallionsUsed
                }

                itemView.tvCompletedDealTransactionId.text = Html.fromHtml("Transaction Id :<font color = '#f7bc00'> $transaction_id</font>")

                itemView.setOnClickListener {
                    itemClick(deal, false)
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): DealsStatusViewHolder {
        return DealsStatusViewHolder(
            (LayoutInflater.from(context).inflate(R.layout.list_completed_deal_item, parent, false))
            , itemClick
        )
    }

    override fun getItemCount() = dealsStatus.size

    override fun onBindViewHolder(holder: DealsStatusViewHolder, position: Int) {
        holder.bindFavourites(context, dealsStatus[position])
    }
}