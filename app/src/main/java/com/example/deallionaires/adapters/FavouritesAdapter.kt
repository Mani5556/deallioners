package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.deallionaires.R
import com.example.deallionaires.model.Getfavouritedetail
import com.example.deallionaires.utils.getExpiry
import kotlinx.android.synthetic.main.activity_search_business_details.*
import kotlinx.android.synthetic.main.item_list_favourites.view.*
import java.sql.Date
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FavouritesAdapter(
    private val context: Context,
    private val favourites: ArrayList<Getfavouritedetail>,
    private val itemClick: (Getfavouritedetail, Boolean) -> Unit
) : RecyclerView.Adapter<FavouritesAdapter.ViewHolder>() {

    class ViewHolder(private val view: View, private val itemClick: (Getfavouritedetail, Boolean) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bindFavourites(
            context: Context,
            getfavouritedetail: Getfavouritedetail,
            position: Int
        ) {
            with(getfavouritedetail) {

                Glide.with(context).load(profileImage)
                    .error(R.drawable.deallionaires_logo)
                    .placeholder(R.color.gray_20)
                    .transition(DrawableTransitionOptions.withCrossFade(1000))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.ivFavouriteItemProfileImage)
                itemView.tvItemFavouriteBusinessName.text = businessName
                itemView.tvItemFavouriteLocation.text = address
                itemView.rbItemFavouriteRating.rating = rating!!
                itemView.tvFavouritesItemRating.text = if(ratingCount!! == 0)"" else "$ratingCount Reviews"
                 if(followerCount!! > 0){
                     itemView.tv_following_count.visibility = View.VISIBLE
                     itemView.tv_following_count.text = "$followerCount Followers"
                 } else {
                     itemView.tv_following_count.visibility = View.GONE
                 }


                if(deallionairesClub.isNullOrBlank()){
                    itemView.rv_badge_list.visibility = View.GONE
                }else{
                    itemView.rv_badge_list.run {
                        visibility = View.VISIBLE
                        this.adapter = BadgeAdapter(context,deallionairesClub!!.replace("D","").toInt())
                        layoutManager = LinearLayoutManager(
                            context,
                            LinearLayoutManager.HORIZONTAL,
                            false
                        )
                    }
                }

                if (deallionairesClub.isNullOrBlank()) {
                    itemView.rv_badge_list.visibility = View.GONE
                    itemView.tv_expires.visibility = View.GONE
                } else {
                    val club =  deallionairesClub!!.replace("D", "").toInt()
                    itemView.tv_expires.visibility = View.GONE

                    itemView.rv_badge_list.run {
                        visibility = View.VISIBLE
                        this.adapter = BadgeAdapter(context,club)
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    }
                    if(club == 1){
                        itemView.tv_expires.text = "No Expiry"
                    }else{
                        itemView.tv_expires.text = getExpiry(expiry_date).let { it }?:""
                    }
                }


                expiry_date?.let {
                    val outputFormater = SimpleDateFormat("dd-MMM-yyyy")
                    val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
                    itemView.tv_expires.visibility = View.VISIBLE
                    val date = outputFormater.format(inputFormat.parse(it))
                    itemView.tv_expires.setText("Club Exp:${date}")
                }?: run {
                    itemView.tv_expires.visibility = View.GONE
                }

                if (subcategory_name.isNullOrEmpty()){
                    itemView.tvFavouritesItemCategorySubCategory.text = "${category_name}/NA"
                }else {
                    itemView.tvFavouritesItemCategorySubCategory.text =
                        "$category_name/$subcategory_name"
                }

                itemView.ivFavouritesItemFavourite.setOnClickListener {
                    itemClick(getfavouritedetail, true)
                }

                itemView.setOnClickListener {
                    itemClick(getfavouritedetail, false)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(
            (LayoutInflater.from(context).inflate(R.layout.item_list_favourites, parent, false)), itemClick
        )
    }

    override fun getItemCount() = favourites.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindFavourites(context, favourites[position], position)
    }
}