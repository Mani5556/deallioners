package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.deallionaires.R
import com.example.deallionaires.databinding.ItemFaqsBinding
import com.example.deallionaires.databinding.ItemFaqsCategoryBinding
import com.example.deallionaires.model.FaqCategory
import com.example.deallionaires.model.Faqs

class FaqsCategoriesAdapter(val context: Context, val categoriesList: List<FaqCategory>) :
    RecyclerView.Adapter<FaqsCategoriesAdapter.FaqsCategoryViewHolder>() {
    var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqsCategoryViewHolder {
        return FaqsCategoryViewHolder(
            DataBindingUtil.inflate<ItemFaqsCategoryBinding>(
                LayoutInflater.from(context),
                R.layout.item_faqs_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = categoriesList.size

    override fun onBindViewHolder(holder: FaqsCategoryViewHolder, position: Int) {
        holder.bind(position)
    }


    inner class FaqsCategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ItemFaqsCategoryBinding? = null
        var currentPosition = 0
        var faqsAdapter: FaqsAdapter? = null

        constructor(binding: ItemFaqsCategoryBinding) : this(binding.root) {
            this.binding = binding

            binding?.rvFaqs?.run {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            }

            binding?.lytHeader?.setOnClickListener {
                if (selectedPosition != currentPosition) {
                    categoriesList[selectedPosition].isExpanded = false
                    notifyItemChanged(selectedPosition)
                    selectedPosition = currentPosition
                }

                categoriesList[selectedPosition].isExpanded = !categoriesList[selectedPosition].isExpanded
                notifyItemChanged(selectedPosition)

            }


        }

        fun bind(position: Int) {
            currentPosition = position
            binding?.faqCategory = categoriesList.get(position)

            if (faqsAdapter == null) {
                faqsAdapter = categoriesList[position].faqsList?.let { FaqsAdapter(context, it) }
                binding?.rvFaqs?.adapter = faqsAdapter

            } else {
                faqsAdapter?.updateList(categoriesList[position].faqsList)
            }

        }
    }

}


class FaqsAdapter(val context: Context, var faqsList: List<Faqs>) :
    RecyclerView.Adapter<FaqsAdapter.FaqsViewHolder>() {
    var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FaqsViewHolder {
        return FaqsViewHolder(
            DataBindingUtil.inflate<ItemFaqsBinding>(
                LayoutInflater.from(context),
                R.layout.item_faqs,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = faqsList.size

    override fun onBindViewHolder(holder: FaqsViewHolder, position: Int) {
        holder.bind(position)
    }

    fun updateList(newFaqsList: List<Faqs>?) {
        newFaqsList?.let {
            faqsList = newFaqsList
            notifyDataSetChanged()
        }

    }

    inner class FaqsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: ItemFaqsBinding? = null

        constructor(binding: ItemFaqsBinding) : this(binding.root) {
            this.binding = binding
        }

        fun bind(position: Int) {
            binding?.faqs = faqsList.get(position)
            if (position == faqsList.size - 1) {
                binding?.divider?.visibility = View.GONE
            } else {
                binding?.divider?.visibility = View.VISIBLE
            }

            binding?.tvFaqQuestion?.setOnClickListener {
                if (selectedPosition != position) {
                    faqsList[selectedPosition].isExpanded = false
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }

                faqsList[selectedPosition].isExpanded = !faqsList[selectedPosition].isExpanded
                notifyItemChanged(selectedPosition)
            }
        }
    }

}