package com.example.deallionaires.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.deallionaires.R
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.merchantWise.MerchantWiseFragment
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise.TransactionWiseFragment

class DeallionsEarnedPageAdapter(
    val context: Context,
    private val fragmentManager: FragmentManager
) : FragmentPagerAdapter(
    fragmentManager,
    FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {
    override fun getItem(p0: Int): Fragment {
        return when (p0) {
            0 -> {
                MerchantWiseFragment()
            }
            1 -> {
                TransactionWiseFragment()
            }
            else -> Fragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.txt_merchant_wise)
            else -> {
                context.getString(R.string.txt_transaction_wise)
            }
        }
    }

}