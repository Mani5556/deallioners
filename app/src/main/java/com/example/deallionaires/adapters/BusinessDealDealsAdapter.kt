package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Viewdeal
import kotlinx.android.synthetic.main.content_search_business_details.view.*
import kotlinx.android.synthetic.main.list_item_search_business_branches.view.*
import kotlinx.android.synthetic.main.list_item_search_business_deals.view.*

class BusinessDealDealsAdapter (
    private val context: Context,
    private val searchBusinessDeals: ArrayList<Viewdeal>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class BusinessDealDealsViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindBusinessDealDeals(
            context: Context,
            searchBusinessDeal: Viewdeal,
            position: Int
        ) {
            with(searchBusinessDeal) {
                Glide.with(context).load(searchBusinessDeal.dealImage)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.civSearchBusinessDealImage)
                itemView.tvSearchBusinessDealDealName.text = searchBusinessDeal.title
                itemView.tvSearchBusinessDealRegularPrice.text =
                    "$" + searchBusinessDeal.regularPrice
                itemView.tvSearchBusinessDealDealDescription.text = searchBusinessDeal.description
                itemView.tvSearchBusinessDealOfferPrice.text = "Value:$"+searchBusinessDeal.offerPrice

                itemView.setOnClickListener {
                    click(dealid)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return BusinessDealDealsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_search_business_deals,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchBusinessDeals.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderBusinessDealDeals = holder as BusinessDealDealsViewHolder
        holderBusinessDealDeals.bindBusinessDealDeals(
            context,
            searchBusinessDeals[position],
            position
        )
    }
}