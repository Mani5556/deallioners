package com.example.deallionaires.adapters

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Offervaluepoint
import com.example.deallionaires.utils.addCommasToNumericString
import kotlinx.android.synthetic.main.list_item_merchant_wise.view.*

class MerchantWiseDeallionsAdapter(
    private val context: Context,
    private val app_level_loyalty_points: Int,
    private val offerValuePoints: ArrayList<Offervaluepoint>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class MerchantWiseDeallionsViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindMerchantWiseDeallions(
            context: Context,
            appLevelLoyaltyPoints: Int,
            offerValuePoint: Offervaluepoint,
            position: Int
        ) {
            with(offerValuePoint) {
                Glide.with(context).load(profileImage)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.civMerchantWiseDeallionsProfileImage)

                itemView.tvMerchantWiseDeallionsCategoryName.text = categoryName ?: "N/A"
                itemView.tvMerchantWiseDeallionsBusinessName.text = businessName ?: "N/A"
                itemView.tv_app_level_points.text = ": ${appLevelLoyaltyPoints}"
                itemView.tv_deallions_points.text = ": ${marchentLevelPoints}"
                itemView.tv_total_points_reddem.text = ": ${totalPoints}"

                if (isAppLevelPointAllow == "1") {
                    itemView.tvAppLevelDeallionYes.visibility = View.VISIBLE
                    itemView.tvAppLevelDeallionNo.visibility = View.GONE
                    itemView.lyt_app_level_points.visibility = View.VISIBLE

                    if (totalPoints != null) {
                        val loyaltyPoints = totalPoints?.toInt()?:"" + appLevelLoyaltyPoints
                        itemView.tvMerchantWiseDeallionsLoyaltyPoints.text =
                            addCommasToNumericString(loyaltyPoints.toString())

                    } else {
                        itemView.tvMerchantWiseDeallionsLoyaltyPoints.text = "0"
                    }
                } else {
                    itemView.tvAppLevelDeallionYes.visibility = View.GONE
                    itemView.tvAppLevelDeallionNo.visibility = View.VISIBLE
                    itemView.lyt_app_level_points.visibility = View.GONE
                    if (totalDeallions != null) {
                        itemView.tvMerchantWiseDeallionsLoyaltyPoints.text =
                            addCommasToNumericString(totalDeallions.toString())
                    } else {
                        itemView.tvMerchantWiseDeallionsLoyaltyPoints.text = "0"
                    }
                }

                itemView.setOnClickListener {
                    click(merchantId!!)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return MerchantWiseDeallionsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_merchant_wise,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = offerValuePoints.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderMerchantWiseDeallions = holder as MerchantWiseDeallionsViewHolder
        holderMerchantWiseDeallions.bindMerchantWiseDeallions(
            context,
            app_level_loyalty_points,
            offerValuePoints[position],
            position
        )
    }
}