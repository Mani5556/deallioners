package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Cart
import kotlinx.android.synthetic.main.item_list_cart.view.*

class CartAdapter(
    private val context: Context,
    private val carts: ArrayList<Cart>
) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindCart(
            context: Context,
            cart: Cart,
            position: Int
        ) {
            with(cart) {
                Glide.with(context).load(image)
                    .error(R.drawable.ic_default_category)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.ivItemCartImage)
                itemView.tvItemCartTitle.text = title
                itemView.tvItemCartDescription.text = description
                itemView.tvItemCartCount.text = "$count"
                itemView.tvItemCartExpireOnDate.text = expireOn
                itemView.tvItemCartTransactionId.text =
                    "${context.getString(R.string.txt_transaction_id)}$transactionId"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return ViewHolder(
            (LayoutInflater.from(context).inflate(R.layout.item_list_cart, parent, false))
        )
    }

    override fun getItemCount() = carts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindCart(context, carts[position], position)
    }
}