package com.example.deallionaires.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.SearchDealsCategoryItems
import com.example.deallionaires.model.Searchbusiness
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.getDpSize
import com.example.deallionaires.utils.getExpiryDays
import kotlinx.android.synthetic.main.list_home_deals.view.*
import kotlinx.android.synthetic.main.list_item_user_deals_items.view.*

class SearchDealsItemsAdapter(
    private val context: Activity,
    private var searchDealsCategoryItems: ArrayList<SearchDealsCategoryItems>,
    var canCheckApplicability:Boolean = false,
    private val itemClick: (Int) -> Unit

) : RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return UserDealsItemsViewHolder(
            (LayoutInflater.from(context).inflate(
//                R.layout.list_item_user_deals_items,
                R.layout.list_home_deals,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchDealsCategoryItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderUserDealsSubCategories = holder as UserDealsItemsViewHolder
        holderUserDealsSubCategories.bindUserDealsItems(
            context,
            searchDealsCategoryItems[position],
            canCheckApplicability
        )
    }

    fun updateData(searchDealsList: ArrayList<SearchDealsCategoryItems>): Unit {
        this.searchDealsCategoryItems = searchDealsList
        notifyDataSetChanged()
    }
}

//this class is commen for the both adapters SearchDealsItemsAdapter,SearchDealsItemsAdapterLoadMore
class UserDealsItemsViewHolder(view: View, private val click: (Int) -> Unit) :
    RecyclerView.ViewHolder(view) {
    fun bindUserDealsItems(
        context: Activity,
        searchDealsCategoryItem: SearchDealsCategoryItems,
        canCheckApplicability:Boolean

    ) {
        with(searchDealsCategoryItem) {
            Glide.with(context).load(dealImage)
                .error(R.drawable.deallionaires_logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(itemView.ivHomeDeal)

            itemView.tv_discount_price.text = "$offerPrice BD"
            Glide.with(context).load(profileImage)
                .error(R.drawable.deallionaires_logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)

                .into(itemView.civDealBusinessIcon)
            itemView.tvDealTitle.text = business_name
            itemView.tvDealDesc.text =
                if (typeOfDeal.equals("fixed")) "$loyaltyPoints" else "$loyaltyPoints per one BD purchase"
            itemView.tv_address.text = address
            itemView.tv_expires.text = expiryDate?.let { "EXP : ${getExpiryDays(it)} " } ?: ""

            itemView.tv_original_price?.run {
                text = regularPrice.toString()
                paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            }

            if(isApplicable == 0 && canCheckApplicability){
                itemView.lyt_overlay.visibility = View.VISIBLE
            }else{
                itemView.lyt_overlay.visibility = View.GONE
            }

            itemView.tv_save_amount.text = "Save ${savingAmount} BD"

            itemView.tv_deal_discount_percent.text = "${Math.round(discount)} %"
            itemView.tv_discount_benifits.text = "$title"
            itemView.tv_deal_name.text = "$catslug / $offerCategory"
            itemView.tv_redeemed.text =
                "$redeemed_deals Redeemed /$remainingDeals Remaining"

            itemView.setOnClickListener {
                click(dealid)
            }
        }

    }
}

/**
 * this adapter load the serach deals data and supports the load more option also
 */
class SearchDealsItemsAdapterLoadMore(
    private val context: Activity,
    private var searchDealsCategoryItems: ArrayList<SearchDealsCategoryItems?>,
    var canCheckApplicability:Boolean = false,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {

    val ITEM_DEAL = 1
    val ITEM_LOAD_MORE = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if(viewType == ITEM_DEAL)
            UserDealsItemsViewHolder((LayoutInflater.from(context).inflate(R.layout.list_home_deals, parent, false)), itemClick)
        else
            LoadMoreViewHolder((LayoutInflater.from(context).inflate(R.layout.item_load_more, parent,false)))

    }

    override fun getItemCount() = searchDealsCategoryItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(holder is UserDealsItemsViewHolder ){
            holder.bindUserDealsItems(
                context,
                searchDealsCategoryItems[position]!!,
                canCheckApplicability
            )
        }else{
//            (holder as LoadMoreViewHolder
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if(searchDealsCategoryItems[position] != null) ITEM_DEAL else ITEM_LOAD_MORE
    }


    fun updateData(searchDealsList: ArrayList<SearchDealsCategoryItems?>): Unit {
        this.searchDealsCategoryItems = searchDealsList
        notifyDataSetChanged()
    }

    fun addLoadMore() {
        this.searchDealsCategoryItems.add(null)
        notifyItemChanged(searchDealsCategoryItems.size-1)
    }

    fun removeLoadMore(){
        try{
            searchDealsCategoryItems.removeAt(searchDealsCategoryItems.size-1)
            notifyItemChanged(searchDealsCategoryItems.size)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    inner class LoadMoreViewHolder(view: View):RecyclerView.ViewHolder(view){}
}