package com.example.deallionaires.adapters

import android.content.Context
import android.graphics.Paint
import android.text.Html
import android.view.LayoutInflater
import android.view.SurfaceControl
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Specialoffer
import com.example.deallionaires.model.TransactionDetail
import kotlinx.android.synthetic.main.item_transaction_detail.view.*
import kotlinx.android.synthetic.main.list_item_special_deals.view.*
import java.text.SimpleDateFormat

class TransactionDeatilsAdapter(
    private val context: Context,
    private val transactionDetailslList: List<TransactionDetail>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    class TransactionViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        fun bindSpecialDeals(
            context: Context,
            transaction:TransactionDetail?,
            position: Int
        ) {
            with(transaction) {
                if(this?.transactionType!!.equals("CREDITED")){
                   itemView.tv_amount.setText(Html.fromHtml("<font color='#0AAF0F'>${this.loyaltyPoints}  </font> "))
                   itemView.tv_type_record.setText(Html.fromHtml("<font color='#0AAF0F'>"+ "Earned" + "</font> "))
                }else{
                    itemView.tv_amount.setText(Html.fromHtml("<font color='#FF0000'>${this.loyaltyPoints} </font> "))
                    itemView.tv_type_record.setText(Html.fromHtml("<font color='#FF0000'>"+ "Redeemed" + "</font> "))
                }

                itemView.tv_date.text = this?.transactionDate?.split("T")?.first()


            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(LayoutInflater.from(context).inflate(R.layout.item_transaction_detail, parent, false))
    }

    override fun getItemCount() = transactionDetailslList!!.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holderSpecialDeals = holder as TransactionViewHolder
        holderSpecialDeals.bindSpecialDeals(context, transactionDetailslList?.get(position),position)
    }
}