package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Searchbusiness
import com.example.deallionaires.utils.getExpiry
import kotlinx.android.synthetic.main.item_list_favourites.view.*
import kotlinx.android.synthetic.main.item_list_favourites.view.tv_expires
import kotlinx.android.synthetic.main.list_home_deals.view.*
import kotlinx.android.synthetic.main.list_item_search_business.view.*

class SearchBusinessItemsAdapter(
    private val context: Context,
    private var searchBusinessItems: ArrayList<Searchbusiness>,
    private val itemClick: (Searchbusiness, Boolean, Int, Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return SearchBusinessItemsViewHolder(
            (LayoutInflater.from(context).inflate(
//                R.layout.list_item_search_business,
                R.layout.item_list_favourites,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchBusinessItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderSearchBusinessSubCategories = holder as SearchBusinessItemsViewHolder
        holderSearchBusinessSubCategories.bindSearchBusinessItems(
            context,
            searchBusinessItems[position],
            position
        )
    }

    fun updateData(searchBusinessList: ArrayList<Searchbusiness>): Unit {
        this.searchBusinessItems = searchBusinessList
        notifyDataSetChanged()
    }
}


class SearchBusinessItemsViewHolder(
    view: View,
    private val click: (Searchbusiness, Boolean, Int, Int) -> Unit
) : RecyclerView.ViewHolder(view) {
    fun bindSearchBusinessItems(
        context: Context,
        searchBusinessItem: Searchbusiness,
        position: Int
    ) {
//            with(searchBusinessItem) {
//                Glide.with(context).load(profileImage)
//                    .error(R.drawable.deallionaires_logo)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(itemView.ivSearchBusinessItem)
//                itemView.tvSearchBusinessItemName.text = businessName
//                itemView.tvSearchBusinessItemLocation.text = address
//                itemView.ratingBarSearchBusinessItem.rating = rating
//                itemView.tvSearchBusinessItemRating.text = "($rating)"
//                itemView.tvSearchBusinessItemFollowers.text = "($followers)"
//                if (subcategoryName.isNullOrEmpty()){
//                    itemView.tvSearchBusinessItemCategorySubCategory.text = "${slug}/NA"
//                }else {
//                    itemView.tvSearchBusinessItemCategorySubCategory.text =
//                        "$slug/$subcategoryName"
//                }
//
//                if (isLiked) {
//                    itemView.ivSearchBusinessItemFavourite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_liked_red))
//                } else {
//                    itemView.ivSearchBusinessItemFavourite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
//                }
//
//
//                itemView.ivSearchBusinessItemFavourite.setOnClickListener {
//                    click(searchBusinessItem, true, isLiked)
//                }
//
//                itemView.setOnClickListener {
//                    click(searchBusinessItem, false, false)
//                }
//
//            }


        with(searchBusinessItem) {

            Glide.with(context).load(profileImage)
                .error(R.drawable.deallionaires_logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.color.gray_20)
                .into(itemView.ivFavouriteItemProfileImage)
            itemView.tvItemFavouriteBusinessName.text = businessName
            itemView.tvItemFavouriteLocation.text = address
            itemView.rbItemFavouriteRating.rating = rating!!
            itemView.tvFavouritesItemRating.text =
                if (ratingCount == null || ratingCount == 0) "" else "$ratingCount Reviews"
            itemView.tv_following_count.text = "$followers Followers"



//                if (subcategory_name.isNullOrEmpty()){
//                    itemView.tvFavouritesItemCategorySubCategory.text = "${category_name}/NA"
//                }else {
//                    itemView.tvFavouritesItemCategorySubCategory.text =
//                        "$category_name/$subcategory_name"
//                }


            if (deallionairsclub.isNullOrBlank()) {
                itemView.rv_badge_list.visibility = View.GONE
                itemView.tv_expires.visibility = View.GONE
            } else {
                val club =  deallionairsclub!!.replace("D", "").toInt()
                itemView.tv_expires.visibility = View.VISIBLE

                itemView.rv_badge_list.run {
                    visibility = View.VISIBLE
                    this.adapter = BadgeAdapter(context,club)
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                }

                if(club == 1){
                    itemView.tv_expires.text = "No Expiry"
                }else{
                    itemView.tv_expires.text = getExpiry(expiry_date).let { it }?:""
                }
            }

            if (isLiked == 1) {
                itemView.ivFavouritesItemFavourite.setImageDrawable(
                    context.resources.getDrawable(
                        R.drawable.ic_favourite_liked_red
                    )
                )
            } else {
                itemView.ivFavouritesItemFavourite.setImageDrawable(
                    context.resources.getDrawable(
                        R.drawable.ic_favourite_un_liked_red
                    )
                )
            }

            itemView.ivFavouritesItemFavourite.setOnClickListener {
                click(searchBusinessItem, true, isLiked, position)
            }

            itemView.setOnClickListener {
                click(searchBusinessItem, false, 0, position)
            }

        }

    }
}



class SearchBusinessItemsAdapterWithLoadMore(
    private val context: Context,
    private var searchBusinessItems: ArrayList<Searchbusiness?>,
    private val itemClick: (Searchbusiness, Boolean, Int, Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {

    val ITEM_BUSINESS = 1
    val ITEM_LOAD_MORE = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if(viewType == ITEM_BUSINESS)
            SearchBusinessItemsViewHolder((LayoutInflater.from(context).inflate(R.layout.item_list_favourites, parent,false)), itemClick)
        else
            LoadMoreViewHolder((LayoutInflater.from(context).inflate(R.layout.item_load_more, parent,false)))

    }

    override fun getItemCount() = searchBusinessItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(holder is SearchBusinessItemsViewHolder ){
            val holderSearchBusinessSubCategories = holder
            holderSearchBusinessSubCategories.bindSearchBusinessItems(
                context,
                searchBusinessItems[position]!!,
                position
            )
        }else{
//            (holder as LoadMoreViewHolder
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if(searchBusinessItems[position] != null) ITEM_BUSINESS else ITEM_LOAD_MORE
    }

    fun updateData(searchBusinessList: ArrayList<Searchbusiness?>): Unit {
        this.searchBusinessItems = searchBusinessList
        notifyDataSetChanged()
    }

    fun addLoadMore() {
        searchBusinessItems.add(null)
        notifyItemChanged(searchBusinessItems.size-1)
    }

    fun removeLoadMore(){
        try{
            searchBusinessItems.removeAt(searchBusinessItems.size-1)
            notifyItemChanged(searchBusinessItems.size)
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    inner class LoadMoreViewHolder(view: View):RecyclerView.ViewHolder(view){}
}