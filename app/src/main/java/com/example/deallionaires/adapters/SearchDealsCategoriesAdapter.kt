package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Categories
import kotlinx.android.synthetic.main.list_item_user_deals_categories.view.*
import kotlinx.android.synthetic.main.list_item_user_deals_sub_categories.view.*

class SearchDealsCategoriesAdapter(
    private val context: Context,
    private val userDealsCategories: ArrayList<Categories>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {

    private var selectedPosition = -1
    private var itemClicked = false

    class UserDealsCategoriesViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {

        fun bindUserDealsCategory(
            context: Context,
            category: Categories,
            position: Int,
            selectedPosition: Int,
            itemClicked: Boolean
        ) {
            with(category) {
                Glide.with(context).load(banner_image)
                    .error(R.drawable.deallionaires_logo)
                    .placeholder(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.civUserDealsCategoryImage)
                itemView.tvUserDealsCategoryName.text = category_name

                if (selectedPosition == position) {
                    itemView.civUserDealsCategoryImage.apply {
                        borderWidth = 3
                    }
                    itemView.tvUserDealsCategoryName.setTextColor(context.resources.getColor(R.color.colorYellow))

                } else {
                    itemView.civUserDealsCategoryImage.apply {
                        borderWidth = 0
                    }
                    itemView.tvUserDealsCategoryName.setTextColor(context.resources.getColor(R.color.colorWhite))

                }

                if(!itemClicked){
                    if(position == 0){
                        itemView.civUserDealsCategoryImage.apply {
                            borderWidth = 3
                        }
                        itemView.tvUserDealsCategoryName.setTextColor(context.resources.getColor(R.color.colorYellow))

                    }
                }

                itemView.setOnClickListener {
                    click(position)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return UserDealsCategoriesViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_user_deals_categories,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = userDealsCategories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val holderUserDealsCategories = holder as UserDealsCategoriesViewHolder
        holderUserDealsCategories.bindUserDealsCategory(context, userDealsCategories[position], position, selectedPosition, itemClicked)

        holder.itemView.clItemUserDealsCategory.setOnClickListener {
            itemClicked = true
            selectedPosition = position
            notifyDataSetChanged()
            itemClick(position)
        }

    }
}