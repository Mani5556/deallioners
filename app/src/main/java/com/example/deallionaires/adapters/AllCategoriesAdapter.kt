package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Categories
import kotlinx.android.synthetic.main.list_home_categories.view.*

class AllCategoriesAdapter (val context: Context,
private val categories: ArrayList<Categories>,
private val itemClick: (Categories, Int) -> Unit
) : RecyclerView.Adapter<AllCategoriesAdapter.AllCategoriesViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    inner  class AllCategoriesViewHolder(view: View, private val click: (Categories, Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, category: Categories, position: Int) {
            with(category) {
                Glide.with(context).load(banner_image)
                    .error(R.drawable.deallionaires_logo)
                    .placeholder(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.civCategoryImage)
                itemView.tvCategoryName.text = slug
                itemView.setOnClickListener {
                    click(category,position)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllCategoriesViewHolder {
        return AllCategoriesViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.item_all_category,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: AllCategoriesViewHolder, position: Int) {

        val holderNewsFeed = holder as AllCategoriesViewHolder
        holderNewsFeed.bindView(context, categories[position],position)

    }
}