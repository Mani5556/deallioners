package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.DeallionairesClub
import com.example.deallionaires.utils.getExpiry
import kotlinx.android.synthetic.main.item_home_merchants.view.*
import kotlinx.android.synthetic.main.item_list_favourites.view.*
import kotlinx.android.synthetic.main.item_list_favourites.view.tv_expires
import kotlinx.android.synthetic.main.item_list_favourites.view.tv_following_count
import kotlinx.android.synthetic.main.list_home_deals.view.*
import java.text.SimpleDateFormat

class DeallionairesClubAdapter(
    private val context: Context,
    private val deallionairesClubs: ArrayList<DeallionairesClub>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class DeallionairesClubViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindDeallionairesClub(
            context: Context,
            deallionairesClubData: DeallionairesClub,
            position: Int
        ) {

            with(deallionairesClubData) {
                Glide.with(context).load(profileImage)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.color.gray_20)
                    .into(itemView.ivFavouriteItemProfileImage)
                itemView.tvItemFavouriteBusinessName.text = businessName
                itemView.tvItemFavouriteLocation.text = address
                itemView.rbItemFavouriteRating.rating = rating?:0f
                itemView.tvFavouritesItemRating.text =
                    if (ratingCount?:0 == 0) "" else "$ratingCount Reviews"

                if(followerCount?.let { it > 0 } ?:false){
                    itemView.tv_following_count.visibility = View.VISIBLE
                    itemView.tv_following_count.text = "$followerCount Followers"
                } else {
                    itemView.tv_following_count.visibility = View.GONE
                }



//                clubExpiry?.let {
////                    val outputFormater = java.text.SimpleDateFormat("dd-MMM-yyyy")
////                    val inputFormat = java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
////                    itemView.tv_expires.visibility = android.view.View.VISIBLE
////                    val date = outputFormater.format(inputFormat.parse(it))
//                    itemView.tv_expires.setText("Club Exp:${clubExpiry}")
//                } ?: run {
//                    itemView.tv_expires.visibility = View.GONE
//                }

                if (deallionairesClub.isNullOrBlank()) {
                    itemView.rv_badge_list.visibility = View.GONE
                    itemView.tv_expires.visibility = View.GONE
                } else {
                    val club =  deallionairesClub!!.replace("D", "").toInt()
                    itemView.tv_expires.visibility = View.VISIBLE

                    itemView.rv_badge_list.run {
                        visibility = View.VISIBLE
                        this.adapter = BadgeAdapter(context,club)
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    }

                    if(club == 1){
                        itemView.tv_expires.text = "No Expiry"
                    }else{

                        val inputFormate = SimpleDateFormat("dd-MM-YYYY")
                        val outPutFormat = SimpleDateFormat("dd-MMM-YYYY")
                        val date  = clubExpiry.let { outPutFormat.format(inputFormate.parse(it))}?:""
                        if(!date.isNullOrEmpty())
                             itemView.tv_expires.text = "Club Exp : ${date}"
                        else
                            itemView.tv_expires.text = ""
                    }
                }

                if (isLiked == 1) {
                    itemView.ivFavouritesItemFavourite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_liked_red))
                } else {
                    itemView.ivFavouritesItemFavourite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
                }

                if (subcategoryName.isNullOrEmpty()) {
                    itemView.tvFavouritesItemCategorySubCategory.text = "${subcategoryName}"
                } else {
                    itemView.tvFavouritesItemCategorySubCategory.text = "$categoryName/$subcategoryName"
                }

                itemView.ivFavouritesItemFavourite.setOnClickListener {
                    click(merchantid)
                }

                itemView.setOnClickListener {
                    click(merchantid)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return DeallionairesClubViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.item_list_favourites,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = deallionairesClubs.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderDeallionairesClub = holder as DeallionairesClubViewHolder
        holderDeallionairesClub.bindDeallionairesClub(
            context,
            deallionairesClubs[position],
            position
        )
    }
}