package com.example.deallionaires.adapters


import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Specialoffer
import kotlinx.android.synthetic.main.list_home_deals.view.*
import kotlinx.android.synthetic.main.list_item_special_deals.view.*

class SpecialDealsAdapter(
    private val context: Context,
    private val specialOffers: ArrayList<Specialoffer>,
    private val itemClick: (Specialoffer) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class SpecialDealsViewHolder(view: View, private val click: (Specialoffer) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindSpecialDeals(
            context: Context,
            specialOffer: Specialoffer,
            position: Int
        ) {
//            with(specialOffer) {
//                Glide.with(context).load(dealImage)
//                    .placeholder(R.drawable.deallionaires_logo)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(itemView.ivSpecialDealImage)
//                itemView.tvSpecialDealRegularPrice.paintFlags =
//                    itemView.tvSpecialDealRegularPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//                itemView.tvSpecialDealRegularPrice.text = "$regularPrice"
//                itemView.tvSpecialDealOfferPrice.text = "$offerPrice BD"
//                itemView.tvSpecialDealSavingPrice.text = "Save $savingAmount BD"
//                itemView.setOnClickListener {
//                    click(specialOffer)
//                }
//            }


            with(specialOffer) {
                com.bumptech.glide.Glide.with(context).load(dealImage)
                    .error(com.example.deallionaires.R.drawable.deallionaires_logo)
                    .diskCacheStrategy(com.bumptech.glide.load.engine.DiskCacheStrategy.ALL)
                    .into(itemView.ivHomeDeal)
                itemView.tv_discount_price.text = "$offerPrice BD"
                com.bumptech.glide.Glide.with(context).load(profileImage)
                    .error(com.example.deallionaires.R.drawable.deallionaires_logo)
                    .diskCacheStrategy(com.bumptech.glide.load.engine.DiskCacheStrategy.ALL)
                    .into(itemView.civDealBusinessIcon)
                itemView.tvDealTitle.text = businessName
                itemView.tvDealDesc.text = description
                itemView.tv_address.text = address
                itemView.tv_discount_benifits.text = "$title"
//                itemView.tvDealDesc.text = if(typeOfDeal?.equals("fixed")?:true) "D$loyaltyPoints" else "D$loyaltyPoints per one BD purchase"

                itemView.tv_original_price?.run {
                    text = regularPrice.toString()
                    paintFlags = android.graphics.Paint.STRIKE_THRU_TEXT_FLAG
                }
                itemView.tv_save_amount.text = "Save ${savingAmount} BD"
                itemView.tv_deal_name.text = title


                itemView.tv_redeemed.text =
                    "$redeemed_deals Redeemed / $total_left Remaining"
                itemView.setOnClickListener {
                    click(specialOffer)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return SpecialDealsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_home_deals,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = specialOffers.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderSpecialDeals = holder as SpecialDealsViewHolder
        holderSpecialDeals.bindSpecialDeals(
            context,
            specialOffers[position],
            position
        )
    }
}