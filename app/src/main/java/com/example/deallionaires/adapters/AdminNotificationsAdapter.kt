package com.example.deallionaires.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Categories
import com.example.deallionaires.model.Notiadmin
import com.example.deallionaires.model.NotificationModel
import com.example.deallionaires.model.Notimerchant
import kotlinx.android.synthetic.main.list_home_categories.view.*
import kotlinx.android.synthetic.main.list_notifications.view.*

/**
 * to load news feed into home screen UI
 * @param context context of the calling activity
 * @param list of notifications to be displayed
 * @param itemClick returns the url/item type of the clicked item
 */
class AdminNotificationsAdapter(
    val context: Context,
    private val notifications: ArrayList<Notiadmin>,
    private val itemClick: (String) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    class NotificationsViewHolder(view: View, val click: (String) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, notification: Notiadmin) {
            with(notification) {
                itemView.tvNotificationTitle.text = "Deal for you"
                itemView.tvNotificationSubTitle.text = created_at
                itemView.tvNotificationContent.text = description
                itemView.setOnClickListener { click(id.toString()) }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return NotificationsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_notifications,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = notifications.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val holderNewsFeed = holder as NotificationsViewHolder
        holderNewsFeed.bindView(context, notifications[position])

    }
}