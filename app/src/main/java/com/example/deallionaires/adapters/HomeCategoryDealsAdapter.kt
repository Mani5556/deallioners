package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Searchbusiness
import kotlinx.android.synthetic.main.list_item_home_category_deals.view.*

class HomeCategoryDealsAdapter(
    private val context: Context,
    private val categoryName: String,
    private val searchBusinessItems: ArrayList<Searchbusiness>,
    private val itemClick: (Searchbusiness, Boolean, Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    class SearchBusinessItemsViewHolder(view: View, private val click: (Searchbusiness, Boolean, Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindSearchBusinessItems(
            context: Context,
            categoryName: String,
            searchBusinessItem: Searchbusiness,
            position: Int
        ) {
            with(searchBusinessItem) {
                Glide.with(context).load(profileImage)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.ivHomeCategoryDealItem)
                itemView.tvHomeCategoryDealItemName.text = businessName
                itemView.tvHomeCategoryDealItemCategoryName.text = categoryName
                if (subcategoryName.isNullOrEmpty()){
                    itemView.tvHomeCategoryDealItemSubCategoryName.text = "N/A"
                }else {
                    itemView.tvHomeCategoryDealItemSubCategoryName.text = subcategoryName
                }
                if (isLiked == 1) {
                    itemView.ivHomeCategoryFavourite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_liked_red))
                } else {
                    itemView.ivHomeCategoryFavourite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
                }

                itemView.ivHomeCategoryFavourite.setOnClickListener {
                    click(searchBusinessItem, true, isLiked)
                }

                itemView.setOnClickListener {
                    click(searchBusinessItem, false, 0)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SearchBusinessItemsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_home_category_deals,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchBusinessItems.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holderSearchBusinessSubCategories = holder as SearchBusinessItemsViewHolder
        holderSearchBusinessSubCategories.bindSearchBusinessItems(
            context,
            categoryName,
            searchBusinessItems[position],
            position
        )
    }
}