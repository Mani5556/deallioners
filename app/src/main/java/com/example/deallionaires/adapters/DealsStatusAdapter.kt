package com.example.deallionaires.adapters

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.TransitionOptions
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.Favourite
import com.example.deallionaires.model.HomeDeals
import kotlinx.android.synthetic.main.item_list_favourites.view.*
import kotlinx.android.synthetic.main.list_deals_status_item.view.*

class DealsStatusAdapter(
    val context: Context,
    private val dealsStatus: ArrayList<DealStatus>,
    private val itemClick: (DealStatus, Boolean) -> Unit
) : RecyclerView.Adapter<DealsStatusAdapter.DealsStatusViewHolder>() {

    class DealsStatusViewHolder(
        private val view: View,
        val itemClick: (DealStatus, Boolean) -> Unit
    ) :
        RecyclerView.ViewHolder(view) {
        fun bindFavourites(
            context: Context,
            deal: DealStatus
        ) {
            with(deal) {
                Glide.with(context).load(profile_image)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.color.gray_20)
                    .into(itemView.ivDealStatus)
                itemView.tvDealStatusBusinessName.text = business_name
                itemView.tvDealStatusBusinessName.isSelected = true
                itemView.tvDealStatusBusinessDesc.text = title
                itemView.tvDealStatusBusinessTranscId.text = Html.fromHtml("Transaction Id : <font color = '#F7BC00'>$transaction_id</font>")
                itemView.tvDealStatusPrice.text = total_deallions.toString()

                if (status == 1) {
                    itemView.btnDealsStatusClaim.visibility = View.INVISIBLE
                    itemView.tv_status.text = Html.fromHtml("Status:<font color = '#F70000'>Completed</font>")

                } else {
                    itemView.btnDealsStatusClaim.visibility = View.VISIBLE
                    itemView.tv_status.text = Html.fromHtml("Status : <font color = '#D4073F'>Pending</font>")

                }

                itemView.setOnClickListener {
                    itemClick(deal, false)
                }
                itemView.btnDealsStatusClaim.setOnClickListener { itemClick(deal, true) }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): DealsStatusViewHolder {
        return DealsStatusViewHolder(
            (LayoutInflater.from(context).inflate(R.layout.list_deals_status_item, parent, false))
            , itemClick
        )
    }

    override fun getItemCount() = dealsStatus.size

    override fun onBindViewHolder(holder: DealsStatusViewHolder, position: Int) {
        holder.bindFavourites(context, dealsStatus[position])
    }
}