package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.deallionaires.R
import com.example.deallionaires.model.SearchUserSubcategory
import kotlinx.android.synthetic.main.list_item_user_deals_sub_categories.view.*

class SearchDealsSubCategoriesAdapter(
    private val context: Context,
    private val searchUserSubcategories: ArrayList<SearchUserSubcategory>,
    private val itemClick: (SearchUserSubcategory) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    private var selectedPosition = -1
    private var itemClicked = false

    inner class UserDealsSubCategoriesViewHolder(view: View, private val click: (SearchUserSubcategory) -> Unit) :
        RecyclerView.ViewHolder(view) {

        fun bindUserSubDealsCategory(
            context: Context,
            searchUserSubCategory: SearchUserSubcategory,
            position: Int,
            selectedPosition: Int,
            itemClicked: Boolean
        ) {
            with(searchUserSubCategory) {
                itemView.tvItemUserDealSubCategoryName.text = subcategoryName

                if(position == searchUserSubcategories.size -1){
                    itemView.border.visibility = View.GONE
                }else{
                    itemView.border.visibility = View.VISIBLE
                }
                itemView.setOnClickListener {
                    click(searchUserSubCategory)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return UserDealsSubCategoriesViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_user_deals_sub_categories,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchUserSubcategories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderUserDealsSubCategories = holder as UserDealsSubCategoriesViewHolder
        holderUserDealsSubCategories.bindUserSubDealsCategory(
            context,
            searchUserSubcategories[position],
            position,
            selectedPosition,
            itemClicked
        )
        holder.itemView.llItemUserDealSubCategoryName.setOnClickListener {
            itemClicked = true
            selectedPosition = position
            itemClick(searchUserSubcategories[position])
            notifyDataSetChanged()
        }
    }
}