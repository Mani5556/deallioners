package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.deallionaires.R
import com.example.deallionaires.model.Viewreviewrating
import kotlinx.android.synthetic.main.list_item_search_business_reviews.view.*

class BusinessDealReviewsAdapter(
    private val context: Context,
    private val searchBusinessReviews: ArrayList<Viewreviewrating>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class BusinessDealReviewsViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindBusinessDealReviews(
            context: Context,
            searchBusinessReview: Viewreviewrating,
            position: Int
        ) {
            with(searchBusinessReview) {
                itemView.tv_title.text =
                    searchBusinessReview.firstName + "  " + searchBusinessReview.lastName
                val separated = searchBusinessReview.createdAt.split("T")
                itemView.tv_review_date.text = separated[0]
                itemView.ratingbar.rating =
                    searchBusinessReview.rating.toFloat()
                itemView.tv_review.text = searchBusinessReview.review
                if (searchBusinessReview.merchantReply.isNullOrEmpty()) {
                    itemView.lyt_merchent_review.visibility = View.GONE
                } else {
                    itemView.lyt_merchent_review.visibility = View.VISIBLE
                    itemView.tv_review_merchant.text = searchBusinessReview.merchantReply
                    val separated = searchBusinessReview.createdAt.split("T")
                    itemView.tv_review_date_merchant.text = separated.first()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return BusinessDealReviewsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_search_business_reviews,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchBusinessReviews.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderBusinessDealReviews = holder as BusinessDealReviewsViewHolder
        holderBusinessDealReviews.bindBusinessDealReviews(
            context,
            searchBusinessReviews[position],
            position
        )
    }
}