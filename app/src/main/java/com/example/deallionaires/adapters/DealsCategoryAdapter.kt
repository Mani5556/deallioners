package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.deallionaires.R
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.HomeDeals
import com.example.deallionaires.model.SearchDealsCategoryItems
import com.example.deallionaires.model.Searchbusiness
import com.example.deallionaires.ui.home.fragments.home.HomeCallBacks
import com.example.deallionaires.utils.PercentageLinearLayoutManager
import kotlinx.android.synthetic.main.list_deal_category.view.*

/**
 * to load news feed into home screen UI
 * @param context context of the calling activity
 * @param list of news feed to be displayed
 * @param itemClick returns the url/item type of the clicked item
 */
class DealsCategoryAdapter(
    val context: Context,
    private var categories: HashMap<String, ArrayList<Any>>,
    private val homeCallBacks: HomeCallBacks
) : RecyclerView.Adapter<ViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    class DealsCategoryViewHolder(view: View, val homeCallBacks: HomeCallBacks) : RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, categoryTitle: String, deals: ArrayList<Any>, position: Int) {
            itemView.tvDealCategory.text = categoryTitle.let { it.replace("_", " ").capitalize() }

          if(deals.first() is DealStatus){
              itemView.rvDeals.layoutManager =
                  PercentageLinearLayoutManager(context, RecyclerView.HORIZONTAL, 0.95f)
          }else{
              itemView.rvDeals.layoutManager =
                  PercentageLinearLayoutManager(context, RecyclerView.HORIZONTAL, 0.75f)
          }


            itemView.rvDeals.adapter = HomeDealsAdapter(context, deals,homeCallBacks,position)
            if (deals.size <= 3) {
                itemView.tvDealViewAll.visibility = View.GONE
            } else {
                itemView.tvDealViewAll.visibility = View.VISIBLE
            }


            itemView.tvDealViewAll.setOnClickListener {
                if (deals[0] is Searchbusiness) {
                   homeCallBacks.viewAllBusinessClicked(categoryTitle)
                } else if(deals[0] is SearchDealsCategoryItems) {
                    homeCallBacks.viewAllDealsClicked(categoryTitle)
                }
                else {
                    homeCallBacks.viewAllPendingDealsClicked(categoryTitle)
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return DealsCategoryViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_deal_category,
                parent,
                false
            )), homeCallBacks
        )
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val keys: Set<String> = categories.keys
        val myKeysList = ArrayList<String>()
        for (s in keys) myKeysList.add(s)
        val holderNewsFeed = holder as DealsCategoryViewHolder
        categories[myKeysList[position]]?.let {
            holderNewsFeed.bindView(
                context,
                myKeysList[position],
                it, position
            )
        }
    }

    fun updateItem(parentAdapterPosition: Int, position: Int,isLiked:Int) {
        val keys: Set<String> = categories.keys
        val myKeysList = ArrayList<String>()
        for (s in keys) myKeysList.add(s)
        val key:String = myKeysList.get(parentAdapterPosition)
        val likedObjectList:List<Searchbusiness> = categories[key] as ArrayList<Searchbusiness>
        likedObjectList[position].isLiked = isLiked
        notifyItemChanged(parentAdapterPosition)
    }


    fun updateList(categories: HashMap<String, ArrayList<Any>>){
        this.categories = categories
        notifyDataSetChanged()

    }
}