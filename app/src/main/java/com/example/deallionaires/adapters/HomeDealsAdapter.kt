package com.example.deallionaires.adapters

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.HomeDeals
import com.example.deallionaires.model.SearchDealsCategoryItems
import com.example.deallionaires.model.Searchbusiness
import com.example.deallionaires.ui.home.fragments.home.HomeCallBacks
import com.example.deallionaires.utils.getExpiry
import com.example.deallionaires.utils.getExpiryDays
import com.futuremind.recyclerviewfastscroll.Utils
import kotlinx.android.synthetic.main.item_home_merchants.view.*
import kotlinx.android.synthetic.main.item_home_merchants.view.tv_following_count
import kotlinx.android.synthetic.main.item_list_favourites.view.*
import kotlinx.android.synthetic.main.list_deals_status_item.view.*
import kotlinx.android.synthetic.main.list_home_deals.view.*
import kotlinx.android.synthetic.main.list_home_deals.view.tv_deal_name
import kotlinx.android.synthetic.main.list_home_deals.view.tv_expires

/**
 * to load news feed into home screen UI
 * @param context context of the calling activity
 * @param list of news feed to be displayed
 * @param itemClick returns the url/item type of the clicked item
 */
class HomeDealsAdapter(
    val context: Context,
    private val dataList: ArrayList<Any>,
    private val homeCallBacks: HomeCallBacks, val parentAdapterPostion:Int
) : RecyclerView.Adapter<ViewHolder>() {

    companion object{
        val ITEM_TYPE_MERCHANT = 1
        val ITEM_TYPE_DEAL = 2
        val ITEM_TYPE_PENDING_DEAL = 3
    }

    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
   inner class HomeDealsViewHolder(view: View, val homeCallBacks: HomeCallBacks) : RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, homeDeals: SearchDealsCategoryItems) {
            with(homeDeals) {
                Glide.with(context).load(this.dealImage)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(itemView.ivHomeDeal)
                itemView.tv_discount_price.text = "$$offerPrice"
                Glide.with(context).load(this.profileImage)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.civDealBusinessIcon)
                itemView.tvDealTitle.text = business_name
                itemView.tvDealDesc.text = if(typeOfDeal?.equals("fixed")?:true) "$loyaltyPoints" else "$loyaltyPoints per one BD purchase"
               itemView.tv_address.text = address
               itemView.tv_expires.text = expiryDate?.let { "EXP : ${getExpiryDays(it)} "}?:""
                itemView.tv_original_price?.run {
                    text = regularPrice.toString()
                    paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                }
                itemView.tv_save_amount.text = "Save ${regularPrice - offerPrice} BD"
                itemView.tv_deal_discount_percent.text = "${Math.round(discount)} %"
                itemView.tv_discount_benifits.text = "$title"
                itemView.tv_deal_name.text = "$catslug / $offerCategory "
                itemView.tv_redeemed.text = "$redeemed_deals Redeemed /$remainingDeals Remaining"


                itemView.setOnClickListener {
                    homeCallBacks.dealClicked(dealid)
//                    click(dealid, ITEM_TYPE_DEAL)
//                    context.startActivity(context.intentFor<SearchDealDetailsActivity>(Constants.DEAL_ID to homeDeals.id))
                }
            }
        }
    }

   inner class HomeMerchantViewHolder(view: View, val homeCallBacks: HomeCallBacks) : RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, searchbusiness : Searchbusiness,position: Int) {
            with(searchbusiness) {
                Glide.with(context).load(profileImage)
                    .placeholder(R.color.gray_50)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(itemView.iv_merchant_image)
                itemView.tv_merchant_title.text = businessName
                itemView.tv_merchant_address.text = address
                itemView.ratingbar.rating = rating
                itemView.tv_merchant_sub_category_name.text = slug
                itemView.tv_reviews_count.text = if(ratingCount!! > 0) "$ratingCount Reviews" else ""


                if (deallionairsclub.isNullOrBlank()) {
                    itemView.rv_badges.visibility = View.GONE
                    itemView.tv_club_expires.visibility = View.INVISIBLE
                } else {
                    val club =  deallionairsclub!!.replace("D", "").toInt()
                    itemView.tv_club_expires.visibility = View.VISIBLE

                    itemView.rv_badges.run {
                        visibility = View.VISIBLE
                        this.adapter = BadgeAdapter(context,club)
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    }

                    if(club == 1){
                        itemView.tv_club_expires.text = "No Expiry"
                    }else{
                        itemView.tv_club_expires.text = "Exp : ${getExpiryDays(expiry_date!!)?:""}"
                    }
                }

                itemView.tv_following_count.text = if(followers > 0) " $followers  Followers" else "Follow"

                if (isLiked == 1) {
                    itemView.iv_favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_liked_red))
                } else {
                    itemView.iv_favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
                }
                itemView.iv_favorite.setOnClickListener {
                    homeCallBacks.likeClicked(if(isLiked == 1) 0 else 1,merchantid,position,parentAdapterPostion)
                }
                itemView.setOnClickListener {
                    homeCallBacks.businessItemClicked(merchantid)
//                    click(merchantid,ITEM_TYPE_MERCHANT)
//                    context.startActivity(context.intentFor<SearchDealDetailsActivity>(Constants.DEAL_ID to homeDeals.id))
                }


            }
        }
    }

    inner class HomePendingDealsViewHolder(view: View, val homeCallBacks: HomeCallBacks) : RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, pendingDeals: DealStatus) {
            with(pendingDeals) {
               Glide.with(context).load(profile_image)
                    .error(com.example.deallionaires.R.drawable.deallionaires_logo)
                    .diskCacheStrategy(com.bumptech.glide.load.engine.DiskCacheStrategy.ALL)
                    .into(itemView.ivDealStatus)
                itemView.tvDealStatusBusinessName.text = business_name
                itemView.tvDealStatusBusinessName.isSelected = true
                itemView.tvDealStatusBusinessDesc.text = title
                itemView.tvDealStatusBusinessTranscId.text = android.text.Html.fromHtml("Transaction Id : <font color = '#F7BC00'>$transaction_id</font>")
                itemView.tvDealStatusPrice.text = total_deallions.toString()

                if (status == 1) {
                    itemView.btnDealsStatusClaim.visibility = android.view.View.INVISIBLE
                    itemView.tv_status.text = android.text.Html.fromHtml("Status:<font color = '#F70000'>Completed</font>")

                } else {
                    itemView.btnDealsStatusClaim.visibility = android.view.View.VISIBLE
                    itemView.tv_status.text = android.text.Html.fromHtml("Status : <font color = '#D4073F'>Pending</font>")

                }

                itemView.setOnClickListener {
//                    itemClick(deal, false)
                    homeCallBacks.pendingDealClickked(dealid)
                }
//                itemView.btnDealsStatusClaim.setOnClickListener { itemClick(deal, true) }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        when (viewType) {

            ITEM_TYPE_DEAL -> {
                return HomeDealsViewHolder((LayoutInflater.from(context).inflate(R.layout.list_home_deals, parent, false)), homeCallBacks)
            }

            ITEM_TYPE_MERCHANT -> {
                return HomeMerchantViewHolder(
                    (LayoutInflater.from(context).inflate(
                        R.layout.item_home_merchants,
                        parent,
                        false
                    )), homeCallBacks
                )
            }

            else ->{
                return HomePendingDealsViewHolder(
                    (LayoutInflater.from(context).inflate(
                        R.layout.list_deals_status_item,
                        parent,
                        false
                    )), homeCallBacks
                )
            }


        }

    }

    override fun getItemCount() = dataList.size.coerceAtMost(4)

    override fun getItemViewType(position: Int) = if(dataList[position] is Searchbusiness) ITEM_TYPE_MERCHANT
    else if(dataList[position] is SearchDealsCategoryItems) ITEM_TYPE_DEAL
    else  ITEM_TYPE_PENDING_DEAL


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if(dataList[position] is Searchbusiness){
            val merchantViewHolder = holder as HomeMerchantViewHolder
            merchantViewHolder.bindView(context, dataList[position] as Searchbusiness,position)
        }
        else if(dataList[position] is SearchDealsCategoryItems){
            val homeDealsViewHolder = holder as HomeDealsViewHolder
            homeDealsViewHolder.bindView(context, dataList[position] as SearchDealsCategoryItems)
        }else{
            val homePendingDealsViewHolder = holder as HomePendingDealsViewHolder
            homePendingDealsViewHolder.bindView(context, dataList[position] as DealStatus)
        }


    }
}