package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.RecyclerView
import com.example.deallionaires.R
import com.example.deallionaires.model.Viewbranch
import kotlinx.android.synthetic.main.list_item_search_business_branches.view.*

class BusinessDealBranchesAdapter (
    private val context: Context,
    private val searchBusinessBranches: ArrayList<Viewbranch>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class BusinessDealBranchesViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindBusinessDealBranches(
            context: Context,
            searchBusinessBranch: Viewbranch,
            position: Int
        ) {
            with(searchBusinessBranch) {
                itemView.tvSearchBusinessBranchName.text = searchBusinessBranch.businessName
                itemView.tvSearchBusinessBranchAddress.text = searchBusinessBranch.address
                if(searchBusinessBranch.businessContactNo.isNullOrEmpty()){
                    itemView.tvSearchBusinessBranchContactNumber.text = "NA"
                }else {
                    itemView.tvSearchBusinessBranchContactNumber.text = searchBusinessBranch.businessContactNo
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return BusinessDealBranchesViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_search_business_branches,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchBusinessBranches.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderBusinessDealBranches = holder as BusinessDealBranchesViewHolder
        holderBusinessDealBranches.bindBusinessDealBranches(
            context,
            searchBusinessBranches[position],
            position
        )
    }
}