package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.deallionaires.R
import com.example.deallionaires.adapters.FilterAdapter.FilterViewHolder
import kotlinx.android.synthetic.main.list_item_user_deals_sub_categories.view.*

class FilterAdapter(val context: Context,val list:ArrayList<String>,val callBacK: (position:Int)->Unit):
    RecyclerView.Adapter<FilterViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    inner class FilterViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindView(position: Int) {
            itemView.tvItemUserDealSubCategoryName.text = list[position]
            if(position == list.size -1){
                itemView.border.visibility = View.GONE
            }else{
                itemView.border.visibility = View.VISIBLE
            }
            itemView.setOnClickListener { callBacK(position) }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FilterViewHolder {
        return FilterViewHolder((LayoutInflater.from(context).inflate(R.layout.list_item_user_deals_sub_categories, parent, false))
        )
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        holder.bindView(position)
    }

}