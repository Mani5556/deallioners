package com.example.deallionaires.adapters


import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.deallionaires.R
import com.example.deallionaires.ui.notification.admin.AdminNotificationsFragment
import com.example.deallionaires.ui.notification.merchant.MerchantNotificationsFragment


class NotificationPageAdapter(
    val context: Context,
    val fm: FragmentManager
) : FragmentPagerAdapter(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(p0: Int): Fragment {
        return when (p0) {
            0 -> {
                AdminNotificationsFragment()
            }
            1 -> {
                MerchantNotificationsFragment()
            }
            else -> Fragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {

        return when (position) {
            0 -> context.getString(R.string.string_admin_notifications)
            else -> {
                context.getString(R.string.string_merchant_notifications)
            }
        }
    }


}