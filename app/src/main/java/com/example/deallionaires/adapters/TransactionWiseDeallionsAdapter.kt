package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Transactionvaluepoints
import kotlinx.android.synthetic.main.list_item_transaction_wise.view.*

class TransactionWiseDeallionsAdapter (
    private val context: Context,
    private val appLevelPoints: ArrayList<Transactionvaluepoints>,
    private val itemClick: (Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    class TransactionWiseDeallionsViewHolder(view: View, private val click: (Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindTransactionWiseDeallions(
            context: Context,
            appLevelPoint: Transactionvaluepoints,
            position: Int
        ) {
            with(appLevelPoint) {
                Glide.with(context).load(profile_image)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.ivTransactionWiseDeallionsProfileImage)

                if(business_name != null){
                    itemView.tvTransactionWiseDeallionsName.text = business_name
                }else{
                    itemView.tvTransactionWiseDeallionsName.text = "N/A"
                }

                if (deallions_offered != null) {
                    itemView.tvTransactionWiseDeallionsLoyaltyPoints.text = deallions_offered.toString()
                } else {
                    itemView.tvTransactionWiseDeallionsLoyaltyPoints.text = "N/A"
                }

                itemView.setOnClickListener {
                    click(merchant_id!!)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return TransactionWiseDeallionsViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_item_transaction_wise,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = appLevelPoints.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderTransactionWiseDeallions = holder as TransactionWiseDeallionsViewHolder
        holderTransactionWiseDeallions.bindTransactionWiseDeallions(
            context,
            appLevelPoints[position],
            position
        )
    }
}