package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.SearchDealsLocal
import kotlinx.android.synthetic.main.item_search_home_deals.view.*

class SearchHomeDealsAdapter(
    val context: Context,
    private val searchHomeDealsList: ArrayList<SearchDealsLocal>,
    private val itemClick: (SearchDealsLocal) -> Unit
) : RecyclerView.Adapter<SearchHomeDealsAdapter.StickyHeaderViewHolder>() {

    class StickyHeaderViewHolder(private val view: View, val itemClick: (SearchDealsLocal) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindFavourites(
            context: Context,
            searchDealsLocalModel: SearchDealsLocal
        ) {
            with(searchDealsLocalModel) {
                Glide.with(context).load(searchDealsLocalModel.profile_image)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.ivSearchHomeDealItemProfileImage)
                itemView.tvSearchHomeDealBusinessName.text = searchDealsLocalModel.business_name
                if (searchDealsLocalModel.searchHomeType == "Deals") {
                    itemView.tvSearchHomeDealSlug.text = searchDealsLocalModel.catslug
                } else {
                    itemView.tvSearchHomeDealSlug.text = searchDealsLocalModel.slug
                }
                if (searchDealsLocalModel.subcategory_name.isNullOrEmpty()) {
                    itemView.tvSearchHomeDealSubCategoryName.text = "N/A"
                } else {
                    itemView.tvSearchHomeDealSubCategoryName.text =
                        searchDealsLocalModel.subcategory_name
                }

                itemView.setOnClickListener {
                    itemClick(searchDealsLocalModel)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): StickyHeaderViewHolder {
        return StickyHeaderViewHolder(
            (LayoutInflater.from(context).inflate(R.layout.item_search_home_deals, parent, false))
            , itemClick
        )
    }

    override fun getItemCount() = searchHomeDealsList.size

    override fun onBindViewHolder(holder: StickyHeaderViewHolder, position: Int) {
        holder.bindFavourites(context, searchHomeDealsList[position])
    }
}