package com.example.deallionaires.adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.HomeDeals
import com.example.deallionaires.model.SearchDealsCategoryItems
import com.example.deallionaires.model.Searchbusiness
import com.example.deallionaires.utils.getExpiry
import kotlinx.android.synthetic.main.item_home_merchants.view.*
import kotlinx.android.synthetic.main.item_home_merchants.view.tv_following_count
import kotlinx.android.synthetic.main.list_home_deals.view.*
import kotlinx.android.synthetic.main.list_home_view_all_deals.view.*

class HomeViewAllBusinessAdapter (
    private val context: Context,
    private var searchBusinessItems: ArrayList<Searchbusiness>,
    private val itemClick: (Searchbusiness, Boolean, Int,Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    class HomeBusinessViewHolder(view: View,val itemClick: (Searchbusiness, Boolean, Int,Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, searchbusiness : Searchbusiness,position: Int) {
            with(searchbusiness) {
                Glide.with(context).load(profileImage?:"")
                    .placeholder(R.color.gray_50)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(itemView.iv_merchant_image)
                itemView.tv_merchant_title.text = businessName
                itemView.tv_merchant_address.text = address
                itemView.ratingbar.rating = rating
                itemView.tv_reviews_count.text = if(ratingCount?:0 > 0) "$ratingCount Reviews" else ""
                itemView.tv_merchant_sub_category_name.text = slug
//                itemView.tv_club_expires.apply {
//                    expiry_date?.split("T")?.first()?.let {
//                        visibility = View.VISIBLE
//                        text = "Club Expiry : $it"
//                    }?: let { visibility = View.INVISIBLE}
//                }

                if (deallionairsclub.isNullOrBlank()) {
                    itemView.rv_badges.visibility = View.GONE
                    itemView.tv_club_expires.visibility = View.INVISIBLE
                } else {
                    val club =  deallionairsclub!!.replace("D", "").toInt()
                    itemView.tv_club_expires.visibility = View.VISIBLE

                    itemView.rv_badges.run {
                        visibility = View.VISIBLE
                        this.adapter = BadgeAdapter(context,club)
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    }

                    if(club == 1){
                        itemView.tv_club_expires.text = "No Expiry"
                    }else{
                        itemView.tv_club_expires.text = getExpiry(expiry_date).let { it }?:""
                    }
                }
                itemView.tv_following_count.text = if(followers > 0) " $followers  Followers" else "Follow"
                if (isLiked == 1) {
                    itemView.iv_favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_liked_red))
                } else {
                    itemView.iv_favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
                }

                itemView.iv_favorite.setOnClickListener {
                    itemClick(this, true, isLiked,position)
                }

                itemView.setOnClickListener {
                    itemClick(this, false, 0,position)
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return HomeBusinessViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.item_home_merchants,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = searchBusinessItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val holderNewsFeed = holder as HomeBusinessViewHolder
        holderNewsFeed.bindView(context, searchBusinessItems[position],position)
    }

    fun updateData(searchBusinessList:ArrayList<Searchbusiness>):Unit{
        this.searchBusinessItems = searchBusinessList
        notifyDataSetChanged()
    }

}