package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Categories
import kotlinx.android.synthetic.main.item_badge.view.*
import kotlinx.android.synthetic.main.list_home_categories.view.*

class BadgeAdapter(val context: Context, val size: Int) :
    RecyclerView.Adapter<BadgeAdapter.BadgeViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    class BadgeViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        fun bindView() {
//            itemView.iv_badge.setImageResource(R.drawable.deallionaires_small_logo)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BadgeAdapter.BadgeViewHolder {
        return BadgeAdapter.BadgeViewHolder(
            (LayoutInflater.from(context).inflate(R.layout.item_badge, parent, false))
        )
    }

    override fun getItemCount() = size

    override fun onBindViewHolder(holder: BadgeViewHolder, position: Int) {
        holder.bindView()
    }

}