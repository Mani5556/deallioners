package com.example.deallionaires.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Categories
import kotlinx.android.synthetic.main.list_home_categories.view.*

/**
 * to load news feed into home screen UI
 * @param context context of the calling activity
 * @param list of news feed to be displayed
 * @param itemClick returns the url/item type of the clicked item
 */
class HomeCategoriesAdapter(
    val context: Context,
    private val categories: ArrayList<Categories>,
    private val itemClick: (Categories,Int) -> Unit
) : RecyclerView.Adapter<ViewHolder>() {
    /**
     * for assigning data to views
     * @param view the view to be displayed
     * @param click returns the url of the clicked item
     */
    class HomeCategoriesViewHolder(view: View, private val click: (Categories,Int) -> Unit) :
        RecyclerView.ViewHolder(view) {
        fun bindView(context: Context, category: Categories,position: Int) {
            with(category) {
                Glide.with(context).load(banner_image)
                    .placeholder(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView.civCategoryImage)
                itemView.tvCategoryName.text = slug
                itemView.setOnClickListener {
                    click(category,position)
                }
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return HomeCategoriesViewHolder(
            (LayoutInflater.from(context).inflate(
                R.layout.list_home_categories,
                parent,
                false
            )), itemClick
        )
    }

    override fun getItemCount() = if(categories.size >= 4) 4 else categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val holderNewsFeed = holder as HomeCategoriesViewHolder
        holderNewsFeed.bindView(context, categories[position],position)

    }
}