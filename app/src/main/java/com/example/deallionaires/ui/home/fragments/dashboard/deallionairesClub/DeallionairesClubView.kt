package com.example.deallionaires.ui.home.fragments.dashboard.deallionairesClub

import com.example.deallionaires.model.DeallionairesClubResponse

interface DeallionairesClubView {

    fun displayDeallionairesClub(deallionairesClubResponse: DeallionairesClubResponse)

    fun noDeallionairesClub(message: String)

    fun deallionairesClubNoInternet()

}