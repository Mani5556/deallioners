package com.example.deallionaires.ui.profile

import com.example.deallionaires.model.EditProfile
import com.example.deallionaires.utils.ProgressCallBacks

interface ProfilePresenter {

    fun getProfilePresenter(tokenKey: String)

    fun editUserProfile(tokenKey: String, editProfile: EditProfile)

    fun updateProfilePic(tokenKey: String, profileurl: String,progressCallBacks: ProgressCallBacks)

    fun destroyProfile()

}