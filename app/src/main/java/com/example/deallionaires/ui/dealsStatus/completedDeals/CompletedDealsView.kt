package com.example.deallionaires.ui.dealsStatus.completedDeals

import com.example.deallionaires.model.DealStatus

interface CompletedDealsView {

    fun onGettingCompletedDealsSuccess(deals:ArrayList<DealStatus>)

    fun onGettingCompletedDealsFail(message: String)

    fun onGettingCompletedDealsNoInternet()
}