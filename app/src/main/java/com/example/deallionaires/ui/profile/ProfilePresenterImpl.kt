package com.example.deallionaires.ui.profile

import android.content.Context
import com.example.deallionaires.model.EditProfile
import com.example.deallionaires.model.EditProfileResponse
import com.example.deallionaires.model.ImageUploadResponse
import com.example.deallionaires.model.ResponseProfile
import com.example.deallionaires.network.APIRequests.editUserDetails
import com.example.deallionaires.network.APIRequests.getUserDetails
import com.example.deallionaires.network.APIRequests.uploadProfilePic
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.ProgressCallBacks
import com.example.deallionaires.utils.isNetworkAvailable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File

class ProfilePresenterImpl(private val context: Context, private var profileView: ProfileView?) :
    ProfilePresenter {

    override fun getProfilePresenter(tokenKey: String) {
        if (isNetworkAvailable(context)) {
            getUserDetails(tokenKey, object : NWResponseCallback<ResponseProfile> {
                override fun onSuccess(call: Call<ResponseProfile>, response: Response<ResponseProfile>) {
                    ResponseProfile().profile = response.body()!!.profile
                    profileView!!.onGettingProfileSuccess(response.body()!!.profile?.get(0)!!)
                }
                override fun onResponseBodyNull(call: Call<ResponseProfile>, response: Response<ResponseProfile>) {
                    profileView!!.onGettingProfileFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<ResponseProfile>,
                    response: Response<ResponseProfile>
                ) {
                    profileView!!.onGettingProfileFail(response.message())
                }

                override fun onFailure(call: Call<ResponseProfile>, t: Throwable) {
                    profileView!!.onGettingProfileFail(t.message!!)
                }
            })
        } else {
            profileView!!.onGettingProfileNoInternet()
        }
    }

    override fun editUserProfile(tokenKey: String, editProfile: EditProfile) {
        if(isNetworkAvailable(context)){
            editUserDetails(tokenKey, editProfile, object : NWResponseCallback<EditProfileResponse?>{
                override fun onSuccess(
                    call: Call<EditProfileResponse?>,
                    response: Response<EditProfileResponse?>
                ) {
                    profileView!!.onEditingProfileSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<EditProfileResponse?>,
                    response: Response<EditProfileResponse?>
                ) {
                    profileView!!.onEditingProfileFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<EditProfileResponse?>,
                    response: Response<EditProfileResponse?>
                ) {
                    profileView!!.onEditingProfileFail(response.message())
                }

                override fun onFailure(call: Call<EditProfileResponse?>, t: Throwable) {
                    profileView!!.onEditingProfileFail(t.message!!)
                }
            })
        }else{
            profileView!!.onGettingProfileNoInternet()
        }
    }

    override fun updateProfilePic(tokenKey: String, profileurl: String, progressCallBacks: ProgressCallBacks) {
        if(isNetworkAvailable(context)){
            uploadProfilePic(tokenKey, profileurl,progressCallBacks, object : NWResponseCallback<ImageUploadResponse?>{
                override fun onSuccess(
                    call: Call<ImageUploadResponse?>,
                    response: Response<ImageUploadResponse?>
                ) {
                    progressCallBacks.uploadSuccess(response.body()?.imageLink?:"")
                }

                override fun onResponseBodyNull(
                    call: Call<ImageUploadResponse?>,
                    response: Response<ImageUploadResponse?>
                ) {
                    progressCallBacks.uploadFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<ImageUploadResponse?>,
                    response: Response<ImageUploadResponse?>
                ) {
                    progressCallBacks.uploadFail(response.message())
                }

                override fun onFailure(call: Call<ImageUploadResponse?>, t: Throwable) {
                    progressCallBacks.uploadFail(t.message.toString())
                }
            })
        }else{
            progressCallBacks.uploadFail("No internet Connection")
        }
    }


    override fun destroyProfile() {
        profileView = null
    }
}