package com.example.deallionaires.ui.specialDeals

import com.example.deallionaires.model.SpecialOfferResponse

interface SpecialDealsView {

    fun onGettingSpecialDealsSuccess(specialOfferResponse: SpecialOfferResponse)

    fun onGettingSpecialDealsFail(message: String)

    fun onGettingSpecialDealsNoInternet()

}