package com.example.deallionaires.ui.notification.admin

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.AdminNotificationsAdapter
import com.example.deallionaires.model.Notiadmin
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class AdminNotificationsFragment : Fragment(), AdminNotificationView {
    private lateinit var adminNotificationPresenter: AdminNotificationPresenter
    private lateinit var rvAdminNotification: RecyclerView
    private lateinit var tvAdminNoNotification: TextView
    private lateinit var activity: Activity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        rvAdminNotification = view.findViewById(R.id.rvAdminNotifications)
        tvAdminNoNotification = view.findViewById(R.id.tvAdminNoNotifications)

        adminNotificationPresenter = AdminNotificationPresenter(activity, this)
        if(isNetworkAvailable(activity)){
            adminNotificationPresenter.getAdminNotification(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        }else{
            activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onGetAdminNotificationSuccess(deals: ArrayList<Notiadmin>) {
        if(deals.isEmpty()){
            tvAdminNoNotification.visibility = View.VISIBLE
            rvAdminNotification.visibility = View.GONE
        }else {
            tvAdminNoNotification.visibility = View.GONE
            rvAdminNotification.visibility = View.VISIBLE
            rvAdminNotification.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            rvAdminNotification.adapter = AdminNotificationsAdapter(requireContext(), deals) {}
        }
        LoadingDialog.getInstance().cancel()
    }

    override fun onGetAdminNotificationFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGetAdminNotificationNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun showAdminNotificationFragmentProgress() {
//        LoadingDialog.getInstance().show(requireContext())
    }

    override fun hideAdminNotificationFragmentProgress() {
//        LoadingDialog.getInstance().cancel()
    }

}
