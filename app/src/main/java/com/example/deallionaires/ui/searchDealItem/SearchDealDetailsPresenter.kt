package com.example.deallionaires.ui.searchDealItem

import com.example.deallionaires.model.RedeemDeal

interface SearchDealDetailsPresenter {

    fun getDealDetailsResponse(userToken: String, dealid: Int)

    fun postRedeemDeal(userToken: String, redeemDeal: RedeemDeal)

    fun dealDetailsDestroy()

}