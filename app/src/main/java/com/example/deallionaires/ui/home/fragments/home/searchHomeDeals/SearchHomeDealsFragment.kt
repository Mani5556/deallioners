package com.example.deallionaires.ui.home.fragments.home.searchHomeDeals

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.deallionaires.R
import com.example.deallionaires.adapters.SearchHomeDealsAdapter
import com.example.deallionaires.model.SearchDealsLocal
import com.example.deallionaires.model.SearchHomeDealsResponse
import com.example.deallionaires.ui.MapActivity
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.*
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.fragment_search_home_deals.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class SearchHomeDealsFragment : Fragment(), SearchHomeDealsView {

    private var dataList = ArrayList<SearchDealsLocal>()

    var isUserSearchedDeals = false

    val SEARCH_HOME_REQUEST_CODE = 562
    var locationPermanentlyDenied = false

    lateinit var activity: Activity
    lateinit var userKey: String

    var userLocationLatitude = "0.0"
    var userLocationLongitude = "0.0"




    private lateinit var searchHomeDealsPresenter: SearchHomeDealsPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_home_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!


        var storedLatitute by PreferenceExt.appPreference(
            activity,
            Constants.USER_LOCATION_LATITUDE,
            Constants.DEFAULT_STRING
        )
        var storedLongitude by PreferenceExt.appPreference(
            getActivity()!!,
            Constants.USER_LOCATION_LONGITUDE,
            Constants.DEFAULT_STRING
        )



        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        userLocationLatitude = storedLatitute
        userLocationLongitude = storedLongitude

//        var userLocationLatitude by PreferenceExt.appPreference(
//            activity,
//            Constants.USER_LOCATION_LATITUDE,
//            Constants.DEFAULT_STRING
//        )
//
//        var userLocationLongitude by PreferenceExt.appPreference(
//            activity,
//            Constants.USER_LOCATION_LONGITUDE,
//            Constants.DEFAULT_STRING
//        )

        userKey = userTokenKey
        activity.tbEtSearchHome.setText("")

        if (!Places.isInitialized()) {
            Places.initialize(activity, Constants.API_KEY)
        }

        val fields: List<Place.Field> =
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS
            )

        val autocompleteFragment: AutocompleteSupportFragment? =
            requireActivity().supportFragmentManager.findFragmentById(R.id.fcvSearchHomeDeals) as AutocompleteSupportFragment?
        autocompleteFragment!!.setCountries("IN")
        autocompleteFragment.setPlaceFields(fields)

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                Log.i(
                    "Home Activity",
                    "Place: " + place.name.toString() + ", " + place.id
                )
//                userLat = place.latLng!!.latitude
//                userLong = place.latLng!!.longitude
//                userLocationLatitude = place.latLng!!.latitude.toString()
//                userLocationLongitude = place.latLng!!.longitude.toString()
                userLocationLatitude = place.latLng!!.latitude.toString()
                userLocationLongitude = place.latLng!!.longitude.toString()
            }

            override fun onError(status: Status) {
                Log.i("Home Activity", "An error occurred: $status")
            }
        })

        searchHomeDealsPresenter =
            SearchHomeDealsPresenter(activity, this)

        activity.tbEtSearchHome.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable) {
//                if(isNetworkAvailable(this@SearchHomeDealsActivity)){
//                    searchHomeDealsPresenter.getDealsAtUserSearchedLocation(50.66397682,26.23730377, editable.toString().trim())
//                }else{
//                    toast(getString(R.string.txt_please_check_your_internet_connection))
//                }
//                searchDeals(false)
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {

            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if(s.length == 0){
                    showSearchSuggition()
                }
//                searchDeals(false)
            }
        })

        activity.tbEtSearchHome.setOnEditorActionListener { v, actionId, event ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                /* Write your logic here that will be executed when user taps next button */
                searchDeals(true)
                handled = true
            }
            handled
        }

        activity.tbIvSearchHomeDealsSearch.setOnClickListener {
            searchDeals(true)
        }

        activity.tbIvSearchHomeDealsLocation.setOnClickListener {
            checkLocationPermission()
        }

        showSearchSuggition()
    }

    private fun showSearchSuggition() {
        activity.rvSearchHomeDeals.visibility = View.GONE
        activity.lyt_no_results.visibility = View.VISIBLE
        activity.tv_no_results.text = "Search Near By Results"
        activity.iv_error.setImageResource(R.drawable.ic_no_data)
     }


    private fun searchDeals(buttonClicked: Boolean) {
        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        if (isNetworkAvailable(activity)) {
            if (userLocationLatitude == "0.0" || userLocationLongitude == "0.0") {
                if (activity.tbEtSearchHome.text.isEmpty()) {
                    activity.toast("Search field cannot be empty")
                } else {
                    searchHomeDealsPresenter.getUserHomeDeals(
                        userTokenKey,
                        activity.tbEtSearchHome.text.toString()
                    )
                    LoadingDialog.getInstance().show(activity)
                }
            } else {
//                if (buttonClicked) {
//
//                } else {
//                    searchHomeDealsPresenter.getDealsAtUserSearchedLocation(
//                        userTokenKey,
//                        userLocationLatitude,
//                        userLocationLongitude,
//                        activity.tbEtSearchHome.text.toString()
//                    )
//                    LoadingDialog.getInstance().show(activity)
//                }
                if (activity.tbEtSearchHome.text.isEmpty()) {
                    activity.toast("Search field cannot be empty")
                } else {
                    searchHomeDealsPresenter.getDealsAtUserSearchedLocation(
                        userTokenKey,
                        userLocationLatitude.toDouble(),
                        userLocationLongitude.toDouble(),
                        activity.tbEtSearchHome.text.toString()
                    )
                    Log.e(
                        "Location ",
                        "" + userLocationLatitude.toDouble() + userLocationLongitude.toDouble()
                    )
                    LoadingDialog.getInstance().show(activity)
                }
            }
        } else {
            activity.toast(getString(R.string.txt_please_check_your_internet_connection))
        }
    }

    private fun checkLocationPermission() {
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
//                    val intent: Intent = Autocomplete.IntentBuilder(
//                        AutocompleteActivityMode.FULLSCREEN, listOf(
//                            Place.Field.ID,
//                            Place.Field.NAME,
//                            Place.Field.LAT_LNG,
//                            Place.Field.ADDRESS
//                        )
//                    )
//                        .build(activity)
                    startActivityForResult(activity.intentFor<MapActivity>(),SEARCH_HOME_REQUEST_CODE)
//                    startActivityForResult(intent, SEARCH_HOME_REQUEST_CODE)
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    // check for permanent denial of permission
                    if (response.isPermanentlyDenied) {
                        if (locationPermanentlyDenied) {
                            showSettingsDialog()
                        }
                        locationPermanentlyDenied = true
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.txt_permission_title)
        builder.setMessage(R.string.txt_permission_description)
        builder.setPositiveButton(
            getString(R.string.txt_go_to_settings)
        ) { dialog, _ ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            getString(R.string.txt_cancel)
        ) { dialog, _ ->
            dialog.cancel()
        }
        builder.show()
    }

    //this method is used to open application settings for allowing run time permissions
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts(getString(R.string.txt_package), activity.packageName, null)
        intent.data = uri
        startActivityForResult(intent, SEARCH_HOME_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SEARCH_HOME_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
//                    val place =
//                        Autocomplete.getPlaceFromIntent(data!!)
//                    Log.i(
//                        "Home Activity",
//                        "Place: " + place.name + ", " + place.id + ", " + place.address + ", " + place.latLng
//                    )


                    val selectedLocation: LatLng? = data?.getParcelableExtra<LatLng>("selectedLocation")
                    val selectedAddress:String? = data?.getStringExtra("selectedAddress")
                    userLocationLatitude = selectedLocation?.latitude.toString()
                    userLocationLongitude = selectedLocation?.longitude.toString()
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status =
                        Autocomplete.getStatusFromIntent(data!!)
                    Log.i("Home Activity", status.statusMessage!!)
                }
                Activity.RESULT_CANCELED -> {
                    // The user canceled the operation.
                }
            }
        }
    }

    override fun onGettingSearchHomeDealsSuccess(body: SearchHomeDealsResponse) {
        LoadingDialog.getInstance().cancel()
        activity.rvSearchHomeDeals.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        setSearchHomeDataList(body)
        if (dataList.isEmpty()) {

           noResults(null)

        } else {
            showResults()

            if (isUserSearchedDeals) {
                val recyclerItemDecoration = RecyclerItemDecoration(
                    activity,
                    0,
                    true,
                    getSectionCallback(dataList)
                )
                activity.rvSearchHomeDeals.addItemDecoration(recyclerItemDecoration)
            } else {
                val recyclerItemDecoration = RecyclerItemDecoration(
                    activity,
                    activity.resources.getDimensionPixelSize(R.dimen.header_height),
                    true,
                    getSectionCallback(dataList)
                )
                isUserSearchedDeals = true
                activity.rvSearchHomeDeals.addItemDecoration(recyclerItemDecoration)
            }
            activity.rvSearchHomeDeals.adapter = SearchHomeDealsAdapter(activity, dataList) {
                if (it.searchHomeType == "Deals") {
                    activity.startActivity(
                        activity.intentFor<SearchDealDetailsActivity>(
                            Constants.DEAL_ID to it.dealid,
                            Constants.FROM_ACTIVITY to Constants.FROM_SEARCH_HOME_FRAGMENT
                        )
                    )
                } else {
                    activity.startActivity(
                        activity.intentFor<SearchBusinessDetailsActivity>(
                            Constants.MERCHANT_ID to it.merchantid
                        )
                    )
                }
            }
        }
    }

    private fun setSearchHomeDataList(body: SearchHomeDealsResponse) {
        if (dataList.isNotEmpty()) {
            dataList.clear()
        }
        if (!body.business.isNullOrEmpty()) {
            for (i in body.business!!.indices) {
                dataList.add(
                    SearchDealsLocal(
                        "Business",
                        body.business!![i].merchantid,
                        body.business!![i].slug,
                        body.business!![i].subcategoryName,
                        body.business!![i].businessName,
                        body.business!![i].address,
                        body.business!![i].profileImage,
                        body.business!![i].businessDescription,
                        body.business!![i].distance,
                        "",
                        "",
                        0,
                        "",
                        0,
                        0,
                        0,
                        0,
                        ""
                    )
                )
            }
        }
        if (!body.deals.isNullOrEmpty()) {
            for (i in body.deals!!.indices) {
                dataList.add(
                    SearchDealsLocal(
                        "Deals",
                        0,
                        body.deals!![i].slug,
                        body.deals!![i].subcategoryName,
                        body.deals!![i].businessName,
                        "",
                        body.deals!![i].dealImage,
                        body.deals!![i].description,
                        body.deals!![i].distance,
                        body.deals!![i].status,
                        body.deals!![i].catslug,
                        body.deals!![i].dealid,
                        body.deals!![i].termsAndConditions,
                        body.deals!![i].regularPrice,
                        body.deals!![i].offerPrice,
                        body.deals!![i].discount,
                        body.deals!![i].savingAmount,
                        body.deals!![i].expiryDate
                    )
                )
            }
        }
    }

    override fun onGettingSearchHomeDealsFail(message: String?) {
        LoadingDialog.getInstance().cancel()
        noResults(message)
//        activity.toast(message)
    }

    override fun onGettingSearchHomeDealsNoInternet() {
        LoadingDialog.getInstance().cancel()
        noResults(getString(R.string.txt_please_check_your_internet_connection))
    }

    private fun getSectionCallback(list: ArrayList<SearchDealsLocal>): RecyclerItemDecoration.SectionCallback? {
        return object : RecyclerItemDecoration.SectionCallback {
            override fun isSection(pos: Int): Boolean {
                return pos == 0 || list[pos].searchHomeType !== list[pos - 1].searchHomeType
            }

            override fun getSectionHeaderName(pos: Int): String {
                val dataMap = list[pos]
                return dataMap.searchHomeType!!
            }
        }
    }

    private fun noResults(message: String?){
        activity.rvSearchHomeDeals.visibility = View.GONE
        activity.lyt_no_results.visibility = View.VISIBLE
        activity.iv_error.setImageResource(R.drawable.ic_sad_emoji)
        if(message.isNullOrEmpty()){
            activity.tv_no_results.text = Html.fromHtml("No results found for <font color = '#f7bc00'>${ activity.tbEtSearchHome.text.toString()}</font>")
        }else{
            activity.tv_no_results.text = message
        }
    }

    private fun showResults(){
        activity.rvSearchHomeDeals.visibility = View.VISIBLE
        activity.lyt_no_results.visibility = View.GONE
    }

    override fun onDetach() {
        super.onDetach()
        searchHomeDealsPresenter.onDetach()
    }
}
