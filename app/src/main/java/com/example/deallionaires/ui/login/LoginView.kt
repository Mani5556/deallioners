package com.example.deallionaires.ui.login

import com.example.deallionaires.model.LoginResponse
import com.example.deallionaires.model.SignUp

interface LoginView {

    fun onLoginSuccess(loginResponse: LoginResponse)

    fun onLoginFail(message: String)

    fun onLoginNoInternet()


    fun socialLoginSuccess(socialLogin: SignUp)

    fun socialLoginFail(message: String)

    fun socialLoginNoInternet()

}