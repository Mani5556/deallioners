package com.example.deallionaires.ui.help

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.deallionaires.model.FaqCategory
import com.example.deallionaires.model.FaqsResponse
import com.example.deallionaires.model.HelpResponse
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.NetworkState
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class FaqsViewModel (application: Application): AndroidViewModel(application){

    val  faqsList : MutableLiveData<List<FaqCategory>> = MutableLiveData()
    val networkCall: MutableLiveData<NetworkState> = MutableLiveData<NetworkState>()

    fun getFaqs(userAccessKey: String) {
        networkCall.value = NetworkState.IN_PROGRESS

        if (isNetworkAvailable(getApplication())) {
            APIRequests.getFaqs(
                userAccessKey,
                object : NWResponseCallback<FaqsResponse?> {
                    override fun onSuccess(
                        call: Call<FaqsResponse?>,
                        response: Response<FaqsResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            networkCall.value = NetworkState.COMPLETED
                            faqsList.value = response.body()?.faqsResult
                        } else {
                            networkCall.value = NetworkState.ERROR
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<FaqsResponse?>,
                        response: Response<FaqsResponse?>
                    ) {
                        networkCall.value = NetworkState.ERROR
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FaqsResponse?>,
                        response: Response<FaqsResponse?>
                    ) {
                        networkCall.value = NetworkState.ERROR
                    }

                    override fun onFailure(call: Call<FaqsResponse?>, t: Throwable) {
                        networkCall.value = NetworkState.ERROR
                    }
                })
        } else {
            networkCall.value = NetworkState.NO_NETWORK
        }
    }

}