package com.example.deallionaires.ui.profile

import com.example.deallionaires.model.EditProfileResponse
import com.example.deallionaires.model.Profile

interface ProfileView {
    fun onGettingProfileSuccess(userProfile: Profile)

    fun onGettingProfileFail(message: String)

    fun onEditingProfileSuccess(editProfileResponse: EditProfileResponse)

    fun onEditingProfileFail(message: String)

    fun onGettingProfileNoInternet()
}