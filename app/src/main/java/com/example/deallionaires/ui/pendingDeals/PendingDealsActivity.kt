package com.example.deallionaires.ui.pendingDeals

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.deallionaires.R
import com.example.deallionaires.utils.Constants
import kotlinx.android.synthetic.main.activity_pending_deals.*

class PendingDealsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pending_deals)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_PENDING_DEALS_UPDATED))
        super.onDestroy()
    }
}