package com.example.deallionaires.ui.home.fragments.home.homeDeals

import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.Searchbusiness
import com.google.gson.JsonArray
import java.text.FieldPosition

interface ViewAllDealsBusinessView{

    fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded,position: Int)

    fun onAddingDealToFavouritesFail(message: String)

    fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved,position: Int)

    fun onRemovingDealToFavouritesFail(message: String)

    fun onGetItemsSuccess(searchbusiness: JsonArray?)

    fun onGetItemsFail(message: String)

    fun onGetItemsNoInternet()
}