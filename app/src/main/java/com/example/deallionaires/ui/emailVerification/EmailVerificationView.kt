package com.example.deallionaires.ui.emailVerification

import com.example.deallionaires.model.EmailVerificationResponse
import com.example.deallionaires.model.OtpVerificationResponse

interface EmailVerificationView {

    fun onEmailVerificationSuccess(emailVerificationResponse: EmailVerificationResponse)

    fun onEmailVerificationFailed(message: String)

    fun onOtpVerificationSuccess(otpVerificationResponse: OtpVerificationResponse)

    fun onOtpVerificationFailed(message: String)

    fun onEmailVerificationNoInternet()

}