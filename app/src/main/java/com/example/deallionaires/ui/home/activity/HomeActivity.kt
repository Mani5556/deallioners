package com.example.deallionaires.ui.home.activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.provider.Settings
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.get
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.example.deallionaires.R
import com.example.deallionaires.model.DeallionairesCount
import com.example.deallionaires.model.Profile
import com.example.deallionaires.model.UserLatLong
import com.example.deallionaires.ui.MapActivity
import com.example.deallionaires.ui.dealsStatus.completedDeals.CompletedDealsFragment
import com.example.deallionaires.ui.dealsStatus.pendingDeals.PendingDealsFragment
import com.example.deallionaires.ui.help.HelpSupportActivity
import com.example.deallionaires.ui.home.fragments.cart.CartFragment
import com.example.deallionaires.ui.home.fragments.dashboard.DashboardFragment
import com.example.deallionaires.ui.home.fragments.dashboard.deallionairesClub.DeallionairesClubFragment
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.DeallionairesEarnedFragment
import com.example.deallionaires.ui.home.fragments.favourites.FavouritesFragment
import com.example.deallionaires.ui.home.fragments.home.HomeFragment
import com.example.deallionaires.ui.home.fragments.home.searchHomeDeals.SearchHomeDealsFragment
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.ui.notification.NotificationFragment
import com.example.deallionaires.ui.profile.ProfileFragment
import com.example.deallionaires.ui.searchBusiness.SearchBusinessFragment
import com.example.deallionaires.ui.searchDeals.SearchDealsFragment
import com.example.deallionaires.ui.specialDeals.SpecialDealsFragment
import com.example.deallionaires.utils.*
import com.futuremind.recyclerviewfastscroll.Utils
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.LocationBias
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.gson.JsonObject
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_full_photo.*
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.nav_header_home.*
import kotlinx.android.synthetic.main.navigation_footer.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import android.content.BroadcastReceiver as BroadcastReceiver1


class HomeActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    HomeView {

    private var profileImageUrl: String? = null
    private lateinit var ivHeaderProfilePic: ImageView
    private lateinit var tv_email: TextView
    private lateinit var toolbarHome: Toolbar
    private lateinit var tvToolbarTitle: TextView
    private lateinit var tvHeaderUserName: TextView
    private lateinit var toolbarDeallionaires: Toolbar
    private lateinit var toolbarSearchDeals: Toolbar
    private lateinit var toolbarHomeCategoryDeals: Toolbar
    private lateinit var tbTvLocation: TextView
    private var doubleBackToExitPressedOnce = false

    private lateinit var homePresenter: HomePresenter
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private val LOCATION_PERMISSION_REQUEST_CODE = 2
    private var addressResultReceiver: LocationAddressResultReceiver? = null
    private var currentLocation: Location? = null
    private var locationCallback: LocationCallback? = null

    var locationPermanentlyDenied = false
    var isUserClickedHomeDealsBack = false
    var isUserClickedSearch = false

    private lateinit var context: Context
    private var flag = false

    private lateinit var toggle: ActionBarDrawerToggle

    private var fromActivity: String = Constants.FROM_ACTIVITY

    private var userCurrentLocation by PreferenceExt.appPreference(
        this,
        Constants.USER_LOCATION,
        Constants.LOCATION
    )

    private var isUserLogin by PreferenceExt.appPreference(this, Constants.IS_USER_LOGIN, false)

    var userTokenKey by PreferenceExt.appPreference(
        this,
        Constants.USER_TOKEN_KEY,
        ""
    )

    private var userLatLong by PreferenceExt.appPreference(
        this,
        Constants.USER_LAT_LONG,
        Constants.DEFAULT_STRING
    )

    var userLocationLatitude by PreferenceExt.appPreference(
        this,
        Constants.USER_LOCATION_LATITUDE,
        Constants.DEFAULT_STRING
    )

    var userLocationLongitude by PreferenceExt.appPreference(
        this,
        Constants.USER_LOCATION_LONGITUDE,
        Constants.DEFAULT_STRING
    )

    var userProfileName by PreferenceExt.appPreference(
        this,
        Constants.USER_PROFILE_NAME,
        Constants.DEFAULT_STRING
    )

    private var userSkipLogin by PreferenceExt.appPreference(
        this,
        Constants.USER_SKIP_LOGIN,
        false
    )

    private var userLatLngLocal by PreferenceExt.appPreference(
        this,
        Constants.USER_LAT_LONG,
        Constants.DEFAULT_LAT_LONG
    )

    var AUTOCOMPLETE_REQUEST_CODE = 123

    private lateinit var bottomNavigationView: BottomNavigationView
    private var isLocationClicked = false

    private var mToolBarNavigationListenerIsRegistered = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        toolbarHome = findViewById(R.id.toolbarHome)
        toolbarDeallionaires = findViewById(R.id.tbDeallionsEarned)
        toolbarHomeCategoryDeals = findViewById(R.id.tbHomeCategoryDeals)
        toolbarSearchDeals = findViewById(R.id.tbSearchHomeDeals)
        tvToolbarTitle = findViewById(R.id.tvToolbarTitle)
        tbTvLocation = findViewById(R.id.tbTvLocation)



        setSupportActionBar(toolbarHome)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        context = this

        homePresenter = HomePresenter(this, this)

        addressResultReceiver = LocationAddressResultReceiver(Handler())
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                currentLocation = locationResult.locations[0]
                userLocationLatitude = locationResult.locations[0].latitude.toString()
                userLocationLongitude = locationResult.locations[0].longitude.toString()
                Constants.USER_LATITUDE_VALUE = userLocationLatitude.toDouble()
                Constants.USER_LONGITUDE_VALUE = userLocationLongitude.toDouble()
                address
            }
        }

        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, Constants.API_KEY)
        }

        val fields: List<Place.Field> =
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.LAT_LNG,
                Place.Field.ADDRESS
            )

        toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbarHome,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        /*Navigation drawer with transparent background*/
//        drawer_layout.setScrimColor(resources.getColor(android.R.color.transparent))
        drawer_layout.drawerElevation = 0f

        toggle.toolbarNavigationClickListener = View.OnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }

        bottomNavigationView = findViewById<BottomNavigationView>(R.id.btmNavView)
        bottomNavigationView.setOnNavigationItemSelectedListener(myOnNavigationItemSelectedListener)

//        bottomNavigationView.getOrCreateBadge(R.id.navigation_cart).number = 3

        bottomNavigationView.itemIconTintList = null
        val headerView: View = navigation_body.getHeaderView(0)
        tvHeaderUserName =
            headerView.findViewById<View>(R.id.tvHeaderUserName) as TextView

        ivHeaderProfilePic = headerView.findViewById<View>(R.id.ivHeaderProfileImage) as ImageView





        navigation_body.setNavigationItemSelectedListener(this)

        tbTvLocation.setOnClickListener {
            isLocationClicked = true
            checkLocationPermission()
        }

        val iv_close = headerView.findViewById<ImageView>(R.id.iv_cross)
        tv_email = headerView.findViewById<TextView>(R.id.tv_header_user_email);

        iv_close.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        btn_share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Share our application")
            shareIntent.putExtra(Intent.EXTRA_TEXT, "<<App Play Store link>>")
            startActivity(Intent.createChooser(shareIntent, "Share with:"))
        }
        btn_needhelp.setOnClickListener {
          startActivity(intentFor<HelpSupportActivity>())
            drawer_layout.closeDrawer(GravityCompat.START)
        }

        if (userCurrentLocation == Constants.LOCATION) {
            startLocationUpdates()
        } else {
            tbTvLocation.text = userCurrentLocation
        }

        initNavigationDrawerMenuItems()

        val autocompleteFragment: AutocompleteSupportFragment? =
            supportFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment?
        autocompleteFragment!!.setCountries("IN")
        autocompleteFragment.setPlaceFields(fields)
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                Log.i(
                    "Home Activity",
                    "Place: " + place.name.toString() + ", " + place.id
                )
                userCurrentLocation = place.address.toString()
                tbTvLocation.text = place.address.toString()
                userLocationLatitude = place.latLng!!.latitude.toString()
                userLocationLongitude = place.latLng!!.longitude.toString()
                Constants.USER_LATITUDE_VALUE = userLocationLatitude.toDouble()
                Constants.USER_LONGITUDE_VALUE = userLocationLongitude.toDouble()
                LocalBroadcastManager.getInstance(this@HomeActivity).sendBroadcast(Intent(Constants.ACTION_LOCATION_UPDATE_RECEIVER))

            }

            override fun onError(status: Status) {
                // TODO: Handle the error.
                tbTvLocation.text = Constants.LOCATION
                Log.i("Home Activity", "An error occurred: $status")
            }
        })

        loadFragment("Home", HomeFragment())

        ivToolbarNotification.setOnClickListener {
            if (userSkipLogin) {
                displayLoginDialog("Deallionaires \nLogin in to view Notifications")
            } else {
                initToolbarTitleAndHomeVisible()
                drawerLockedAndClickable()
                loadFragment("Notifications", NotificationFragment())
            }
        }

        ivToolbarSearch.setOnClickListener {
            openSearchFragment()
        }


        ivSearchDealsBack.setOnClickListener {
            isUserClickedHomeDealsBack = true
            onBackPressed()
        }

        tvDeallionsEarnedBack.setOnClickListener {
            onBackPressed()
        }

        ivHomeCategoryDealsBack.setOnClickListener {
            isUserClickedHomeDealsBack = true
            onBackPressed()
        }

        //This will called after redeeming any deal from SearchDealDetailPage
        val openPendingDeal = intent.getBooleanExtra("openPendingDeals",false)
        if(openPendingDeal){
//            openPendingDealsFragment()
            bottomNavigationView.selectedItemId = R.id.navigation_cart
//            initToolbarHomeViews(false)
//            fromActivity = Constants.FROM_PENDING_DEALS_FRAGMENT
//            loadFragment("Pending Deals", PendingDealsFragment())
        }
    }

    fun openSearchFragment() {
        if (userSkipLogin) {
            displayLoginDialog("Deallionaires \nLogin in to Search deals/business")
        } else {
            userLatLong = userLatLngLocal
            isUserClickedSearch = true
            isUserClickedHomeDealsBack = true
            initToolbarViews(Constants.HOME_SEARCH)
            loadFragment(Constants.HOME_SEARCH, SearchHomeDealsFragment())
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            toggle.isDrawerIndicatorEnabled = false
        }
    }

    private fun checkLocationPermission() {

        Dexter.withActivity(this)
            .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    if (isLocationClicked) {
//                        val intent: Intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, listOf(
//                                Place.Field.ID,
//                                Place.Field.NAME,
//                                Place.Field.LAT_LNG,
//                                Place.Field.ADDRESS
//                            )
//                        )
//                            .setLocationBias(null)
//                            .build(this@HomeActivity)
//                        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)

                        startActivityForResult(intentFor<MapActivity>(),AUTOCOMPLETE_REQUEST_CODE)

                    } else {
                        startLocationUpdates()
                    }
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    // check for permanent denial of permission
                    if (response.isPermanentlyDenied) {
                        if (locationPermanentlyDenied) {
                            showSettingsDialog()
                        }
                        locationPermanentlyDenied = true
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        } else {
            val locationRequest = LocationRequest()
            locationRequest.interval = 1000
            locationRequest.fastestInterval = 500
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            fusedLocationClient!!.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null
            )
        }
    }

    private val address: Unit
        get() {
            if (!Geocoder.isPresent()) {
                Toast.makeText(
                    this@HomeActivity,
                    "Can't find current address, ",
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
            val intent = Intent(this, GetAddressIntentService::class.java)
            intent.putExtra("add_receiver", addressResultReceiver)
            intent.putExtra("add_location", currentLocation)
            startService(intent)
        }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                startLocationUpdates()
            } else {
                alertBox("", "")
            }
        }
    }

    private inner class LocationAddressResultReceiver internal constructor(handler: Handler?) :
        ResultReceiver(handler) {
        override fun onReceiveResult(resultCode: Int, resultData: Bundle) {
            if (resultCode == 0) {
                Log.d("Address", "Location null retrying")
                address
            }
            if (resultCode == 1) {
                Toast.makeText(
                    this@HomeActivity,
                    "Address not found, ",
                    Toast.LENGTH_SHORT
                ).show()
            }
            val currentAdd = resultData.getString("address_result")
            showResults(currentAdd)
        }
    }

    private fun showResults(currentAdd: String?) {
        tbTvLocation.text = currentAdd
        userCurrentLocation = currentAdd!!
        if (userCurrentLocation != Constants.LOCATION) {
            fusedLocationClient!!.removeLocationUpdates(locationCallback)
        }
    }

    private fun alertBox(title: String, myMessage: String) {
        val builder =
            AlertDialog.Builder(this)
        builder.setMessage("Restart your application or enter manually by clicking location button.")
            .setCancelable(false)
            .setTitle("Current location fetching failed")
            .setPositiveButton(
                "Restart"
            ) { dialog, id -> // finish the current activity
                finish()
                dialog.cancel()
            }
            .setNegativeButton(
                "Cancel"
            ) { dialog, id -> // cancel the dialog box
                dialog.cancel()
            }
        val alert = builder.create()
        alert.show()
    }

    private fun showSettingsDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.txt_permission_title)
        builder.setMessage(R.string.txt_permission_description)
        builder.setPositiveButton(
            getString(R.string.txt_go_to_settings)
        ) { dialog, _ ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton(
            getString(R.string.txt_cancel)
        ) { dialog, _ ->
            dialog.cancel()
        }
        builder.show()
    }

    //this method is used to open application settings for allowing run time permissions
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts(getString(R.string.txt_package), packageName, null)
        intent.data = uri
        startActivityForResult(intent, Constants.LOCATION_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
//                    val place =
//                        Autocomplete.getPlaceFromIntent(data!!)
//                    Log.i(
//                        "Home Activity",
////                        "Place: " + place.name + ", " + place.id + ", " + place.address + ", " + place.latLng
//                    )

                    val selectedLocation:LatLng? = data?.getParcelableExtra<LatLng>("selectedLocation")
                    val selectedAddress:String? = data?.getStringExtra("selectedAddress")

                    userCurrentLocation = selectedAddress?:Constants.LOCATION
                    tbTvLocation.text = selectedAddress?:Constants.LOCATION
                    val latLong by getStringFromModel(
                        UserLatLong(
                            selectedLocation!!.latitude,
                            selectedLocation!!.longitude
                        )
                    )
                    userLocationLatitude =  selectedLocation!!.latitude.toString()
                    userLocationLongitude =selectedLocation!!.longitude.toString()
                    Constants.USER_LATITUDE_VALUE = userLocationLatitude.toDouble()
                    Constants.USER_LONGITUDE_VALUE = userLocationLongitude.toDouble()
                    LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(Constants.ACTION_LOCATION_UPDATE_RECEIVER))
                    userLatLngLocal = latLong
                }
                AutocompleteActivity.RESULT_ERROR -> {
                    val status =
                        Autocomplete.getStatusFromIntent(data!!)
                    Log.i("Home Activity", status.statusMessage!!)
                    tbTvLocation.text = Constants.LOCATION
                }
                Activity.RESULT_CANCELED -> {
                    tbTvLocation.text = userCurrentLocation
                }
            }
        } else if (requestCode == Constants.LOCATION_CODE) {
            startLocationUpdates()
        }
    }

    /**
     * to act on bottom navigation items click
     */
    private val myOnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                val fragment: Fragment
                when (item.itemId) {
                    R.id.navigation_home -> {
                        fromActivity = Constants.FROM_HOME_FRAGMENT
                        fragment = HomeFragment()
                        loadFragment(getString(R.string.title_nav_home), fragment)
                        initToolbarHomeViews(true)
                        return true
                    }
                    R.id.navigation_dashboard -> {
                        fromActivity = Constants.FROM_DASHBOARD_FRAGMENT
                        if (userSkipLogin) {
                            displayLoginDialog("Deallionaires \nLogin to view Dashboard")
                        } else {
                            fragment = DashboardFragment()
                            loadFragment(getString(R.string.title_nav_dashboard), fragment)
                            initToolbarHomeViews(false)
                        }

                        return true
                    }
                    R.id.navigation_cart -> {
                        fromActivity = Constants.FROM_CART_FRAGMENT
                        if (userSkipLogin) {
                            displayLoginDialog("Deallionaires \nLogin to view Cart")
                        } else {
                            fragment = CartFragment()
                            loadFragment(getString(R.string.title_nav_cart), fragment)
                            initToolbarHomeViews(false)
                        }
                        return true
                    }
                    R.id.navigation_favourites -> {
                        fromActivity = Constants.FROM_FAVOURITES_FRAGMENT
                        if (userSkipLogin) {
                            displayLoginDialog("Deallionaires \nLogin to view Favourites")
                        } else {
                            fragment = FavouritesFragment()
                            loadFragment(getString(R.string.title_nav_favourites), fragment)
                            initToolbarHomeViews(false)
                        }
                        return true
                    }
                    R.id.navigation_deallions_club -> {
                        fromActivity = Constants.FROM_DEALLIONAIRES_FRAGMENT
                        if (userSkipLogin) {
                            displayLoginDialog("Deallionaires \nLogin to view Deallions Earned")
                        } else {
                            fragment = DeallionairesEarnedFragment()
                            loadFragment(getString(R.string.txt_deallionaires_earned), fragment)
                            initToolbarDeallionairesEarnedViews()
                        }
                        return true
                    }
                }
                return false
            }
        }

    private fun initToolbarDeallionairesEarnedViews() {
        homePresenter.getDeallionairesCount(userTokenKey)
        toolbarSearchDeals.visibility = View.GONE
        toolbarHomeCategoryDeals.visibility = View.GONE
        toolbarDeallionaires.visibility = View.GONE
        tvDeallionsEarnedBack.visibility = View.GONE
        toolbarHome.visibility = View.VISIBLE
        tvToolbarTitle.visibility = View.GONE
        tvTbHomeDeallionsPointsEarned.visibility = View.VISIBLE
        ivDeallionairesTextLogo.visibility = View.VISIBLE
        ivToolbarSearch.visibility = View.GONE
        tbTvLocation.visibility = View.GONE
        ivToolbarNotification.visibility = View.GONE
        toolbarHome.setBackgroundColor(resources.getColor(R.color.colorAppBlue))
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        toggle.isDrawerIndicatorEnabled = true
        toggle.toolbarNavigationClickListener = null
        mToolBarNavigationListenerIsRegistered = false
    }

    /**
     * to load fragments into the container
     */
    fun loadFragment(toolbarTitle: String, fragment: Fragment) {
        if (userSkipLogin) {
            if (toolbarTitle == getString(R.string.txt_user_deals) || toolbarTitle == "User Business") {
                bapNavHome.visibility = View.GONE
                val params = CoordinatorLayout.LayoutParams(
                    CoordinatorLayout.LayoutParams.MATCH_PARENT,
                    CoordinatorLayout.LayoutParams.MATCH_PARENT
                )
                params.setMargins(0, getDpSize(48,this), 0, 0)
                flContainerHome.layoutParams = params
            } else {
                val params = CoordinatorLayout.LayoutParams(
                    CoordinatorLayout.LayoutParams.MATCH_PARENT,
                    CoordinatorLayout.LayoutParams.MATCH_PARENT
                )
                params.setMargins(0,  getDpSize(48,this), 0,  getDpSize(48,this))
                flContainerHome.layoutParams = params
                bapNavHome.visibility = View.VISIBLE
            }
        } else {
            val params = CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.MATCH_PARENT
            )
            params.setMargins(0, getDpSize(48,this), 0, getDpSize(48,this))
            flContainerHome.layoutParams = params
            bapNavHome.visibility = View.VISIBLE
        }
        tvToolbarTitle.text = toolbarTitle
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContainerHome, fragment)
        transaction.commit()
    }

    fun showUserLocation() {
        tbTvLocation.text = userCurrentLocation
        tvToolbarTitle.text = "Home"
        if (userCurrentLocation != Constants.LOCATION) {
            fusedLocationClient!!.removeLocationUpdates(locationCallback)
        }
    }

    fun showUserName(){
        if (userSkipLogin) {
            Log.e("Home", "User Skipped Login")

        } else {
            if (isNetworkAvailable(this)) {
                homePresenter.getHomeProfilePresenter(userTokenKey)
            }
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if (userSkipLogin && isUserClickedHomeDealsBack) {
            bottomNavigationView.selectedItemId = R.id.navigation_home
            isUserClickedHomeDealsBack = false
        } else if (userSkipLogin && tvToolbarTitle.text == "Deals") {
            bottomNavigationView.selectedItemId = R.id.navigation_home
        } else if (userSkipLogin) {
            if (!doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = true
                toast(getString(R.string.txt_press_back_to_exit)).duration.toShort()
                Handler().postDelayed({
                    doubleBackToExitPressedOnce = false
                }, 2000)
            } else {
                super.onBackPressed()
            }
        } else if (isUserClickedSearch && fromActivity == Constants.FROM_DASHBOARD_FRAGMENT) {
            bottomNavigationView.selectedItemId = R.id.navigation_dashboard
            handleSearchBack(false)
        } else if (isUserClickedSearch && fromActivity == Constants.FROM_CART_FRAGMENT) {
            bottomNavigationView.selectedItemId = R.id.navigation_cart
            handleSearchBack(false)
        } else if (isUserClickedSearch && fromActivity == Constants.FROM_FAVOURITES_FRAGMENT) {
            bottomNavigationView.selectedItemId = R.id.navigation_favourites
            handleSearchBack(false)
        } else if (isUserClickedSearch && fromActivity == Constants.FROM_PENDING_DEALS_FRAGMENT) {
            openPendingDealsFragment()
        } else if (isUserClickedSearch && fromActivity == Constants.FROM_COMPLETED_DEALS_FRAGMENT) {
            handleSearchBack(true)
            loadFragment("Completed Deals", CompletedDealsFragment())
        } else if (isUserClickedSearch && fromActivity == Constants.FROM_SPECIAL_DEALS_FRAGMENT) {
            handleSearchBack(true)
            loadFragment("Special Deals", SpecialDealsFragment())
        } else if (fromActivity == Constants.FROM_DASHBOARD) {
            bottomNavigationView.selectedItemId = R.id.navigation_dashboard
        } else if (tvToolbarTitle.text.toString() != "Home") {
            bottomNavigationView.selectedItemId = R.id.navigation_home
        } else if (tvToolbarTitle.text.toString() == "Home") {
            if (!doubleBackToExitPressedOnce) {
                this.doubleBackToExitPressedOnce = true
                toast(getString(R.string.txt_press_back_to_exit)).duration.toShort()
                Handler().postDelayed({
                    doubleBackToExitPressedOnce = false
                }, 2000)
            } else {
                super.onBackPressed()
            }
        }
    }

    private fun handleSearchBack(bottomNav: Boolean) {
        if (bottomNav) {
            initToolbarHomeViews(false)
        }
        isUserClickedSearch = false
        fromActivity = Constants.FROM_HOME_FRAGMENT
    }

    fun loadCatagoriesFragment(fragment:Fragment){
        loadFragment("User Business", fragment)
        isUserClickedHomeDealsBack = true
        initToolbarHomeViews(true)
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.navHome -> {
                bottomNavigationView.selectedItemId = R.id.navigation_home
            }

            R.id.navDashboard -> {
                bottomNavigationView.selectedItemId = R.id.navigation_dashboard
            }

            R.id.navMyAccount -> {
                initToolbarHomeViews(false)
                loadFragment("Profile", ProfileFragment())
            }

            R.id.navMyFavourites -> {
                bottomNavigationView.selectedItemId = R.id.navigation_favourites
            }

            R.id.navSearchDeals -> {
                loadFragment(getString(R.string.txt_user_deals), SearchDealsFragment())
                isUserClickedHomeDealsBack = true
                initToolbarHomeViews(false)
            }

            R.id.navSpecialDeals -> {
                loadFragment("Special Deals", SpecialDealsFragment())
                fromActivity = Constants.FROM_SPECIAL_DEALS_FRAGMENT
                initToolbarHomeViews(false)
            }

            R.id.navPendingDeals -> {
                initToolbarHomeViews(false)
                fromActivity = Constants.FROM_PENDING_DEALS_FRAGMENT
                loadFragment("Pending Deals", PendingDealsFragment())
            }

            R.id.navCompletedDeals -> {
                initToolbarHomeViews(false)
                fromActivity = Constants.FROM_COMPLETED_DEALS_FRAGMENT
                loadFragment("Completed Deals", CompletedDealsFragment())
            }

            R.id.navSearchBusiness -> {
                loadFragment("User Business", SearchBusinessFragment())
                isUserClickedHomeDealsBack = true
                initToolbarHomeViews(false)
            }

            R.id.navDeallionairesClub -> {
                initToolbarHomeViews(false)
                loadFragment("Deallions Club", DeallionairesClubFragment())
            }

            R.id.navLogin -> {
                startActivity(intentFor<LoginActivity>())
            }

            R.id.navLogout -> {
                displayLogoutDialog()
            }

            R.id.btn_share -> {
               shareAppIntent()
            }

        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    /**
     * this method shares the playstore link of app
     */
    private fun shareAppIntent(){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Share our application")
        shareIntent.putExtra(Intent.EXTRA_TEXT, "<<App Play Store link>>")
        startActivity(Intent.createChooser(shareIntent, "Share with:"))
    }

    private fun displayLogoutDialog() {
        val builder = AlertDialog.Builder(this).create()
        val loginFormView: View = layoutInflater.inflate(R.layout.activity_logout_dialogue, null)
        builder.setView(loginFormView)
        val tvDoneLogout = loginFormView.findViewById<TextView>(R.id.tvLogout)
        val tvCancelLogout = loginFormView.findViewById<TextView>(R.id.tvLogoutCancel)
        builder.setCancelable(true)
        tvDoneLogout.setOnClickListener {
            isUserLogin = false
            userTokenKey = Constants.DEFAULT_STRING
            userProfileName = Constants.DEFAULT_STRING
            userCurrentLocation = Constants.LOCATION
            userLatLngLocal = Constants.DEFAULT_LAT_LONG
            userLocationLatitude = Constants.DEFAULT_STRING
            userLocationLongitude = Constants.DEFAULT_STRING
            Constants.USER_LATITUDE_VALUE = 0.0
            Constants.USER_LONGITUDE_VALUE = 0.0
            userSkipLogin = true
            finish()
            toast("Logged Out Successfully")
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            builder.cancel()
        }
        tvCancelLogout.setOnClickListener {
            builder.cancel()
        }
        builder.show()
    }

    override fun onGettingHomeProfileSuccess(userProfile: Profile?) {
        userProfileName = "${userProfile?.firstName} ${userProfile?.lastName}"
        tvHeaderUserName.text = "${userProfile?.firstName} ${userProfile?.lastName}"

        ivHeaderProfilePic.load(userProfile?.profileImage,true) {isLoaded->
            if(isLoaded){
                ivHeaderProfilePic.setOnClickListener {imageView->
                    userProfile?.profileImage?.let {
                        goToFullImageView(it,imageView)
                    }
                }
            }
        }

        tv_email.text = "${userProfile?.email?:""}"
    }

    override fun onGettingHomeProfileFail(message: String) {
        Log.i("Home Activity", "Getting profile failed")
    }

    override fun onGettingHomeDeallionairesCountSuccess(deallionairesCount: DeallionairesCount) {
        if(!deallionairesCount.sum.isNullOrEmpty()){
            tvTbDeallionsPointsEarned.text =  addCommasToNumericString( deallionairesCount.sum.toString()) + " " + "Points"
            tvTbHomeDeallionsPointsEarned.text =  addCommasToNumericString( deallionairesCount.sum.toString()) + " " + "Points"
        }else {
            tvTbHomeDeallionsPointsEarned.text = "0 Points"
        }
    }

    override fun onGettingHomeDeallionairesCountFail(message: String) {
        toast(message)
    }

    override fun onGettingHomeProfileNoInternet() {
        Log.i("Home Activity", "No internet")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (userSkipLogin) {
            tvHeaderUserName.visibility = View.GONE
        } else {
            tvHeaderUserName.visibility = View.VISIBLE
            tvHeaderUserName.text = userProfileName
        }
        return true
    }

    private fun clearAllBackStack() {
        val fm = supportFragmentManager
        for (i in 0 until fm.backStackEntryCount - 1) {
            fm.popBackStack()
        }
    }

    fun loadFragmentInHomeActivity(
        title: String,
        fragment: Fragment,
        hamburgerShow: Boolean,
        fragmentFrom: String
    ) {
        fromActivity = fragmentFrom
        clearAllBackStack()
        tvToolbarTitle.text = title
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.flContainerHome, fragment)
        transaction.commit()
        initToolbarViews(title)
        if (!hamburgerShow) {
            if (title != Constants.CART) {
                drawerLockedAndClickable()
            }
        }
    }

    private fun initToolbarViews(fromString: String) {
        when (fromString) {
            Constants.HOME_SEARCH -> {
                toolbarSearchDeals.visibility = View.VISIBLE
                toolbarHomeCategoryDeals.visibility = View.GONE
                toolbarDeallionaires.visibility = View.GONE
                toolbarHome.visibility = View.GONE
                tvTbHomeDeallionsPointsEarned.visibility = View.GONE
                ivDeallionairesTextLogo.visibility = View.GONE
                toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            }
            Constants.DEALLIONS_EARNED -> {
                homePresenter.getDeallionairesCount(userTokenKey)
                toolbarSearchDeals.visibility = View.GONE
                toolbarHomeCategoryDeals.visibility = View.GONE
                toolbarDeallionaires.visibility = View.VISIBLE
                tvDeallionsEarnedBack.visibility = View.VISIBLE
                tvTbHomeDeallionsPointsEarned.visibility = View.GONE
                ivDeallionairesTextLogo.visibility = View.GONE
                toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                toolbarHome.visibility = View.GONE
            }
            Constants.DEALLIONS_CLUB -> {
                toolbarSearchDeals.visibility = View.GONE
                toolbarHomeCategoryDeals.visibility = View.GONE
                toolbarDeallionaires.visibility = View.GONE
                toolbarHome.visibility = View.VISIBLE
                tvTbHomeDeallionsPointsEarned.visibility = View.GONE
                ivDeallionairesTextLogo.visibility = View.GONE
                toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                tvToolbarTitle.visibility = View.VISIBLE
                ivToolbarSearch.visibility = View.GONE
            }
            Constants.COMPLETED_DEALS -> {
                toolbarSearchDeals.visibility = View.GONE
                toolbarHomeCategoryDeals.visibility = View.GONE
                toolbarDeallionaires.visibility = View.GONE
                toolbarHome.visibility = View.VISIBLE
                tvTbHomeDeallionsPointsEarned.visibility = View.GONE
                ivDeallionairesTextLogo.visibility = View.GONE
                toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                tvToolbarTitle.visibility = View.VISIBLE
                ivToolbarSearch.visibility = View.GONE
            }
            Constants.FAVOURITES -> {
                toolbarSearchDeals.visibility = View.GONE
                toolbarHomeCategoryDeals.visibility = View.GONE
                toolbarDeallionaires.visibility = View.GONE
                toolbarHome.visibility = View.VISIBLE
                tvTbHomeDeallionsPointsEarned.visibility = View.GONE
                ivDeallionairesTextLogo.visibility = View.GONE
                toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                tvToolbarTitle.visibility = View.VISIBLE
                ivToolbarSearch.visibility = View.GONE
            }
            Constants.HOME_DEALS -> {
                initToolbarTitleAndHomeVisible()
                drawerLockedAndClickable()
            }
            Constants.CART -> {
                bottomNavigationView.selectedItemId = R.id.navigation_cart
            }
            Constants.HOME_CATEGORY_DEALS -> {
                toolbarSearchDeals.visibility = View.GONE
                toolbarHomeCategoryDeals.visibility = View.VISIBLE
                toolbarDeallionaires.visibility = View.GONE
                toolbarHome.visibility = View.GONE
                tvTbHomeDeallionsPointsEarned.visibility = View.GONE
                ivDeallionairesTextLogo.visibility = View.GONE
                toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                val mainCategoryName by PreferenceExt.appPreference(
                    this,
                    Constants.MAIN_CATEGORY_NAME,
                    Constants.DEFAULT_STRING
                )
                tvTbHomeCategoryDealsTitle.text = mainCategoryName
            }
            else -> {
                initToolbarHomeViews(true)
            }
        }
    }

    private fun drawerLockedAndClickable() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        toggle.isDrawerIndicatorEnabled = false
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        if (!mToolBarNavigationListenerIsRegistered) {
            toggle.toolbarNavigationClickListener =
                View.OnClickListener { // Doesn't have to be onBackPressed
                    onBackPressed()
                }
            mToolBarNavigationListenerIsRegistered = true
        }
    }

    private fun initToolbarTitleAndHomeVisible() {
        toolbarHome.visibility = View.VISIBLE
        tvToolbarTitle.visibility = View.VISIBLE
        toolbarDeallionaires.visibility = View.GONE
        toolbarSearchDeals.visibility = View.GONE
        toolbarHomeCategoryDeals.visibility = View.GONE
        ivToolbarSearch.visibility = View.GONE
        ivToolbarNotification.visibility = View.GONE
        tbTvLocation.visibility = View.GONE
        tvTbHomeDeallionsPointsEarned.visibility = View.GONE
        ivDeallionairesTextLogo.visibility = View.GONE
        toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))
    }

    public fun initToolbarHomeViews(fromHome: Boolean) {
        if (fromHome) {
            tvToolbarTitle.visibility = View.GONE
            ivToolbarNotification.visibility = View.VISIBLE
            tbTvLocation.visibility = View.VISIBLE
        } else {
            tvToolbarTitle.visibility = View.VISIBLE
            ivToolbarNotification.visibility = View.GONE
            tbTvLocation.visibility = View.GONE
        }

        tvTbHomeDeallionsPointsEarned.visibility = View.GONE
        ivDeallionairesTextLogo.visibility = View.GONE
        toolbarHome.setBackgroundColor(resources.getColor(R.color.colorPrimary))

        toolbarHome.visibility = View.VISIBLE

        if (tvToolbarTitle.text.toString() == "User Deals" || tvToolbarTitle.text.toString() == "User Business" || tvToolbarTitle.text.toString() == "Home") {
            ivToolbarSearch.visibility = View.GONE
        } else {
            ivToolbarSearch.visibility = View.VISIBLE
        }

        toolbarSearchDeals.visibility = View.GONE
        toolbarHomeCategoryDeals.visibility = View.GONE
        toolbarDeallionaires.visibility = View.GONE
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        toggle.isDrawerIndicatorEnabled = true
        toggle.toolbarNavigationClickListener = null
        mToolBarNavigationListenerIsRegistered = false
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient!!.removeLocationUpdates(locationCallback)
    }

    private fun displayLoginDialog(loginText: String) {
        CustomDialog(this).showDialog(
            loginText,
            "Login",
            "Not Now"
        ) {
            startActivity(intentFor<LoginActivity>())
            finish()
        }
    }

    private fun initNavigationDrawerMenuItems() {
        val menuItems = navigation_body.menu
        if (userSkipLogin) {
            menuItems.findItem(R.id.navHome).isVisible = true
            menuItems.findItem(R.id.navSearchDeals).isVisible = true
            menuItems.findItem(R.id.navSearchBusiness).isVisible = true
            menuItems.findItem(R.id.navDashboard).isVisible = false
            menuItems.findItem(R.id.navSpecialDeals).isVisible = false
            menuItems.findItem(R.id.navPendingDeals).isVisible = false
            menuItems.findItem(R.id.navCompletedDeals).isVisible = false
            menuItems.findItem(R.id.navMyFavourites).isVisible = false
            menuItems.findItem(R.id.navDeallionairesClub).isVisible = false
            menuItems.findItem(R.id.navMyAccount).isVisible = false
            menuItems.findItem(R.id.navLogout).isVisible = false
            menuItems.findItem(R.id.navLogin).isVisible = true
//            menuItems.findItem(R.id.navCommunicate).isVisible = true
//            menuItems.findItem(R.id.btn_share).isVisible = true
            menuItems.findItem(R.id.navHome).isVisible = true
        } else {
            menuItems.findItem(R.id.navHome).isVisible = true
            menuItems.findItem(R.id.navSearchDeals).isVisible = true
            menuItems.findItem(R.id.navSearchBusiness).isVisible = true
            menuItems.findItem(R.id.navDashboard).isVisible = true
            menuItems.findItem(R.id.navSpecialDeals).isVisible = true
            menuItems.findItem(R.id.navPendingDeals).isVisible = true
            menuItems.findItem(R.id.navCompletedDeals).isVisible = true
            menuItems.findItem(R.id.navMyFavourites).isVisible = true
            menuItems.findItem(R.id.navDeallionairesClub).isVisible = true
            menuItems.findItem(R.id.navMyAccount).isVisible = true
            menuItems.findItem(R.id.navLogout).isVisible = true
            menuItems.findItem(R.id.navLogin).isVisible = false
//            menuItems.findItem(R.id.navCommunicate).isVisible = true
//            menuItems.findItem(R.id.btn_share).isVisible = true
            menuItems.findItem(R.id.navHome).isVisible = true
        }
    }




    fun openPendingDealsFragment() {
        handleSearchBack(true)
        loadFragment("Pending Deals", PendingDealsFragment())
    }

}
