package com.example.deallionaires.ui.mobileVerification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.example.deallionaires.R
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.hideKeyboard
import kotlinx.android.synthetic.main.activity_mobile_number.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.jetbrains.anko.intentFor

class MobileNumberActivity : AppCompatActivity() {

    private val verifyMobNumValidation = AwesomeValidation(ValidationStyle.BASIC)
    private lateinit var countryCode: String
    private lateinit var signUpKey: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_number)

        countryCode = ccpVerifyMobNum.selectedCountryCode

        signUpKey = intent.getStringExtra(Constants.SIGN_UP_ACCESS_TOKEN)!!

        intent.getStringExtra(Constants.SIGN_UP_ACCESS_TOKEN)!!

        ccpVerifyMobNum.setOnCountryChangeListener {
            countryCode = ccpVerifyMobNum.selectedCountryCode
            Log.e("Verify Mob Num", countryCode)
        }

        tvSendOtp.setOnClickListener {
            hideKeyboard(tvSendOtp)
            verifyMobNumValidation.addValidation(
                this,
                R.id.etVerifyMobileNumber,
                RegexTemplate.NOT_EMPTY,
                R.string.txt_enter_mobile_number
            )

            val mobileNum = ".{8,}"

            verifyMobNumValidation.addValidation(
                this,
                R.id.etVerifyMobileNumber,
                mobileNum,
                R.string.error_valid_mob_num
            )

            verifyMobNumValidation.addValidation(
                this,
                R.id.etVerifyMobileNumber,
                RegexTemplate.TELEPHONE,
                R.string.error_valid_mob_num
            )

            if (verifyMobNumValidation.validate()) {
                startActivity(
                    intentFor<MobileVerificationActivity>(
                        Constants.FROM_ACTIVITY to Constants.FROM_MOBILE_NUMBER,
                        Constants.SIGN_UP_ACCESS_TOKEN to signUpKey,
                        Constants.COUNTRY_CODE to countryCode,
                        Constants.VERIFICATION_MOBILE_NUMBER to etVerifyMobileNumber.text.toString()
                    )
                )
            }
        }
    }
}