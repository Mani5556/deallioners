package com.example.deallionaires.ui.home.fragments.dashboard.deallionairesClub

interface DeallionairesClubPresenter {

    fun getDeallionairesClub(userTokenKey: String)

    fun destroyDeallionairesClub()

}