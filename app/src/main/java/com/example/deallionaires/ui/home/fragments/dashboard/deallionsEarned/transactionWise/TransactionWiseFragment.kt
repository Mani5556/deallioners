package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.TransactionWiseDeallionsAdapter
import com.example.deallionaires.model.Transactionvaluepoints
import com.example.deallionaires.model.TransactionWiseResponse
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_transaction_wise.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class TransactionWiseFragment : Fragment(), TransactionWiseView {

    private lateinit var activity: Activity
    private lateinit var transactionWisePresenter: TransactionWisePresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_wise, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        transactionWisePresenter = TransactionWisePresenter(activity, this)
        if (isNetworkAvailable(activity)) {
            transactionWisePresenter.getTransactionWiseDeallions(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            LoadingDialog.getInstance().cancel()
        }
    }

    override fun onGettingTransactionWiseDeallionsSuccess(transactionWiseResponse: TransactionWiseResponse) {
        LoadingDialog.getInstance().cancel()
        if (transactionWiseResponse.transactionvaluepoints.isNullOrEmpty()) {
            activity.tvNoTransactionWiseDeallionairesFound.visibility = View.VISIBLE
            activity.rvTransactionWiseDeallions.visibility = View.GONE
        } else {
            activity.tvNoTransactionWiseDeallionairesFound.visibility = View.GONE
            activity.rvTransactionWiseDeallions.visibility = View.VISIBLE
            activity.rvTransactionWiseDeallions.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            activity.rvTransactionWiseDeallions.adapter =
                TransactionWiseDeallionsAdapter(
                    activity,
                    transactionWiseResponse.transactionvaluepoints as ArrayList<Transactionvaluepoints>
                ) {
                    activity.startActivity(activity.intentFor<SearchBusinessDetailsActivity>(Constants.MERCHANT_ID to it))
                }
        }
    }

    override fun onGettingTransactionWiseDeallionsFail(message: String) {
        LoadingDialog.getInstance().cancel()
    }

    override fun onGettingTransactionWiseNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onResume() {
        super.onResume()
        if (!isNetworkAvailable(activity)) {
            LoadingDialog.getInstance().cancel()
        }
    }
}
