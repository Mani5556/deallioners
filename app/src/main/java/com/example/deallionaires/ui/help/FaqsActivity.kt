package com.example.deallionaires.ui.help

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.example.deallionaires.R
import com.example.deallionaires.adapters.FaqsCategoriesAdapter
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.NetworkState
import com.example.deallionaires.utils.PreferenceExt
import kotlinx.android.synthetic.main.activity_faqs.*


class FaqsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faqs)
        toolbar.setNavigationOnClickListener { finish() }

        var userTokenKey by PreferenceExt.appPreference(
            this,
            Constants.USER_TOKEN_KEY,
            ""
        )

        val viewModel = ViewModelProviders.of(this).get(FaqsViewModel::class.java)
        viewModel.faqsList.observe(this, Observer {
            showBody()
            rv_faqs.run {
                adapter = FaqsCategoriesAdapter(this@FaqsActivity, it)
                layoutManager = LinearLayoutManager(this@FaqsActivity, LinearLayoutManager.VERTICAL, false)
            }
        })



        viewModel.networkCall.observe(this, Observer {
            when (it) {
                NetworkState.ERROR -> {
                    showErrorMessage("Some thing went wrong")
                }
                NetworkState.COMPLETED -> {
                    progress_bar.visibility = View.GONE
                }
                NetworkState.IN_PROGRESS -> {
                    progress_bar.visibility = View.VISIBLE
                }
                NetworkState.NO_NETWORK -> {
                    showErrorMessage("No network connection")
                }
                else -> {
                    showErrorMessage("Some thing went wrong")
                }
            }
        })

        viewModel.getFaqs(userTokenKey)

    }

    private fun showErrorMessage(message: String) {
        progress_bar.visibility = View.GONE
        lyt_error.visibility = View.VISIBLE
        tv_no_data_found.text = message
        lyt_body.visibility = View.GONE
    }

    private fun showBody() {
        lyt_error.visibility = View.GONE
        progress_bar.visibility = View.GONE
        lyt_body.visibility = View.VISIBLE

    }
}