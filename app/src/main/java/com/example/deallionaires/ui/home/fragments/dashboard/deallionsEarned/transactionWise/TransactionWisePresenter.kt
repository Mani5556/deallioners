package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise

import android.content.Context
import com.example.deallionaires.model.TransactionWiseResponse
import com.example.deallionaires.network.APIRequests.getUserTransactionWiseDeallions
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class TransactionWisePresenter(
    private val context: Context,
    private var transactionWiseView: TransactionWiseView?
) {

    fun getTransactionWiseDeallions(userTokenKey: String) {
        if(isNetworkAvailable(context)){
            getUserTransactionWiseDeallions(userTokenKey, object : NWResponseCallback<TransactionWiseResponse>{
                override fun onSuccess(
                    call: Call<TransactionWiseResponse>,
                    response: Response<TransactionWiseResponse>
                ) {
                    transactionWiseView!!.onGettingTransactionWiseDeallionsSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<TransactionWiseResponse>,
                    response: Response<TransactionWiseResponse>
                ) {
                    transactionWiseView!!.onGettingTransactionWiseDeallionsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<TransactionWiseResponse>,
                    response: Response<TransactionWiseResponse>
                ) {
                    transactionWiseView!!.onGettingTransactionWiseDeallionsFail(response.message())
                }

                override fun onFailure(call: Call<TransactionWiseResponse>, t: Throwable) {
                    transactionWiseView!!.onGettingTransactionWiseDeallionsFail(t.message!!)
                }
            })
        }else {
            transactionWiseView!!.onGettingTransactionWiseNoInternet()
        }
    }

}