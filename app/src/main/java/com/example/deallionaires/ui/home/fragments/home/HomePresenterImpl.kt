package com.example.deallionaires.ui.home.fragments.home

import android.content.Context
import android.util.Log
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.APIRequests.getLandingPageCategories
import com.example.deallionaires.network.APIRequests.getUserLandingPageDeals
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Response
import kotlin.coroutines.CoroutineContext


class HomePresenterImpl(val context: Context,private var homeView: HomeView?) :
    HomePresenter {
    override fun getCategories(userTokenKey: String, context: Context,latitude:String,longitude:String) {
        if (isNetworkAvailable(context)) {

            getLandingPageCategories(
                userTokenKey,
                context,
                object : NWResponseCallback<List<Categories>> {
                    override fun onSuccess(
                        call: Call<List<Categories>>,
                        response: Response<List<Categories>>
                    ) {
                        val deals = ArrayList<HomeDeals>()

                        homeView?.onGetCategoriesSuccess(response.body() as ArrayList<Categories>)
                    }

                    override fun onResponseBodyNull(
                        call: Call<List<Categories>>,
                        response: Response<List<Categories>>
                    ) {
                        homeView?.onGetCategoriesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<List<Categories>>,
                        response: Response<List<Categories>>
                    ) {
                        homeView?.onGetCategoriesFail(response.message())
                    }

                    override fun onFailure(call: Call<List<Categories>>, t: Throwable) {
                        homeView?.onGetCategoriesFail("Some thing went wrong")
                    }

                })



            getUserLandingPageDeals(
                userTokenKey,
                context,
                latitude,longitude,
                object : NWResponseCallback<JsonObject> {
                    override fun onSuccess(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {

                         GlobalScope.async {
                             withContext(Dispatchers.IO) {
                                 val headingsArray: JsonArray? =
                                     response.body()?.getAsJsonArray("allheadings")
                                 val finalHomeData: HashMap<String, List<Any>> = HashMap()
                                 headingsArray?.let {headings->
                                     headings.forEach {heading->
                                         val json: JsonObject? =
                                             response.body()?.getAsJsonObject(heading.asString)
                                         var responseList: ArrayList<Any> = ArrayList()
                                         json?.let {
                                             if (json["type"].asString.equals("merchants")) {
                                                 responseList = Gson().fromJson(
                                                     json["response_obj"],
                                                     object :
                                                         TypeToken<ArrayList<Searchbusiness>>() {}.type
                                                 )
                                             } else if(json["type"].asString.equals("deals")){
                                                     responseList = Gson().fromJson(
                                                         json["response_obj"],
                                                         object :
                                                             TypeToken<ArrayList<SearchDealsCategoryItems>>() {}.type
                                                     )

                                             }else {
                                                 responseList = Gson().fromJson(
                                                     json["response_obj"],
                                                     object :
                                                         TypeToken<ArrayList<DealStatus>>() {}.type
                                                 )
                                             }
                                         }
                                         if (responseList.size > 0) {
                                             finalHomeData.put(heading.asString, responseList)
                                         }
                                     }
                                 }
                                 withContext(Dispatchers.Main) {
                                     homeView?.onGetLandingPageDataSuccess(finalHomeData)
                                 }

                             }
                        }

                    }

                    override fun onResponseBodyNull(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {
                        homeView?.onGetLandingPageDataFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<JsonObject>,
                        response: Response<JsonObject>
                    ) {
                        homeView?.onGetLandingPageDataFail(response.message())
                    }

                    override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                        homeView?.onGetLandingPageDataFail("Oops! some thing went wrong")
                    }

                })
        } else {
            homeView?.onGetCategoriesNoInternet()
        }

    }


    override fun addDealToFavourites(userTokenKey: String, merchantid: Int,position:Int,parentAdapterPosition:Int) {
        if (isNetworkAvailable(context)) {
            APIRequests.getAddDealToFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteAdded?> {
                    override fun onSuccess(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        homeView?.onAddingDealToFavouritesSuccess(response.body()!!,position,parentAdapterPosition)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        homeView?.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        homeView?.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteAdded?>, t: Throwable) {
                        homeView?.onAddingDealToFavouritesFail(t.message!!)
                    }
                })
        } else {
            homeView?.onGetCategoriesNoInternet()
        }
    }

    override fun removeDealFromFavourites(userTokenKey: String, merchantid: Int,position: Int,parentAdapterPosition:Int) {
        if (isNetworkAvailable(context)) {
            APIRequests.getRemoveDealFromFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteRemoved?> {
                    override fun onSuccess(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        homeView?.onRemovingDealToFavouritesSuccess(response.body()!!,position,parentAdapterPosition)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        homeView?.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        homeView?.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                        homeView?.onRemovingDealToFavouritesFail(t.message!!)
                    }

                })
        } else {
            homeView?.onGetCategoriesNoInternet()
        }
    }

    override fun onDetach() {
        homeView = null
    }


    override fun onHomeFragmentDestroy() {
        homeView == null
    }
}