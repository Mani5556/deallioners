package com.example.deallionaires.ui.login

import com.example.deallionaires.model.SignUpUser

interface LoginPresenter {

    fun loginUser(email: String, password: String, loginType: String, socialId: String)
    fun socilalogin(signUpUser: SignUpUser)

    fun onDestroy()

}