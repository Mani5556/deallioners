package com.example.deallionaires.ui.dealsStatus.pendingDeals

import android.app.Activity
import android.app.AlertDialog
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.transition.TransitionManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.DealsStatusAdapter
import com.example.deallionaires.model.ClaimDeal
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottomsheet_claim_pendingdeals.*
import kotlinx.android.synthetic.main.bottomsheet_claim_pendingdeals.view.*
import kotlinx.android.synthetic.main.dialog_all_categories.view.*
import kotlinx.android.synthetic.main.dialog_all_categories.view.iv_cancel
import kotlinx.android.synthetic.main.fragment_pending_deals.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class PendingDealsFragment : Fragment(), PendingDealsView {

    private var claimDealDialog: AlertDialog? = null
    lateinit var activity: Activity
    private lateinit var pendingDealsPresenter: PendingDealsPresenter
    private lateinit var userKey: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pending_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!
        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        userKey = userTokenKey

        pendingDealsPresenter = PendingDealsPresenterImpl(activity, this)

        if (isNetworkAvailable(activity)) {
            pendingDealsPresenter.getGetPendingDealsPresenter(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }
    }

    override fun onGettingPendingDealsSuccess(deals: ArrayList<DealStatus>) {
        LoadingDialog.getInstance().cancel()
        if (deals.isEmpty()) {
            activity.tvNoPendingDealsFound.visibility = View.VISIBLE
            activity.rvPendingDeals.visibility = View.GONE
        } else {
            activity.tvNoPendingDealsFound.visibility = View.GONE
            activity.rvPendingDeals.visibility = View.VISIBLE
            activity.rvPendingDeals.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            activity.rvPendingDeals.adapter =
                DealsStatusAdapter(activity, deals) { deal: DealStatus, isClaimClicked: Boolean ->
                    if (isClaimClicked) {
                        showClaimDealDialog(deal)
                    } else {
                        activity.startActivity(
                            activity.intentFor<SearchDealDetailsActivity>(
                                Constants.DEAL_ID to deal.dealid,
                                Constants.FROM_ACTIVITY to Constants.FROM_PENDING_DEALS_FRAGMENT
                            )
                        )
                    }
                }
        }
    }

    fun showClaimDealDialog(dealStatus: DealStatus) {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.bottomsheet_claim_pendingdeals, null, false)
        view.iv_cancel.setOnClickListener {
            claimDealDialog?.cancel()
        }

        fun validate():Boolean {
            if (view.et_final_amount.data.isNullOrBlank()) {
                view.et_final_amount.setError("Please enter amount")
                return false
            }

            if (view.checkbox_deallians_pay.isChecked) {
//                if (view.et_cash_paid.data.isNullOrBlank()) {
//                    view.et_cash_paid.setError("Please enter amount")
//                    return false
//                }

                when (view.rg_pints_type.checkedRadioButtonId) {
                    R.id.rb_custom -> {
                        if (view.et_loyalty_points.data.isNullOrBlank()) {
                            view.et_loyalty_points.setError("Please enter amount")
                            return false
                        }
                    }
                    R.id.rb_full -> {

                    }
                }

            }

            if (view.et_verification_code.data.isNullOrBlank()) {
                view.et_verification_code.setError("Please enter verificationCode")
                return false
            }
            if (view.et_final_amount.data.isNullOrBlank()) {
                view.et_final_amount.setError("Please enter amount")
                return false
            }

            return true
        }

        view.tvClaimSheetLoyaltyPointsTitle.text = addCommasToNumericString(dealStatus.total_deallions.toString())


        val bulder: AlertDialog.Builder = AlertDialog.Builder(context).apply {
            setView(view)
        }



        view.checkbox_deallians_pay.setOnCheckedChangeListener { buttonView, isChecked ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                TransitionManager.beginDelayedTransition(view as ViewGroup)
            }
            if(isChecked){
                view.rb_full.isChecked = true
                view.lyt_deallion_pay.visibility = View.VISIBLE

            }else{
                view.lyt_deallion_pay.visibility = View.GONE
            }
        }
        view.rg_pints_type.setOnCheckedChangeListener{rg:RadioGroup,checkedId:Int->
            when(checkedId){
                R.id.rb_full ->{
                    view.lyt_loyalty_points.visibility = View.GONE
                    val totalAmount:Double = view.et_final_amount.getDoubleValue()
                    val payableamount:Double = if(totalAmount - dealStatus.total_deallions!!/1000 < 0) 0.0 else totalAmount - dealStatus.total_deallions/1000
                    view.tv_cash_paid.text = "${ "%.3f".format(payableamount)}"
                }
                R.id.rb_custom ->{
                    view.lyt_loyalty_points.visibility = View.VISIBLE
                    val totalAmount:Double = view.et_final_amount.getDoubleValue()
                    val customDeallionsPoints:Double =  view.et_loyalty_points.getDoubleValue()
                    val payableamount:Double = if(totalAmount - customDeallionsPoints/1000 < 0) 0.0 else totalAmount - customDeallionsPoints/1000
                    view.tv_cash_paid.text = "${ "%.3f".format(payableamount)}"
                }
            }
        }
        view.iv_cancel.setOnClickListener {
            claimDealDialog?.cancel()
        }

        view.et_loyalty_points.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if(!s.isNullOrBlank()){
                    if(s.toString().trim().toDouble() > dealStatus.total_deallions!!){
                        view.et_loyalty_points.setError("cant use more than ${dealStatus.total_deallions} points")
                    }else if(s.toString().trim().toDouble()/1000 >  view.et_final_amount.getDoubleValue()) {
                        view.et_loyalty_points.setError("cant use more than final amount")
                    }else{
                        val totalAmount:Double = view.et_final_amount.getDoubleValue()
                        val payableamount:Double = totalAmount - s.toString().trim().toDouble()/1000

                        view.tv_cash_paid.text = "${ "%.3f".format(payableamount)}"
                    }
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })


        fun areValidPoints(){

        }

        view.tv_claim_deal.setOnClickListener {
            if (!validate()) {
                return@setOnClickListener
            }

            var loyaltipoints: Double = 0.0
            if (dealStatus.verification_code.isNullOrEmpty()) {
                activity.toast("Verification Code cannot be empty")
            } else {
                loyaltipoints = if (view.checkbox_deallians_pay.isChecked) {
                    when (view.rg_pints_type.checkedRadioButtonId) {
                        R.id.rb_custom -> {
                            val points:Double = view.et_loyalty_points.getLongValue().toDouble()
                            if (points > dealStatus.total_deallions!!){
                                view.et_loyalty_points.error = "You can use max of ${dealStatus.total_deallions}"
                                return@setOnClickListener
                            }else if(points/1000 > view.et_final_amount.getDoubleValue()){
                                view.et_loyalty_points.error = "You can use max of ${view.et_final_amount.getDoubleValue()*1000}"
                                return@setOnClickListener
                            }else{
                                points
                            }

                        }
                        else -> {
                            val points:Double = dealStatus.total_deallions!!.toDouble()
                            if(points/1000 > view.et_final_amount.getDoubleValue()  ){
                                view.et_final_amount.getDoubleValue() * 1000
                            }else{
                                points
                            }
                        }
                    }
                } else {
                    0.0
                }

                if(loyaltipoints < 0  || view.et_final_amount.getDoubleValue() < 0){
                    context?.toast("final amount must not be negative value")
                }

                val savingAmount =  if(view.et_saving_amount!!.text.toString().isEmpty())"0" else  view.et_saving_amount!!.text.toString()
                val claimDeal = ClaimDeal(
                    dealStatus.verification_code!!,
                    view.et_final_amount.text.toString(),
                    savingAmount,
                    dealStatus.dealid!!,
                    dealStatus.merchantid!!,
                    loyaltipoints.toString()
                )
//                    val claimDeal = ClaimDeal(
//                        dealStatus.verification_code,
//                        "" + dealStatus.dealid,
//                        "" + dealStatus.merchantid,
//                        etTotalBillAmount.text.toString(),
//                        etUserSavingAmount.text.toString(),
//                        etLoyaltyPoints.text.toString()
//                    )
                pendingDealsPresenter.claimToCompleteDeal(userKey, dealStatus.redeemdealid!!, claimDeal)
                LoadingDialog.getInstance().show(activity)
                claimDealDialog?.cancel()
            }
        }
        view.tv_cancel_deal.setOnClickListener { claimDealDialog?.cancel() }

        claimDealDialog = bulder.create()
        claimDealDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent);
        claimDealDialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        claimDealDialog?.show()
    }


    override fun onGettingPendingDealsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGettingPendingDealsNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onClaimSuccess(successMessage: String) {
        LoadingDialog.getInstance().cancel()
        if (isNetworkAvailable(activity)) {
            pendingDealsPresenter.getGetPendingDealsPresenter(userKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }
        activity.toast(successMessage)
    }

    override fun onClaimFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onClaimNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
    }

}
