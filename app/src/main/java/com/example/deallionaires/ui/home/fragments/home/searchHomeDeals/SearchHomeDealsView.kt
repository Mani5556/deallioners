package com.example.deallionaires.ui.home.fragments.home.searchHomeDeals

import com.example.deallionaires.model.SearchHomeDealsResponse

interface SearchHomeDealsView {

    fun onGettingSearchHomeDealsSuccess(body: SearchHomeDealsResponse)

    fun onGettingSearchHomeDealsFail(message: String?)

    fun onGettingSearchHomeDealsNoInternet()

}