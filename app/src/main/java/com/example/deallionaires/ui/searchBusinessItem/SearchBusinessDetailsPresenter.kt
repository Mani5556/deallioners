package com.example.deallionaires.ui.searchBusinessItem

import com.example.deallionaires.model.AddReviewRating

interface SearchBusinessDetailsPresenter {

    fun getSearchBusinessDetailsResponse(userTokenKey: String, merchantId: Int)

    fun addDealToFavourites(userTokenKey: String, merchantid: Int)

    fun removeDealFromFavourites(userTokenKey: String, merchantid: Int)

    fun addSearchBusinessReviewTaring(userTokenKey: String, addReviewRating: AddReviewRating)

    fun searchBusinessDetailsDestroy()
    fun getDealseByFilter(userKey: String,merchantId:Int, i: Int)

}