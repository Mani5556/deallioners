package com.example.deallionaires.ui.notification.merchant

import com.example.deallionaires.model.MerchantNotificationModel
import com.example.deallionaires.model.NotificationModel
import com.example.deallionaires.model.Notimerchant

interface MerchantNotificationView {

    fun onGetMerchantNotificationSuccess(categories: ArrayList<Notimerchant>)

    fun onGetMerchantNotificationFail(message: String)

    fun onGetMerchantNotificationNoInternet()

    fun showMerchantNotificationFragmentProgress()

    fun hideMerchantNotificationFragmentProgress()
}