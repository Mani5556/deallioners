package com.example.deallionaires.ui.searchBusiness

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ethanhua.skeleton.Skeleton
import com.ethanhua.skeleton.SkeletonScreen
import com.example.deallionaires.R
import com.example.deallionaires.adapters.FilterAdapter
import com.example.deallionaires.adapters.SearchBusinessItemsAdapterWithLoadMore
import com.example.deallionaires.adapters.SearchDealsCategoriesAdapter
import com.example.deallionaires.adapters.SearchDealsSubCategoriesAdapter
import com.example.deallionaires.model.*
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import com.google.android.material.snackbar.Snackbar
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.fragment_search_business.*
import kotlinx.android.synthetic.main.fragment_search_business.iv_filter
import kotlinx.android.synthetic.main.fragment_search_business.rvCategories
import kotlinx.android.synthetic.main.fragment_search_business.rvHomeDeals
import kotlinx.android.synthetic.main.fragment_search_business.tvSearchDealsSortBy
import kotlinx.android.synthetic.main.fragment_search_business.tv_search
import kotlinx.android.synthetic.main.fragment_search_deals.*
import kotlinx.android.synthetic.main.subcategories_list.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast


/**
 * A simple [Fragment] subclass.
 */
class SearchBusinessFragment : Fragment(), SearchBusinessView {

    private var filterPosition: Int = -1
    private var pageNumber: Int = 1
    private var canLoadMore: Boolean = false
    private var locationUpdateReceiver: BroadcastReceiver? = null
    private lateinit var searchBusinessAdapter: SearchBusinessItemsAdapterWithLoadMore
    private var searchbusiness: ArrayList<Searchbusiness?> = ArrayList()
    private var skeletonScreen: SkeletonScreen? = null
    private var selectedCatogoryPostion: Int = 0
    lateinit var activity: Activity

    private lateinit var categoryName: String
    private lateinit var categoryId: String
    private var subCategoryId: String = "0"
    private var subCategoryName: String = "All"

    private lateinit var userKey: String

    var isFromOnPause = false
    var selectedCategoryView: View? = null;
    var subcategoriesBuilder: SimpleTooltip? = null
    var filterBuilder: SimpleTooltip? = null

    private lateinit var searchBusinessPresenter: SearchBusinessPresenter

    private var isUserOpenForFirstTime = true
    private var searchBusinessCategoriesAll = ArrayList<SearchUserCategory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_business, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        selectedCatogoryPostion = arguments?.getInt(Constants.SELECTED_CATEGORY_POSITION) ?: 0

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        userKey = userTokenKey

        searchBusinessPresenter = SearchBusinessPresenterImpl(activity, this)
        searchBusinessPresenter.getSearchBusinessCategories(userTokenKey)
//        LoadingDialog.getInstance().show(activity)

//        activity.tvSearchBusinessSortBy.text = "Sort By"

        activity.iv_filter.setOnClickListener {

            showFilterPopup(activity.findViewById(R.id.iv_filter))
        }


        activity.rvHomeDeals.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        searchBusinessAdapter = SearchBusinessItemsAdapterWithLoadMore(
            activity,
            searchbusiness as ArrayList<Searchbusiness?>
        ) { searchbusinessFromAdapter: Searchbusiness, isFavouriteClicked: Boolean, isLiked: Int, position: Int ->
            if (isFavouriteClicked) {
                if (isLiked == 1) {
                    searchBusinessPresenter.removeDealFromFavourites(
                        userKey,
                        searchbusinessFromAdapter.merchantid,
                        position
                    )
                } else {
                    searchBusinessPresenter.addDealToFavourites(
                        userKey,
                        searchbusinessFromAdapter.merchantid,
                        position
                    )
                }
            } else {
                activity.startActivity(activity.intentFor<SearchBusinessDetailsActivity>(Constants.MERCHANT_ID to searchbusinessFromAdapter.merchantid))
            }
        }

        activity.rvHomeDeals.adapter = searchBusinessAdapter

        skeletonScreen = Skeleton.bind(activity.rvHomeDeals)
            .adapter(searchBusinessAdapter)
            .load(R.layout.favorites_sceleton)
            .color(R.color.shimmer_bg)
            .duration(1500)
            .show()

        tv_search.setOnClickListener {
            showSubCategoryPopup(selectedCatogoryPostion, tv_search)
        }


        val mLayoutManager = activity.rvHomeDeals.layoutManager as LinearLayoutManager

        activity.rvHomeDeals.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { //check for scroll down
                    val visibleItemCount = mLayoutManager.getChildCount();
                    val totalItemCount = mLayoutManager.getItemCount();
                    val pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    Log.v("...", "Last Item Wow !")
                    if (canLoadMore) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            canLoadMore = false
                            searchBusinessAdapter.addLoadMore()

                            if (filterPosition != -1) {
                                loadFilterData(filterPosition, ++pageNumber)
                            } else {
                                searchBusinessPresenter.loadMoreBusinesses(
                                    userKey,
                                    categoryId,
                                    subCategoryId,
                                    ++pageNumber
                                )

                            }
                            Log.v("...", "Last Item Wow !")
                            // Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

//        activity.lyt_nested_scroll.setOnScrollChangeListener(object: NestedScrollView.OnScrollChangeListener{
//            override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
//                Log.v("...", "Last Item Wow !")
//                if (v?.getChildAt(v.childCount - 1) != null) {
//                    if (scrollY >= v.getChildAt(v.childCount - 1).measuredHeight - v.measuredHeight && scrollY > oldScrollY) {
//                        //code to fetch more data for endless scrolling
//
//                        if (canLoadMore) {
//                            canLoadMore = false
//                            searchBusinessAdapter.addLoadMore()
//                            searchBusinessPresenter.loadMoreBusinesses(userKey, categoryId, subCategoryId, ++pageNumber)
//                            Log.v("...", "Last Item Wow !")
//                            // Do pagination.. i.e. fetch new data
//                        }
//                    }
//                }
//            }
//        })


        locationUpdateReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                categoryId?.let {
                    getFreshData()
                }
            }
        }
    }

    override fun onGetSearchBusinessCategoriesSuccess(searchUserCategoriesResult: ArrayList<SearchUserCategory>) {
        searchBusinessCategoriesAll = searchUserCategoriesResult
        activity.rvCategories.layoutManager =
            LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        val categories = ArrayList<Categories>()
        for (i in 0 until searchUserCategoriesResult.size) {
            categories.add(
                Categories(
                    searchUserCategoriesResult[i].id.toString(),
                    searchUserCategoriesResult[i].categoryName,
                    searchUserCategoriesResult[i].slug,
                    searchUserCategoriesResult[i].bannerImage
                )
            )
        }

        activity.rvCategories.adapter = SearchDealsCategoriesAdapter(activity, categories) {
            selectedCatogoryPostion = it
            this@SearchBusinessFragment.selectedCategoryView =
                activity.rvCategories.findViewHolderForAdapterPosition(it)?.itemView

            categoryName = searchUserCategoriesResult[it].slug
            categoryId = searchUserCategoriesResult[it].id
            subCategoryId = "0"
            activity?.tv_search.text =
                Html.fromHtml("$categoryName ${resources.getString(R.string.down_arrow)} : <font color = '#f7bc00'> All</font>")
            activity.tvSearchDealsSortBy.visibility = View.GONE
            searchBusinessPresenter.getSearchBusinessSubCategories(userKey, it)
            getFreshData()

        }

        activity?.rvCategories.scrollToPosition(selectedCatogoryPostion)

        activity?.rvCategories?.viewTreeObserver?.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                activity?.rvCategories?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                activity?.rvCategories?.findViewHolderForAdapterPosition(selectedCatogoryPostion)?.itemView?.performClick()
                this@SearchBusinessFragment.selectedCategoryView =
                    activity.rvCategories.findViewHolderForAdapterPosition(selectedCatogoryPostion)?.itemView
            }
        })


        if (isUserOpenForFirstTime) {
            getSearchBusinessSubCategories(0)
            categoryName = searchUserCategoriesResult[0].slug
            categoryId = searchUserCategoriesResult[0].id
            subCategoryId = "0"
            pageNumber = 1
            activity?.tv_search.text =
                Html.fromHtml("$categoryName ${resources.getString(R.string.down_arrow)}: <font color = '#f7bc00'> $subCategoryName</font>")

            getFreshData()
            isUserOpenForFirstTime = false
        }

    }

    private fun getSearchBusinessSubCategories(position: Int) {
//        LoadingDialog.getInstance().cancel()
        selectedCategoryView?.let {
//            showSubCategoryPopup(position,selectedCategoryView!!)
        }
    }

    private fun showSubCategoryPopup(position: Int, anchorView: View) {

        if (subcategoriesBuilder?.isShowing ?: false) {
            subcategoriesBuilder?.dismiss()
            return
        }
        if (!isUserOpenForFirstTime) {
            subCategoryName =
                searchBusinessCategoriesAll[position].searchUserSubcategories[0].subcategoryName
            subCategoryId = searchBusinessCategoriesAll[position].searchUserSubcategories[0].id

            val dropDownRecyclerView: View =
                layoutInflater.inflate(R.layout.subcategories_list, null, false)
            subcategoriesBuilder = SimpleTooltip.Builder(context)
                .anchorView(anchorView)
                .contentView(dropDownRecyclerView, -1)
                .text("Texto do Tooltip")
                .gravity(Gravity.BOTTOM)
                .focusable(false)
                .dismissOnInsideTouch(false)
                .margin(-10f)
                .ignoreOverlay(true)
                .arrowColor(resources.getColor(R.color.colorPrimary))
                .transparentOverlay(false)
                .build()
            dropDownRecyclerView.rv_subcategories.run {
                layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                adapter = SearchDealsSubCategoriesAdapter(
                    activity,
                    searchBusinessCategoriesAll[position].searchUserSubcategories as ArrayList<SearchUserSubcategory>
                ) {
                    subCategoryName = it.subcategoryName
                    subCategoryId = it.id

                    getFreshData()

                    subcategoriesBuilder?.dismiss()

                    activity?.tv_search.text =
                        Html.fromHtml("$categoryName ${context.getString(R.string.down_arrow)}: <font color = '#f7bc00'> $subCategoryName</font>")
                    activity.tvSearchDealsSortBy.visibility = View.GONE
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dropDownRecyclerView.elevation = 10f
            }
            subcategoriesBuilder?.show()

        }

    }

    private fun showFilterPopup(anchorView: View) {

        if (filterBuilder?.isShowing ?: false) {
            filterBuilder?.dismiss()
            return
        }
        val filterList = arrayListOf<String>("Followers", "Rating", "Distance")
        val dropDownRecyclerView: View =
            layoutInflater.inflate(R.layout.subcategories_list, null, false)
        dropDownRecyclerView.lyt_top.visibility = View.VISIBLE

        filterBuilder = SimpleTooltip.Builder(context)
            .anchorView(anchorView)
            .margin(10f)
            .contentView(dropDownRecyclerView, -1)
            .text("Texto do Tooltip")
            .gravity(Gravity.BOTTOM)
            .focusable(false)
            .dismissOnInsideTouch(false)
            .margin(-10f)
            .ignoreOverlay(true)
            .arrowColor(resources.getColor(R.color.colorPrimary))
            .transparentOverlay(false)
            .build()

        dropDownRecyclerView.rv_subcategories.run {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

            dropDownRecyclerView.rv_subcategories.adapter =
                FilterAdapter(activity, filterList) { position ->
                    filterBuilder?.dismiss()
                    activity.tvSearchDealsSortBy.visibility = View.VISIBLE
                    clearOldData()
                    filterPosition = position
                    loadFilterData(filterPosition, pageNumber)
                }


        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dropDownRecyclerView.elevation = 10f
        }

        try {
            filterBuilder?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //this method loads the filtered data for both first page and remaining all pages
    private fun loadFilterData(filterPosition: Int, pageNumber: Int) {
        try {
            val userLocationLatitude by PreferenceExt.appPreference(
                activity,
                Constants.USER_LOCATION_LATITUDE,
                Constants.DEFAULT_STRING
            )

            val userLocationLongitude by PreferenceExt.appPreference(
                activity,
                Constants.USER_LOCATION_LONGITUDE,
                Constants.DEFAULT_STRING
            )

            var filterKey = "followers"
            when (filterPosition) {
                0 -> {
                    filterKey = "followers"
                    activity.tvSearchDealsSortBy.text = "Sort By : Followers"
                }
                1 -> {
                    filterKey = "high_rating"
                    activity.tvSearchDealsSortBy.text = "Sort By : Rating"

                }
                2 -> {
                    filterKey = "distance"
                    activity.tvSearchDealsSortBy.text = "Sort By : Distance"

                }
            }

            if (pageNumber == 1) {
                searchBusinessPresenter.getSearchBusinessCategorySubCategoryItemsSortBy(
                    userKey,
                    categoryId,
                    subCategoryId,
                    filterKey,
                    pageNumber
                )
            } else {
                searchBusinessPresenter.getSearchBusinessCategorySubCategoryItemsSortByLoadMore(
                    userKey,
                    categoryId,
                    subCategoryId,
                    filterKey,
                    pageNumber
                )

            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onGetSearchBusinessCategoriesFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(message)
//        activity.toast(message)
    }

    override fun onGetSearchBusinessCategoriesNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onGetSearchBusinessSubCategoriesSuccess(position: Int) {
        getSearchBusinessSubCategories(position)
    }

    override fun onGetSearchBusinessSubCategoriesFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(message)
//        activity.toast(message)
    }

    override fun onGetSearchBusinessSubCategoriesNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onGetSearchBusinessItemsSuccess(searchbusiness: List<Searchbusiness?>) {
        if (searchbusiness.isEmpty()) {
//            activity.tvNoUserDealsFound?.visibility = View.VISIBLE
//            activity.rvHomeDeals.visibility = View.GONE
            showError("No business found")
            canLoadMore = false
        } else {
//            activity.tvNoUserDealsFound?.visibility = View.GONE
//            activity.rvHomeDeals.visibility = View.VISIBLE

            showData()
            this.searchbusiness = searchbusiness as ArrayList<Searchbusiness?>

            if (searchbusiness.size < 10) {
                canLoadMore = false
            } else {
                canLoadMore = true
            }
            searchBusinessAdapter.updateData(this.searchbusiness)
        }
        hideShimmer()
    }

    override fun onGetSearchBusinessItemsFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(message)
//        activity.toast(message)
    }

    override fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded,postion:Int) {
        activity.toast(favouriteAdded.msg.toString())
        try {
            searchbusiness[postion]?.isLiked = 1
            searchBusinessAdapter.notifyItemChanged(postion)
        }catch (e:java.lang.Exception){
            e.printStackTrace()
        }
    }

    override fun onAddingDealToFavouritesFail(message: String,postion:Int) {
        activity.toast(message)
    }

    override fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved,postion:Int) {
        activity.toast(favouriteRemoved.msg.toString())
//        searchBusinessPresenter.getSearchBusinessCategorySubCategoryItems(
//            userKey,
//            categoryId,
//            subCategoryId
//        )

       try {
           searchbusiness[postion]?.isLiked = 0
           searchBusinessAdapter.notifyItemChanged(postion)
       }catch (e:java.lang.Exception){
           e.printStackTrace()
       }

    }

    override fun onRemovingDealToFavouritesFail(message: String,postion:Int) {
        showError(message)
//        activity.toast(message)
    }

    override fun onGetHomeCategoryItemsFail(message: String) {
//        activity.toast(message)
        showError(message)
    }

    override fun onGetSearchBusinessNoInternet() {
//        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
        showError(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun showShimmer() {
        activity.rvHomeDeals.visibility = View.VISIBLE
        lyt_no_search_business.visibility = View.GONE
        skeletonScreen?.show()
    }

    override fun hideShimmer() {
        skeletonScreen?.hide()
    }

    override fun onGetLoadMoreDataSuccess(searchBusinessList: List<Searchbusiness>) {
        searchBusinessAdapter.removeLoadMore()

        if (searchBusinessList != null && searchBusinessList.size > 0) {
            this.searchbusiness.addAll(searchBusinessList)
            searchBusinessAdapter.updateData(this.searchbusiness)

            if (searchBusinessList.size < 10) {
                canLoadMore = false
            } else {
                canLoadMore = true
            }

        } else {
            canLoadMore = false
        }
    }

    override fun onGetLoadMoreFail(message: String) {
        searchBusinessAdapter.removeLoadMore()
        canLoadMore = true
        Snackbar.make(activity.main_view, message, Snackbar.LENGTH_LONG).show()

    }


    private fun showError(message: String) {
        lyt_no_search_business.visibility = View.VISIBLE
        rvHomeDeals.visibility = View.GONE
        activity.findViewById<TextView>(R.id.tv_no_data_found).text = message
        skeletonScreen?.hide()
    }

    private fun showData() {
        skeletonScreen?.hide()
        lyt_no_search_business.visibility = View.GONE
        rvHomeDeals.visibility = View.VISIBLE
    }


    override fun onResume() {
        super.onResume()
        if (isFromOnPause) {
            searchBusinessPresenter = SearchBusinessPresenterImpl(activity, this)
            getFreshData()
            isFromOnPause = false
        }
    }

    override fun onPause() {
        super.onPause()
        isFromOnPause = true
    }

    override fun onDetach() {
        super.onDetach()
        subcategoriesBuilder?.dismiss()
        filterBuilder?.dismiss()
        searchBusinessPresenter.onDetach()
        locationUpdateReceiver?.let {
            LocalBroadcastManager.getInstance(context!!).unregisterReceiver(it)
        }
    }


    fun getFreshData() {
        clearOldData()
        searchBusinessPresenter.getSearchBusinessCategorySubCategoryItems(
            userKey,
            categoryId,
            subCategoryId
        )
    }


    fun clearOldData() {
        searchbusiness.clear()
        pageNumber = 1
        filterPosition = -1
        canLoadMore = false
    }

}
