package com.example.deallionaires.ui.notification.merchant

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.MerchantNotificationsAdapter
import com.example.deallionaires.model.Notimerchant
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class MerchantNotificationsFragment : Fragment(), MerchantNotificationView {
    private lateinit var merchantNotificationPresenter: MerchantNotificationPresenter
    private lateinit var rvMerchantNotification: RecyclerView
    private lateinit var tvMerchantNoNotification: TextView
    private lateinit var activity: Activity
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_merchant_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity = getActivity()!!
        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        rvMerchantNotification = view.findViewById(R.id.rvMerchantNotifications)
        tvMerchantNoNotification = view.findViewById(R.id.tvMerchantNoNotifications)

        merchantNotificationPresenter = MerchantNotificationPresenter(activity, this)

        if (isNetworkAvailable(activity)) {
            merchantNotificationPresenter.getMerchantNotification(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onGetMerchantNotificationSuccess(categories: ArrayList<Notimerchant>) {
        if (categories.isEmpty()) {
            tvMerchantNoNotification.visibility = View.VISIBLE
            rvMerchantNotification.visibility = View.GONE
        } else {
            tvMerchantNoNotification.visibility = View.GONE
            rvMerchantNotification.visibility = View.VISIBLE
            rvMerchantNotification.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            rvMerchantNotification.adapter =
                MerchantNotificationsAdapter(requireContext(), categories) {}
        }
        LoadingDialog.getInstance().cancel()
    }

    override fun onGetMerchantNotificationFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGetMerchantNotificationNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun showMerchantNotificationFragmentProgress() {
        LoadingDialog.getInstance().show(activity)
    }

    override fun hideMerchantNotificationFragmentProgress() {
        LoadingDialog.getInstance().cancel()
    }

}
