package com.example.deallionaires.ui.home.fragments.home.homeCategoryDeals

import android.content.Context
import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.SearchBusinessCategoryResponse
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.APIRequests.getSearchBusinessCategoryResponse
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class HomeCategoryPresenter(private val context: Context, private val homeCategoryView: HomeCategoryView?) {

    fun getHomeCategoryDealItems(userTokenKey: String, categoryName: String){
        if(isNetworkAvailable(context)){
            getSearchBusinessCategoryResponse(userTokenKey, categoryName, object :
                NWResponseCallback<SearchBusinessCategoryResponse> {
                override fun onSuccess(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    homeCategoryView!!.onGetHomeCategoryItemsSuccess(response.body()!!.searchbusiness)
                }

                override fun onResponseBodyNull(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    homeCategoryView!!.onGetHomeCategoryItemsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    homeCategoryView!!.onGetHomeCategoryItemsFail(response.body().toString())
                }

                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    homeCategoryView!!.onGetHomeCategoryItemsFail(t.message!!)
                }

            })
        }else {
            homeCategoryView!!.onGetHomeCategoryNoInternet()
        }
    }

    fun addDealToFavourites(userTokenKey: String, merchantid: Int){
        if(isNetworkAvailable(context)){
            APIRequests.getAddDealToFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteAdded?> {
                    override fun onSuccess(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        homeCategoryView!!.onAddingDealToFavouritesSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        homeCategoryView!!.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        homeCategoryView!!.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteAdded?>, t: Throwable) {
                        homeCategoryView!!.onAddingDealToFavouritesFail(t.message!!)
                    }
                })
        }else{
            homeCategoryView!!.onGetHomeCategoryNoInternet()
        }
    }

    fun removeDealFromFavourites(userTokenKey: String, merchantid: Int){
        if(isNetworkAvailable(context)){
            APIRequests.getRemoveDealFromFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteRemoved?> {
                    override fun onSuccess(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        homeCategoryView!!.onRemovingDealToFavouritesSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        homeCategoryView!!.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        homeCategoryView!!.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                        homeCategoryView!!.onRemovingDealToFavouritesFail(t.message!!)
                    }

                })
        }else {
            homeCategoryView!!.onGetHomeCategoryNoInternet()
        }
    }

}