package com.example.deallionaires.ui.searchDealItem

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.Dealsdetail
import com.example.deallionaires.model.RedeemDealResponse
import com.example.deallionaires.model.RedeemDeal
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.home.fragments.home.HomeFragment
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.utils.*
import kotlinx.android.synthetic.main.content_search_deals_item.*
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class SearchDealItemFragment : Fragment(), SearchDealDetailsView {

    private lateinit var dealsDetailResponse: Dealsdetail
    private lateinit var tvDealDetailDesc: TextView
    private lateinit var tvDealDetailTnC: TextView
    private lateinit var tvDealDetailDescReadMore: TextView
    private lateinit var tvDealDetailTnCReadMore: TextView
    private lateinit var tvDealDetailClaimDeal: TextView
    private lateinit var ivDealImage: ImageView
    private lateinit var tvRemaining: TextView
    private lateinit var tvRedeemed: TextView
    private lateinit var tvDealDate: TextView
    private lateinit var tvDealDetailTitle: TextView
    private lateinit var tvDealDetailStoreTitle: TextView
    private lateinit var tvDealDetailStoreAddress: TextView
    private lateinit var tvDealDetailStoreRewardPoints: TextView
    private lateinit var tvDealDetailOfferCategory: TextView
    private lateinit var tvDealDetailOfferType: TextView
    private lateinit var tvDealDetailPricePercent: TextView
    private lateinit var tvDealDetailDealPrice: TextView
    private lateinit var tvDealDetailActualPrice: TextView
    private lateinit var tvNoDealDetailFoundFragment: TextView
    private lateinit var svDealDetailFragment: ScrollView

    private var textDescriptionIsExpanded = false
    private var textTermsAndConditionsIsExpanded = false

    private lateinit var activity: HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search_deal_item, container, false)
        tvDealDetailDesc = view.findViewById(R.id.tvDealDetailDesc)
        tvDealDetailTnC = view.findViewById(R.id.tvDealDetailTnC)
        tvDealDetailDescReadMore = view.findViewById(R.id.tvDealDetailDescReadMore)
        tvDealDetailTnCReadMore = view.findViewById(R.id.tvDealDetailTnCReadMore)
        tvDealDetailClaimDeal = view.findViewById(R.id.tvDealDetailClaimDeal)
        ivDealImage = view.findViewById(R.id.ivDealImage)
        tvRemaining = view.findViewById(R.id.tvRemaining)
        tvRedeemed = view.findViewById(R.id.tvRedeemed)
        tvDealDate = view.findViewById(R.id.tvDealDate)
        tvDealDetailTitle = view.findViewById(R.id.tvDealDetailTitle)
        tvDealDetailStoreTitle = view.findViewById(R.id.tvDealDetailStoreTitle)
        tvDealDetailStoreAddress = view.findViewById(R.id.tvDealDetailStoreAddress)
        tvDealDetailStoreRewardPoints = view.findViewById(R.id.tvDealDetailStoreRewardPoints)
        tvDealDetailOfferCategory = view.findViewById(R.id.tvDealDetailOfferCategory)
        tvDealDetailOfferType = view.findViewById(R.id.tvDealDetailOfferType)
        tvDealDetailPricePercent = view.findViewById(R.id.tvDealDetailPricePercent)
        tvDealDetailDealPrice = view.findViewById(R.id.tvDealDetailDealPrice)
        tvDealDetailActualPrice = view.findViewById(R.id.tvDealDetailActualPrice)
        tvNoDealDetailFoundFragment = view.findViewById(R.id.tvNoDealDetailFoundFragment)
        svDealDetailFragment = view.findViewById(R.id.svDealDetailFragment)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = requireActivity() as HomeActivity

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        val userSkipLogin by PreferenceExt.appPreference(
            activity,
            Constants.USER_SKIP_LOGIN,
            false
        )

        val searchDealDetailsPresenter =
            SearchDealDetailsPresenterImpl(activity, this)
        val homeDealId by PreferenceExt.appPreference(
            activity,
            Constants.HOME_DEAL_ID,
            0
        )
        if (isNetworkAvailable(activity)) {
            searchDealDetailsPresenter.getDealDetailsResponse(userTokenKey, homeDealId)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
        }

        if (userSkipLogin) {
            tvDealDetailClaimDeal.visibility = View.GONE
        } else {
            tvDealDetailClaimDeal.visibility = View.VISIBLE
        }

        tvDealDetailClaimDeal.setOnClickListener {
            if (userSkipLogin) {
//                toast(getString(R.string.txt_sign_in_to_claim_deal))
                displayLoginDialog()
            } else {
                CustomDialog(activity).showDialog(
                    "Do You Want to Claim The Deal?",
                    "Claim",
                    "Not Now"
                ) {
                    val redeemDeal = RedeemDeal(
                        "" + dealsDetailResponse.merchantid,
                        "" + homeDealId,
                        "" + dealsDetailResponse.offerTypeId,
                        "" + dealsDetailResponse.offerCategoryId,
                        dealsDetailResponse.loyaltyPoints,
                        dealsDetailResponse.regularPrice.toInt(),
                        dealsDetailResponse.offerPrice.toInt(),
                        dealsDetailResponse.discount.toInt(),
                        dealsDetailResponse.customerBaseOfferTypeId
                    )
                    searchDealDetailsPresenter.postRedeemDeal(userTokenKey, redeemDeal)
                    LoadingDialog.getInstance().show(activity)
                }
            }
        }

        tvDealDetailDescReadMore.setOnClickListener {
            if (!textDescriptionIsExpanded) {
                tvDealDetailDesc.maxLines = Int.MAX_VALUE
                tvDealDetailDescReadMore.text = "Read Less"
                textDescriptionIsExpanded = true
            } else {
                tvDealDetailDesc.maxLines = 1
                tvDealDetailDescReadMore.text = "Read More"
                textDescriptionIsExpanded = false
            }
        }

        tvDealDetailTnCReadMore.setOnClickListener {
            if (!textTermsAndConditionsIsExpanded) {
                tvDealDetailTnC.maxLines = Int.MAX_VALUE
                tvDealDetailTnCReadMore.text = "Read Less"
                textTermsAndConditionsIsExpanded = true
            } else {
                tvDealDetailTnC.maxLines = 1
                tvDealDetailTnCReadMore.text = "Read More"
                textTermsAndConditionsIsExpanded = false
            }
        }
    }

    private fun displayLoginDialog() {
        CustomDialog(activity).showDialog(
            "Login to claim Deal",
            "Login",
            "Not Now"
        ) {
            val intent = Intent(activity, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            activity.startActivity(intent)
        }
    }

    override fun onGetDealDealsSuccess(dealsdetail: Dealsdetail?) {
        if (dealsdetail != null) {
            tvNoDealDetailFoundFragment.visibility = View.GONE
            svDealDetailFragment.visibility = View.VISIBLE
            dealsDetailResponse = dealsdetail
            Glide.with(activity)
                .load(dealsdetail.dealImage)
                .error(R.drawable.deallionaires_logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivDealImage)
            tvRemaining.text = "Remaining : ${dealsdetail.remaining}"
            tvRedeemed.text = "Redeemed : ${dealsdetail.claimed}"
            if(dealsdetail.expiryDate != null){
                val separated = dealsdetail.expiryDate!!.split("T")
                tvDealDate.text = separated[0]
            }
            tvDealDetailTitle.text = dealsdetail.title
            tvDealDetailStoreTitle.text = dealsdetail.businessName
            tvDealDetailStoreAddress.text = dealsdetail.address
            tvDealDetailStoreRewardPoints.text = "Reward Points ${dealsdetail.rewardPoints}"
            tvDealDetailOfferCategory.text = "Offer Category:- ${dealsdetail.offerCategoryId}"
            tvDealDetailOfferType.text = "Offer Type:- ${dealsdetail.offerTypeId}"
            tvDealDetailDesc.text = dealsdetail.description
            tvDealDetailTnC.text = dealsdetail.termsAndConditions
            tvDealDetailDealPrice.text = "${dealsdetail.offerPrice}$"
            tvDealDetailActualPrice.paintFlags =
                tvDealDetailActualPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            tvDealDetailActualPrice.text = "${dealsdetail.regularPrice}$"
            tvDealDetailPricePercent.text = "${"Percent: " + dealsdetail.discount}$"

            if (dealsdetail.userClub == dealsdetail.dealClub) {
                tvDealDetailClaimDeal.visibility = View.VISIBLE
            } else {
                tvDealDetailClaimDeal.visibility = View.GONE
            }
            getTextEllipsize()
        }else{
            tvNoDealDetailFoundFragment.visibility = View.VISIBLE
            svDealDetailFragment.visibility = View.GONE
        }
        LoadingDialog.getInstance().cancel()
    }

    private fun getTextEllipsize() {
        val vtoDescription: ViewTreeObserver = tvDealDetailDesc.viewTreeObserver
        vtoDescription.addOnGlobalLayoutListener {
            val lDescription: Layout = tvDealDetailDesc.layout
            val lines = lDescription.lineCount
            if (lines > 0) {
                if (lDescription.getEllipsisCount(lines - 1) > 0) {
                    tvDealDetailDescReadMore.visibility = View.VISIBLE
                }
            }
        }

        val vtoTermsAndCondition: ViewTreeObserver = tvDealDetailTnC.viewTreeObserver
        vtoTermsAndCondition.addOnGlobalLayoutListener {
            val lTermsAndCondition: Layout = tvDealDetailTnC.layout
            val lines = lTermsAndCondition.lineCount
            if (lines > 0) {
                if (lTermsAndCondition.getEllipsisCount(lines - 1) > 0) {
                    tvDealDetailTnCReadMore.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onGetDealDetailsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGetDealDetailsNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onPostRedeemDealSuccess(redeemDealResponse: RedeemDealResponse) {
        LoadingDialog.getInstance().cancel()
        activity.toast(redeemDealResponse.msg)
        activity.loadFragmentInHomeActivity(
            "Home",
            HomeFragment(),
            true,
            Constants.FROM_DEAL_ITEM_FRAGMENT
        )
    }

    override fun onPostRedeemDealFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }
}
