package com.example.deallionaires.ui.searchBusinessItem

import com.example.deallionaires.model.*

interface SearchBusinessDetailsView {

    fun onGetSearchBusinessDetailsSuccess(searchBusinessItemDetail: SearchBusinessItemDetail)

    fun onGetSearchBusinessDetailsFail(message: String)

    fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded)

    fun onAddingDealToFavouritesFail(message: String)

    fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved)

    fun onRemovingDealToFavouritesFail(message: String)

    fun onAddingSearchBusinessReviewSuccess(reviewAdded: ReviewAdded)

    fun onAddingSearchBusinessReviewFail(message: String)

    fun onGetSearchBusinessDetailsNoInternet()

    fun filterResultsSuccess(filteredList:List<SearchDealsCategoryItems>?)
    fun filterResultsFail(message: String)
}