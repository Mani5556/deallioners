package com.example.deallionaires.ui.searchBusinessItem

import android.content.Context
import android.content.LocusId
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests.getAddBusinessReviewRating
import com.example.deallionaires.network.APIRequests.getAddDealToFavourites
import com.example.deallionaires.network.APIRequests.getFilteredDealsList
import com.example.deallionaires.network.APIRequests.getRemoveDealFromFavourites
import com.example.deallionaires.network.APIRequests.getSearchBusinessDetailResponse
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SearchBusinessDetailsPresenterImpl(
    private val context: Context,
    private var searchBusinessDetailsView: SearchBusinessDetailsView?
) : SearchBusinessDetailsPresenter {
    override fun getSearchBusinessDetailsResponse(userTokenKey: String, merchantId: Int) {
        if (isNetworkAvailable(context)) {
            getSearchBusinessDetailResponse(
                userTokenKey,
                merchantId,
                object : NWResponseCallback<SearchBusinessItemDetail> {
                    override fun onSuccess(
                        call: Call<SearchBusinessItemDetail>,
                        response: Response<SearchBusinessItemDetail>
                    ) {
                        searchBusinessDetailsView!!.onGetSearchBusinessDetailsSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<SearchBusinessItemDetail>,
                        response: Response<SearchBusinessItemDetail>
                    ) {
                        searchBusinessDetailsView!!.onGetSearchBusinessDetailsFail(
                            response.body().toString()
                        )
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<SearchBusinessItemDetail>,
                        response: Response<SearchBusinessItemDetail>
                    ) {
                        searchBusinessDetailsView!!.onGetSearchBusinessDetailsFail(
                            response.body().toString()
                        )
                    }

                    override fun onFailure(call: Call<SearchBusinessItemDetail>, t: Throwable) {
                        searchBusinessDetailsView!!.onGetSearchBusinessDetailsFail(t.message!!)
                    }

                })
        } else {
            searchBusinessDetailsView?.onGetSearchBusinessDetailsNoInternet()
        }
    }

    override fun addDealToFavourites(userTokenKey: String, merchantid: Int) {
        if (isNetworkAvailable(context)) {
            getAddDealToFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteAdded?> {
                    override fun onSuccess(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        searchBusinessDetailsView!!.onAddingDealToFavouritesSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        searchBusinessDetailsView!!.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        searchBusinessDetailsView!!.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteAdded?>, t: Throwable) {
                        searchBusinessDetailsView!!.onAddingDealToFavouritesFail(t.message!!)
                    }
                })
        } else {
            searchBusinessDetailsView!!.onGetSearchBusinessDetailsNoInternet()
        }
    }

    override fun removeDealFromFavourites(userTokenKey: String, merchantid: Int) {
        if (isNetworkAvailable(context)) {
            getRemoveDealFromFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteRemoved?> {
                    override fun onSuccess(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        searchBusinessDetailsView!!.onRemovingDealToFavouritesSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        searchBusinessDetailsView!!.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        searchBusinessDetailsView!!.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                        searchBusinessDetailsView!!.onRemovingDealToFavouritesFail(t.message!!)
                    }

                })
        } else {
            searchBusinessDetailsView!!.onGetSearchBusinessDetailsNoInternet()
        }
    }

    override fun addSearchBusinessReviewTaring(
        userTokenKey: String,
        addReviewRating: AddReviewRating
    ) {
        if (isNetworkAvailable(context)) {
            getAddBusinessReviewRating(
                userTokenKey,
                addReviewRating,
                object : NWResponseCallback<ReviewAdded?> {
                    override fun onSuccess(
                        call: Call<ReviewAdded?>,
                        response: Response<ReviewAdded?>
                    ) {
                        searchBusinessDetailsView!!.onAddingSearchBusinessReviewSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<ReviewAdded?>,
                        response: Response<ReviewAdded?>
                    ) {
                        searchBusinessDetailsView!!.onAddingSearchBusinessReviewFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<ReviewAdded?>,
                        response: Response<ReviewAdded?>
                    ) {
                        searchBusinessDetailsView!!.onAddingSearchBusinessReviewFail(response.message())
                    }

                    override fun onFailure(call: Call<ReviewAdded?>, t: Throwable) {
                        searchBusinessDetailsView!!.onAddingSearchBusinessReviewFail(t.message!!)
                    }

                })
        } else {
            searchBusinessDetailsView!!.onGetSearchBusinessDetailsNoInternet()
        }
    }

    override fun searchBusinessDetailsDestroy() {
        searchBusinessDetailsView = null
    }

    override fun getDealseByFilter(userKey: String,marchantId:Int, filterId: Int) {
        LoadingDialog.getInstance().show(context)
            if (isNetworkAvailable(context)) {
                getFilteredDealsList(
                    userKey,
                    marchantId,
                    filterId,
                    object : NWResponseCallback<FilteredDeals?> {
                        override fun onSuccess(
                            call: Call<FilteredDeals?>,
                            response: Response<FilteredDeals?>
                        ) {
                            LoadingDialog.getInstance().cancel()
                            searchBusinessDetailsView!!.filterResultsSuccess(response.body()?.msg?.viewdeals)
                        }

                        override fun onResponseBodyNull(
                            call: Call<FilteredDeals?>,
                            response: Response<FilteredDeals?>
                        ) {
                            LoadingDialog.getInstance().cancel()
                            searchBusinessDetailsView!!.filterResultsFail(response.message())
                        }

                        override fun onResponseUnsuccessful(
                            call: Call<FilteredDeals?>,
                            response: Response<FilteredDeals?>
                        ) {
                            LoadingDialog.getInstance().cancel()
                            searchBusinessDetailsView!!.filterResultsFail(response.message())
                        }

                        override fun onFailure(call: Call<FilteredDeals?>, t: Throwable) {
                            LoadingDialog.getInstance().cancel()
                            searchBusinessDetailsView!!.filterResultsFail(t.message!!)
                        }

                    })
            } else {
                LoadingDialog.getInstance().cancel()
                searchBusinessDetailsView!!.onGetSearchBusinessDetailsNoInternet()
            }
    }

}