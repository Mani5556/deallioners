package com.example.deallionaires.ui.searchBusiness

import android.content.Context
import android.util.Log
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.APIRequests.getSearchBusinessCategoryResponse
import com.example.deallionaires.network.APIRequests.getSearchBusinessCategorySubCategoryItemsWithSortBy
import com.example.deallionaires.network.APIRequests.getSearchBusinessCategorySubCategoryItemsWithSortByDistance
import com.example.deallionaires.network.APIRequests.getSearchBusinessCategorySubCategoryResponse
import com.example.deallionaires.network.APIRequests.getSearchCategoriesWithSubCategories
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SearchBusinessPresenterImpl(private val context: Context, private var searchBusinessView: SearchBusinessView?) : SearchBusinessPresenter{

    private  var loadMoreDataCallBack: NWResponseCallback<SearchBusinessCategoryResponse>? = null
    private  var initialDataCallBack: NWResponseCallback<SearchBusinessCategoryResponse>? = null

    override fun getSearchBusinessCategories(userTokenKey: String) {
        if (isNetworkAvailable(context)) {
            getSearchCategoriesWithSubCategories(
                userTokenKey,
                context,
                object : NWResponseCallback<SearchUserCategories?> {
                    override fun onSuccess(
                        call: Call<SearchUserCategories?>,
                        response: Response<SearchUserCategories?>
                    ) {
                        Log.e("User Deals", response.body().toString())
                        val searchUserCategoriesResult = ArrayList<SearchUserCategory>()
                        for (i in response.body()!!.categories.indices) {
                            searchUserCategoriesResult.add(
                                SearchUserCategory(
                                    response.body()!!.categories[i].id,
                                    response.body()!!.categories[i].categoryName,
                                    response.body()!!.categories[i].slug,
                                    response.body()!!.categories[i].bannerImage,
                                    response.body()!!.categories[i].searchUserSubcategories
                                )
                            )
                        }
                        searchBusinessView!!.onGetSearchBusinessCategoriesSuccess(searchUserCategoriesResult)
                    }

                    override fun onResponseBodyNull(
                        call: Call<SearchUserCategories?>,
                        response: Response<SearchUserCategories?>
                    ) {
                        searchBusinessView?.onGetSearchBusinessCategoriesFail("Some thing went wrong!")
                        Log.e("User Deals null", response.body().toString())
                        searchBusinessView?.hideShimmer()
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<SearchUserCategories?>,
                        response: Response<SearchUserCategories?>
                    ) {
                        searchBusinessView?.onGetSearchBusinessCategoriesFail("Some thing went wrong!")
                        Log.e("User Deals unsu", response.body().toString())
                        searchBusinessView?.hideShimmer()
                    }

                    override fun onFailure(call: Call<SearchUserCategories?>, t: Throwable) {
                        searchBusinessView?.onGetSearchBusinessCategoriesFail("Some thing went wrong!")
                        searchBusinessView?.hideShimmer()
                    }

                })
        } else {
            searchBusinessView!!.onGetSearchBusinessCategoriesNoInternet()
        }
    }

    override fun getSearchBusinessSubCategories(userTokenKey: String, position: Int) {
        searchBusinessView!!.onGetSearchBusinessSubCategoriesSuccess(position)
    }

//    override fun getSearchBusinessCategoryItems(userTokenKey: String, categoryId: String) {
//        searchBusinessView?.showShimmer()
//        if(isNetworkAvailable(context)){
//            getSearchBusinessCategoryResponse(userTokenKey, categoryId, object : NWResponseCallback<SearchBusinessCategoryResponse>{
//                override fun onSuccess(
//                    call: Call<SearchBusinessCategoryResponse>,
//                    response: Response<SearchBusinessCategoryResponse>
//                ) {
//                    searchBusinessView?.onGetSearchBusinessItemsSuccess(response.body()!!.searchbusiness)
//                }
//
//                override fun onResponseBodyNull(
//                    call: Call<SearchBusinessCategoryResponse>,
//                    response: Response<SearchBusinessCategoryResponse>
//                ) {
//                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body().toString())
//                    searchBusinessView?.hideShimmer()
//                }
//
//                override fun onResponseUnsuccessful(
//                    call: Call<SearchBusinessCategoryResponse>,
//                    response: Response<SearchBusinessCategoryResponse>
//                ) {
//                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body().toString())
//                    searchBusinessView?.hideShimmer()
//                }
//
//                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
//                    searchBusinessView?.onGetSearchBusinessItemsFail(t.message!!)
//                    searchBusinessView?.hideShimmer()
//                }
//
//            })
//        }else {
//            searchBusinessView?.onGetSearchBusinessCategoriesNoInternet()
//            searchBusinessView?.hideShimmer()
//        }
//    }

    override fun getSearchBusinessCategorySubCategoryItems(
        userTokenKey: String,
        categoryId: String,
        sub_categoryId: String
    ) {
        searchBusinessView?.showShimmer()
        if(isNetworkAvailable(context)){
            loadMoreDataCallBack?.let {
                loadMoreDataCallBack = null
            }

            initialDataCallBack = object : NWResponseCallback<SearchBusinessCategoryResponse> {
                override fun onSuccess(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsSuccess(response.body()!!.searchbusiness)
                }

                override fun onResponseBodyNull(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body()!!.toString())
                    searchBusinessView?.hideShimmer()
                }

                override fun onResponseUnsuccessful(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(
                        response.body()?.toString() ?: "Some thing went wrong"
                    )
                    searchBusinessView?.hideShimmer()
                }

                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(t.message!!)
                    searchBusinessView?.hideShimmer()
                }
            }
            getSearchBusinessCategorySubCategoryResponse(userTokenKey, categoryId, sub_categoryId,1,initialDataCallBack!!)
        }else {
            searchBusinessView?.onGetSearchBusinessCategoriesNoInternet()
            searchBusinessView?.hideShimmer()
        }
    }



    override fun loadMoreBusinesses(userTokenKey: String, categoryId: String, sub_categoryId: String,pageNumber: Int){
        if(isNetworkAvailable(context)){

            loadMoreDataCallBack = getLoadMoreDataCallBack()
            getSearchBusinessCategorySubCategoryResponse(userTokenKey, categoryId, sub_categoryId,pageNumber,loadMoreDataCallBack!!)
        }else {
            searchBusinessView?.onGetLoadMoreFail("No internet Connection")

        }
    }


    override fun getSearchBusinessCategorySubCategoryItemsSortBy(
        userTokenKey: String,
        categoryName: String,
        sub_category: String,
        sortOptions: String,
        pageNumber: Int
    ) {
        searchBusinessView?.showShimmer()
        if(isNetworkAvailable(context)){
            getSearchBusinessCategorySubCategoryItemsWithSortBy(userTokenKey, categoryName, sub_category, sortOptions,pageNumber,object : NWResponseCallback<SearchBusinessCategoryResponse>{
                override fun onSuccess(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    response.body()!!.searchbusiness?.let {
                        searchBusinessView?.onGetSearchBusinessItemsSuccess(response.body()!!.searchbusiness)
                    }?: kotlin.run {
                        searchBusinessView?.onGetSearchBusinessItemsFail(response.body()!!.msg)
                    }

                }

                override fun onResponseBodyNull(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body()!!.toString())
                    searchBusinessView?.hideShimmer()
                }

                override fun onResponseUnsuccessful(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body()!!.toString())
                    searchBusinessView?.hideShimmer()
                }

                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(t.message!!)
                    searchBusinessView?.hideShimmer()
                }

            })
        }else {
            searchBusinessView?.onGetSearchBusinessCategoriesNoInternet()
            searchBusinessView?.hideShimmer()
        }
    }

    override fun getSearchBusinessCategorySubCategoryItemsSortByLoadMore(
        userTokenKey: String,
        categoryName: String,
        sub_category: String,
        sortOptions: String,
        pageNumber: Int
    ) {
        if(isNetworkAvailable(context)){
            loadMoreDataCallBack = getLoadMoreDataCallBack()
            getSearchBusinessCategorySubCategoryItemsWithSortBy(userTokenKey, categoryName, sub_category, sortOptions,pageNumber,
                loadMoreDataCallBack!!
            )
        }else {
            searchBusinessView?.onGetSearchBusinessCategoriesNoInternet()
            searchBusinessView?.hideShimmer()
        }
    }


    override fun getSearchBusinessCategorySubCategoryItemsSortByDistance(
        userTokenKey: String,
        userlat: Double,
        userlong: Double,
        pageNumber: Int
    ) {
        searchBusinessView?.showShimmer()
        if(isNetworkAvailable(context)){
            getSearchBusinessCategorySubCategoryItemsWithSortByDistance(userTokenKey, userlat, userlong,pageNumber, object : NWResponseCallback<SearchBusinessCategoryResponse>{
                override fun onSuccess(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsSuccess(response.body()!!.searchbusiness)
                }

                override fun onResponseBodyNull(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body()!!.toString())
                    searchBusinessView?.hideShimmer()
                }

                override fun onResponseUnsuccessful(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(response.body()?.toString()?:"Some thing went wrong")
                    searchBusinessView?.hideShimmer()
                }

                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    searchBusinessView?.onGetSearchBusinessItemsFail(t.message!!)
                    searchBusinessView?.hideShimmer()
                }

            })
        }else {
            searchBusinessView?.onGetSearchBusinessCategoriesNoInternet()
            searchBusinessView?.hideShimmer()
        }
    }

    override fun getSearchBusinessCategorySubCategoryItemsSortByDistanceLoadMore(
        userTokenKey: String,
        userlat: Double,
        userlong: Double,
        pageNumber: Int
    ) {
        if(isNetworkAvailable(context)){
            loadMoreDataCallBack = getLoadMoreDataCallBack()
            getSearchBusinessCategorySubCategoryItemsWithSortByDistance(userTokenKey, userlat, userlong,pageNumber,
                loadMoreDataCallBack!!)
        }else {
            searchBusinessView?.onGetSearchBusinessCategoriesNoInternet()
        }
    }

    override fun addDealToFavourites(userTokenKey: String, merchantid: Int,position: Int) {
        if(isNetworkAvailable(context)){
            APIRequests.getAddDealToFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteAdded?> {
                    override fun onSuccess(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        searchBusinessView?.onAddingDealToFavouritesSuccess(response.body()!!,position)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        searchBusinessView?.onAddingDealToFavouritesFail(response.message(),position)
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        searchBusinessView?.onAddingDealToFavouritesFail(response.message(),position)
                    }

                    override fun onFailure(call: Call<FavouriteAdded?>, t: Throwable) {
                        searchBusinessView?.onAddingDealToFavouritesFail(t.message!!,position)
                    }
                })
        }else{
            searchBusinessView?.onGetSearchBusinessNoInternet()
        }
    }

    override fun removeDealFromFavourites(userTokenKey: String, merchantid: Int,position: Int) {
        if(isNetworkAvailable(context)){
            APIRequests.getRemoveDealFromFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteRemoved?> {
                    override fun onSuccess(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        searchBusinessView?.onRemovingDealToFavouritesSuccess(response.body()!!,position)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        searchBusinessView?.onRemovingDealToFavouritesFail(response.message(),position)
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        searchBusinessView?.onRemovingDealToFavouritesFail(response.message(),position)
                    }

                    override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                        searchBusinessView?.onRemovingDealToFavouritesFail(t.message!!,position)
                    }

                })
        }else {
            searchBusinessView?.onGetSearchBusinessNoInternet()
        }
    }

    override fun destroySearchBusiness() {
        searchBusinessView = null
        loadMoreDataCallBack = null
        initialDataCallBack = null
    }

    override fun onDetach() {
        searchBusinessView =  null
        loadMoreDataCallBack = null
        initialDataCallBack = null
    }


    fun getLoadMoreDataCallBack():NWResponseCallback<SearchBusinessCategoryResponse>{
        val callBack = object : NWResponseCallback<SearchBusinessCategoryResponse>{
            override fun onSuccess(
                call: Call<SearchBusinessCategoryResponse>,
                response: Response<SearchBusinessCategoryResponse>
            ) {
                searchBusinessView?.onGetLoadMoreDataSuccess(response.body()!!.searchbusiness)
            }

            override fun onResponseBodyNull(
                call: Call<SearchBusinessCategoryResponse>,
                response: Response<SearchBusinessCategoryResponse>
            ) {
                searchBusinessView?.onGetLoadMoreFail(response.body()!!.toString())

            }

            override fun onResponseUnsuccessful(
                call: Call<SearchBusinessCategoryResponse>,
                response: Response<SearchBusinessCategoryResponse>
            ) {
                searchBusinessView?.onGetLoadMoreFail(response.body()!!.toString())

            }

            override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                searchBusinessView?.onGetLoadMoreFail("Load Again")

            }
        }

        return callBack
    }
}