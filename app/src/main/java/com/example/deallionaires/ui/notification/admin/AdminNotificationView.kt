package com.example.deallionaires.ui.notification.admin

import com.example.deallionaires.model.Notiadmin
import com.example.deallionaires.model.NotificationModel

interface AdminNotificationView {


    fun onGetAdminNotificationSuccess(deals: ArrayList<Notiadmin>)

    fun onGetAdminNotificationFail(message: String)

    fun onGetAdminNotificationNoInternet()

    fun showAdminNotificationFragmentProgress()

    fun hideAdminNotificationFragmentProgress()
}