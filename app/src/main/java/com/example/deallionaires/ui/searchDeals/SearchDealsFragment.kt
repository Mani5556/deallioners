package com.example.deallionaires.ui.searchDeals

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ethanhua.skeleton.RecyclerViewSkeletonScreen
import com.ethanhua.skeleton.Skeleton
import com.example.deallionaires.R
import com.example.deallionaires.adapters.*
import com.example.deallionaires.model.*
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import com.google.android.material.snackbar.Snackbar
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home_deals.*
import kotlinx.android.synthetic.main.fragment_search_business.*
import kotlinx.android.synthetic.main.fragment_search_deals.*
import kotlinx.android.synthetic.main.fragment_search_deals.iv_filter
import kotlinx.android.synthetic.main.fragment_search_deals.rvCategories
import kotlinx.android.synthetic.main.fragment_search_deals.rvHomeDeals
import kotlinx.android.synthetic.main.fragment_search_deals.tvNoUserDealsFound
import kotlinx.android.synthetic.main.fragment_search_deals.tvSearchDealsSortBy
import kotlinx.android.synthetic.main.fragment_search_deals.tv_search
import kotlinx.android.synthetic.main.subcategories_list.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast


/**
 * A simple [Fragment] subclass.
 */
class SearchDealsFragment : Fragment(), SearchDealsView {

    private var filterPosition: Int = -1
    private var pageNumber: Int = 1
    private var canLoadMore: Boolean = false

    private var selectedCatogoryPostion: Int = 0
    private var skeletonScreen: RecyclerViewSkeletonScreen? = null
    private var searchDealsCategoryItems: ArrayList<SearchDealsCategoryItems?> = ArrayList()
    private lateinit var searchDealsItemAdapter: SearchDealsItemsAdapterLoadMore
    lateinit var activity: Activity
    private lateinit var categoryName: String
    private lateinit var categoryId: String
    private var subCategoryName: String = "All"
    private var subCategoryId: String = "0"
    private lateinit var searchDealsPresenter: SearchDealsPresenter
    private var isUserOpenForFirstTime = true
    private var searchUserCategoriesAll = ArrayList<SearchUserCategory>()
    private lateinit var userKey: String

    var subcateogriesBulder: SimpleTooltip? = null
    var filterBuilder: SimpleTooltip? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        userKey = userTokenKey

        searchDealsPresenter = SearchDealsPresenterImpl(activity, this)
        searchDealsPresenter.getUserDealsCategories(userTokenKey)
//        LoadingDialog.getInstance().show(activity)

//        activity.tvSearchDealsSortBy.text = "Sort By"

        activity.iv_filter.setOnClickListener {
            showFilterPopup(activity.findViewById(R.id.iv_filter))
        }


        activity.rvHomeDeals.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        searchDealsItemAdapter = SearchDealsItemsAdapterLoadMore(
            activity,
            searchDealsCategoryItems,
            true
        ) {

            activity.startActivity(
                activity.intentFor<SearchDealDetailsActivity>(
                    Constants.DEAL_ID to it,
                    Constants.FROM_ACTIVITY to Constants.FROM_SEARCH_DEALS
                )
            )
        }

        activity.rvHomeDeals.adapter = searchDealsItemAdapter

        skeletonScreen = Skeleton.bind(activity.rvHomeDeals)
            .adapter(searchDealsItemAdapter)
            .load(R.layout.deals_skeleton)
            .color(R.color.shimmer_bg)
            .duration(1500)
            .show()

        tv_search.setOnClickListener {
            showSubCategoryPopup(selectedCatogoryPostion, tv_search)
        }


        val mLayoutManager = activity.rvHomeDeals.layoutManager as LinearLayoutManager

        activity.rvHomeDeals.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { //check for scroll down
                    val visibleItemCount = mLayoutManager.getChildCount();
                    val totalItemCount = mLayoutManager.getItemCount();
                    val pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    Log.v("...", "Last Item Wow !")
                    if (canLoadMore) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            canLoadMore = false
                            searchDealsItemAdapter.addLoadMore()

                            if (filterPosition != -1) {
                                loadFilterData(filterPosition, ++pageNumber)
                            } else {
                                searchDealsPresenter.getSearchDealsCategorySubCategoryItemsLoadMore(
                                    userKey,
                                    categoryId,
                                    subCategoryId,
                                    ++pageNumber
                                )

                            }
                            Log.v("...", "Last Item Wow !")
                            // Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }


    private fun showSubCategoryPopup(position: Int, anchorView: View) {
//
//        if(isSubCategoriesDailogShowing){
//            subcateogriesBulder?.dismiss()
//            isSubCategoriesDailogShowing = false
//            return
//        }

        if (!isUserOpenForFirstTime) {
            subCategoryName =
                searchUserCategoriesAll[position].searchUserSubcategories[0].subcategoryName
            subCategoryId = searchUserCategoriesAll[position].searchUserSubcategories[0].id

            val dropDownRecyclerView: View =
                layoutInflater.inflate(R.layout.subcategories_list, null, false)

            subcateogriesBulder = SimpleTooltip.Builder(context)
                .anchorView(anchorView)
                .contentView(dropDownRecyclerView, -1)
                .text("Texto do Tooltip")
                .gravity(Gravity.BOTTOM)
                .focusable(false)
                .dismissOnInsideTouch(false)
                .margin(-10f)
                .ignoreOverlay(true)
                .arrowColor(resources.getColor(R.color.colorPrimary))
                .transparentOverlay(false)

                .build()

            dropDownRecyclerView.rv_subcategories.run {
                layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                dropDownRecyclerView.rv_subcategories.adapter = SearchDealsSubCategoriesAdapter(
                    activity,
                    searchUserCategoriesAll[position].searchUserSubcategories as ArrayList<SearchUserSubcategory>
                ) {
                    subCategoryName = it.subcategoryName
                    subCategoryId = it.id

                    getFreshData()

                    activity?.tv_search.text =
                        Html.fromHtml("$categoryName ${context.getString(R.string.down_arrow)}: <font color = '#f7bc00'> $subCategoryName</font>")
                    activity.tvSearchDealsSortBy.visibility = View.GONE
                    subcateogriesBulder?.dismiss()
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dropDownRecyclerView.elevation = 10f
            }

            try {
                subcateogriesBulder?.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }


    private fun showFilterPopup(anchorView: View) {

        if (filterBuilder?.isShowing ?: false) {
            filterBuilder?.dismiss()
            return
        }
        val filterList = arrayListOf<String>("Low to high", "High to low", "Deal Club")
        val dropDownRecyclerView: View =
            layoutInflater.inflate(R.layout.subcategories_list, null, false)

        dropDownRecyclerView.lyt_top.visibility = View.VISIBLE

        filterBuilder = SimpleTooltip.Builder(context)
            .anchorView(anchorView)
            .contentView(dropDownRecyclerView, -1)
            .text("Texto do Tooltip")
            .gravity(Gravity.BOTTOM)
            .focusable(false)
            .dismissOnInsideTouch(false)
            .margin(-10f)
            .ignoreOverlay(true)
            .arrowColor(resources.getColor(R.color.colorPrimary))
            .transparentOverlay(false)
            .build()

        dropDownRecyclerView.rv_subcategories.run {

            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            dropDownRecyclerView.rv_subcategories.adapter =
                FilterAdapter(activity, filterList) { position ->
                    filterBuilder?.dismiss()
                    activity.tvSearchDealsSortBy.visibility = View.VISIBLE


                    clearOldData()
                    filterPosition = position

                    loadFilterData(filterPosition,pageNumber)

                }

        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dropDownRecyclerView.elevation = 10f
        }

        try {
            filterBuilder?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun loadFilterData(filterPosition: Int, pageNumber: Int) {
        try {
            val userLocationLatitude by PreferenceExt.appPreference(
                activity,
                Constants.USER_LOCATION_LATITUDE,
                Constants.DEFAULT_STRING
            )

            val userLocationLongitude by PreferenceExt.appPreference(
                activity,
                Constants.USER_LOCATION_LONGITUDE,
                Constants.DEFAULT_STRING
            )

            var filterKey = "price_low"

            when (filterPosition) {
                0 -> {
                    activity.tvSearchDealsSortBy.text = "Sort By : Low to high"
                    filterKey = "price_low"
                }
                1 -> {
                    activity.tvSearchDealsSortBy.text = "Sort By : High to low"
                    filterKey = "price_high"
                }
                2 -> {
                    activity.tvSearchDealsSortBy.text = "Sort By : Deal Club"
                    filterKey = "deal_club"
                }
            }

            if (pageNumber == 1) {
                searchDealsPresenter.getSearchDealsCategorySubCategoryItemsSortBy(userKey, categoryId, subCategoryId, filterKey, pageNumber)
            } else {
                searchDealsPresenter.getSearchDealsCategorySubCategoryItemsSortByLoadMore(userKey, categoryId, subCategoryId, filterKey, pageNumber)
            }



        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    override fun onGetUserDealsCategoriesSuccess(searchUserCategoriesResult: ArrayList<SearchUserCategory>) {
        searchUserCategoriesAll = searchUserCategoriesResult
        activity.rvCategories.layoutManager =
            LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        val categories = ArrayList<Categories>()
        for (i in 0 until searchUserCategoriesResult.size) {
            categories.add(
                Categories(
                    searchUserCategoriesResult[i].id.toString(),
                    searchUserCategoriesResult[i].categoryName,
                    searchUserCategoriesResult[i].slug,
                    searchUserCategoriesResult[i].bannerImage
                )
            )
        }

        activity.rvCategories.adapter = SearchDealsCategoriesAdapter(activity, categories) {
            categoryName = searchUserCategoriesResult[it].slug
            categoryId = searchUserCategoriesResult[it].id
//                searchDealsPresenter.getUserDealsSubCategories(userKey, it)

            selectedCatogoryPostion = it
            activity.tvSearchDealsSortBy.visibility = View.GONE
            activity?.tv_search.text =
                Html.fromHtml("$categoryName ${context?.getString(R.string.down_arrow)}: <font color = '#f7bc00'> All </font>")

            getFreshData()

        }

        if (isUserOpenForFirstTime) {
            getUserDealsSubCategories(0)
            categoryName = searchUserCategoriesResult[0].slug
            categoryId = searchUserCategoriesResult[0].id
            subCategoryId =
                searchUserCategoriesResult[0].searchUserSubcategories.firstOrNull()?.id ?: "0"

            getFreshData()

            isUserOpenForFirstTime = false
            activity?.tv_search.text =
                Html.fromHtml("$categoryName ${context?.getString(R.string.down_arrow)}: <font color = '#f7bc00'> $subCategoryName</font>")

        }
    }

    private fun getUserDealsSubCategories(position: Int) {

    }

    override fun onGetUserDealsCategoriesFail(message: String) {
        showError(message)
//        hideUserDealsProgress()
    }

    override fun onGetUserDealsCategoriesNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError(getString(R.string.txt_please_check_your_internet_connection))
        hideUserDealsProgress()
    }

    override fun onGetUserDealsSubCategoriesSuccess(position: Int) {
        getUserDealsSubCategories(position)
    }

    override fun onGetUserDealsSubCategoriesFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onGetUserDealsSubCategoriesNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onGetUserDealsItemsSuccess(searchDealsCategoryItems: List<SearchDealsCategoryItems>?) {
        displaySearchDealsCategoryItems(searchDealsCategoryItems as ArrayList<SearchDealsCategoryItems?>)
//        LoadingDialog.getInstance().cancel()
    }

    private fun displaySearchDealsCategoryItems(searchDealsCategoryItems: ArrayList<SearchDealsCategoryItems?>) {
        if (searchDealsCategoryItems.isEmpty()) {
//            activity.tvNoUserDealsFound.visibility = View.VISIBLE
//            activity.rvHomeDeals.visibility = View.GONE
            showError("No Deals found")
            canLoadMore = false
        } else {
//            activity.tvNoUserDealsFound.visibility = View.GONE
//            activity.rvHomeDeals.visibility = View.VISIBLE
            showData()
            this.searchDealsCategoryItems = searchDealsCategoryItems

            if (this.searchDealsCategoryItems.size < 10) {
                canLoadMore = false
            } else {
                canLoadMore = true
            }
            searchDealsItemAdapter.updateData(this.searchDealsCategoryItems)
        }

        hideShimmer()

    }

    override fun onGetUserDealsItemsFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(message)
        canLoadMore = true

    }

    override fun onGetUserDealsNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun showUserDealsProgress() {
//        LoadingDialog.getInstance().show(activity)
    }

    override fun hideUserDealsProgress() {
//        LoadingDialog.getInstance().cancel()
    }

    override fun showShimmer() {
        activity.rvHomeDeals.visibility = View.VISIBLE
        activity.findViewById<View>(R.id.lyt_no_searchDeals).visibility = View.GONE
        skeletonScreen?.show()
    }

    override fun hideShimmer() {
        skeletonScreen?.hide()
    }

    override fun onGetLoadMoreDataSuccess(searchDealsCategoryItems: List<SearchDealsCategoryItems>?) {
        searchDealsItemAdapter.removeLoadMore()

        if (searchDealsCategoryItems != null && searchDealsCategoryItems.size > 0) {
            this.searchDealsCategoryItems.addAll(searchDealsCategoryItems)
            searchDealsItemAdapter.updateData(this.searchDealsCategoryItems)

            if (searchDealsCategoryItems.size < 10) {
                canLoadMore = false
            } else {
                canLoadMore = true
            }

        } else {
            canLoadMore = false
        }
    }

    override fun onGetLoadMoreFail(message: String) {
        searchDealsItemAdapter.removeLoadMore()
        canLoadMore = true
        Snackbar.make(activity.root_view, message, Snackbar.LENGTH_LONG).show()
    }

    private fun showError(message: String) {
        activity.findViewById<View>(R.id.lyt_no_searchDeals).visibility = View.VISIBLE
        rvHomeDeals.visibility = View.GONE
        activity.findViewById<TextView>(R.id.tv_no_data_found).text = message
        skeletonScreen?.hide()
    }

    private fun showData() {
        skeletonScreen?.hide()
        activity.findViewById<View>(R.id.lyt_no_searchDeals).visibility = View.GONE
        rvHomeDeals.visibility = View.VISIBLE
    }

    override fun onDetach() {
        super.onDetach()
        filterBuilder?.dismiss()
        subcateogriesBulder?.dismiss()
        searchDealsPresenter.onDetach()
    }

    fun getFreshData() {
        clearOldData()
        searchDealsPresenter.getSearchDealsCategorySubCategoryItems(
            userKey,
            categoryId,
            subCategoryId,
            pageNumber
        )
    }


    fun clearOldData() {
        searchDealsCategoryItems.clear()
        pageNumber = 1
        filterPosition = -1
        canLoadMore = false
    }
}
