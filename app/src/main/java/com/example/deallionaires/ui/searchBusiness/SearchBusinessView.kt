package com.example.deallionaires.ui.searchBusiness

import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.SearchUserCategory
import com.example.deallionaires.model.Searchbusiness

interface SearchBusinessView {

    fun onGetSearchBusinessCategoriesSuccess(searchUserCategoriesResult: ArrayList<SearchUserCategory>)

    fun onGetSearchBusinessCategoriesFail(message: String)

    fun onGetSearchBusinessCategoriesNoInternet()

    fun onGetSearchBusinessSubCategoriesSuccess(position: Int)

    fun onGetSearchBusinessSubCategoriesFail(message: String)

    fun onGetSearchBusinessSubCategoriesNoInternet()

    fun onGetSearchBusinessItemsSuccess(searchbusiness: List<Searchbusiness?>)

    fun onGetSearchBusinessItemsFail(message: String)

    fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded,position: Int)

    fun onAddingDealToFavouritesFail(message: String,position: Int)

    fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved,position: Int)

    fun onRemovingDealToFavouritesFail(message: String,position: Int)

    fun onGetHomeCategoryItemsFail(message: String)

    fun onGetSearchBusinessNoInternet()

    fun showShimmer()
    fun hideShimmer()

    fun onGetLoadMoreDataSuccess(searchBusinessList: List<Searchbusiness>)

    fun onGetLoadMoreFail(message: String)

}