package com.example.deallionaires.ui.profile

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.model.EditProfile
import com.example.deallionaires.model.EditProfileResponse
import com.example.deallionaires.model.Profile
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.mobileVerification.MobileVerificationActivity
import com.example.deallionaires.utils.*
import com.facebook.FacebookSdk.getApplicationContext
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.imageselection.view.*
import kotlinx.coroutines.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment(), ProfileView {

    private var userprofile: Profile? = null
    private var isProfilePicUpdated: Boolean = false
    private val REQUEST_CODE_CAPTUR_IMAGE: Int = 22
    private  var filePath: String? = null
    private val REQUEST_CODE_GALLERY_PHOTO: Int = 11
    lateinit var activity: Activity
    private val editProfileValidation = AwesomeValidation(ValidationStyle.BASIC)
    private var age: Int? = null
    private var serverContactNumber: String? = null
    private var verifiedContactNumber: String? = null
    private var isFromOnPause = false
    private var serverAge: String? = null
    private var countryCode: String = "91"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)


        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        var mobileNumberVerified by PreferenceExt.appPreference(
            activity,
            Constants.VERIFIED_PROFILE_MOBILE_NUMBER,
            false
        )

        mobileNumberVerified = false

        val profilePresenter: ProfilePresenter = ProfilePresenterImpl(requireContext(), this)

        if (isNetworkAvailable(activity)) {
            profilePresenter.getProfilePresenter(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(getString(R.string.txt_please_check_your_internet_connection))
        }

        activity.img_profile.setOnClickListener {
            val imageUrl = if(filePath.isNullOrEmpty())  userprofile?.profileImage  else filePath
            activity.goToFullImageView(imageUrl,activity.img_profile)
        }


        activity.etProfileDateOfBirth.setOnClickListener {
            CustomDateUtils(
                activity,
                Constants.FROM_PROFILE,
                etProfileDateOfBirth
            ) { displayTextView: Boolean, timeLong: Long ->
                age = calculateAge(timeLong)
                Log.e("Age --", "" + age)

            }.launchMaxDatePicker()
        }

        activity.iv_edit.isEnabled = false
        activity.iv_edit.setOnClickListener {
            showDialogToSelectImage();
        }

        activity.tvEditProfile.setOnClickListener {
            if (tvEditProfile.text == activity.resources.getString(R.string.txt_edit)) {
                tvEditProfile.text = activity.resources.getString(R.string.txt_save)
                activity.etProfileFirstName.isEnabled = true
                activity.etProfileLastName.isEnabled = true
                activity.etProfileEmailAddress.isEnabled = false
                activity.etProfileCountry.isEnabled = true
                activity.etProfileState.isEnabled = true
                activity.etProfileCity.isEnabled = true
                activity.etProfileAddress.isEnabled = true
                activity.etProfileContactNumber.isEnabled = true
                activity.etProfileGender.isEnabled = true
                activity.rb_female.isEnabled = true
                activity.rbMale.isEnabled = true
                activity.iv_edit.isEnabled = true
                activity.etProfileDateOfBirth.isEnabled = true
            } else {
                validateEditProfile(profilePresenter, userTokenKey)
            }
        }



        activity.etProfileContactNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (serverContactNumber != activity.etProfileContactNumber.text.toString()) {
                    activity.tvProfileMobileNumberValidate.visibility = View.VISIBLE
                } else {
//                    activity.tvProfileMobileNumberValidate.visibility = View.GONE
                    activity.tvProfileMobileNumberValidate.text = "Mobile Number is Validated"
                }

                if (activity.etProfileContactNumber.text.toString() == verifiedContactNumber) {
                    activity.tvProfileMobileNumberValidate.text = "Mobile Number is Validated"
                } else {
                    activity.tvProfileMobileNumberValidate.text =
                        activity.resources.getString(R.string.txt_validate_mobile_num)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        activity.tvProfileMobileNumberValidate.setOnClickListener {
            when {
                activity.etProfileContactNumber.text.toString().isEmpty() -> {
                    activity.etProfileContactNumber.error =
                        activity.resources.getString(R.string.txt_enter_mobile_number)
                }
                activity.etProfileContactNumber.text.toString().length < 8 -> {
                    activity.etProfileContactNumber.error =
                        activity.resources.getString(R.string.error_valid_mob_num)
                }
                else -> {
                    activity.startActivity(
                        activity.intentFor<MobileVerificationActivity>(
                            Constants.FROM_ACTIVITY to Constants.FROM_PROFILE,
                            Constants.COUNTRY_CODE to countryCode,
                            Constants.VERIFICATION_MOBILE_NUMBER to activity.etProfileContactNumber.text.toString()
                        )
                    )
                }
            }
        }
    }

    private fun showDialogToSelectImage() {
        var dialog: AlertDialog? = null
        val builder: AlertDialog.Builder = AlertDialog.Builder(context).apply {
            val view = LayoutInflater.from(context).inflate(R.layout.imageselection, null, false)
            view.tv_cemara.setOnClickListener {
                dialog?.cancel()
                openCemera()
            }
            view.tv_gallery.setOnClickListener {
                dialog?.cancel()
                openGallery()
            }
            setView(view)
        }

        dialog = builder.show()
    }

    private fun openGallery() {
        Dexter.withActivity(activity)
            .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse) {
                    val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    i.type = "image/*"
                    startActivityForResult(i, REQUEST_CODE_GALLERY_PHOTO)
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse) {
                    // check for permanent denial of permission
                    if (response.isPermanentlyDenied) {

                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken
                ) {
                    token.continuePermissionRequest()
                }
            }).check()

    }

    private fun openCemera() {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {


//                override fun onPermissionGranted(response: PermissionGrantedResponse) {
//
//                }
//
//                override fun onPermissionDenied(response: PermissionDeniedResponse) {
//                    // check for permanent denial of permission
//                    if (response.isPermanentlyDenied) {
//
//                    }
//                }


                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    val directoryPath: String =
                        "${Environment.getExternalStorageDirectory()}${Constants.APP_GALLERY_PATH}"
                    val fileName = ("IMG_"
                            + SimpleDateFormat("dd-YYYY-MM hh:mm:ss").format(Date())
                            + ".jpg")
                    val mImagePath: File? = checkFileExistence(directoryPath, fileName)
                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        intent.putExtra(
                            MediaStore.EXTRA_OUTPUT,
                            FileProvider.getUriForFile(
                                getApplicationContext(),
                                getApplicationContext().getPackageName() + ".provider",
                                mImagePath!!
                            )
                        )
                    } else {
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImagePath))
                    }
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                    startActivityForResult(intent, REQUEST_CODE_CAPTUR_IMAGE)
                    filePath = mImagePath?.absolutePath.toString()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            }).check()

    }

    private fun calculateAge(timeLong: Long): Int? {
        val dob: Calendar = Calendar.getInstance()
        dob.timeInMillis = timeLong
        val today: Calendar = Calendar.getInstance()
        var age: Int = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
            age--
        }
        return age
    }

    private fun validateEditProfile(
        profilePresenter: ProfilePresenter,
        userTokenKey: String
    ) {
        activity.hideKeyboard(activity.tvEditProfile)

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileFirstName,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_first_name
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileLastName,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_last_name
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileEmailAddress,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_email
        )

        if (etProfileEmailAddress.text.isNotEmpty()) {
            editProfileValidation.addValidation(
                activity,
                R.id.etProfileEmailAddress,
                android.util.Patterns.EMAIL_ADDRESS,
                R.string.error_valid_email
            )
        }

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileCountry,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_country
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileState,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_state
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileCity,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_city
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileAddress,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_address
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileContactNumber,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_mobile_number
        )

        val mobileNum = ".{8,}"

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileContactNumber,
            mobileNum,
            R.string.error_valid_mob_num
        )

        editProfileValidation.addValidation(
            activity,
            R.id.etProfileContactNumber,
            RegexTemplate.TELEPHONE,
            R.string.error_valid_mob_num
        )

//        editProfileValidation.addValidation(
//            activity,
//            R.id.etProfileGender,
//            RegexTemplate.NOT_EMPTY,
//            R.string.txt_enter_gender
//        )




        if (editProfileValidation.validate()) {
            if (activity.etProfileContactNumber.text.toString() != serverContactNumber) {
                val mobileNumberVerified by PreferenceExt.appPreference(
                    activity,
                    Constants.VERIFIED_PROFILE_MOBILE_NUMBER,
                    false
                )
                if (activity.etProfileDateOfBirth.text.toString() == serverAge) {
                    if (mobileNumberVerified) {
                        updateProfileInServer(profilePresenter, userTokenKey)
                    } else {
                        activity.toast("Please validate your mobile number.")
                    }
                } else {
                    if (age!! < 18) {
                        if (mobileNumberVerified) {
                            activity.toast("Minimum 18 years required to register")
                        } else {
                            activity.toast("Please validate your mobile number.")
                        }
                    } else {
                        updateProfileInServer(profilePresenter, userTokenKey)
                    }
                }
            } else {
                if (activity.etProfileDateOfBirth.text.toString() == serverAge) {
                    updateProfileInServer(profilePresenter, userTokenKey)
                } else {
                    if (age!! < 18) {
                        activity.toast("Minimum 18 years required to register")
                    } else {
                        updateProfileInServer(profilePresenter, userTokenKey)
                    }
                }
            }
//            if (activity.etProfileContactNumber.text.toString() == serverContactNumber) {
//                if (activity.etProfileDateOfBirth.text.toString() == serverAge) {
//                    updateProfileInServer(profilePresenter, userTokenKey)
//                } else {
//                    if (age!! < 18) {
//                        activity.toast("Minimum 18 years required to register")
//                    }else {
//                        updateProfileInServer(profilePresenter, userTokenKey)
//                    }
//                }
//            } else {
//                val mobileNumberVerified by PreferenceExt.appPreference(
//                    activity,
//                    Constants.VERIFIED_PROFILE_MOBILE_NUMBER,
//                    false
//                )
//                if (activity.etProfileDateOfBirth.text.toString() == serverAge) {
//                    if(mobileNumberVerified){
//                        updateProfileInServer(profilePresenter, userTokenKey)
//                    }else {
//                        activity.toast("Please validate your mobile number.")
//                    }
//                } else {
//                    if (age!! < 18) {
//                        activity.toast("Minimum 18 years required to register")
//                    }else {
//                        updateProfileInServer(profilePresenter, userTokenKey)
//                    }
//                }
//            }
        }
    }

    private fun updateProfileInServer(profilePresenter: ProfilePresenter, userTokenKey: String) {

        if(isProfilePicUpdated){

            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Uploading in progress")
            progressDialog.show()
            progressDialog.setCancelable(false)

           val compressedPathDeffered = GlobalScope.async {
                 compressImage(filePath)
            }

            GlobalScope.launch(Dispatchers.Main) {
                val compressedPath = compressedPathDeffered.await()

                compressedPath?.let{
                    profilePresenter.updateProfilePic(userTokenKey,it, object : ProgressCallBacks{
                        override fun progressUpdate(precentage: Float) {
                            progressDialog.setMessage("Uploading in Progress (${precentage.toInt()}%)")
                        }

                        override fun uploadSuccess(imageLink:String) {
                            progressDialog.dismiss()
                            submitUserInfo(profilePresenter,userTokenKey,imageLink)
                        }

                        override fun uploadFail(message:String) {
                            progressDialog.dismiss()
                            context?.toast(message)
                        }
                    })

                }
            }



        }else {
           submitUserInfo(profilePresenter,userTokenKey,userprofile?.profileImage?:"")
        }
    }

    private fun submitUserInfo(profilePresenter: ProfilePresenter, userTokenKey: String,imageLink:String){
        val editProfile = EditProfile(
            null,
            activity.etProfileFirstName.text.toString(),
            activity.etProfileLastName.text.toString(),
            activity.etProfileContactNumber.text.toString(),
            activity.etProfileAddress.text.toString(),
            activity.etProfileCountry.text.toString(),
            activity.etProfileCity.text.toString(),
            activity.etProfileState.text.toString(),
            if (activity.rgGender.checkedRadioButtonId == R.id.rbMale) "male" else "female",
//            activity.etProfileGender.text.toString(),
            activity.etProfileDateOfBirth.text.toString()
        )
        profilePresenter.editUserProfile(userTokenKey, editProfile)
        LoadingDialog.getInstance().show(activity)
    }


    override fun onGettingProfileSuccess(userProfile: Profile) {

        this.userprofile = userProfile;
        val errorImage = if (userProfile.gender.equals("male", true))
            R.drawable.gender_m else R.drawable.gender_f

        Glide.with(activity).load(userProfile.profileImage)
            .placeholder(R.color.gray_20)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .error(errorImage)
            .into(activity.img_profile)

        if (userProfile.countrycode != null) {
            countryCode = "" + userProfile.countrycode
        }
        activity.etProfileFirstName.setText(userProfile.firstName)
        activity.etProfileLastName.setText(userProfile.lastName)
        activity.etProfileEmailAddress.setText(userProfile.email)
        activity.etProfileCountry.setText(userProfile.country)
        activity.etProfileState.setText(userProfile.state)
        activity.etProfileCity.setText(userProfile.city)
        activity.etProfileAddress.setText(userProfile.address)
        serverContactNumber = userProfile.mobileNumber
        activity.etProfileContactNumber.setText(userProfile.mobileNumber)
        activity.etProfileGender.setText(userProfile.gender)
        if (userProfile.gender.equals("male")) {
            activity.rbMale.isChecked = true
        } else {
            activity.rb_female.isChecked = true
        }
        if (userProfile.dateOfBirth != null) {
            val separate = userProfile.dateOfBirth!!.split(" ")
            val outputFormate = SimpleDateFormat("dd-MMM-yyyy");
            val inputFormat = SimpleDateFormat("yyyy-MM-dd");
            try {
                serverAge = outputFormate.format(inputFormat.parse(separate[0]))
            } catch (e: Exception) {
                serverAge = separate[0]
            }
            activity.etProfileDateOfBirth.setText(serverAge)
        }
        LoadingDialog.getInstance().cancel()
    }

    override fun onGettingProfileFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onEditingProfileSuccess(editProfileResponse: EditProfileResponse) {
        var userProfileName by PreferenceExt.appPreference(
            activity,
            Constants.USER_PROFILE_NAME,
            Constants.DEFAULT_STRING
        )
        userProfileName = Constants.DEFAULT_STRING
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_profile_edited_successfully))
        activity.tvEditProfile.text = activity.resources.getString(R.string.txt_edit)
        activity.etProfileFirstName.isEnabled = false
        activity.etProfileLastName.isEnabled = false
        activity.etProfileEmailAddress.isEnabled = false
        activity.etProfileCountry.isEnabled = false
        activity.etProfileState.isEnabled = false
        activity.etProfileCity.isEnabled = false
        activity.etProfileAddress.isEnabled = false
        activity.etProfileContactNumber.isEnabled = false
        activity.etProfileGender.isEnabled = false
        activity.rb_female.isEnabled = false
        activity.rbMale.isEnabled = false
        activity.iv_edit.isEnabled = false
        activity.etProfileDateOfBirth.isEnabled = false

        (activity as HomeActivity).showUserName()
    }

    override fun onEditingProfileFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGettingProfileNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onResume() {
        super.onResume()
        if (isFromOnPause) {
            val mobileNumberVerified by PreferenceExt.appPreference(
                activity,
                Constants.VERIFIED_PROFILE_MOBILE_NUMBER,
                false
            )
            if (mobileNumberVerified) {
                activity.tvProfileMobileNumberValidate.text = "Mobile Number is Validated"
                verifiedContactNumber = activity.etProfileContactNumber.text.toString()
            } else {
                activity.tvProfileMobileNumberValidate.text =
                    activity.resources.getString(R.string.txt_validate_mobile_num)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        isFromOnPause = true
    }


    fun checkFileExistence(path: String?, fileName: String?): File? {
        if (!File(path).exists()) {
            File(path).mkdirs()
        }
        return File(path, fileName)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (true) {
            requestCode == REQUEST_CODE_CAPTUR_IMAGE && resultCode == Activity.RESULT_OK -> {
                Glide.with(context!!).load(filePath.toString()).into(img_profile)
                isProfilePicUpdated = true
            }
            requestCode == REQUEST_CODE_GALLERY_PHOTO && resultCode == Activity.RESULT_OK -> {
                val selectedImageURI = data!!.data
                filePath = getRealPathFromURI(context!!, selectedImageURI!!)!!
//                img_profile.setImageURI(Uri.fromFile(File(filePath)))
                Glide.with(context!!).load(Uri.fromFile(File(filePath))).into(img_profile)

                isProfilePicUpdated = true
//                Glide.with(context!!).load(filePath).into(img_profile)
            }
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }


     fun compressImage(imageUri: String?): String? {
//        val filePath = getRealPathFromURI(context!!,Uri.parse(imageUri))
        var scaledBitmap: Bitmap? = null
        val options: BitmapFactory.Options = BitmapFactory.Options()

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true
        var bmp: Bitmap? = BitmapFactory.decodeFile(imageUri, options)
        var actualHeight: Int = options.outHeight
        var actualWidth: Int = options.outWidth

//      max Height and width values of the compressed image is taken as 816x612
        val maxHeight = 1024.0f
        val maxWidth = 1024.0f
        var imgRatio = actualWidth / actualHeight.toFloat()
        val maxRatio = maxWidth / maxHeight

//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight
                actualWidth = (imgRatio * actualWidth).toInt()
                actualHeight = maxHeight.toInt()
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth
                actualHeight = (imgRatio * actualHeight).toInt()
                actualWidth = maxWidth.toInt()
            } else {
                actualHeight = maxHeight.toInt()
                actualWidth = maxWidth.toInt()
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)
        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(imageUri, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
        val canvas = Canvas(scaledBitmap!!)
        canvas.setMatrix(scaleMatrix)
        canvas.drawBitmap(
            bmp!!,
            middleX - bmp.getWidth() / 2,
            middleY - bmp.getHeight() / 2,
            Paint(Paint.FILTER_BITMAP_FLAG)
        )

//      check the rotation of the image and display it properly
        val exif: ExifInterface
        try {
            exif = ExifInterface(imageUri)
            val orientation: Int = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, 0
            )
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90f)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 3) {
                matrix.postRotate(180f)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 8) {
                matrix.postRotate(270f)
                Log.d("EXIF", "Exif: $orientation")
            }
            scaledBitmap = Bitmap.createBitmap(
                scaledBitmap, 0, 0,
                scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                true
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }
        var out: FileOutputStream? = null
        val filename: String? = getFilename()
        try {
            out = FileOutputStream(filename)

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, out)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return filename
    }

    fun getFilename(): String? {
        val file = File(
            Environment.getExternalStorageDirectory().path,
            "Deallionaires/Images"
        )
        if (!file.exists()) {
            file.mkdirs()
        }
        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"
    }


    fun calculateInSampleSize(
        options: BitmapFactory.Options,
        reqWidth: Int,
        reqHeight: Int
    ): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val heightRatio =
                Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio =
                Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = width * height.toFloat()
        val totalReqPixelsCap = reqWidth * reqHeight * 2.toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
        return inSampleSize
    }

}
