package com.example.deallionaires.ui.dealsStatus.pendingDeals

import com.example.deallionaires.model.ClaimDeal

interface PendingDealsPresenter {

    fun getGetPendingDealsPresenter(tokenKey: String)

    fun claimToCompleteDeal(userToken: String, redeemdealid: Int, claimDeal: ClaimDeal)

    fun destroyPendingDeals()
}