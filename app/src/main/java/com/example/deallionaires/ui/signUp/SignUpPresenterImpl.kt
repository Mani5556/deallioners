package com.example.deallionaires.ui.signUp

import android.content.Context
import com.example.deallionaires.model.SignUp
import com.example.deallionaires.model.SignUpUser
import com.example.deallionaires.network.APIRequests.registerNewUser
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SignUpPresenterImpl(private val context: Context, private var signUpView: SignUpView?) :
    SignUpPresenter {
    override fun signUpNewUser(signUpUser: SignUpUser) {
        if (isNetworkAvailable(context)) {
            registerNewUser(context, signUpUser, object : NWResponseCallback<SignUp?> {
                override fun onSuccess(call: Call<SignUp?>, response: Response<SignUp?>) {
                    signUpView!!.signUpNewUserSuccess(response.body()!!)
//                    if(response.body()!!.success){
//                        signUpView!!.signUpNewUserSuccess(response.body()!!)
//                    }else {
//                        signUpView!!.signUpNewUserFailed(response.body()?.msg ?:"something went wrong")
//                    }
                }

                override fun onResponseBodyNull(call: Call<SignUp?>, response: Response<SignUp?>) {
                    signUpView!!.signUpNewUserFailed(response.message().toString())
                }

                override fun onResponseUnsuccessful(
                    call: Call<SignUp?>,
                    response: Response<SignUp?>
                ) {
                    signUpView!!.signUpNewUserFailed(response.message().toString())
                }

                override fun onFailure(call: Call<SignUp?>, t: Throwable) {
                    signUpView!!.signUpNewUserFailed(t.message!!)
                }

            })
        } else {
            signUpView!!.signUpNewUserNoInternet()
        }
    }

    override fun destroySignUpNewUser() {
        signUpView = null
    }
}