package com.example.deallionaires.ui.help

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.deallionaires.model.EmailVerification
import com.example.deallionaires.model.EmailVerificationResponse
import com.example.deallionaires.model.HelpResponse
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.NetworkState
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.Subject

class HelpCenterViewModel(application: Application): AndroidViewModel(application){

    val helpResponse:MutableLiveData<String> = MutableLiveData<String>()
    val networkCall:MutableLiveData<NetworkState> = MutableLiveData<NetworkState>()

    fun submitQuerry(userAccessKey: String, subject: String,description:String) {
        networkCall.value = NetworkState.IN_PROGRESS

        if (isNetworkAvailable(getApplication())) {
            APIRequests.submitHelpQuery(
                userAccessKey,
                subject,
                description,
                object : NWResponseCallback<HelpResponse?> {
                    override fun onSuccess(
                        call: Call<HelpResponse?>,
                        response: Response<HelpResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            networkCall.value = NetworkState.COMPLETED
                            helpResponse.value = response.body()?.msg
                        } else {
                            networkCall.value = NetworkState.ERROR
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<HelpResponse?>,
                        response: Response<HelpResponse?>
                    ) {
                        networkCall.value = NetworkState.ERROR
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<HelpResponse?>,
                        response: Response<HelpResponse?>
                    ) {
                        networkCall.value = NetworkState.ERROR
                    }

                    override fun onFailure(call: Call<HelpResponse?>, t: Throwable) {
                        networkCall.value = NetworkState.ERROR
                    }
                })
        } else {
            networkCall.value = NetworkState.NO_NETWORK
        }
    }

}

