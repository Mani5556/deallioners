package com.example.deallionaires.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.inputmethodservice.InputMethodService
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.Constraints
import com.example.deallionaires.R
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.mancj.materialsearchbar.MaterialSearchBar
import com.mancj.materialsearchbar.adapter.SuggestionsAdapter
import kotlinx.android.synthetic.main.activity_map.*
import org.jetbrains.anko.toast
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var placeClient: PlacesClient
    private var autocompletePredictions: List<AutocompletePrediction>? = null
    private var myLastLocation: Location? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.frg_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        if (!Places.isInitialized()) {
            Places.initialize(this, "AIzaSyD7XJ6WJYHg8NgX-KkN2Wh5OI1N4p7d3ao")
        }
        placeClient = Places.createClient(this)
        val autoCompleteSuggestionToken = AutocompleteSessionToken.newInstance()

        search_bar.setOnSearchActionListener(object : MaterialSearchBar.OnSearchActionListener {
            override fun onButtonClicked(buttonCode: Int) {}
            override fun onSearchStateChanged(enabled: Boolean) {}
            override fun onSearchConfirmed(text: CharSequence?) {}

        })

        search_bar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.isNullOrEmpty()) {
                    search_bar.clearSuggestions()
                    return
                }
                val predictionsRequest = FindAutocompletePredictionsRequest.builder()
                    .setSessionToken(autoCompleteSuggestionToken)
                    .setLocationBias(null)
                    .setQuery(s.toString())
                    .build()

                placeClient.findAutocompletePredictions(predictionsRequest).addOnCompleteListener {

                    if (it.isSuccessful) {
                        val response = it.result
                        response?.let {
                            autocompletePredictions = it.autocompletePredictions
                            val suggestionsList: ArrayList<String> = autocompletePredictions?.map {
                                it.getFullText(null).toString()
                            } as ArrayList<String>
                            search_bar.updateLastSuggestions(suggestionsList)
                            if (!search_bar.isSuggestionsVisible) {
                                search_bar.showSuggestionsList()
                            }

                        }
                    }
                }
            }
        })
        search_bar.setSuggestionsClickListener(object : SuggestionsAdapter.OnItemViewClickListener {
            override fun OnItemDeleteListener(position: Int, v: View?) {}
            override fun OnItemClickListener(position: Int, v: View?) {
                if (autocompletePredictions!!.size > position) {
                    val selectedSuggestion = autocompletePredictions?.get(position)
                    search_bar.text = selectedSuggestion?.getFullText(null).toString()

                    val inputService: InputMethodManager? =
                        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputService?.hideSoftInputFromWindow(
                        search_bar.windowToken,
                        InputMethodManager.HIDE_IMPLICIT_ONLY
                    )

                    selectedSuggestion?.let {
                        val fetchRequest = FetchPlaceRequest.builder(
                            it.placeId,
                            mutableListOf(Place.Field.LAT_LNG)
                        ).build()
                        placeClient.fetchPlace(fetchRequest).addOnCompleteListener {
                            if (it.isSuccessful) {
                                val latLng = it.result?.place?.latLng
                                mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))

                            }
                        }
                    }

                }

                Handler().postDelayed({
                    search_bar.clearSuggestions()
                }, 1000)
            }

        })

        btn_select.setOnClickListener {
            val selectedLocation = mMap?.getCameraPosition()?.target
            val address = getAddressFromLatLong(selectedLocation?.latitude, selectedLocation?.longitude)
            val resultIntent = Intent()
            resultIntent.putExtra("selectedLocation", selectedLocation)
            resultIntent.putExtra("selectedAddress", address)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    private fun getAddressFromLatLong(latitude: Double?, longitude: Double?): String? {

        val geocoder = Geocoder(this, Locale.getDefault())
        var result: String? = null
        try {
            if (latitude == null && longitude == null) {
                return null
            }
            val addressList = geocoder.getFromLocation(latitude!!, longitude!!, 1)
            if (addressList != null && addressList.size > 0) {
                val address = addressList[0]
                val sb = StringBuilder()
                for (i in 0 until address.maxAddressLineIndex) {
                    sb.append(address.getAddressLine(i)).append(",")
                }
                address.featureName?.let { sb.append(address.featureName).append(", ") }
                address.subLocality?.let { sb.append(address.subLocality).append(", ") }
                address.locality?.let { sb.append(address.locality).append(", ") }
                address.subAdminArea?.let { sb.append(address.subAdminArea).append(", ") }
                address.adminArea?.let { sb.append(address.adminArea).append(", ") }
                address.countryName?.let { sb.append(address.countryName).append(", ") }
                address.postalCode?.let { sb.append(address.postalCode)}
                    result = sb.toString()
                }
            } catch (e: IOException) {
                Log.e("Location Address", "Unable connect to Geocoder", e)

            }
            return result

        }

        @SuppressLint("MissingPermission")
        override fun onMapReady(map: GoogleMap?) {
            mMap = map
            mMap?.isMyLocationEnabled = true
            mMap?.uiSettings?.isMyLocationButtonEnabled = true
            val mapView = frg_map.view
            val mapParent = mapView?.findViewById<View>("1".toInt())
            val currentButton = (mapParent?.parent as View).findViewById<View>("2".toInt())
            val layoutParams = currentButton.layoutParams as RelativeLayout.LayoutParams
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            layoutParams.setMargins(0, 0, 40, 180)

            val locationRequest = LocationRequest.create().apply {
                interval = 10000
                fastestInterval = 5000
                setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            }

            val locationBuilder =
                LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
            val settingsClient = LocationServices.getSettingsClient(this)
            val settingsResponse = settingsClient.checkLocationSettings(locationBuilder.build())
            settingsResponse.addOnSuccessListener {
                getUserCurrentLocation()
            }
            settingsResponse.addOnFailureListener {
                if (it is ResolvableApiException) {
                    it.startResolutionForResult(this, 5)
                }
            }

        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
                getUserCurrentLocation()
            }
        }

        @SuppressLint("MissingPermission")
        private fun getUserCurrentLocation() {
            mFusedLocationProviderClient.lastLocation.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    myLastLocation = task.result
                    mMap?.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                myLastLocation!!.latitude,
                                myLastLocation!!.longitude
                            ), 15f
                        )
                    )
                } else {
                    toast("Cant fetch the current Location")
                }
            }

        }
    }