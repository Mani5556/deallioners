package com.example.deallionaires.ui.searchDeals

interface SearchDealsPresenter {

    fun getUserDealsCategories(userTokenKey: String)

    fun getUserDealsSubCategories(userTokenKey: String, position : Int)

//    fun getSearchDealsCategoryItems(userTokenKey: String, categoryId: String)

    fun getSearchDealsCategorySubCategoryItems(userTokenKey: String, categoryId: String, sub_categoryId: String,pageNumber: Int)
    fun getSearchDealsCategorySubCategoryItemsLoadMore(userTokenKey: String, categoryId: String, sub_categoryId: String,pageNumber: Int)

    fun getSearchDealsCategorySubCategoryItemsSortBy(userTokenKey: String, categoryName: String, sub_category: String, sortOptions: String,pageNumber: Int)
    fun getSearchDealsCategorySubCategoryItemsSortByLoadMore(userTokenKey: String, categoryName: String, sub_category: String, sortOptions: String,pageNumber: Int)

    fun destroyUserDeals()

    fun onDetach()

}