package com.example.deallionaires.ui.home.fragments.favourites

import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.FavouritesResponse

interface FavouritesView {

    fun displayFavourites(favourites: FavouritesResponse)

    fun noFavouritesFound(message: String)

    fun onRemovingDealFromFavouritesSuccess(favouriteRemoved: FavouriteRemoved)

    fun onRemovingDealFromFavouritesFail(message: String)

    fun favouritesNoInternet()

}