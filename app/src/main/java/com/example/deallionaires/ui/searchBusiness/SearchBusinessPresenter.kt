package com.example.deallionaires.ui.searchBusiness

interface SearchBusinessPresenter {

    fun getSearchBusinessCategories(userTokenKey: String)

    fun getSearchBusinessSubCategories(userTokenKey: String, position : Int)

//    fun getSearchBusinessCategoryItems(userTokenKey: String, categoryId: String)

    fun getSearchBusinessCategorySubCategoryItems(userTokenKey: String, categoryId: String, sub_categoryId: String)

    fun getSearchBusinessCategorySubCategoryItemsSortBy(userTokenKey: String, categoryName: String, sub_category: String, sortOptions: String,pageNumber: Int)
    fun getSearchBusinessCategorySubCategoryItemsSortByLoadMore(userTokenKey: String, categoryName: String, sub_category: String, sortOptions: String,pageNumber: Int)

    fun getSearchBusinessCategorySubCategoryItemsSortByDistance(userTokenKey: String, userlat: Double, userlong: Double,pageNumber: Int)
    fun getSearchBusinessCategorySubCategoryItemsSortByDistanceLoadMore(userTokenKey: String, userlat: Double, userlong: Double,pageNumber: Int)

    fun addDealToFavourites(userTokenKey: String, merchantid: Int,position: Int)

    fun removeDealFromFavourites(userTokenKey: String, merchantid: Int,position: Int)

    fun destroySearchBusiness()

    fun onDetach()


    fun loadMoreBusinesses(userTokenKey: String, categoryId: String, sub_categoryId: String,pageNumber: Int)


}