package com.example.deallionaires.ui.notification.admin

import android.content.Context
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class AdminNotificationPresenter(
    val context: Context,
    private val notificationView: AdminNotificationView?
) {

    fun getAdminNotification(tokenKey: String) {
        if (isNetworkAvailable(context)) {
            APIRequests.getAdminNotifications(
                tokenKey,
                object : NWResponseCallback<AdminNotificationModel> {
                    override fun onSuccess(
                        call: Call<AdminNotificationModel>,
                        response: Response<AdminNotificationModel>
                    ) {
                        val merchantNotifications = ArrayList<Notiadmin>()
                        for (i in response.body()!!.notiadmin.indices) {

                            merchantNotifications.add(
                                Notiadmin(
                                    response.body()!!.notiadmin[i].id,
                                    response.body()!!.notiadmin[i].description,
                                    response.body()!!.notiadmin[i].created_at,
                                    response.body()!!.notiadmin[i].profile_image
                                )
                            )

                        }
                        notificationView!!.onGetAdminNotificationSuccess(merchantNotifications)
                    }

                    override fun onResponseBodyNull(
                        call: Call<AdminNotificationModel>,
                        response: Response<AdminNotificationModel>
                    ) {
                        notificationView!!.onGetAdminNotificationFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<AdminNotificationModel>,
                        response: Response<AdminNotificationModel>
                    ) {
                        notificationView!!.onGetAdminNotificationFail(response.message())
                    }

                    override fun onFailure(call: Call<AdminNotificationModel>, t: Throwable) {
                        notificationView!!.onGetAdminNotificationFail(t.message.toString())
                    }
                })
        } else {
            notificationView!!.onGetAdminNotificationNoInternet()
        }
    }

    fun onMerchantNotificationFragmentDestroy() {
        notificationView == null
    }

}