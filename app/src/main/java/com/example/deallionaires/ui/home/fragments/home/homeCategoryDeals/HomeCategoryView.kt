package com.example.deallionaires.ui.home.fragments.home.homeCategoryDeals

import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.Searchbusiness

interface HomeCategoryView {

    fun onGetHomeCategoryItemsSuccess(searchbusiness: List<Searchbusiness>)

    fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded)

    fun onAddingDealToFavouritesFail(message: String)

    fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved)

    fun onRemovingDealToFavouritesFail(message: String)

    fun onGetHomeCategoryItemsFail(message: String)

    fun onGetHomeCategoryNoInternet()

}