package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.merchantWise

import android.content.Context
import com.example.deallionaires.model.MerchantWiseDeallionsResponse
import com.example.deallionaires.network.APIRequests.getUserMerchantWiseDeallions
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class MerchantWisePresenter(
    private val context: Context,
    private var merchantWiseView: MerchantWiseView?
) {
    fun getMerchantWiseDeallions(userTokenKey: String){
        if(isNetworkAvailable(context)){
            getUserMerchantWiseDeallions(userTokenKey, object : NWResponseCallback<MerchantWiseDeallionsResponse>{
                override fun onSuccess(
                    call: Call<MerchantWiseDeallionsResponse>,
                    response: Response<MerchantWiseDeallionsResponse>
                ) {
                    merchantWiseView!!.onGettingMerchantWiseDeallionsSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<MerchantWiseDeallionsResponse>,
                    response: Response<MerchantWiseDeallionsResponse>
                ) {
                    merchantWiseView!!.onGettingMerchantWiseDeallionsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<MerchantWiseDeallionsResponse>,
                    response: Response<MerchantWiseDeallionsResponse>
                ) {
                    merchantWiseView!!.onGettingMerchantWiseDeallionsFail(response.message())
                }

                override fun onFailure(call: Call<MerchantWiseDeallionsResponse>, t: Throwable) {
                    merchantWiseView!!.onGettingMerchantWiseDeallionsFail(t.message!!)
                }
            })
        }else {
            merchantWiseView!!.onGettingMerchantWiseNoInternet()
        }
    }

}