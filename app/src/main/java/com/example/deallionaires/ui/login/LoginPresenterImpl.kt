package com.example.deallionaires.ui.login

import android.content.Context
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.APIRequests.checkUserLogin
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response


class LoginPresenterImpl(private val context: Context, private var loginView: LoginView?):LoginPresenter {

    override fun loginUser(email: String, password: String, loginType: String, socialId: String) {

        val user = User(email, password, loginType, socialId)

        if(isNetworkAvailable(context)){
            checkUserLogin(context, user, object : NWResponseCallback<LoginResponse?>{
                override fun onSuccess(call: Call<LoginResponse?>, response: Response<LoginResponse?>) {
                    if(response.body()!!.success!!){
                        if(response.body()!!.tokenKey != null){
                            loginView!!.onLoginSuccess(response.body()!!)
                        }else {
                            loginView!!.onLoginFail(response.body()!!.msg!!)
                        }
                    }else {
                        loginView!!.onLoginFail(response.body()!!.msg!!)
                    }
                }

                override fun onResponseBodyNull(call: Call<LoginResponse?>, response: Response<LoginResponse?>) {
                    loginView!!.onLoginFail(response.message())
                }

                override fun onResponseUnsuccessful(call: Call<LoginResponse?>, response: Response<LoginResponse?>) {
                    loginView!!.onLoginFail(response.message())
                }

                override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                    loginView!!.onLoginFail(t.message.toString())
                }

            })
        }else {
            loginView!!.onLoginNoInternet()
        }
    }

    override fun socilalogin(signUpUser: SignUpUser) {
        if (isNetworkAvailable(context)) {
            APIRequests.registerNewUser(context, signUpUser, object : NWResponseCallback<SignUp?> {
                override fun onSuccess(call: Call<SignUp?>, response: Response<SignUp?>) {
                    loginView!!.socialLoginSuccess(response.body()!!)
//                    if(response.body()!!.success){
//                        loginView!!.socialLoginSuccess(response.body()!!)
//                    }else {
//                        loginView!!.socialLoginFail(response.body()?.msg?:"Some thing went wrong")
//                    }
                }

                override fun onResponseBodyNull(call: Call<SignUp?>, response: Response<SignUp?>) {
                    loginView!!.socialLoginFail(response.message().toString())
                }

                override fun onResponseUnsuccessful(
                    call: Call<SignUp?>,
                    response: Response<SignUp?>
                ) {
                    loginView!!.socialLoginFail(response.message().toString())
                }

                override fun onFailure(call: Call<SignUp?>, t: Throwable) {
                    loginView!!.socialLoginFail(t.message!!)
                }

            })
        } else {
            loginView!!.socialLoginNoInternet()
        }
    }


    override fun onDestroy() {
        loginView = null
    }

}