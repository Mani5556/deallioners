package com.example.deallionaires.ui.signUp

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.example.deallionaires.R
import com.example.deallionaires.model.SignUp
import com.example.deallionaires.model.SignUpUser
import com.example.deallionaires.ui.emailVerification.EmailVerificationActivity
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.ui.mobileVerification.MobileNumberActivity
import com.example.deallionaires.ui.mobileVerification.MobileVerificationActivity
import com.example.deallionaires.utils.*
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class SignUpActivity : AppCompatActivity(), SignUpView {

    var gender = Constants.GENDER
    var isFromOnPause = false
    var isShown = false
    var isConfirmShown = false

    private val signUpValidation = AwesomeValidation(ValidationStyle.BASIC)
    private var age: Int? = null

    //for email
    private var googleApiClient: GoogleSignInClient? = null
    private val RC_SIGN_IN = 9

    //for facebook
    private var callbackManager: CallbackManager? = null
    private val EMAIL = "email"

    private var socialId = ""
    private var emailId = ""
    private var loginType = Constants.LOGIN_TYPE_NORMAL

    var userTokenKey by PreferenceExt.appPreference(
        this,
        Constants.USER_TOKEN_KEY,
        Constants.DEFAULT_STRING
    )

    var signUpMobNumVerified by PreferenceExt.appPreference(
        this,
        Constants.SIGN_UP_MOBILE_NUM_VERIFIED,
        false
    )

    private lateinit var countryCode: String

    private var emailVerified by PreferenceExt.appPreference(
        this,
        Constants.VERIFIED_EMAIL,
        false
    )

    private lateinit var userSignUpWith: String

    var mobileNumberVerified by PreferenceExt.appPreference(
        this,
        Constants.VERIFIED_MOBILE_NUMBER,
        false
    )

    private lateinit var signUpPresenter: SignUpPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        tvBackToLogin.setText(Html.fromHtml("HAVE AN ACCOUNT ? <u>LOGIN</u>"))
        tv_terms_conditions.setText(Html.fromHtml("I have read and agreethe <u>Trems & conditions</u>"))
        emailVerified = false
        mobileNumberVerified = false

        countryCode = ccpSignUp.selectedCountryCode

        signUpPresenter = SignUpPresenterImpl(this, this)

        //for email
        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .build()
        googleApiClient = GoogleSignIn.getClient(this, gso)

        ibGoogleSignUp.setOnClickListener {
            if (isNetworkAvailable(this)) {
                userSignUpWith = "google"
                val signInIntent: Intent = googleApiClient!!.signInIntent
                startActivityForResult(signInIntent, RC_SIGN_IN)
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }

//            loginType = "gmail"
//            if (isNetworkAvailable(this)) {
//                val profile = SignUpUser(
//                    "lakshman_one",
//                    "family_one",
//                    "mailthree@gmail.com",
//                    "",
//                    0,
//                    "",
//                    "",
//                    "",
//                    "",
//                    loginType,
//                    socialId
//                )
//                signUpPresenter.signUpNewUser(profile)
//                LoadingDialog.getInstance().show(this)
//            } else {
//                toast(getString(R.string.txt_please_check_your_internet_connection))
//            }
        }

        //for facebook
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        callbackManager = CallbackManager.Factory.create()

        ibFacebookSignUp.setOnClickListener {
            if (isNetworkAvailable(this)) {
                userSignUpWith = "facebook"
                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    listOf(EMAIL, "public_profile")
                )
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }
        }
        // Callback registration
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    // App code
                    Log.e("Login", "Facebook Login Successful")
                    getUserDetails(loginResult)
                }

                override fun onCancel() {
                    // App code
                    Log.e("login", "Facebook Login Cancelled")
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Log.e(
                        "Login",
                        "Facebook Login Exception" + exception.message
                    )
                }
            })

        tvDateOfBirth.setOnClickListener {
            CustomDateUtils(
                this,
                "FROM_SIGN_UP",
                tvDateOfBirth
            ) { displayTextView: Boolean, timeLong: Long ->
                tvDateOfBirth.visibility = View.VISIBLE
                age = calculateAge(timeLong)
                Log.e("Age --", "" + age)

            }.launchMaxDatePicker()
        }

        tvSignUpEmailValidate.setOnClickListener {
            validateEmailId()
        }

        tvSignUpMobileNumberValidate.setOnClickListener {
            validateMobileNumber()
        }

        rgGender.setOnCheckedChangeListener { _, checkedId ->
            gender = if (checkedId == R.id.rbMale) {
                getString(R.string.txt_male)
            } else {
                getString(R.string.txt_female)
            }
        }

        ll_sign_up.setOnClickListener {
            validateSignUpDetails()
        }

        tvBackToLogin.setOnClickListener {
            onBackPressed()
        }



        ccpSignUp.setOnCountryChangeListener {
            countryCode = ccpSignUp.selectedCountryCode
            Log.e("Sign Up", countryCode)
        }

        iv_show_password.setOnClickListener {
            if (!isShown) {
                // show password
                etPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_show_password.setImageDrawable(resources.getDrawable(R.drawable.ic_show_password))
                isShown = true
            } else {
                // hide password
                iv_show_password.setImageDrawable(resources.getDrawable(R.drawable.ic_hide_password))
                etPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                isShown = false
            }
        }

        iv_confirm_pass_show.setOnClickListener {
            if (!isConfirmShown) {
                // show password
                etConfirmPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                iv_confirm_pass_show.setImageDrawable(resources.getDrawable(R.drawable.ic_show_password))
                isConfirmShown = true
            } else {
                // hide password
                iv_confirm_pass_show.setImageDrawable(resources.getDrawable(R.drawable.ic_hide_password))
                etConfirmPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                isConfirmShown = false
            }
        }

    }

    private fun validateMobileNumber() {
        hideKeyboard(ll_sign_up)
        signUpValidation.addValidation(
            this,
            R.id.etMobileNumber,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_mobile_number
        )

        val mobileNum = ".{8,}"

        signUpValidation.addValidation(
            this,
            R.id.etMobileNumber,
            mobileNum,
            R.string.error_valid_mob_num
        )

        signUpValidation.addValidation(
            this,
            R.id.etMobileNumber,
            RegexTemplate.TELEPHONE,
            R.string.error_valid_mob_num
        )

        if (signUpValidation.validate()) {
            startActivity(
                intentFor<MobileVerificationActivity>(
                    Constants.FROM_ACTIVITY to Constants.FROM_SIGN_UP,
                    Constants.COUNTRY_CODE to countryCode,
                    Constants.VERIFICATION_MOBILE_NUMBER to etMobileNumber.text.toString()
                )
            )
        }
    }

    private fun validateEmailId() {
        hideKeyboard(ll_sign_up)

        signUpValidation.addValidation(
            this,
            R.id.etEmailAddress,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_email
        )

        if (etEmailAddress.text.isNotEmpty()) {
            signUpValidation.addValidation(
                this,
                R.id.etEmailAddress,
                android.util.Patterns.EMAIL_ADDRESS,
                R.string.error_valid_email
            )
        }

        if (signUpValidation.validate()) {
            startActivity(
                intentFor<EmailVerificationActivity>(
                    Constants.VERIFICATION_EMAIL to etEmailAddress.text.toString(),
                    Constants.FROM_ACTIVITY to Constants.FROM_SIGN_UP
                )
            )
        }
    }

    private fun calculateAge(timeLong: Long): Int? {
        val dob: Calendar = Calendar.getInstance()
        dob.timeInMillis = timeLong
        val today: Calendar = Calendar.getInstance()
        var age: Int = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)
        if (today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
            age--
        }
        return age
    }

    private fun getUserDetails(loginResult: LoginResult?) {
        val dataRequest = GraphRequest.newMeRequest(
            loginResult!!.accessToken
        ) { jsonObject, _ ->
            try {
                var dateOfBirth = "1900-1-1"
                var gender = "dummygender"
                val jsonObjectResponse = JSONObject(jsonObject.toString())
//                val profilePicData = JSONObject(jsonObjectResponse["picture"].toString())
//                val profilePicUrl = JSONObject(profilePicData.getString("data"))
//                etFirstName.setText(jsonObjectResponse["name"].toString())
//                etEmailAddress.setText(jsonObjectResponse["email"].toString())
                Log.e("Facebook email", "" + jsonObjectResponse["email"])
//                loginType = Constants.LOGIN_TYPE_FACEBOOK
                socialId = jsonObjectResponse["id"].toString()

                loginType = "facebook"

                //todo add data of birth conditions here and assign it to dateOfBirth reference
                //todo add gender conditions here and assign it to gender reference

                if (isNetworkAvailable(this)) {
                    val profile = SignUpUser(
                        jsonObjectResponse["name"].toString(),
                        "",
                        jsonObjectResponse["email"].toString(),
                        "",
                        0,
                        "",
                        "",
                        gender,
                        dateOfBirth,
                        loginType,
                        socialId
                    )
                    signUpPresenter.signUpNewUser(profile)
                    LoadingDialog.getInstance().show(this)
                } else {
                    toast(getString(R.string.txt_please_check_your_internet_connection))
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        val permissionParam = Bundle()
        permissionParam.putString("fields", "id,name,email,picture.width(120).height(120)")
        dataRequest.parameters = permissionParam
        dataRequest.executeAsync()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>?) {
        try {
            var dateOfBirth = "1900-1-1"
            var gender = "dummygender"

            val account: GoogleSignInAccount = completedTask!!.getResult(ApiException::class.java)!!
//            val person = Plus.PeopleApi.getCurrentPerson(googleApiClient!!.asGoogleApiClient())
//            Log.e("Login", "gender --" +person.gender)
//            Log.e("Login", "date of Birth --" +person.birthday)
//            etFirstName.setText(account.displayName)
            var familyName = ""
            if (!account.familyName.isNullOrEmpty()) {
                familyName = account.familyName!!
//                etLastName.setText(account.familyName)
            }
//            etEmailAddress.setText(account.email)
            socialId = account.id!!

//            loginType = Constants.LOGIN_TYPE_GOOGLE

            //todo add data of birth conditions here and assign it to dateOfBirth reference
            //todo add gender conditions here and assign it to gender reference

            loginType = "gmail"

            if (isNetworkAvailable(this)) {
                val profile = SignUpUser(
                    account.displayName!!,
                    familyName,
                    account.email!!,
                    "",
                    0,
                    "",
                    "",
                    gender,
                    dateOfBirth,
                    loginType,
                    socialId
                )
                signUpPresenter.signUpNewUser(profile)
                LoadingDialog.getInstance().show(this)
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }

            clearGMailSession()
        } catch (e: ApiException) {
            Log.w("TAG", "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun clearGMailSession() {
        googleApiClient!!.signOut().addOnCompleteListener { Log.e("Tag", "Clean login session") }
    }

    private fun validateSignUpDetails() {
        hideKeyboard(ll_sign_up)

        signUpValidation.addValidation(
            this,
            R.id.etFirstName,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_first_name
        )

        signUpValidation.addValidation(
            this,
            R.id.etLastName,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_last_name
        )

        signUpValidation.addValidation(
            this,
            R.id.etEmailAddress,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_email
        )

        signUpValidation.addValidation(
            this,
            R.id.etMobileNumber,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_mobile_number
        )

        val mobileNum = ".{8,}"

        signUpValidation.addValidation(
            this,
            R.id.etMobileNumber,
            mobileNum,
            R.string.error_valid_mob_num
        )

        signUpValidation.addValidation(
            this,
            R.id.etMobileNumber,
            RegexTemplate.TELEPHONE,
            R.string.error_valid_mob_num
        )

        signUpValidation.addValidation(
            this,
            R.id.etPassword,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_password
        )

        signUpValidation.addValidation(
            this,
            R.id.etConfirmPassword,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_confirm_password
        )

        signUpValidation.addValidation(
            this,
            R.id.etAddress,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_address
        )

        if (etEmailAddress.text.isNotEmpty()) {
            signUpValidation.addValidation(
                this,
                R.id.etEmailAddress,
                android.util.Patterns.EMAIL_ADDRESS,
                R.string.error_valid_email
            )
        }

        if (signUpValidation.validate()) {
            when {
                tvDateOfBirth.text.toString() == Constants.DATE_OF_BIRTH -> {
                    toast(getString(R.string.txt_select_date_of_birth))
                }
                gender == Constants.GENDER -> {
                    toast(getString(R.string.txt_select_gender))
                }
                etConfirmPassword.text.toString() != etPassword.text.toString() -> {
                    etConfirmPassword.error = getString(R.string.txt_password_not_match)
                }

                !chk_terms_cond.isChecked ->{
                    toast(getString(R.string.terms_conditions_string))
                }

                age!! < 18 -> {
                    toast("Minimum 18 years required to register")
                }
                mobileNumberVerified && emailVerified -> {
                    if (isNetworkAvailable(this)) {
                        val profile = SignUpUser(
                            etFirstName.text.toString(),
                            etLastName.text.toString(),
                            etEmailAddress.text.toString(),
                            etMobileNumber.text.toString(),
                            ccpSignUp.selectedCountryCode.toInt(),
                            etConfirmPassword.text.toString(),
                            etAddress.text.toString(),
                            gender,
                            tvDateOfBirth.text.toString(),
                            loginType,
                            socialId
                        )
                        signUpPresenter.signUpNewUser(profile)
                        LoadingDialog.getInstance().show(this)
                    } else {
                        toast(getString(R.string.txt_please_check_your_internet_connection))
                    }
                }
                else -> {
                    toast("Please verify your email id/mobile number.")
                }
            }
        }
    }

    override fun signUpNewUserSuccess(signUp: SignUp) {
        LoadingDialog.getInstance().cancel()
        if (signUp.errorcode == 1000) {
            startActivity(
                intentFor<MobileNumberActivity>(
                    Constants.SIGN_UP_ACCESS_TOKEN to signUp.token,
                    Constants.FROM_ACTIVITY to Constants.FROM_SOCIAL_SIGN_UP
                )
            )
        } else if (signUp.errorcode == 1001) {
            toast("Email already exists. Go to login page for login.")
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)

        }else if (signUp.errorcode == 0) {
                finish()
        } else {
            toast("Please Login again.")
        }
//        if (signUp.errorcode == 0) {
//            startActivity(
//                intentFor<MobileNumberActivity>(
//                    Constants.SIGN_UP_ACCESS_TOKEN to signUp.token,
//                    Constants.FROM_ACTIVITY to Constants.FROM_SOCIAL_SIGN_UP
//                )
//            )
//        } else if (signUp.msg == "Email already exists") {
//            toast("${signUp.msg} Go to login page for login.")
//        } else {
//            toast(signUp.msg.toString())
//            val intent = Intent(this, LoginActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//            startActivity(intent)
//        }
//        if (message.msg == "Email already exists") {
//            if (signUpMobNumVerified) {
//                toast("${message.msg} Go to login page for login.")
//            } else {
//                startActivity(
//                    intentFor<MobileNumberActivity>(
//                        Constants.SIGN_UP_ACCESS_TOKEN to userTokenKey,
//                        Constants.FROM_ACTIVITY to Constants.FROM_SOCIAL_SIGN_UP
//                    )
//                )
//            }
//        } else if (loginType == "gmail" || loginType == "facebook") {
//            userTokenKey = message.token
//            startActivity(
//                intentFor<MobileNumberActivity>(
//                    Constants.SIGN_UP_ACCESS_TOKEN to message.token,
//                    Constants.FROM_ACTIVITY to Constants.FROM_SOCIAL_SIGN_UP
//                )
//            )
//        } else {
//            toast(message.msg)
//            val intent = Intent(this, LoginActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//            startActivity(intent)
//        }
    }

    override fun signUpNewUserFailed(message: String) {
        LoadingDialog.getInstance().cancel()
        toast(message)
    }

    override fun signUpNewUserNoInternet() {
        LoadingDialog.getInstance().cancel()
        toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onResume() {
        super.onResume()
        if (isFromOnPause) {
            if (emailVerified) {
                tvSignUpEmailValidate.text = "Email is Validated"
            } else {
                tvSignUpEmailValidate.text = resources.getString(R.string.txt_validate_email)
            }
            if (mobileNumberVerified) {
                tvSignUpMobileNumberValidate.text = "Mobile Number is Validated"
            } else {
                tvSignUpMobileNumberValidate.text =
                    resources.getString(R.string.txt_validate_mobile_num)
            }
            isFromOnPause = false
        }
    }

    override fun onPause() {
        super.onPause()
        isFromOnPause = true
    }
}
