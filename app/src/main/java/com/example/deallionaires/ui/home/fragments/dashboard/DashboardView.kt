package com.example.deallionaires.ui.home.fragments.dashboard

import com.example.deallionaires.model.Dashboard

interface DashboardView {

    fun displayDashboardPoints(dashboard: Dashboard)

    fun noDashboardPoints()

    fun dashboardNoInternet()

}