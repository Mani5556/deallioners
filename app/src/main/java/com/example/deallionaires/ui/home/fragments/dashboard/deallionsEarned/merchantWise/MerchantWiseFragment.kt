package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.merchantWise

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.MerchantWiseDeallionsAdapter
import com.example.deallionaires.model.MerchantWiseDeallionsResponse
import com.example.deallionaires.model.Offervaluepoint
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.ui.searchBusinessItem.TransactionDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_merchant_wise.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class MerchantWiseFragment : Fragment(), MerchantWiseView {

    private lateinit var activity: Activity
    private lateinit var merchantWisePresenter: MerchantWisePresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_merchant_wise, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        merchantWisePresenter = MerchantWisePresenter(activity, this)
        if (isNetworkAvailable(activity)) {
            merchantWisePresenter.getMerchantWiseDeallions(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            LoadingDialog.getInstance().cancel()
        }

    }

    override fun onGettingMerchantWiseDeallionsSuccess(merchantWiseDeallionsResponse: MerchantWiseDeallionsResponse) {
        LoadingDialog.getInstance().cancel()
        if (merchantWiseDeallionsResponse.offervaluepoints.isNullOrEmpty()) {
            activity.tvNoMerchantWiseDeallionairesFound.visibility = View.VISIBLE
            activity.rvMerchantWiseDeallions.visibility = View.GONE
        } else {
            activity.tvNoMerchantWiseDeallionairesFound.visibility = View.GONE
            activity.rvMerchantWiseDeallions.visibility = View.VISIBLE
            activity.rvMerchantWiseDeallions.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            activity.rvMerchantWiseDeallions.adapter =
                MerchantWiseDeallionsAdapter(
                    activity,
                    merchantWiseDeallionsResponse.applevelpoints!!.app_level_loyalty_points!!,
                    merchantWiseDeallionsResponse.offervaluepoints as ArrayList<Offervaluepoint>
                ) {
//                    activity.startActivity(activity.intentFor<SearchBusinessDetailsActivity>(Constants.MERCHANT_ID to it))
                    activity.startActivity(activity.intentFor<TransactionDetailsActivity>(Constants.MERCHANT_ID to it))
                }
        }
    }

    override fun onGettingMerchantWiseDeallionsFail(message: String) {
        LoadingDialog.getInstance().cancel()
    }

    override fun onGettingMerchantWiseNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

}
