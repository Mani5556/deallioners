package com.example.deallionaires.ui.home.fragments.dashboard

interface DashboardPresenter {

    fun getDashboardPoints(userTokenKey: String)

    fun dashboardDestroy()

}