package com.example.deallionaires.ui.home.fragments.home

import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.ViewFlipper
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.constraintlayout.motion.widget.MotionScene
import androidx.core.graphics.ColorUtils
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ethanhua.skeleton.RecyclerViewSkeletonScreen
import com.ethanhua.skeleton.Skeleton
import com.example.deallionaires.R
import com.example.deallionaires.adapters.AllCategoriesAdapter
import com.example.deallionaires.adapters.DealsCategoryAdapter
import com.example.deallionaires.adapters.HomeCategoriesAdapter
import com.example.deallionaires.adapters.HomeDealsAdapter
import com.example.deallionaires.model.*
import com.example.deallionaires.ui.dealsStatus.pendingDeals.PendingDealsFragment
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.home.fragments.cart.CartFragment
import com.example.deallionaires.ui.home.fragments.home.homeCategoryDeals.HomeCategoryDealsFragment
import com.example.deallionaires.ui.home.fragments.home.homeDeals.HomeViewAllFragment
import com.example.deallionaires.ui.searchBusiness.SearchBusinessFragment
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.ui.searchDealItem.SearchDealItemFragment
import com.example.deallionaires.utils.*
import kotlinx.android.synthetic.main.dialog_all_categories.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.rvCategories
import kotlinx.android.synthetic.main.fragment_home.rvHomeDeals
import kotlinx.android.synthetic.main.fragment_search_deals.*
import org.jetbrains.anko.AlertBuilder
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment(), HomeView,HomeCallBacks{

    private var skeletonScreen: RecyclerViewSkeletonScreen? = null
    private var allCategoriesDialog: AlertDialog? = null
    private lateinit var activity: Activity

    private lateinit var  homePresenter:HomePresenter

    lateinit var tempActivity: HomeActivity

    private var vfBannerImages: ViewFlipper? = null

    lateinit var homeDataAdapter:DealsCategoryAdapter;
     var locationUpdateReceiver:BroadcastReceiver? = null
     var pendingDealsUpdatedReciever:BroadcastReceiver? = null

    var bannerImages = intArrayOf(
        R.drawable.ic_limited_offer,
        R.drawable.ic_no_brain_deal,
        R.drawable.deallionaires_logo
    )


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        vfBannerImages = view.findViewById(R.id.vfBannerImages)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homePresenter = HomePresenterImpl(context!!,this)
        activity = getActivity()!!

        tempActivity = requireActivity() as HomeActivity

        val userSkipLogin by PreferenceExt.appPreference(
            activity,
            Constants.USER_SKIP_LOGIN,
            false
        )

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        var userLocationLatitude by PreferenceExt.appPreference(
            context!!,
            Constants.USER_LOCATION_LATITUDE,
            Constants.DEFAULT_STRING
        )

        var userLocationLongitude by PreferenceExt.appPreference(
            context!!,
            Constants.USER_LOCATION_LONGITUDE,
            Constants.DEFAULT_STRING
        )


        for (element in bannerImages) {
            // create the object of ImageView
            val imageView = ImageView(activity)
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.setImageResource(element) // set image in ImageView
            vfBannerImages!!.addView(imageView) // add the created ImageView in ViewFlipper
        }

        val animationIn: Animation =
            AnimationUtils.loadAnimation(activity, android.R.anim.slide_in_left)
        val animationOut: Animation =
            AnimationUtils.loadAnimation(activity, android.R.anim.slide_out_right)

        vfBannerImages!!.inAnimation = animationIn
        vfBannerImages!!.outAnimation = animationOut
        vfBannerImages!!.flipInterval = 3000
        vfBannerImages!!.isAutoStart = true

        homePresenter.getCategories(userTokenKey, activity,userLocationLatitude,userLocationLongitude)

        activity.tvNoBrainDealsSeeMore.setOnClickListener {
            if (userSkipLogin) {
                activity.toast("Please Login/SignUp")
            } else {
                tempActivity.loadFragmentInHomeActivity(
                    Constants.CART,
                    CartFragment(),
                    false,
                    Constants.FROM_HOME_FRAGMENT
                )
            }
        }


        locationUpdateReceiver = object : BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                showSkeleton()
                homePresenter.getCategories(userTokenKey, activity,userLocationLatitude,userLocationLongitude)
            }
        }
        pendingDealsUpdatedReciever =  object : BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                showSkeleton()
                homePresenter.getCategories(userTokenKey, activity,userLocationLatitude,userLocationLongitude)
            }
        }

        locationUpdateReceiver?.let {
            LocalBroadcastManager.getInstance(context!!).registerReceiver(it,
                IntentFilter(Constants.ACTION_LOCATION_UPDATE_RECEIVER)
            )
        }

        pendingDealsUpdatedReciever?.let {
            LocalBroadcastManager.getInstance(context!!).registerReceiver(it,
                IntentFilter(Constants.ACTION_PENDING_DEALS_UPDATED)
            )
        }

        activity.findViewById<View>(R.id.lyt_search).setOnClickListener {
            (activity as HomeActivity).openSearchFragment()
        }

        activity.rvHomeDeals.layoutManager =
            LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        homeDataAdapter = DealsCategoryAdapter(activity, HashMap(),this)

        activity.rvHomeDeals.adapter = homeDataAdapter
        skeletonScreen = Skeleton.bind(activity.rvHomeDeals)
            .adapter(homeDataAdapter)
            .load(R.layout.home_skeleton)
            .color(R.color.shimmer_bg)
            .duration(1500)
            .show()

        activity.rvHomeDeals.isNestedScrollingEnabled = false

//        activity.findViewById<NestedScrollView>(R.id.lyt_nested_scroll).setOnScrollChangeListener(object :NestedScrollView.OnScrollChangeListener{
//            override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
////              swipe_refresh.isEnabled =  scrollY == 0
//            }
//
//        })
        tempActivity.showUserLocation()
        tempActivity.showUserName()

    }

    override fun onGetCategoriesSuccess(categories: ArrayList<Categories>) {

        activity.iv_more_categories.setOnClickListener {
          showAllCategoriesDialog(categories)
        }

        activity.rvCategories.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)

        activity.rvCategories.adapter = HomeCategoriesAdapter(activity, categories) {it,position ->

            val searchBusinessFragment = SearchBusinessFragment().apply { arguments =
                Bundle().apply {
                    putInt(Constants.SELECTED_CATEGORY_POSITION,position)
                }

            }
            tempActivity.loadCatagoriesFragment(searchBusinessFragment)
        }

    }


    override fun onGetCategoriesFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(message)
    }

    override fun onGetCategoriesNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError(context!!.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onGetLandingPageDataSuccess(landingPageList: Map<String, List<Any>>) {
        val objectsByYear: HashMap<String, ArrayList<Any>> = landingPageList as HashMap<String, ArrayList<Any>>
        if(objectsByYear.isEmpty()){
            showError("No data found for this Location, Please change location")
        }else{
            homeDataAdapter.updateList(objectsByYear)
            activity.rvHomeDeals.isNestedScrollingEnabled = false
            showData()
        }

    }

    override fun onGetLandingPageDataFail(message: String) {
//        LoadingDialog.getInstance().cancel()
        showError(message)
    }

    override fun onGetLandingPageDataNoInternet() {
//        LoadingDialog.getInstance().cancel()
        showError("No internet connection")

    }

    override fun showHomeFragmentProgress() {
        LoadingDialog.getInstance().show(activity)
    }

    override fun hideHomeFragmentProgress() {
        LoadingDialog.getInstance().cancel()
    }

    override fun onAddingDealToFavouritesSuccess(
        favouriteAdded: FavouriteAdded,
        position: Int,
        parentAdapterPosition: Int
    ) {
        LoadingDialog.getInstance().cancel()
        homeDataAdapter.updateItem(parentAdapterPosition,position,1)
    }

    override fun onAddingDealToFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
        context?.toast(message)
    }

    override fun onRemovingDealToFavouritesSuccess(
        favouriteRemoved: FavouriteRemoved,
        position: Int,
        parentAdapterPosition: Int
    ) {
        LoadingDialog.getInstance().cancel()
        homeDataAdapter.updateItem(parentAdapterPosition,position,0)
    }

    override fun onRemovingDealToFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
        context?.toast(message)
    }


    private fun showError(message: String){
        llNoBrainDeals.visibility = View.VISIBLE
        rvHomeDeals.visibility = View.GONE
        activity.findViewById<TextView>(R.id.tv_no_data_found).text = message
        skeletonScreen?.hide()
    }

    private fun showData(){
        skeletonScreen?.hide()
        llNoBrainDeals.visibility = View.GONE
        rvHomeDeals.visibility = View.VISIBLE
    }

    private fun showSkeleton(){
        llNoBrainDeals.visibility = View.GONE
        rvHomeDeals.visibility = View.VISIBLE
        skeletonScreen?.show()
    }

    fun showAllCategoriesDialog(categories: ArrayList<Categories>){
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_all_categories,null,false)
        view.iv_cancel.setOnClickListener {
            allCategoriesDialog?.cancel()
        }
        view.tv_all_categories.run {
            adapter = AllCategoriesAdapter(context, categories) { categories, position ->
                allCategoriesDialog?.cancel()
                val mainCategoryName = categories.category_name
                val categorySlug = categories.slug
                val searchBusinessFragment = SearchBusinessFragment().apply {
                    arguments =
                        Bundle().apply {
                            putInt(Constants.SELECTED_CATEGORY_POSITION, position)
                        }
                }
                tempActivity.loadCatagoriesFragment(searchBusinessFragment)

            }

            layoutManager = GridLayoutManager(context, 3)
        }

        val bulder:AlertDialog.Builder = AlertDialog.Builder(context).apply {
            setView(view)

        }

        allCategoriesDialog = bulder.create()
        allCategoriesDialog?.window?.setBackgroundDrawableResource(android.R.color.transparent);
        allCategoriesDialog?.show()
    }

    override fun viewAllDealsClicked(viewAllCateogory:String) {
        tempActivity.initToolbarHomeViews(false)
        tempActivity.loadFragment(viewAllCateogory.replace("_"," ").capitalize(),HomeViewAllFragment().apply {
            arguments = Bundle().apply {
                putInt("type",ITEM_TYPE_DEAL)
                putString("key",viewAllCateogory)
            }
        })
//        tempActivity.loadFragmentInHomeActivity(
//            viewAllCateogory?.replace("_"," ")!!.capitalize(),
//            HomeViewAllFragment().apply {
//                arguments = Bundle().apply {
//                    putInt("type",ITEM_TYPE_DEAL)
//                    putString("key",viewAllCateogory)
//                }
//            },
//            false,
//            Constants.FROM_HOME_FRAGMENT
//        )
    }

    override fun viewAllBusinessClicked(viewAllCateogory:String) {
        tempActivity.initToolbarHomeViews(false)
        tempActivity.loadFragment(viewAllCateogory.replace("_"," ").capitalize()    ,    HomeViewAllFragment().apply {
            arguments = Bundle().apply {
                putInt("type",ITEM_TYPE_MERCHANT)
                putString("key",viewAllCateogory)
            }
        })

//        tempActivity.loadFragmentInHomeActivity(
//            viewAllCateogory?.replace("_"," ")!!.capitalize(),
//            HomeViewAllFragment().apply {
//                arguments = Bundle().apply {
//                    putInt("type",ITEM_TYPE_MERCHANT)
//                    putString("key",viewAllCateogory)
//                }
//            },
//            false,
//            Constants.FROM_HOME_FRAGMENT
//        )
    }

    override fun viewAllPendingDealsClicked(viewAllCateogory: String) {
        (activity as HomeActivity).openPendingDealsFragment()
    }

    override fun dealClicked(dealId: Int) {
        activity.startActivity(
            activity.intentFor<SearchDealDetailsActivity>(
                Constants.DEAL_ID to dealId,
                Constants.FROM_ACTIVITY to Constants.FROM_SPECIAL_DEALS
            )
        )
    }

    override fun businessItemClicked(murchentId: Int) {
        activity.startActivity(
            activity.intentFor<SearchBusinessDetailsActivity>(
                Constants.MERCHANT_ID to murchentId
            )
        )
    }

    override fun pendingDealClickked(dealId: Int?) {
        (activity as HomeActivity).openPendingDealsFragment()
    }

    override fun likeClicked(isLiked: Int,dealId: Int, position: Int,parentAdapterPosition: Int) {
        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        LoadingDialog.getInstance().show(context!!)

        if(isLiked == 1){
            homePresenter.addDealToFavourites(userTokenKey,dealId,position,parentAdapterPosition)
        }else{
            homePresenter.removeDealFromFavourites(userTokenKey,dealId,position,parentAdapterPosition)
        }
    }

    override fun onDetach() {
        super.onDetach()
        locationUpdateReceiver?.let {
            LocalBroadcastManager.getInstance(context!!).unregisterReceiver(it)
        }
        pendingDealsUpdatedReciever?.let {
            LocalBroadcastManager.getInstance(context!!).unregisterReceiver(it)
        }
        homePresenter.onDetach()
    }

}


interface HomeCallBacks{
    fun viewAllDealsClicked(viewAllCateogory:String)
    fun viewAllBusinessClicked(viewAllCateogory:String)
    fun viewAllPendingDealsClicked(viewAllCateogory:String)
    fun dealClicked(dealId:Int)
    fun businessItemClicked(murchentId : Int)
    fun pendingDealClickked(dealId: Int?)
    fun likeClicked(isLiked: Int,dealId:Int, position:Int,parentAdapterPosition:Int)

}