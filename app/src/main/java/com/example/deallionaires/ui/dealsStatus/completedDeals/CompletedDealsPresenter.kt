package com.example.deallionaires.ui.dealsStatus.completedDeals

interface CompletedDealsPresenter {

    fun getGetCompletedDealsPresenter(tokenKey: String)

    fun destroyCompletedDeals()

}