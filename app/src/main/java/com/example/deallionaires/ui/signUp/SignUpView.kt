package com.example.deallionaires.ui.signUp

import com.example.deallionaires.model.SignUp

interface SignUpView {


    fun signUpNewUserSuccess(signUp: SignUp)

    fun signUpNewUserFailed(message: String)

    fun signUpNewUserNoInternet()

}