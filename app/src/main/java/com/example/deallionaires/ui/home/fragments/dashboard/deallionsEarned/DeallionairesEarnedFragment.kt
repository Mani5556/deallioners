package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.deallionaires.R
import com.example.deallionaires.adapters.DeallionsEarnedPageAdapter
import com.example.deallionaires.adapters.ViewPagerAdapter
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.merchantWise.MerchantWiseFragment
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise.TransactionWiseFragment
import kotlinx.android.synthetic.main.fragment_deallionaires_earned.*

/**
 * A simple [Fragment] subclass.
 */
class DeallionairesEarnedFragment : Fragment() {

    lateinit var activity: Activity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deallionaires_earned, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = getActivity()!!

        val adapter = ViewPagerAdapter(requireActivity().supportFragmentManager)
        adapter.addFragment(MerchantWiseFragment(), "Merchant Wise")
//        adapter.addFragment(TransactionWiseFragment(), "Transaction Wise")

        activity.vpDeallionsEarned.adapter = adapter
        activity.tlDeallionsEarned.setupWithViewPager(activity.vpDeallionsEarned)

//        activity.vpDeallionsEarned.adapter =
//            DeallionsEarnedPageAdapter(activity, requireActivity().supportFragmentManager)
//        activity.tlDeallionsEarned.setupWithViewPager(vpDeallionsEarned)
    }

}
