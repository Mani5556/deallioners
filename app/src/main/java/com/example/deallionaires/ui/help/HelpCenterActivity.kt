package com.example.deallionaires.ui.help

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.example.deallionaires.R
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.NetworkState
import com.example.deallionaires.utils.PreferenceExt
import kotlinx.android.synthetic.main.activity_help_center.*
import kotlinx.android.synthetic.main.activity_help_support.*
import kotlinx.android.synthetic.main.activity_help_support.toolbar

class HelpCenterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_center)
        toolbar.setNavigationOnClickListener { finish() }

        val userTokenKey by PreferenceExt.appPreference(
            this,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        val viewModel = ViewModelProviders.of(this).get(HelpCenterViewModel::class.java)
        viewModel.helpResponse.observe(this, Observer {
           showSuccessMessage(it)
        })

        viewModel.networkCall.observe(this, Observer {
            when(it){
                NetworkState.ERROR->{
                    LoadingDialog.getInstance().cancel()
                    showErrorMessage("Sorry your querry not submitted Successfully, Please try again later")
                }
                NetworkState.COMPLETED->{
                    LoadingDialog.getInstance().cancel()
                }
                NetworkState.IN_PROGRESS->{
                    LoadingDialog.getInstance().show(this)
                }
                NetworkState.NO_NETWORK->{
                    LoadingDialog.getInstance().cancel()
                    showErrorMessage("No network connection")
                }
            }
        })



        btn_submit.setOnClickListener {
            val subject = et_subject.text.toString().trim()
            val description  = et_description.text.toString().trim()

            if(subject.isEmpty()){
                et_subject.setError("Please enter the subject")
                return@setOnClickListener
            }
            if(description.isEmpty()){
                et_description.setError("Please enter the subject")
                return@setOnClickListener
            }

            viewModel.submitQuerry(userTokenKey,subject,description)

        }


    }

    private fun showErrorMessage(message:String){
        lyt_body.visibility = View.GONE
        lyt_error.visibility = View.VISIBLE
        btn_submit.visibility = View.GONE
        tv_no_data_found.text = message
        img_error.setImageResource(R.drawable.ic_no_data)
    }
    private fun showSuccessMessage(message: String){
        lyt_body.visibility = View.GONE
        lyt_error.visibility = View.VISIBLE
        btn_submit.visibility = View.GONE
        tv_no_data_found.text = message
        img_error.setImageResource(R.drawable.ic_thumb_up_button)
    }
}