package com.example.deallionaires.ui.resetPassword

import android.content.Context
import com.example.deallionaires.model.ResetPassword
import com.example.deallionaires.model.ResetPasswordResponse
import com.example.deallionaires.network.APIRequests.getResetPassword
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class ResetPasswordPresenter(
    private val context: Context,
    private var resetPasswordView: ResetPasswordView?
) {
    fun resetNewPassword(userTokenKey: String, resetPassword: ResetPassword) {
        if(isNetworkAvailable(context)){
            getResetPassword(userTokenKey, resetPassword, object : NWResponseCallback<ResetPasswordResponse?>{
                override fun onSuccess(
                    call: Call<ResetPasswordResponse?>,
                    response: Response<ResetPasswordResponse?>
                ) {
                    resetPasswordView!!.onResetPasswordSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<ResetPasswordResponse?>,
                    response: Response<ResetPasswordResponse?>
                ) {
                   resetPasswordView!!.onResetPasswordFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<ResetPasswordResponse?>,
                    response: Response<ResetPasswordResponse?>
                ) {
                    resetPasswordView!!.onResetPasswordFail(response.message())
                }

                override fun onFailure(call: Call<ResetPasswordResponse?>, t: Throwable) {
                    resetPasswordView!!.onResetPasswordFail(t.message!!)
                }
            })
        }else{
            resetPasswordView!!.onResetPasswordNoInternet()
        }
    }
}