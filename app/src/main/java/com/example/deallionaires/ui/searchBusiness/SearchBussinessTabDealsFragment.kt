package com.example.deallionaires.ui.searchBusiness

import android.opengl.Visibility
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.Toast

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.deallionaires.R
import com.example.deallionaires.adapters.BusinessDealReviewsAdapter
import com.example.deallionaires.adapters.FilterAdapter
import com.example.deallionaires.adapters.SearchBusinessItemsAdapter
import com.example.deallionaires.adapters.SearchDealsItemsAdapter
import com.example.deallionaires.model.*

import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsPresenterImpl
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsView
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.fragment_search_business.*
import kotlinx.android.synthetic.main.list_searchbussiness_tabs.*
import kotlinx.android.synthetic.main.list_searchbussiness_tabs.view.*
import kotlinx.android.synthetic.main.subcategories_list.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast


class SearchBusinessTabsFragment(val listData: ArrayList<*>, val fragmentType: Int,val marchentId:Int) :
    Fragment(), SearchBusinessDetailsView {

    private lateinit var businessReviewsAdapter: BusinessDealReviewsAdapter
    private var selectedBrachPosition: Int = -1
    private lateinit var searchBusinessItemsAdapter: SearchBusinessItemsAdapter
    private lateinit var searchBusinessDetailsPresenter: SearchBusinessDetailsPresenterImpl
    var tokenKey:String? = null



    companion object {
        val FRAGMENT_TYPE_DEALS: Int = 1
        val FRAGMENT_TYPE_BRANCHES: Int = 2
        val FRAGMENT_TYPE_REVIEWS: Int = 3
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_searchbussiness_tabs, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userTokenKey by PreferenceExt.appPreference(
            activity!!,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        tokenKey = userTokenKey
        searchBusinessDetailsPresenter = SearchBusinessDetailsPresenterImpl(context!!, this)
        view.rv_common_list.run {
            layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        }


        when (fragmentType) {

            FRAGMENT_TYPE_DEALS -> {
                bindSearchDealsData(view, listData as ArrayList<SearchDealsCategoryItems>)
            }
            FRAGMENT_TYPE_BRANCHES -> {
                bindBranchesData(view)
            }
            FRAGMENT_TYPE_REVIEWS -> {
                bindReviewaData(view)
            }
        }

    }

    private fun bindBranchesData(view: View) {

        val userTokenKey by PreferenceExt.appPreference(
            context!!,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        if (listData.isEmpty()) {
//                scbBusinessDetail.rating = 0f
            view.tv_no_data.visibility = View.VISIBLE
            view.tv_no_data.text = "No Branches found"
            view.rv_common_list.visibility = View.GONE
        } else {
            view.tv_no_data.visibility = View.GONE
//                llAddReview.visibility = View.GONE
            view.rv_common_list.visibility = View.VISIBLE
            searchBusinessItemsAdapter = SearchBusinessItemsAdapter(
                context!!,
                listData as ArrayList<Searchbusiness>
            ) { searchbusinessFromAdapter: Searchbusiness, isFavouriteClicked: Boolean, isLiked: Int, position: Int ->

                selectedBrachPosition = position

                if (isFavouriteClicked) {
                    if (isLiked == 1) {
                        searchBusinessDetailsPresenter.removeDealFromFavourites(
                            userTokenKey,
                            searchbusinessFromAdapter.merchantid
                        )
                    } else {
                        searchBusinessDetailsPresenter.addDealToFavourites(
                            userTokenKey,
                            searchbusinessFromAdapter.merchantid
                        )
                    }
                } else {
                    activity?.startActivity(
                        activity?.intentFor<SearchBusinessDetailsActivity>(
                            Constants.MERCHANT_ID to searchbusinessFromAdapter.merchantid
                        )
                    )
                }
            }

            view.rv_common_list.adapter = searchBusinessItemsAdapter

        }

    }

    private fun bindSearchDealsData(view: View,dealsList:ArrayList<SearchDealsCategoryItems>?) {
        if (dealsList.isNullOrEmpty()) {
//                scbBusinessDetail.rating = 0f
            view.tv_no_data.visibility = View.VISIBLE
            view.tv_no_data.text = "No Deals Found"
            view.rv_common_list.visibility = View.GONE
        } else {
            view.tv_no_data.visibility = View.GONE
            view.lyt_search.visibility = View.VISIBLE
//                llAddReview.visibility = View.GONE
            view.rv_common_list.visibility = View.VISIBLE
            view.rv_common_list.adapter = SearchDealsItemsAdapter(
                activity!!,
                dealsList,
                true
            ) {
                startActivity(activity?.intentFor<SearchDealDetailsActivity>(Constants.DEAL_ID to it, Constants.FROM_ACTIVITY to Constants.FROM_SEARCH_DEALS)
                )
            }

            view.ed_search.setOnClickListener {
               showFilterPopup(it)
            }

        }
    }

    private fun bindReviewaData(view: View) {

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        if (listData.isEmpty()) {
//                scbBusinessDetail.rating = 0f
            view.tv_no_data.visibility = View.VISIBLE
            view.tv_no_data.text = "No Reviews yet! Add your review"
            view.rv_common_list.visibility = View.GONE
        } else {
            view.tv_no_data.visibility = View.GONE
//                llAddReview.visibility = View.GONE
            view.rv_common_list.visibility = View.VISIBLE

            businessReviewsAdapter = BusinessDealReviewsAdapter(context!!, listData as ArrayList<Viewreviewrating>) {}
            view.rv_common_list.adapter = businessReviewsAdapter
        }


        view.btn_submit_rating.setOnClickListener {
            Log.e("My rating ", view.rtbar.rating.toString())
            when {

                view.rtbar.rating == 0f -> {
                    context?.toast("Please give some rating.")
                }
                view.tv_review_message.text.toString().isEmpty() -> {
                    context?.toast("Review text cannot be empty")
                }
                else -> {
                    val addReviewRating = AddReviewRating(
                        marchentId.toString(),
                        view.tv_review_message.text.toString().trim(),
                        view.rtbar.rating.toString()
                    )

                    LoadingDialog.getInstance().show(context!!)
                    searchBusinessDetailsPresenter.addSearchBusinessReviewTaring(
                        tokenKey!!,
                        addReviewRating
                    )
                }
            }
        }

        view.tv_write_review.apply {
            visibility = View.VISIBLE
            text = Html.fromHtml("<u>Write a review</u>")
            setOnClickListener {
                view.lyt_review_submit.visibility = View.VISIBLE
                view.lyt_list.visibility = View.GONE
            }
        }


//        tvSubmitBusinessDetailReview.setOnClickListener {
//

    }

    override fun onGetSearchBusinessDetailsSuccess(searchBusinessItemDetail: SearchBusinessItemDetail) {
        val searchBusinessReviews = searchBusinessItemDetail.viewreviewrating as ArrayList<Viewreviewrating>

        (listData as ArrayList<Viewreviewrating>).apply {
            clear()
            addAll(searchBusinessReviews)
        }
        LoadingDialog.getInstance().cancel()
        bindReviewaData(view!!)
//        businessReviewsAdapter.notifyDataSetChanged()
    }

    override fun onGetSearchBusinessDetailsFail(message: String) {}

    override fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded) {
        LoadingDialog.getInstance().cancel()
        activity?.toast("Added to favorites")
        (listData as ArrayList<Searchbusiness>).get(selectedBrachPosition).isLiked = 1
        searchBusinessItemsAdapter.notifyItemChanged(selectedBrachPosition)
    }

    override fun onAddingDealToFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity?.toast("Some thing went wrong")
    }

    override fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved) {
        LoadingDialog.getInstance().cancel()
        activity?.toast("Removed from favorites")
        (listData as ArrayList<Searchbusiness>).get(selectedBrachPosition).isLiked = 0
        searchBusinessItemsAdapter.notifyItemChanged(selectedBrachPosition)
    }

    override fun onRemovingDealToFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity?.toast(message)
    }

    override fun onAddingSearchBusinessReviewSuccess(reviewAdded: ReviewAdded) {
        searchBusinessDetailsPresenter.getSearchBusinessDetailsResponse(
            tokenKey!!,
            marchentId
        )
        context?.toast(reviewAdded.msg.toString())
        view?.lyt_review_submit?.visibility = View.GONE
        view?.lyt_list?.visibility = View.VISIBLE
    }

    override fun onAddingSearchBusinessReviewFail(message: String) {
        LoadingDialog.getInstance().cancel()
        context?.toast(message)
    }

    override fun onGetSearchBusinessDetailsNoInternet() {

    }

    override fun filterResultsSuccess(filteredList:List<SearchDealsCategoryItems>?) {
      bindSearchDealsData(view!!, filteredList as ArrayList<SearchDealsCategoryItems>?)
    }

    override fun filterResultsFail(message: String) {
       context?.toast(message)
    }

    private fun showFilterPopup(anchorView: View) {
        val filterList = arrayListOf<String>("All","In house deals","Exclusive deals","Customer category based deals","Special Deals")
        val dropDownRecyclerView: View =
            layoutInflater.inflate(R.layout.subcategories_list, null, false)
        dropDownRecyclerView.lyt_top.visibility = View.VISIBLE
        dropDownRecyclerView.tv_sort_type.text = "Filter By"

        val myBuilder: SimpleTooltip = SimpleTooltip.Builder(context)
            .anchorView(anchorView)
            .margin(10f)
            .contentView(dropDownRecyclerView, -1)
            .text("Texto do Tooltip")
            .gravity(Gravity.BOTTOM)
            .focusable(false)
            .dismissOnInsideTouch(false)
            .margin(-10f)
            .ignoreOverlay(true)
            .arrowColor(resources.getColor(R.color.colorPrimary))
            .transparentOverlay(false)
            .build()

        dropDownRecyclerView.rv_subcategories.run {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

            dropDownRecyclerView.rv_subcategories.adapter =
                FilterAdapter(activity!!, filterList) { position->
                    myBuilder.dismiss()
//                    activity?.tvSearchDealsSortBy?.visibility = View.VISIBLE
                    activity!!.ed_search.text = filterList[position]

                    if(isNetworkAvailable(activity!!)){
                        searchBusinessDetailsPresenter.getDealseByFilter(tokenKey!!, marchentId,position)
                    }else{
                        activity!!.toast(activity!!.resources.getString(R.string.txt_please_check_your_internet_connection))
                    }

                }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dropDownRecyclerView.elevation = 10f
        }

        try {
            myBuilder.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }
}
