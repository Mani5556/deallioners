package com.example.deallionaires.ui.home.fragments.home.searchHomeDeals

import android.content.Context
import com.example.deallionaires.model.SearchHomeDealsResponse
import com.example.deallionaires.network.APIRequests.getSearchHomeDeals
import com.example.deallionaires.network.APIRequests.getSearchHomeDealsAtLocation
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SearchHomeDealsPresenter(
    private val context: Context,
    private var searchHomeDealsView: SearchHomeDealsView?
) {

    fun getDealsAtUserSearchedLocation(userAccessKey: String, userLag: Double, userLat: Double, searchQuery: String) {
        if (isNetworkAvailable(context)) {
            getSearchHomeDealsAtLocation(
                userAccessKey,
                userLag,
                userLat,
                searchQuery,
                object : NWResponseCallback<SearchHomeDealsResponse?> {
                    override fun onSuccess(
                        call: Call<SearchHomeDealsResponse?>,
                        response: Response<SearchHomeDealsResponse?>
                    ) {
                        if(response.body()!!.success!!){
                            searchHomeDealsView?.onGettingSearchHomeDealsSuccess(response.body()!!)
                        }else{
                            searchHomeDealsView?.onGettingSearchHomeDealsFail(null)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<SearchHomeDealsResponse?>,
                        response: Response<SearchHomeDealsResponse?>
                    ) {
                        searchHomeDealsView?.onGettingSearchHomeDealsFail(null)
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<SearchHomeDealsResponse?>,
                        response: Response<SearchHomeDealsResponse?>
                    ) {
                        searchHomeDealsView?.onGettingSearchHomeDealsFail(null)
                    }

                    override fun onFailure(call: Call<SearchHomeDealsResponse?>, t: Throwable) {
                        searchHomeDealsView?.onGettingSearchHomeDealsFail(t.message!!)
                    }
                })
        } else {
            searchHomeDealsView?.onGettingSearchHomeDealsNoInternet()
        }
    }

    fun getUserHomeDeals(userAccessKey: String,searchQuery: String) =
        if (isNetworkAvailable(context)) {
            getSearchHomeDeals(
                userAccessKey,
                searchQuery,
                object : NWResponseCallback<SearchHomeDealsResponse?> {
                    override fun onSuccess(
                        call: Call<SearchHomeDealsResponse?>,
                        response: Response<SearchHomeDealsResponse?>
                    ) {
                        if(response.body()?.success?:false){
                            searchHomeDealsView?.onGettingSearchHomeDealsSuccess(response.body()!!)
                        }else{
                            searchHomeDealsView?.onGettingSearchHomeDealsFail("some thing went wrong")
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<SearchHomeDealsResponse?>,
                        response: Response<SearchHomeDealsResponse?>
                    ) {
                        searchHomeDealsView?.onGettingSearchHomeDealsFail(null)
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<SearchHomeDealsResponse?>,
                        response: Response<SearchHomeDealsResponse?>
                    ) {
                        searchHomeDealsView?.onGettingSearchHomeDealsFail(null)
                    }

                    override fun onFailure(call: Call<SearchHomeDealsResponse?>, t: Throwable) {
                        searchHomeDealsView?.onGettingSearchHomeDealsFail(t.message!!)
                    }
                })
        } else {
            searchHomeDealsView?.onGettingSearchHomeDealsNoInternet()
        }


    fun onDetach(){
        searchHomeDealsView = null
    }
}