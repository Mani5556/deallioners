package com.example.deallionaires.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.deallionaires.R
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.PreferenceExt
import org.jetbrains.anko.intentFor

class SplashScreenActivity : AppCompatActivity() {

    private val isUserLogin by PreferenceExt.appPreference(this, Constants.IS_USER_LOGIN, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed(
            {
                if(isUserLogin){
                    startActivity(intentFor<HomeActivity>(Constants.FROM_ACTIVITY to Constants.FROM_SPLASH_SCREEN))
                }else {
                    startActivity(intentFor<LoginActivity>())
                }
                finish()
            }, 3000
        )

    }
}
