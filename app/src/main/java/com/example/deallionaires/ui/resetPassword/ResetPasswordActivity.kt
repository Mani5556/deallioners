package com.example.deallionaires.ui.resetPassword

import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.deallionaires.R
import com.example.deallionaires.model.ResetPassword
import com.example.deallionaires.model.ResetPasswordResponse
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.RC4
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.jetbrains.anko.toast
import java.io.ObjectOutputStream
import java.net.Socket
import java.security.Key
import java.security.NoSuchAlgorithmException
import java.security.spec.InvalidKeySpecException
import java.security.spec.KeySpec
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec

class ResetPasswordActivity : AppCompatActivity(), ResetPasswordView {

    private val initializationVector = "8119745113154120"
    private val cypherInstance = "RC4/ECB/NoPadding"
    private val pswdIterations = 10
    private val keySize = 256
    private val secretKeyInstance = "PBKDF2WithHmacSHA1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        val accessToken = intent.getStringExtra(Constants.FORGOT_PASSWORD_ACCESS_TOKEN)!!

        tvResetPasswordBack.setOnClickListener {
            onBackPressed()
        }

        Log.e("Access Token", accessToken)
        Log.e("Access Token reversed", accessToken.reversed())

//        var even = ""
//        val array = accessToken.toCharArray()
//        for ((count, ch) in array.withIndex()) {
//            if (count % 2 != 0) {
//                even += ch
//            }
//        }
//
//        Log.e("Even Token", even)
//
//        Log.e("Reverse Token", accessToken.reversed())

//        val encryptedToken = RC4(even).rc4(accessToken)
//
//        val encryptedPassword =
//            RC4(Constants.DEALLIONS_SECRET_KEY).rc4(etResetConfirmPassword.text.toString())
//
//        val encryptedActualToken = RC4(Constants.DEALLIONS_SECRET_KEY).rc4(accessToken.reversed())
//
//        Log.e("Response Cipher Key", encryptedToken.toString())

//        val encryptedToken = encrypt(accessToken, even)
//        val encryptedPassword =
//            encrypt(etResetConfirmPassword.text.toString(), Constants.DEALLIONS_SECRET_KEY)
//        val encryptedActualToken = encrypt(accessToken.reversed(), Constants.DEALLIONS_SECRET_KEY)


//        Log.e("Reset Password", "Encrypted token $encryptedToken")
//        Log.e("Reset Password", "Encrypted Password $encryptedPassword")
//        Log.e("Reset Password", "Encrypted Actual token $encryptedActualToken")

        tvResetPasswordSubmit.setOnClickListener {
            if (etResetPassword.text.isNullOrEmpty()) {
                etResetPassword.error = getString(R.string.txt_enter_password)
            } else if (etResetConfirmPassword.text.isNullOrEmpty()) {
                etResetConfirmPassword.error = getString(R.string.txt_enter_confirm_password)
            } else if (etResetPassword.text.toString() != etResetConfirmPassword.text.toString()) {
                etResetConfirmPassword.error = getString(R.string.txt_password_not_match)
            } else {
                val resetPasswordPresenter = ResetPasswordPresenter(this, this)
                if (isNetworkAvailable(this)) {
                    val resetPassword = ResetPassword(
                        etResetConfirmPassword.text.toString(),
                        "password_change"
                    )
                    resetPasswordPresenter.resetNewPassword(accessToken.reversed(), resetPassword)
                    LoadingDialog.getInstance().show(this)
//                    resetPasswordPresenter.resetNewPassword(
//                        accessToken,
//                        String(RC4(even).rc4(accessToken)!!),
//                        String(RC4(Constants.DEALLIONS_SECRET_KEY).rc4(etResetConfirmPassword.text.toString())!!),
//                        String(RC4(Constants.DEALLIONS_SECRET_KEY).rc4(accessToken.reversed())!!))
//                    LoadingDialog.getInstance().show(this)
//                    resetPasswordPresenter.resetNewPassword(
//                        accessToken,
//                        encrypt(accessToken, even)!!,
//                        encrypt(
//                            etResetConfirmPassword.text.toString(),
//                            Constants.DEALLIONS_SECRET_KEY
//                        )!!,
//                        encrypt(accessToken.reversed(), Constants.DEALLIONS_SECRET_KEY)!!
//                    )
                    LoadingDialog.getInstance().show(this)
                } else {
                    toast(getString(R.string.txt_please_check_your_internet_connection))
                }
            }
        }

    }

//    @Throws(java.lang.Exception::class)
//    fun encrypt(textToEncrypt: String, secretKey: String): String? {
//        val secretKeySpec = SecretKeySpec(secretKey.toByteArray(), "RC4")
//        val cipher: Cipher = Cipher.getInstance("RC4/ECB/NoPadding")
//        cipher.init(
//            Cipher.ENCRYPT_MODE,
//            secretKeySpec
//        )
//        val encrypted: ByteArray = cipher.doFinal(textToEncrypt.toByteArray())
//        return Base64.encodeToString(encrypted, Base64.DEFAULT)
//    }
//
//    private fun getRaw(textToEncrypt: String, aes: String): ByteArray? {
//        try {
//            val factory: SecretKeyFactory = SecretKeyFactory.getInstance(secretKeyInstance)
//            val spec: KeySpec =
//                PBEKeySpec(textToEncrypt.toCharArray(), aes.toByteArray(), pswdIterations, keySize)
//            return factory.generateSecret(spec).encoded
//        } catch (e: InvalidKeySpecException) {
//            e.printStackTrace()
//        } catch (e: NoSuchAlgorithmException) {
//            e.printStackTrace()
//        }
//        return ByteArray(0)
//    }
//
//    private fun aliceEncrypt(
//        plaintext: ByteArray?,
//        sharedSecret: ByteArray?,
//        socket: Socket
//    ) {
//        try {
//            val cipher = Cipher.getInstance("RC4/ECB/NoPadding")
//            val sk: Key = SecretKeySpec(sharedSecret, "RC4")
//            cipher.init(Cipher.ENCRYPT_MODE, sk)
//            val cos = CipherOutputStream(socket.getOutputStream(), cipher)
//            val oos = ObjectOutputStream(cos)
//            oos.writeObject(plaintext)
//            oos.close()
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }

    override fun onResetPasswordSuccess(body: ResetPasswordResponse) {
        LoadingDialog.getInstance().cancel()
        toast(body.msg)
        val intent = Intent(this, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    override fun onResetPasswordFail(message: String) {
        LoadingDialog.getInstance().cancel()
        toast(message)
    }

    override fun onResetPasswordNoInternet() {
        LoadingDialog.getInstance().cancel()
        toast(getString(R.string.txt_please_check_your_internet_connection))
    }

}
