package com.example.deallionaires.ui.forgotPassword

import com.example.deallionaires.model.ForgotPasswordResponse

interface ForgotPasswordView {

    fun onForgotPasswordSuccess(forgotPasswordResponse: ForgotPasswordResponse)

    fun onForgotPasswordFail(message: String)

    fun onForgotPasswordNoInternet()

}