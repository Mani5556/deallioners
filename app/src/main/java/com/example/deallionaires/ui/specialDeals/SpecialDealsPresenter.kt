package com.example.deallionaires.ui.specialDeals

import android.content.Context
import com.example.deallionaires.model.SpecialOfferResponse
import com.example.deallionaires.network.APIRequests.getUserSpecialOffers
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SpecialDealsPresenter(
    private val context: Context,
    private var specialDealsView: SpecialDealsView?
) {
    fun getSpecialDeals(userTokenKey: String) {
        if (isNetworkAvailable(context)) {
            getUserSpecialOffers(userTokenKey, object : NWResponseCallback<SpecialOfferResponse> {
                override fun onSuccess(
                    call: Call<SpecialOfferResponse>,
                    response: Response<SpecialOfferResponse>
                ) {
                    specialDealsView!!.onGettingSpecialDealsSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<SpecialOfferResponse>,
                    response: Response<SpecialOfferResponse>
                ) {
                    specialDealsView!!.onGettingSpecialDealsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<SpecialOfferResponse>,
                    response: Response<SpecialOfferResponse>
                ) {
                    specialDealsView!!.onGettingSpecialDealsFail(response.message())
                }

                override fun onFailure(call: Call<SpecialOfferResponse>, t: Throwable) {
                    specialDealsView!!.onGettingSpecialDealsFail(t.message!!)
                }

            })
        } else {
            specialDealsView!!.onGettingSpecialDealsNoInternet()
        }
    }
}