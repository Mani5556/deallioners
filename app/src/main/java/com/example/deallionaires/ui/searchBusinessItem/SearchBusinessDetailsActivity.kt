package com.example.deallionaires.ui.searchBusinessItem

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.text.Layout
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.drawable.toBitmap
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.deallionaires.R
import com.example.deallionaires.adapters.AllCategoriesAdapter
import com.example.deallionaires.adapters.BadgeAdapter
import com.example.deallionaires.adapters.ViewPagerAdapter
import com.example.deallionaires.model.*
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.merchantWise.MerchantWiseFragment
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.ui.searchBusiness.SearchBusinessFragment
import com.example.deallionaires.ui.searchBusiness.SearchBusinessTabsFragment
import com.example.deallionaires.utils.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_search_business_details.*
import kotlinx.android.synthetic.main.activity_search_business_details.tv_description
import kotlinx.android.synthetic.main.activity_search_business_details.tv_expires
import kotlinx.android.synthetic.main.content_search_deals_item.*
import kotlinx.android.synthetic.main.custom_tab.view.*
import kotlinx.android.synthetic.main.dialog_all_categories.view.*
import kotlinx.android.synthetic.main.favorites_sceleton.*
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class SearchBusinessDetailsActivity : AppCompatActivity(), SearchBusinessDetailsView {

    private  var timerRunneble: (() -> Unit)? = null
    private  var  slidingHandler:Handler? = null
    private var textDescriptionIsExpanded: Boolean = false
    private var searchBusinessBranches = ArrayList<Searchbusiness>()
    private var searchBusinessReviews = ArrayList<Viewreviewrating>()
    private var searchBusinessDeals = ArrayList<SearchDealsCategoryItems>()
    private var searchBusinessFilterDeals = ArrayList<Viewdeal>()
    private var viewContacts = ArrayList<Viewcontact>()
    private var viewBusinessName: Viewbusinessname? = null
    private lateinit var gMap: GoogleMap
    private val dealsFilterList = ArrayList<String>()
    private lateinit var context: Context
    private var textIsExpanded = false
    var dealIsLiked = false
    private var mrchantId: Int = 0
    private lateinit var searchBusinessDetailsPresenter: SearchBusinessDetailsPresenter
    private lateinit var supportMapFragment: SupportMapFragment

    val userTokenKey by PreferenceExt.appPreference(
        this,
        Constants.USER_TOKEN_KEY,
        Constants.DEFAULT_STRING
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_business_details)

        context = this
        searchBusinessDetailsPresenter = SearchBusinessDetailsPresenterImpl(this, this)

        val merchantId = intent.getIntExtra(
            Constants.MERCHANT_ID,
            0
        )

        mrchantId = merchantId

        iv_back.setOnClickListener {
            finish()
        }

        searchBusinessDetailsPresenter.getSearchBusinessDetailsResponse(
            userTokenKey,
            merchantId
        )

        LoadingDialog.getInstance().show(this)

        val userSkipLogin by PreferenceExt.appPreference(
            this,
            Constants.USER_SKIP_LOGIN,
            false
        )

        lyt_likes.setOnClickListener {
            if (userSkipLogin) {
                displayLoginDialog()
            } else {
                if (dealIsLiked) {
                    //removing from favourites
                    searchBusinessDetailsPresenter.removeDealFromFavourites(
                        userTokenKey,
                        merchantId
                    )
                } else {
                    //adding from favourites
                    searchBusinessDetailsPresenter.addDealToFavourites(userTokenKey, merchantId)
                }
            }
        }

        tv_view_more.setOnClickListener {
            if (!textDescriptionIsExpanded) {
                tv_description.maxLines = Int.MAX_VALUE
                tv_view_more.text = resources.getString(R.string.view_less)
                textDescriptionIsExpanded = true
            } else {
                tv_description.maxLines = 1
                tv_view_more.text = resources.getString(R.string.view_more)
                textDescriptionIsExpanded = false
            }
        }
        getTextEllipsize()

    }

    private fun getTextEllipsize() {
        val vtoDescription: ViewTreeObserver = tv_description.viewTreeObserver
        vtoDescription.addOnGlobalLayoutListener (object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    // do whatever
                    val lDescription: Layout = tv_description.layout
                    val lines = lDescription.lineCount
                    if (lines > 0) {
                        if (lDescription.getEllipsisCount(lines - 1) > 0) {
                            tv_view_more.visibility = View.VISIBLE
                        }
                    }
//                    vtoDescription.removeOnGlobalLayoutListener(this)
                }
            })
    }

    private fun getCustomTab(title: String): View {
        val view: View = layoutInflater.inflate(R.layout.custom_tab, null, false)
        view.tv_custom_tab.text = title
        return view
    }

    private fun displayLoginDialog() {
        CustomDialog(this).showDialog(
            "Login to add Favourites",
            "Login",
            "Not Now"
        ) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }


    override fun onGetSearchBusinessDetailsSuccess(searchBusinessItemDetail: SearchBusinessItemDetail) {
        LoadingDialog.getInstance().cancel()
        searchBusinessReviews =
            searchBusinessItemDetail.viewreviewrating as ArrayList<Viewreviewrating>
        searchBusinessBranches = searchBusinessItemDetail.viewbranch as ArrayList<Searchbusiness>
        searchBusinessDeals =
            searchBusinessItemDetail.viewdeals as ArrayList<SearchDealsCategoryItems>


        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(
            SearchBusinessTabsFragment(
                searchBusinessDeals,
                SearchBusinessTabsFragment.FRAGMENT_TYPE_DEALS,
                mrchantId
            ), "Deals"
        )
        adapter.addFragment(
            SearchBusinessTabsFragment(
                searchBusinessBranches,
                SearchBusinessTabsFragment.FRAGMENT_TYPE_BRANCHES,
                mrchantId
            ), "Branches"
        )
        adapter.addFragment(
            SearchBusinessTabsFragment(
                searchBusinessReviews,
                SearchBusinessTabsFragment.FRAGMENT_TYPE_REVIEWS,
                mrchantId
            ), "Reviews"
        )

        viewpager.adapter = adapter
        tabs.setupWithViewPager(viewpager)

        tabs.getTabAt(0)?.setCustomView(getCustomTab("Deals"))
        tabs.getTabAt(1)?.setCustomView(getCustomTab("Branches"))
        tabs.getTabAt(2)?.setCustomView(getCustomTab("Reviews"))




        viewBusinessName = searchBusinessItemDetail.viewbusinessname[0]
        viewContacts = searchBusinessItemDetail.viewcontact as ArrayList<Viewcontact>

        val imagesList:ArrayList<String> = searchBusinessItemDetail.viewimages as ArrayList<String>

        //this method sets the sliding behavior to the cover pic
        setSlider(imagesList)
//        Glide.with(this).load(searchBusinessItemDetail.viewimages[0])
//            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .placeholder(color)
//            .into(iv_business_cover)
        Glide.with(this).load(searchBusinessItemDetail.viewbusinessname[0].profileImage)
            .error(R.drawable.deallionaires_logo)
            .placeholder(R.color.gray_20)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(iv_business_profile)

        iv_business_profile.setOnClickListener {
            goToFullImageView(searchBusinessItemDetail.viewbusinessname[0].profileImage,it)
        }


        if(searchBusinessItemDetail.viewbusinessname[0].deallionairesClub.isNullOrEmpty()){
            rv_badge_list.visibility = View.GONE
            tv_expires.visibility = View.GONE
        }else{
            rv_badge_list.visibility = View.VISIBLE
            tv_expires.visibility = View.VISIBLE
            val club:Int  = searchBusinessItemDetail.viewbusinessname[0].deallionairesClub!!.replace("D","").toInt()
            if(club == 1){
                tv_expires.text = "No Expiry"
            }else{
                val expiry = searchBusinessItemDetail.viewbusinessname[0].clubExpiry?.split(" ")?.first()

                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
                val expTime = simpleDateFormat.parse(expiry).time
                val newDateFormate = SimpleDateFormat("dd-MMM-yyyy")
                val timeUpdated = newDateFormate.format(expTime)
                tv_expires.text = "Exp : ${timeUpdated}"
            }
            rv_badge_list.run {
                this.adapter = BadgeAdapter(this@SearchBusinessDetailsActivity, club)
                layoutManager = LinearLayoutManager(
                    this@SearchBusinessDetailsActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            }
        }


        ratingbar.rating =
            searchBusinessItemDetail.viewbusinessrating[0].rating!!.toFloat()

        tv_business_title.text = searchBusinessItemDetail.viewbusinessname[0].businessName

        var textString = searchBusinessItemDetail.viewcategoryname[0].categoryName
        tv_category_name.text = textString

        var subcategories = ""
       searchBusinessItemDetail.viewsubcategoryname.mapIndexed { index, sub ->
            subcategories +=  if(index == 0) "${sub}" else "/${sub}"
        }
        if(!subcategories.isNullOrEmpty()){
            tv_sub_category_names.text = subcategories
        }else{
            tv_sub_category_names.visibility = View.GONE
        }


        with(searchBusinessItemDetail.viewbusinessname.first().businessDescription) {
            if (isNullOrBlank()) {
                tv_description.visibility = View.GONE
            } else {
                tv_description.text = this
            }
        }


        tv_location.apply {
            text =
                Html.fromHtml("<u>${searchBusinessItemDetail.viewbusinessname[0].address},${searchBusinessItemDetail.viewbusinessname[0].city}</u>")
            setOnClickListener {
                val lat: Double? =
                    searchBusinessItemDetail.viewbusinessname[0].latitude?.run { if (!isNullOrEmpty()) this.toDouble() else null }
                val lon: Double? =
                    searchBusinessItemDetail.viewbusinessname[0].longitude?.run { if (!isNullOrEmpty()) this.toDouble() else null }
                val dialog = CustomMapDialog.newInstance(
                    lat,
                    lon
                    ,
                    searchBusinessItemDetail.viewbusinessname[0].businessName,
                    searchBusinessItemDetail.viewbusinessname[0].address,
                    searchBusinessItemDetail.viewbusinessname[0].mobileNumbers[0]
                )
                dialog.show(supportFragmentManager, "showLocation")
            }
        }



        if (searchBusinessItemDetail.viewbusinessname[0].mobileNumbers.size > 0) {
            var contactText = ""
            searchBusinessItemDetail.viewbusinessname[0].mobileNumbers.forEachIndexed { i, str ->
                if (i == searchBusinessItemDetail.viewbusinessname[0].mobileNumbers.size - 1) {
                    contactText += str
                } else {
                    contactText += str + ","
                }
            }
            tv_contact.text = "Contact : ${contactText}"

        } else {
            tv_contact.visibility = View.GONE
        }

        tv_available_points.text = "${addCommasToNumericString(viewBusinessName?.loyaltyPoints?:0.toString())}"

        if (searchBusinessItemDetail.followers!! > 0) {
            lyt_followers.visibility = View.VISIBLE
            tv_follower_count.text = searchBusinessItemDetail.followers!!.toString()
        } else {
            lyt_followers.visibility = View.GONE
        }


        dealIsLiked = if (searchBusinessItemDetail.isLiked == 1) {
            iv_favorite.setImageDrawable(resources.getDrawable(R.drawable.ic_favourite_liked_red))
            true
        } else {
            iv_favorite.setImageDrawable(resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
            false
        }

    }

    private fun setSlider(imagesList:ArrayList<String>) {
        view_pager_cover.adapter = SlidingPagerAdapter(this,imagesList)

        if(imagesList.size > 0){
            timerRunneble = {
                var currentImagePosition:Int = view_pager_cover.currentItem
                currentImagePosition = ++currentImagePosition % imagesList.size
                view_pager_cover.currentItem = currentImagePosition
                slidingHandler?.postDelayed(timerRunneble,5000)
            }
            slidingHandler = Handler()
            slidingHandler?.postDelayed(timerRunneble,5000)
        }

    }

    override fun onGetSearchBusinessDetailsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        toast(message)
    }

    override fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded) {
        iv_favorite.setImageDrawable(resources.getDrawable(R.drawable.ic_favourite_liked_red))
        toast(favouriteAdded.msg.toString())
        dealIsLiked = true
    }

    override fun onAddingDealToFavouritesFail(message: String) {
        toast(message)
    }

    override fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved) {
        iv_favorite.setImageDrawable(resources.getDrawable(R.drawable.ic_favourite_un_liked_red))
        toast(favouriteRemoved.msg.toString())
        dealIsLiked = false
    }

    override fun onRemovingDealToFavouritesFail(message: String) {
        toast(message)
    }

    override fun onAddingSearchBusinessReviewSuccess(reviewAdded: ReviewAdded) {
        LoadingDialog.getInstance().cancel()
        searchBusinessDetailsPresenter.getSearchBusinessDetailsResponse(
            userTokenKey,
            mrchantId
        )
        toast(reviewAdded.msg.toString())
    }

    override fun onAddingSearchBusinessReviewFail(message: String) {
        LoadingDialog.getInstance().cancel()
        toast(message)
    }

    override fun onGetSearchBusinessDetailsNoInternet() {
        LoadingDialog.getInstance().cancel()
        toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun filterResultsSuccess(filterData: List<SearchDealsCategoryItems>?) {

    }

    override fun filterResultsFail(message: String) {

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        slidingHandler?.removeCallbacks(timerRunneble)
    }

}
