package com.example.deallionaires.ui.home.fragments.home

import com.example.deallionaires.model.Categories
import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.HomeDeals

interface HomeView {

    fun onGetCategoriesSuccess(
        categories: ArrayList<Categories>
    )

    fun onGetCategoriesFail(message: String)

    fun onGetCategoriesNoInternet()

    fun onGetLandingPageDataSuccess(
        landingPageList: Map<String,List<Any>>
    )

    fun onGetLandingPageDataFail(message: String)

    fun onGetLandingPageDataNoInternet()

    fun showHomeFragmentProgress()

    fun hideHomeFragmentProgress()

    fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded, position: Int,parentAdapterPosition:Int)

    fun onAddingDealToFavouritesFail(message: String)

    fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved, position: Int,parentAdapterPosition:Int)

    fun onRemovingDealToFavouritesFail(message: String)
}