package com.example.deallionaires.ui.help

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.deallionaires.R
import kotlinx.android.synthetic.main.activity_help_support.*
import org.jetbrains.anko.intentFor

class HelpSupportActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_support)
        toolbar.setNavigationOnClickListener { finish() }

        lyt_faqs.setOnClickListener {
            startActivity(intentFor<FaqsActivity>())
        }

        lyt_help_center.setOnClickListener {
            startActivity(intentFor<HelpCenterActivity>())
        }
    }
}