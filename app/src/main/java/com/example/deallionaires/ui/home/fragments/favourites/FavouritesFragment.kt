package com.example.deallionaires.ui.home.fragments.favourites

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.FavouritesAdapter
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.FavouritesResponse
import com.example.deallionaires.model.Getfavouritedetail
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_favourites.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 * Use the [FavouritesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FavouritesFragment : Fragment(), FavouritesView {

    lateinit var activity: Activity
    private lateinit var favouritesPresenter: FavouritesPresenter
    private lateinit var userKey: String

    var isFromOnPause = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        userKey = userTokenKey

        favouritesPresenter = FavouritesPresenterImpl(activity, this)

        if (isNetworkAvailable(activity)) {
            favouritesPresenter.getFavourites(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(getString(R.string.txt_please_check_your_internet_connection))
        }

    }

    override fun displayFavourites(favourites: FavouritesResponse) {
        LoadingDialog.getInstance().cancel()
        if (favourites.getfavouritedetail!!.isEmpty()) {
            activity.tvNoFavouritesFound.visibility = View.VISIBLE
            activity.rvFavourites.visibility = View.GONE
        } else {
            activity.tvNoFavouritesFound.visibility = View.GONE
            activity.rvFavourites.visibility = View.VISIBLE
            activity.rvFavourites.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            activity.rvFavourites.itemAnimator = DefaultItemAnimator()
            val adapter = FavouritesAdapter(
                activity,
                favourites.getfavouritedetail as ArrayList<Getfavouritedetail>
            ) { getfavouritedetail: Getfavouritedetail, isFavouriteClicked: Boolean ->
                if (isFavouriteClicked) {
                    favouritesPresenter.removeDealFromFavourites(userKey, getfavouritedetail.merchentId?:-1)
                    LoadingDialog.getInstance().show(activity)
                } else {
                    activity.startActivity(
                        activity.intentFor<SearchBusinessDetailsActivity>(
                            Constants.MERCHANT_ID to getfavouritedetail.merchentId
                        )
                    )
                }
            }
            activity.rvFavourites.adapter = adapter
        }
    }

    override fun noFavouritesFound(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onRemovingDealFromFavouritesSuccess(favouriteRemoved: FavouriteRemoved) {
        LoadingDialog.getInstance().cancel()
        if (isNetworkAvailable(activity)) {
            favouritesPresenter.getFavourites(userKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(getString(R.string.txt_please_check_your_internet_connection))
        }
    }

    override fun onRemovingDealFromFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun favouritesNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onResume() {
        super.onResume()
        if (isFromOnPause) {
            if (isNetworkAvailable(activity)) {
                favouritesPresenter.getFavourites(userKey)
                LoadingDialog.getInstance().show(activity)
            } else {
                activity.toast(getString(R.string.txt_please_check_your_internet_connection))
            }
        }
    }

    override fun onPause() {
        super.onPause()
        isFromOnPause = true
    }
}
