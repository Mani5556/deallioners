package com.example.deallionaires.ui.home.fragments.dashboard

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.deallionaires.R
import com.example.deallionaires.model.Dashboard
import com.example.deallionaires.ui.dealsStatus.completedDeals.CompletedDealsFragment
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.home.fragments.dashboard.deallionairesClub.DeallionairesClubFragment
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.DeallionairesEarnedFragment
import com.example.deallionaires.ui.home.fragments.favourites.FavouritesFragment
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.addCommasToNumericString
import kotlinx.android.synthetic.main.fragment_dashboard.*
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 * Use the [DashboardFragment] factory method to
 * create an instance of this fragment.
 */
class DashboardFragment : Fragment(), DashboardView {

    private lateinit var activity: Activity
    private lateinit var deallionsEarned: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity = getActivity()!!

        val tempActivity = requireActivity() as HomeActivity

        val userTokenKey by PreferenceExt.appPreference(
            requireContext(),
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        var userFromActivity by PreferenceExt.appPreference(
            activity,
            Constants.FROM_ACTIVITY,
            Constants.ACTIVITY
        )

        val dashboardPresenter = DashboardPresenterImpl(context, this)
        LoadingDialog.getInstance().show(activity)
        dashboardPresenter.getDashboardPoints(userTokenKey)


        activity.llDeallionsClub.setOnClickListener {
            userFromActivity = Constants.FROM_DASHBOARD
            tempActivity.loadFragmentInHomeActivity(Constants.DEALLIONS_CLUB, DeallionairesClubFragment(), false, Constants.FROM_DASHBOARD)
        }

        activity.llDeallionsEarned.setOnClickListener {
            userFromActivity = Constants.FROM_DASHBOARD
            tempActivity.loadFragmentInHomeActivity(Constants.DEALLIONS_EARNED, DeallionairesEarnedFragment(), false, Constants.FROM_DASHBOARD)
        }

        activity.llDealsClaimed.setOnClickListener {
            userFromActivity = Constants.FROM_DASHBOARD
            tempActivity.loadFragmentInHomeActivity(Constants.COMPLETED_DEALS, CompletedDealsFragment(), false, Constants.FROM_DASHBOARD)
        }

        activity.llFavourites.setOnClickListener {
            userFromActivity = Constants.FROM_DASHBOARD
            tempActivity.loadFragmentInHomeActivity(Constants.FAVOURITES, FavouritesFragment(), false, Constants.FROM_DASHBOARD)
        }
    }

    override fun displayDashboardPoints(dashboard: Dashboard) {
        LoadingDialog.getInstance().cancel()
        activity.tvMyDeals_count.text = "${dashboard.myDeals}"
        deallionsEarned = dashboard.loyaltyPoints.toString()
        activity.tv_earned_count.text = addCommasToNumericString(dashboard.loyaltyPoints.toString())
        activity.tv_following_count.text = "${dashboard.favorites}"
        activity.tv_dealsclub_count.text = "${dashboard.businessCount}"
    }

    override fun noDashboardPoints() {
        LoadingDialog.getInstance().cancel()
    }

    override fun dashboardNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.getString(R.string.txt_please_check_your_internet_connection))
    }

}
