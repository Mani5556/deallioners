package com.example.deallionaires.ui.home.fragments.favourites

import android.content.Context
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.FavouritesResponse
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.APIRequests.getUserFavourites
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class FavouritesPresenterImpl(
    private var context: Context?,
    private var favouritesView: FavouritesView?
) : FavouritesPresenter {

    override fun getFavourites(userTokenKey: String) {
        if(isNetworkAvailable(context!!)){
            getUserFavourites(userTokenKey, object : NWResponseCallback<FavouritesResponse>{
                override fun onSuccess(
                    call: Call<FavouritesResponse>,
                    response: Response<FavouritesResponse>
                ) {
                    favouritesView!!.displayFavourites(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<FavouritesResponse>,
                    response: Response<FavouritesResponse>
                ) {
                    favouritesView!!.noFavouritesFound(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<FavouritesResponse>,
                    response: Response<FavouritesResponse>
                ) {
                    favouritesView!!.noFavouritesFound(response.message())
                }

                override fun onFailure(call: Call<FavouritesResponse>, t: Throwable) {
                    favouritesView!!.noFavouritesFound(t.message!!)
                }

            })
        }else{
            favouritesView!!.favouritesNoInternet()
        }
    }

    override fun removeDealFromFavourites(userTokenKey: String, merchantid: Int) {
        if(isNetworkAvailable(context!!)){
            APIRequests.getRemoveDealFromFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteRemoved?> {
                    override fun onSuccess(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        favouritesView!!.onRemovingDealFromFavouritesSuccess(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        favouritesView!!.onRemovingDealFromFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        favouritesView!!.onRemovingDealFromFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                        favouritesView!!.onRemovingDealFromFavouritesFail(t.message!!)
                    }

                })
        }else {
            favouritesView!!.favouritesNoInternet()
        }
    }

    override fun destroyFavourites() {
        favouritesView = null
    }
}