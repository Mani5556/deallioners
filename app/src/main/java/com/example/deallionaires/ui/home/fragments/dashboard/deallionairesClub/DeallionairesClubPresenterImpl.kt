package com.example.deallionaires.ui.home.fragments.dashboard.deallionairesClub

import android.content.Context
import com.example.deallionaires.model.DeallionairesClubResponse
import com.example.deallionaires.network.APIRequests.getUserDeallionairesClub
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class DeallionairesClubPresenterImpl(
    private val context: Context,
    private var deallionairesClubView: DeallionairesClubView?
) : DeallionairesClubPresenter {
    override fun getDeallionairesClub(userTokenKey: String) {
        if (isNetworkAvailable(context)) {
            getUserDeallionairesClub(
                userTokenKey,
                object : NWResponseCallback<DeallionairesClubResponse> {
                    override fun onSuccess(
                        call: Call<DeallionairesClubResponse>,
                        response: Response<DeallionairesClubResponse>
                    ) {
                        deallionairesClubView!!.displayDeallionairesClub(response.body()!!)
                    }

                    override fun onResponseBodyNull(
                        call: Call<DeallionairesClubResponse>,
                        response: Response<DeallionairesClubResponse>
                    ) {
                        deallionairesClubView!!.noDeallionairesClub(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<DeallionairesClubResponse>,
                        response: Response<DeallionairesClubResponse>
                    ) {
                        deallionairesClubView!!.noDeallionairesClub(response.message())
                    }

                    override fun onFailure(call: Call<DeallionairesClubResponse>, t: Throwable) {
                        deallionairesClubView!!.noDeallionairesClub(t.message!!)
                    }

                })
        } else {
            deallionairesClubView!!.deallionairesClubNoInternet()
        }
    }

    override fun destroyDeallionairesClub() {
        deallionairesClubView = null
    }

}