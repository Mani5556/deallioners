package com.example.deallionaires.ui.dealsStatus.completedDeals

import android.content.Context
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.DealStatusResponse
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class CompletedDealsPresenterImpl(
    private val context: Context,
    private var completedDealsView: CompletedDealsView?
) : CompletedDealsPresenter {
    override fun getGetCompletedDealsPresenter(tokenKey: String) {
        if (isNetworkAvailable(context)) {
            APIRequests.getUserCompetedDeals(tokenKey, object : NWResponseCallback<DealStatusResponse> {
                override fun onSuccess(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {
                    val myCompletedDeals = ArrayList<DealStatus>()
                    for (i in response.body()!!.getreedemdetail.indices) {
                        if (response.body()!!.getreedemdetail[i].status == 1) {
                            myCompletedDeals.add(
                                DealStatus(
                                    response.body()!!.getreedemdetail[i].redeemdealid,
                                    response.body()!!.getreedemdetail[i].merchantid,
                                    response.body()!!.getreedemdetail[i].dealid,
                                    response.body()!!.getreedemdetail[i].business_name,
                                    response.body()!!.getreedemdetail[i].city,
                                    response.body()!!.getreedemdetail[i].address,
                                    response.body()!!.getreedemdetail[i].latitude,
                                    response.body()!!.getreedemdetail[i].longitude,
                                    response.body()!!.getreedemdetail[i].profile_image,
                                    response.body()!!.getreedemdetail[i].title,
                                    response.body()!!.getreedemdetail[i].expiry_date,
                                    response.body()!!.getreedemdetail[i].deallions_offered,
                                    response.body()!!.getreedemdetail[i].total_deallions,
                                    response.body()!!.getreedemdetail[i].regular_price,
                                    response.body()!!.getreedemdetail[i].offer_price,
                                    response.body()!!.getreedemdetail[i].transaction_id,
                                    response.body()!!.getreedemdetail[i].sys_ref_code,
                                    response.body()!!.getreedemdetail[i].verification_code,
                                    response.body()!!.getreedemdetail[i].purchase_amount,
                                    response.body()!!.getreedemdetail[i].saving_amount,
                                    response.body()!!.getreedemdetail[i].status,
                                    response.body()!!.getreedemdetail[i].offer_category_id,
                                    response.body()!!.getreedemdetail[i].offer_type_id,
                                    response.body()!!.getreedemdetail[i].customer_base_offer_type_id,
                                    response.body()!!.getreedemdetail[i].created_at,
                                    response.body()!!.getreedemdetail[i].deallionsUsed,
                                    response.body()!!.getreedemdetail[i].deallionsEarned
                                )
                            )
                        }
                    }
                    completedDealsView!!.onGettingCompletedDealsSuccess(myCompletedDeals)
                }

                override fun onResponseBodyNull(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {
                    completedDealsView!!.onGettingCompletedDealsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {
                    completedDealsView!!.onGettingCompletedDealsFail(response.message())
                }

                override fun onFailure(call: Call<DealStatusResponse>, t: Throwable) {
                    completedDealsView!!.onGettingCompletedDealsFail(t.message.toString())
                }
            })
        } else {
            completedDealsView!!.onGettingCompletedDealsNoInternet()
        }
    }

    override fun destroyCompletedDeals() {
        completedDealsView == null
    }
}