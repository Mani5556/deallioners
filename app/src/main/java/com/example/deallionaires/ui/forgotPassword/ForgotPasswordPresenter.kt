package com.example.deallionaires.ui.forgotPassword

import android.content.Context
import com.example.deallionaires.model.ForgotPassword
import com.example.deallionaires.model.ForgotPasswordResponse
import com.example.deallionaires.network.APIRequests.getForgotPassword
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class ForgotPasswordPresenter(
    private val context: Context,
    private var forgotPasswordView: ForgotPasswordView?
) {
    fun getForgotPasswordAccessToken(userAccessKey: String, forgotPassword: ForgotPassword) {
        if (isNetworkAvailable(context)) {
            getForgotPassword(userAccessKey, forgotPassword, object : NWResponseCallback<ForgotPasswordResponse?> {
                override fun onSuccess(
                    call: Call<ForgotPasswordResponse?>,
                    response: Response<ForgotPasswordResponse?>
                ) {
                    if (response.body()!!.token != null) {
                        forgotPasswordView!!.onForgotPasswordSuccess(response.body()!!)
                    } else {
                        forgotPasswordView!!.onForgotPasswordFail(response.body()!!.msg)
                    }
                }

                override fun onResponseBodyNull(
                    call: Call<ForgotPasswordResponse?>,
                    response: Response<ForgotPasswordResponse?>
                ) {
                    forgotPasswordView!!.onForgotPasswordFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<ForgotPasswordResponse?>,
                    response: Response<ForgotPasswordResponse?>
                ) {
                    forgotPasswordView!!.onForgotPasswordFail(response.message())
                }

                override fun onFailure(call: Call<ForgotPasswordResponse?>, t: Throwable) {
                    forgotPasswordView!!.onForgotPasswordFail(t.message!!)
                }
            })
        } else {
            forgotPasswordView!!.onForgotPasswordNoInternet()
        }
    }
}