package com.example.deallionaires.ui.forgotPassword

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.example.deallionaires.R
import com.example.deallionaires.ui.emailVerification.EmailVerificationActivity
import com.example.deallionaires.ui.mobileVerification.MobileVerificationActivity
import com.example.deallionaires.utils.Constants
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_mobile_number.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.jetbrains.anko.intentFor
import java.util.regex.Pattern

class ForgotPasswordActivity : AppCompatActivity() {

    private val forgotPasswordValidation = AwesomeValidation(ValidationStyle.BASIC)

    private var isEmailIdSelected: Boolean = true

    private lateinit var countryCode: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        countryCode = ccpForgotPassword.selectedCountryCode

        ivForgotPasswordBack.setOnClickListener {
            onBackPressed()
        }

        ccpForgotPassword.setOnCountryChangeListener {
            countryCode = ccpForgotPassword.selectedCountryCode
            Log.e("Forgot password" , countryCode)
        }

        tvForgotPasswordValidate.setOnClickListener {
            validateForgotPassword()
        }

        rgForgotPassword.setOnCheckedChangeListener { _, checkedId ->
            if(checkedId == R.id.rbMail){
                isEmailIdSelected = true
                llForgotPasswordEmail.visibility = View.VISIBLE
                llForgotPasswordMobile.visibility = View.GONE
                etRegisteredMailId.setText("")
            }else {
                isEmailIdSelected = false
                llForgotPasswordEmail.visibility = View.GONE
                llForgotPasswordMobile.visibility = View.VISIBLE
                etRegisteredMobile.setText("")
            }
        }

    }

    private fun validateForgotPassword() {

        if(isEmailIdSelected){

            forgotPasswordValidation.addValidation(
                this,
                R.id.etRegisteredMailId,
                RegexTemplate.NOT_EMPTY,
                R.string.txt_enter_email
            )

            if(forgotPasswordValidation.validate()){
                startActivity(
                    intentFor<EmailVerificationActivity>(
                        Constants.FROM_ACTIVITY to Constants.FORGOT_PASSWORD,
                        Constants.VERIFICATION_EMAIL to etRegisteredMailId.text.toString()
                    )
                )
            }

        }else {

            if (etRegisteredMobile.text.toString().length < 8) {
                etRegisteredMobile.error =
                    resources.getString(R.string.error_valid_mob_num)
            } else {
                startActivity(
                    intentFor<MobileVerificationActivity>(
                        Constants.FROM_ACTIVITY to Constants.FORGOT_PASSWORD,
                        Constants.COUNTRY_CODE to countryCode,
                        Constants.VERIFICATION_MOBILE_NUMBER to etRegisteredMobile.text.toString()
                    )
                )
            }

        }

//        if (forgotPasswordValidation.validate()) {
//            if (ifIsEmailOrNot(etRegisteredMailId.text.toString())) {
//                startActivity(
//                    intentFor<EmailVerificationActivity>(
//                        Constants.FROM_ACTIVITY to Constants.FORGOT_PASSWORD,
//                        Constants.VERIFICATION_EMAIL to etRegisteredMailId.text.toString()
//                    )
//                )
//            } else if (etRegisteredMailId.text.toString().length < 8) {
//                etRegisteredMailId.error =
//                    resources.getString(R.string.txt_enter_mobile_number)
//            } else {
//                startActivity(
//                    intentFor<MobileVerificationActivity>(
//                        Constants.FROM_ACTIVITY to Constants.FORGOT_PASSWORD,
//                        Constants.VERIFICATION_MOBILE_NUMBER to etRegisteredMailId.text.toString()
//                    )
//                )
//            }
//        }
    }

    private fun ifIsEmailOrNot(toString: String): Boolean {
        val regEx = ("[a-zA-Z0-9+._%-+]{1,256}" + "@"
                + "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" + "(" + "."
                + "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" + ")+")

        val matcherObj = Pattern.compile(regEx).matcher(toString)
        return matcherObj.matches()
    }

}
