package com.example.deallionaires.ui.searchDealItem

import com.example.deallionaires.model.Dealsdetail
import com.example.deallionaires.model.RedeemDealResponse

interface SearchDealDetailsView {

    fun onGetDealDealsSuccess(dealsdetail: Dealsdetail?)

    fun onGetDealDetailsFail(message: String)

    fun onGetDealDetailsNoInternet()

    fun onPostRedeemDealSuccess(redeemDealResponse: RedeemDealResponse)

    fun onPostRedeemDealFail(message: String)

}