package com.example.deallionaires.ui.searchDeals

import com.example.deallionaires.model.SearchDealsCategoryItems
import com.example.deallionaires.model.SearchDealsCategoryResponse
import com.example.deallionaires.model.SearchUserCategory

interface SearchDealsView {

    fun onGetUserDealsCategoriesSuccess(searchUserCategoriesResult: ArrayList<SearchUserCategory>)

    fun onGetUserDealsCategoriesFail(message: String)

    fun onGetUserDealsCategoriesNoInternet()

    fun onGetUserDealsSubCategoriesSuccess(position: Int)

    fun onGetUserDealsSubCategoriesFail(message: String)

    fun onGetUserDealsSubCategoriesNoInternet()

    fun onGetUserDealsItemsSuccess(searchDealsCategoryItems: List<SearchDealsCategoryItems>?)

    fun onGetUserDealsItemsFail(message: String)

    fun onGetUserDealsNoInternet()

    fun showUserDealsProgress()

    fun hideUserDealsProgress()

    fun showShimmer()

    fun hideShimmer()
    fun onGetLoadMoreDataSuccess( saerchDeals: List<SearchDealsCategoryItems>?)
    fun onGetLoadMoreFail(toString: String)

}