package com.example.deallionaires.ui.specialDeals

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.SpecialDealsAdapter
import com.example.deallionaires.model.SpecialOfferResponse
import com.example.deallionaires.model.Specialoffer
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_special_deals.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class SpecialDealsFragment : Fragment(), SpecialDealsView {

    lateinit var activity: Activity
    private lateinit var userToken: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_special_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        if (isNetworkAvailable(activity)) {
            val specialDealsPresenter = SpecialDealsPresenter(activity, this)
            specialDealsPresenter.getSpecialDeals(userTokenKey)
        } else {
            activity.toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }

    }

    override fun onGettingSpecialDealsSuccess(specialOfferResponse: SpecialOfferResponse) {
        LoadingDialog.getInstance().cancel()
        if (specialOfferResponse.specialoffers.isEmpty()) {
            activity.lyt_no_data.visibility = View.VISIBLE
            activity.rvSpecialDeals.visibility = View.GONE
        } else {
            activity.lyt_no_data.visibility = View.GONE
            activity.rvSpecialDeals.visibility = View.VISIBLE
            activity.rvSpecialDeals.layoutManager =
                LinearLayoutManager(activity,  RecyclerView.VERTICAL, false)
            activity.rvSpecialDeals.adapter =
                SpecialDealsAdapter(
                    activity,
                    specialOfferResponse.specialoffers as ArrayList<Specialoffer>
                ) {
                    activity.startActivity(
                        activity.intentFor<SearchDealDetailsActivity>(
                            Constants.DEAL_ID to it.dealid,
                            Constants.FROM_ACTIVITY to Constants.FROM_SPECIAL_DEALS
                        )
                    )
                }
        }
    }

    override fun onGettingSpecialDealsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGettingSpecialDealsNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(resources.getString(R.string.txt_please_check_your_internet_connection))
    }
}
