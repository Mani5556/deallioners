package com.example.deallionaires.ui.resetPassword

import com.example.deallionaires.model.ResetPasswordResponse

interface ResetPasswordView {

    fun onResetPasswordSuccess(body: ResetPasswordResponse)

    fun onResetPasswordFail(message: String)

    fun onResetPasswordNoInternet()

}