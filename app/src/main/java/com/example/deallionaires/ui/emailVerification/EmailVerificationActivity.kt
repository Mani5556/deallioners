package com.example.deallionaires.ui.emailVerification

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.deallionaires.R
import com.example.deallionaires.model.EmailVerification
import com.example.deallionaires.model.EmailVerificationResponse
import com.example.deallionaires.model.OtpVerification
import com.example.deallionaires.model.OtpVerificationResponse
import com.example.deallionaires.ui.resetPassword.ResetPasswordActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.hideKeyboard
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_email_verification.*
import org.jetbrains.anko.toast
import java.util.*


class EmailVerificationActivity : AppCompatActivity(), EmailVerificationView {

    private var emailVerified by PreferenceExt.appPreference(
        this,
        Constants.VERIFIED_EMAIL,
        false
    )

    private lateinit var fromActivity: String

    private lateinit var emailVerificationToken: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email_verification)

        val emailVerificationPresenter = EmailVerificationPresenter(this, this)

        val verificationText = "Enter the verification code we just sent to your Email Id : "

        val enteredMailId = intent.getStringExtra(
            Constants.VERIFICATION_EMAIL
        )!!

        fromActivity = intent.getStringExtra(Constants.FROM_ACTIVITY)!!

        val verificationEmailText: String =
            verificationText + enteredMailId.toUpperCase(Locale.ROOT)

        val spannable: Spannable = SpannableString(verificationEmailText)

        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorYellow)),
            verificationText.length,
            (verificationText + intent.getStringExtra(
                Constants.VERIFICATION_EMAIL
            )).length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        tvVerificationEmail.setText(spannable, TextView.BufferType.SPANNABLE)

        ivEmailVerificationBack.setOnClickListener {
            onBackPressed()
        }

        val userTokenKey by PreferenceExt.appPreference(
            this,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        if (isNetworkAvailable(this)) {
            val emailVerification = EmailVerification(enteredMailId, "email")
            if (fromActivity == Constants.FORGOT_PASSWORD) {
                emailVerificationPresenter.getEmailOtpForForgotPassword(
                    userTokenKey,
                    emailVerification
                )
            } else {
                emailVerificationPresenter.getEmailVerificationOtpForSignUp(userTokenKey, emailVerification)
            }

        } else {
            toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }

        etEmailVerifyOtpFirstDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    etEmailVerifyOtpSecondDigit.requestFocus()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        etEmailVerifyOtpSecondDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    etEmailVerifyOtpThirdDigit.requestFocus()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        etEmailVerifyOtpThirdDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    etEmailVerifyOtpFourthDigit.requestFocus()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        etEmailVerifyOtpFourthDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    hideKeyboard(etEmailVerifyOtpFourthDigit)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        tvEmailOtpResend.paintFlags = tvEmailOtpResend.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        tvEmailVerifyOtpVerify.setOnClickListener {
            if (etEmailVerifyOtpFirstDigit.text.toString()
                    .isEmpty() || etEmailVerifyOtpSecondDigit.text.toString()
                    .isEmpty() || etEmailVerifyOtpThirdDigit.text.toString()
                    .isEmpty() || etEmailVerifyOtpFourthDigit.text.toString().isEmpty()
            ) {
                toast("Please enter OTP")
            } else {
                val otp = etEmailVerifyOtpFirstDigit.text.toString() +
                        etEmailVerifyOtpSecondDigit.text.toString() +
                        etEmailVerifyOtpThirdDigit.text.toString() +
                        etEmailVerifyOtpFourthDigit.text.toString()
                if (isNetworkAvailable(this)) {
                    if (fromActivity == Constants.FORGOT_PASSWORD) {
                        val otpVerification = OtpVerification(otp, "verification")
                        emailVerificationPresenter.getVerifyOtpForForgotPassword(
                            emailVerificationToken,
                            otpVerification
                        )
                    } else {
                        val otpVerification = OtpVerification(otp, "mail_verification")
                        emailVerificationPresenter.getVerifyEmailOtp(emailVerificationToken, otpVerification)
                    }
                } else {
                    toast(resources.getString(R.string.txt_please_check_your_internet_connection))
                }
            }

        }

        tvEmailOtpResend.setOnClickListener {
            if (isNetworkAvailable(this)) {
                val emailVerification = EmailVerification(enteredMailId, "email")
                if (fromActivity == Constants.FORGOT_PASSWORD) {
                    emailVerificationPresenter.getEmailOtpForForgotPassword(
                        userTokenKey,
                        emailVerification
                    )
                } else {
                    emailVerificationPresenter.getEmailVerificationOtpForSignUp(
                        userTokenKey,
                        emailVerification
                    )
                }

            } else {
                toast(resources.getString(R.string.txt_please_check_your_internet_connection))
            }
        }

    }

    override fun onEmailVerificationSuccess(emailVerificationResponse: EmailVerificationResponse) {
        toast(emailVerificationResponse.msg!!)
        emailVerificationToken = if(emailVerificationResponse.token != null){
            emailVerificationResponse.token!!
        }else {
            ""
        }
    }

    override fun onEmailVerificationFailed(message: String) {
        toast(message)
    }

    override fun onOtpVerificationSuccess(otpVerificationResponse: OtpVerificationResponse) {
        toast(otpVerificationResponse.msg!!)
        if (fromActivity == Constants.FORGOT_PASSWORD) {
            val intent = Intent(this, ResetPasswordActivity::class.java)
            intent.putExtra(Constants.FORGOT_PASSWORD_ACCESS_TOKEN, otpVerificationResponse.token)
            startActivity(intent)
        } else {
            onBackPressed()
            emailVerified = true
        }
    }

    override fun onOtpVerificationFailed(message: String) {
        toast(message)
    }

    override fun onEmailVerificationNoInternet() {
        toast(resources.getString(R.string.txt_please_check_your_internet_connection))
    }
}