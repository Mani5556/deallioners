package com.example.deallionaires.ui.signUp

import com.example.deallionaires.model.SignUpUser

interface SignUpPresenter {

    fun signUpNewUser(signUpUser: SignUpUser)

    fun destroySignUpNewUser()

}