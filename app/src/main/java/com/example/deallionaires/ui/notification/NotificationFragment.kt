package com.example.deallionaires.ui.notification

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.deallionaires.R
import com.example.deallionaires.adapters.MerchantNotificationsAdapter
import com.example.deallionaires.adapters.NotificationPageAdapter
import com.example.deallionaires.adapters.ViewPagerAdapter
import com.example.deallionaires.ui.notification.admin.AdminNotificationsFragment
import com.example.deallionaires.ui.notification.merchant.MerchantNotificationsFragment
import kotlinx.android.synthetic.main.fragment_notification.*

/**
 * A simple [Fragment] subclass.
 */
class NotificationFragment : Fragment() {
    lateinit var activity: Activity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity = getActivity()!!

        val adapter = ViewPagerAdapter(requireActivity().supportFragmentManager)
        adapter.addFragment(MerchantNotificationsFragment(), "MERCHANT NOTIFICATIONS")
        adapter.addFragment(AdminNotificationsFragment(), "ADMIN NOTIFICATIONS")

        activity.vpNotifications.adapter = adapter
        activity.tlNotificationTabs.setupWithViewPager(activity.vpNotifications)

//        activity.vpNotifications.adapter =
//            NotificationPageAdapter(activity, requireActivity().supportFragmentManager)
//        activity.tlNotificationTabs.setupWithViewPager(activity.vpNotifications)

    }

}
