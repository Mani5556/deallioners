package com.example.deallionaires.ui.profile

import android.graphics.drawable.Drawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.example.deallionaires.R
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.getRandomColor
import com.example.deallionaires.utils.load
import com.facebook.internal.NativeProtocol.IMAGE_URL_KEY
import kotlinx.android.synthetic.main.activity_full_photo.*
import org.jetbrains.anko.toast

class FullPhotoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_photo)
        val transitionName = intent.getStringExtra(Constants.TRANSITION_NAME)
        transitionName?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                iv_pic.transitionName = transitionName
            }
        }
        supportPostponeEnterTransition()
        val url = intent.getStringExtra(IMAGE_URL_KEY)
        iv_pic.load(url) {isLoaded->
            supportStartPostponedEnterTransition()

            if(!isLoaded){
                toast("No image found")
            }
        }
    }

}