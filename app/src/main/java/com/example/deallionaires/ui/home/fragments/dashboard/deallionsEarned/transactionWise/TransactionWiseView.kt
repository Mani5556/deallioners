package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise

import com.example.deallionaires.model.TransactionWiseResponse

interface TransactionWiseView {

    fun onGettingTransactionWiseDeallionsSuccess(transactionWiseResponse: TransactionWiseResponse)

    fun onGettingTransactionWiseDeallionsFail(message: String)

    fun onGettingTransactionWiseNoInternet()

}