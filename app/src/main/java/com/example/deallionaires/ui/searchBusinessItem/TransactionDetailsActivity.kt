package com.example.deallionaires.ui.searchBusinessItem

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.deallionaires.R
import com.example.deallionaires.adapters.TransactionDeatilsAdapter
import com.example.deallionaires.model.TransactionWiseResponse
import com.example.deallionaires.model.Transactionvaluepoints
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise.TransactionWisePresenter
import com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.transactionWise.TransactionWiseView
import com.example.deallionaires.utils.*
import kotlinx.android.synthetic.main.activity_business_details.*
import kotlinx.android.synthetic.main.list_item_merchant_wise.view.*
import java.lang.Exception

class TransactionDetailsActivity : AppCompatActivity(), TransactionWiseView {

    private lateinit var transactionWisePresenter: TransactionWisePresenter
    private  var murchentKey:Int = 0
    val userTokenKey by PreferenceExt.appPreference(
        this,
        Constants.USER_TOKEN_KEY,
        Constants.DEFAULT_STRING
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_business_details)
        murchentKey = intent.getIntExtra(Constants.MERCHANT_ID,0)
        transactionWisePresenter = TransactionWisePresenter(this, this)
        transactionWisePresenter.getTransactionWiseDeallions(userTokenKey)

        if (isNetworkAvailable(this)) {
            transactionWisePresenter.getTransactionWiseDeallions(userTokenKey)
            LoadingDialog.getInstance().show(this)
        } else {
            LoadingDialog.getInstance().cancel()
        }

        iv_back.setOnClickListener {
            finish()
        }

    }

    override fun onGettingTransactionWiseDeallionsSuccess(transactionWiseResponse: TransactionWiseResponse) {
        LoadingDialog.getInstance().cancel()
        try{
            val transactionvaluepoints: Transactionvaluepoints? = transactionWiseResponse.transactionvaluepoints?.filter {it.merchant_id!!.equals(murchentKey) }?.first()
            transactionvaluepoints?.run {
                Glide.with(this@TransactionDetailsActivity).load(profile_image)
                    .error(R.drawable.deallionaires_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_profile)

                tv_business_name.text = business_name
                tv_address.text = address
                rv_transaction_history.adapter = TransactionDeatilsAdapter(
                    this@TransactionDetailsActivity,
                    transactionDetails?.let { it } ?: ArrayList()
                )
                rv_transaction_history.layoutManager = LinearLayoutManager(
                    this@TransactionDetailsActivity,
                    LinearLayoutManager.VERTICAL,
                    false
                )

            }
        }catch (e:Exception){
            e.printStackTrace()
        }


    }

    override fun onGettingTransactionWiseDeallionsFail(message: String) {
        LoadingDialog.getInstance().cancel()
    }

    override fun onGettingTransactionWiseNoInternet() {
        LoadingDialog.getInstance().cancel()
    }
}