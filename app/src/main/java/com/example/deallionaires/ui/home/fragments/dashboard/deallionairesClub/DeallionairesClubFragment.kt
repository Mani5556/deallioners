package com.example.deallionaires.ui.home.fragments.dashboard.deallionairesClub

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.deallionaires.R
import com.example.deallionaires.adapters.DeallionairesClubAdapter
import com.example.deallionaires.model.DeallionairesClub
import com.example.deallionaires.model.DeallionairesClubResponse
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_deallionaires_club.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class DeallionairesClubFragment : Fragment(), DeallionairesClubView {

    lateinit var activity: Activity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deallionaires_club, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        val deallionairesClubPresenter = DeallionairesClubPresenterImpl(activity, this)
        if (isNetworkAvailable(activity)) {
            deallionairesClubPresenter.getDeallionairesClub(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }

    }

    override fun displayDeallionairesClub(deallionairesClubResponse: DeallionairesClubResponse) {
        LoadingDialog.getInstance().cancel()
        if (deallionairesClubResponse.deallionairesClub == null) {
            activity.tvNoDeallionairesClubFound.visibility = View.VISIBLE
            activity.rvDeallionairesClub.visibility = View.GONE
        } else {
            activity.tvNoDeallionairesClubFound.visibility = View.GONE
            activity.rvDeallionairesClub.visibility = View.VISIBLE
            activity.rvDeallionairesClub.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            activity.rvDeallionairesClub.adapter =
                DeallionairesClubAdapter(
                    activity,
                    deallionairesClubResponse.deallionairesClub as ArrayList<DeallionairesClub>
                ) {
                    activity.startActivity(activity.intentFor<SearchBusinessDetailsActivity>(Constants.MERCHANT_ID to it))
                }
        }
    }

    override fun noDeallionairesClub(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun deallionairesClubNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(activity.resources.getString(R.string.txt_please_check_your_internet_connection))
    }

}
