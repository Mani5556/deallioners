package com.example.deallionaires.ui.dealsStatus.pendingDeals

import android.content.Context
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response

class PendingDealsPresenterImpl(
    private val context: Context,
    private var pendingDealsView: PendingDealsView?
) : PendingDealsPresenter {
    override fun getGetPendingDealsPresenter(tokenKey: String) {
        if (isNetworkAvailable(context)) {
            APIRequests.getUserPendingDeals(tokenKey, object : NWResponseCallback<DealStatusResponse> {
                override fun onSuccess(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {
                    val myPendingDeals = ArrayList<DealStatus>()
                    for (i in response.body()!!.getreedemdetail.indices) {
                        if (response.body()!!.getreedemdetail[i].status == 0) {
                            myPendingDeals.add(
                                DealStatus(
                                    response.body()!!.getreedemdetail[i].redeemdealid,
                                    response.body()!!.getreedemdetail[i].merchantid,
                                    response.body()!!.getreedemdetail[i].dealid,
                                    response.body()!!.getreedemdetail[i].business_name,
                                    response.body()!!.getreedemdetail[i].city,
                                    response.body()!!.getreedemdetail[i].address,
                                    response.body()!!.getreedemdetail[i].latitude,
                                    response.body()!!.getreedemdetail[i].longitude,
                                    response.body()!!.getreedemdetail[i].profile_image,
                                    response.body()!!.getreedemdetail[i].title,
                                    response.body()!!.getreedemdetail[i].expiry_date,
                                    response.body()!!.getreedemdetail[i].deallions_offered,
                                    response.body()!!.getreedemdetail[i].total_deallions,
                                    response.body()!!.getreedemdetail[i].regular_price,
                                    response.body()!!.getreedemdetail[i].offer_price,
                                    response.body()!!.getreedemdetail[i].transaction_id,
                                    response.body()!!.getreedemdetail[i].sys_ref_code,
                                    response.body()!!.getreedemdetail[i].verification_code,
                                    response.body()!!.getreedemdetail[i].purchase_amount,
                                    response.body()!!.getreedemdetail[i].saving_amount,
                                    response.body()!!.getreedemdetail[i].status,
                                    response.body()!!.getreedemdetail[i].offer_category_id,
                                    response.body()!!.getreedemdetail[i].offer_type_id,
                                    response.body()!!.getreedemdetail[i].customer_base_offer_type_id,
                                    response.body()!!.getreedemdetail[i].created_at,
                                    response.body()!!.getreedemdetail[i].deallionsUsed,
                                    response.body()!!.getreedemdetail[i].deallionsEarned
                                )
                            )
                        }
                    }
                    pendingDealsView!!.onGettingPendingDealsSuccess(myPendingDeals)
                }

                override fun onResponseBodyNull(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {
                    pendingDealsView!!.onGettingPendingDealsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {

                    pendingDealsView!!.onGettingPendingDealsFail(response.message())
                }

                override fun onFailure(call: Call<DealStatusResponse>, t: Throwable) {
                    pendingDealsView!!.onGettingPendingDealsFail(t.message.toString())
                }
            })
        } else {
            pendingDealsView!!.onGettingPendingDealsNoInternet()
        }
    }

    override fun claimToCompleteDeal(
        userToken: String, redeemdealid: Int, claimDeal: ClaimDeal
    ) {
        if (isNetworkAvailable(context)) {
            APIRequests.postClaimToCompleteRequest(
                userToken,
                redeemdealid,
                claimDeal,
                object : NWResponseCallback<ClaimDealResponse> {
                    override fun onSuccess(call: Call<ClaimDealResponse>, response : Response<ClaimDealResponse>) {
                        pendingDealsView!!.onClaimSuccess(response.body()!!.msg)
                    }

                    override fun onResponseBodyNull(
                        call: Call<ClaimDealResponse>,
                        response: Response<ClaimDealResponse>
                    ) {
                        pendingDealsView!!.onClaimFail(response.body().toString())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<ClaimDealResponse>,
                        response: Response<ClaimDealResponse>
                    ) {
                        val gson = Gson()
                        val type = object : TypeToken<ClaimDealResponse>() {}.type
                        var errorResponse: ClaimDealResponse? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        pendingDealsView!!.onClaimFail(errorResponse?.msg?:"Some thing went wrong")
                    }

                    override fun onFailure(call: Call<ClaimDealResponse>, t: Throwable) {
                        pendingDealsView!!.onClaimFail(t.message!!)
                    }

                })
        } else {
            pendingDealsView!!.onClaimNoInternet()
        }
    }

    override fun destroyPendingDeals() {
        pendingDealsView = null
    }
}