package com.example.deallionaires.ui.dealsStatus.completedDeals

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.deallionaires.R
import com.example.deallionaires.adapters.CompletedDealsAdapter
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.example.deallionaires.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_completed_deals.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class CompletedDealsFragment : Fragment(), CompletedDealsView {

    lateinit var activity: Activity

    lateinit var userKey: String

    lateinit var completedDealsPresenter: CompletedDealsPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_completed_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        completedDealsPresenter = CompletedDealsPresenterImpl(activity, this)
        if (isNetworkAvailable(activity)) {
            completedDealsPresenter.getGetCompletedDealsPresenter(userTokenKey)
            LoadingDialog.getInstance().show(activity)
        } else {
            activity.toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }
    }

    override fun onGettingCompletedDealsSuccess(deals: ArrayList<DealStatus>) {
        LoadingDialog.getInstance().cancel()
        if (deals.isEmpty()) {
            activity.tvNoCompletedDealsFound.visibility = View.VISIBLE
            activity.rvCompletedDeals.visibility = View.GONE
        } else {
            activity.tvNoCompletedDealsFound.visibility = View.GONE
            activity.rvCompletedDeals.visibility = View.VISIBLE
            activity.rvCompletedDeals.layoutManager =
                LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            activity.runOnUiThread {
                activity.rvCompletedDeals.adapter =
                    CompletedDealsAdapter(activity, deals) { deal: DealStatus, _: Boolean ->
                        activity.startActivity(
                            activity.intentFor<SearchDealDetailsActivity>(
                                Constants.DEAL_ID to deal.dealid,
                                Constants.FROM_ACTIVITY to Constants.FROM_COMPLETED_DEALS_FRAGMENT
                            )
                        )
                    }
            }
        }
    }

    override fun onGettingCompletedDealsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGettingCompletedDealsNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
    }

}
