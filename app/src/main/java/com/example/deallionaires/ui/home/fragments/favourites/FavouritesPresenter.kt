package com.example.deallionaires.ui.home.fragments.favourites

interface FavouritesPresenter {

    fun getFavourites(userTokenKey: String)

    fun removeDealFromFavourites(userTokenKey: String, merchantid: Int)

    fun destroyFavourites()

}