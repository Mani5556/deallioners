package com.example.deallionaires.ui.home.fragments.home.homeCategoryDeals

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.deallionaires.R
import com.example.deallionaires.adapters.HomeCategoryDealsAdapter
import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.Searchbusiness
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.utils.*
import kotlinx.android.synthetic.main.fragment_home_category_deals.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class HomeCategoryDealsFragment : Fragment(), HomeCategoryView {

    private lateinit var homeCategoryPresenter: HomeCategoryPresenter

    //    private lateinit var userTokenKeyString: String
    private var searchBusinessList = ArrayList<Searchbusiness>()
    private val categoryDealsFilterList = ArrayList<String>()
    private var searchBusinessFilterList = ArrayList<Searchbusiness>()
    private lateinit var selectedCategoryName: String
    private lateinit var selectedCategorySlug: String
    var isFromOnPause = false

    lateinit var activity: Activity
    lateinit var userKey: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_category_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!

        val userTokenKey by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        val mainCategoryName by PreferenceExt.appPreference(
            activity,
            Constants.MAIN_CATEGORY_NAME,
            Constants.DEFAULT_STRING
        )

        val categorySlug by PreferenceExt.appPreference(
            activity,
            Constants.SLUG,
            Constants.DEFAULT_STRING
        )

        userKey = userTokenKey

        selectedCategoryName = mainCategoryName
        selectedCategorySlug = categorySlug


        homeCategoryPresenter = HomeCategoryPresenter(activity, this)

        if (isNetworkAvailable(activity)) {
            homeCategoryPresenter.getHomeCategoryDealItems(
                userTokenKey,
                selectedCategorySlug
            )
            LoadingDialog.getInstance().show(activity)
        }

    }

    override fun onGetHomeCategoryItemsSuccess(searchbusiness: List<Searchbusiness>) {
        searchBusinessList = searchbusiness as ArrayList<Searchbusiness>
        activity.rvHomeCategoryDeals.layoutManager =
            GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)

        for (i in 0 until searchBusinessList.size) {
            if (searchBusinessList[i].subcategoryName != null && !categoryDealsFilterList.contains(
                    searchBusinessList[i].subcategoryName
                )
            ) {
                categoryDealsFilterList.add(searchBusinessList[i].subcategoryName!!)
            }
        }
        if (!categoryDealsFilterList.contains("Sub Categories")) {
            categoryDealsFilterList.add(0, "Sub Categories")
        }

        if (!categoryDealsFilterList.contains("All")) {
            categoryDealsFilterList.add(1, "All")
        }

        val spinnerArrayAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(
                activity, android.R.layout.simple_spinner_dropdown_item, categoryDealsFilterList
            )
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        activity.spHomeCategoryDealsSubCategory.adapter = spinnerArrayAdapter
        activity.spHomeCategoryDealsSubCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    addHomeCategoryDeals(selectedCategoryName, searchBusinessList)
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (spHomeCategoryDealsSubCategory.selectedItem.toString() == "All" || spHomeCategoryDealsSubCategory.selectedItem.toString() == "Sub Categories") {
                        addHomeCategoryDeals(selectedCategoryName, searchBusinessList)
                    } else {
                        if (searchBusinessFilterList.size != 0) {
                            searchBusinessFilterList.clear()
                        }
                        for (i in 0 until searchBusinessList.size) {
                            if (spHomeCategoryDealsSubCategory.selectedItem.toString() == searchBusinessList[i].subcategoryName) {
                                searchBusinessFilterList.add(
                                    Searchbusiness(
                                        searchBusinessList[i].merchantid,
                                        searchBusinessList[i].status,
                                        searchBusinessList[i].slug,
                                        searchBusinessList[i].subcategoryName,
                                        searchBusinessList[i].businessName,
                                        searchBusinessList[i].address,
                                        searchBusinessList[i].profileImage,
                                        searchBusinessList[i].businessDescription,
                                        searchBusinessList[i].followers,
                                        searchBusinessList[i].rating,
                                        searchBusinessList[i].isLiked
                                    )
                                )
                            }
                        }
                        addHomeCategoryDeals(selectedCategoryName, searchBusinessFilterList)
                    }
                }
            }
        addHomeCategoryDeals(selectedCategoryName, searchBusinessList)
        LoadingDialog.getInstance().cancel()
    }

    private fun addHomeCategoryDeals(
        selectedCategoryName: String,
        searchBusinessList: ArrayList<Searchbusiness>
    ) {
        activity.rvHomeCategoryDeals.adapter =
            HomeCategoryDealsAdapter(
                activity,
                selectedCategoryName,
                searchBusinessList
            ) { searchbusinessFromAdapter: Searchbusiness, isFavouriteClicked: Boolean, isLiked: Int ->
                if (isFavouriteClicked) {
                    val userSkipLogin by PreferenceExt.appPreference(
                        activity,
                        Constants.USER_SKIP_LOGIN,
                        false
                    )
                    if (userSkipLogin) {
                        displayLoginDialog()
                    } else {
                        if (isLiked == 1) {
                            homeCategoryPresenter.removeDealFromFavourites(
                                userKey,
                                searchbusinessFromAdapter.merchantid
                            )
                        } else {
                            homeCategoryPresenter.addDealToFavourites(
                                userKey,
                                searchbusinessFromAdapter.merchantid
                            )
                        }
                    }
                } else {
                    activity.startActivity(
                        activity.intentFor<SearchBusinessDetailsActivity>(
                            Constants.MERCHANT_ID to searchbusinessFromAdapter.merchantid
                        )
                    )
                }
            }
    }

    override fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded) {
        activity.toast(favouriteAdded.msg.toString())
        homeCategoryPresenter.getHomeCategoryDealItems(userKey, selectedCategorySlug)
        LoadingDialog.getInstance().show(activity)
    }

    override fun onAddingDealToFavouritesFail(message: String) {
        activity.toast(message)
    }

    override fun onRemovingDealToFavouritesSuccess(favouriteRemoved: FavouriteRemoved) {
        activity.toast(favouriteRemoved.msg.toString())
        homeCategoryPresenter.getHomeCategoryDealItems(userKey, selectedCategorySlug)
    }

    override fun onRemovingDealToFavouritesFail(message: String) {
        activity.toast(message)
    }

    override fun onGetHomeCategoryItemsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        activity.toast(message)
    }

    override fun onGetHomeCategoryNoInternet() {
        LoadingDialog.getInstance().cancel()
        activity.toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    private fun displayLoginDialog() {
        CustomDialog(activity).showDialog(
            "Login to add Favourites",
            "Login",
            "Not Now"
        ) {
            val intent = Intent(activity, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            activity.startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        if (isFromOnPause) {
            if (isNetworkAvailable(activity)) {
                homeCategoryPresenter.getHomeCategoryDealItems(
                    userKey,
                    selectedCategorySlug
                )
                LoadingDialog.getInstance().show(activity)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        isFromOnPause = true
    }
}
