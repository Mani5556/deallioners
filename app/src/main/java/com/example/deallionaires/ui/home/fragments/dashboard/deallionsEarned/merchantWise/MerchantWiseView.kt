package com.example.deallionaires.ui.home.fragments.dashboard.deallionsEarned.merchantWise

import com.example.deallionaires.model.MerchantWiseDeallionsResponse

interface MerchantWiseView {

    fun onGettingMerchantWiseDeallionsSuccess(merchantWiseDeallionsResponse: MerchantWiseDeallionsResponse)

    fun onGettingMerchantWiseDeallionsFail(message: String)

    fun onGettingMerchantWiseNoInternet()

}