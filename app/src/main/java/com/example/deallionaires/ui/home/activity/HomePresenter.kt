package com.example.deallionaires.ui.home.activity

import android.content.Context
import com.example.deallionaires.model.DeallionairesCount
import com.example.deallionaires.model.ResponseProfile
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class HomePresenter(private val context: Context, private var homeView: HomeView?) {

    fun getHomeProfilePresenter(tokenKey: String) {
        if (isNetworkAvailable(context)) {
            APIRequests.getUserDetails(tokenKey, object : NWResponseCallback<ResponseProfile> {
                override fun onSuccess(
                    call: Call<ResponseProfile>,
                    response: Response<ResponseProfile>
                ) {
                    ResponseProfile().profile = response.body()!!.profile
                    if(response.body()?.profile?.size!! > 0){
                        homeView?.onGettingHomeProfileSuccess(response.body()?.profile?.get(0))
                    }

                }

                override fun onResponseBodyNull(
                    call: Call<ResponseProfile>,
                    response: Response<ResponseProfile>
                ) {
                    homeView?.onGettingHomeProfileFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<ResponseProfile>,
                    response: Response<ResponseProfile>
                ) {
                    homeView?.onGettingHomeProfileFail(response.message())
                }

                override fun onFailure(call: Call<ResponseProfile>, t: Throwable) {
                    homeView?.onGettingHomeProfileFail(t.message!!)
                }
            })
        } else {
            homeView!!.onGettingHomeProfileNoInternet()
        }
    }

    fun getDeallionairesCount(tokenKey: String){
        if (isNetworkAvailable(context)) {
            APIRequests.getUserDeallionairesCount(tokenKey, object : NWResponseCallback<DeallionairesCount> {
                override fun onSuccess(
                    call: Call<DeallionairesCount>,
                    response: Response<DeallionairesCount>
                ) {
                    homeView?.onGettingHomeDeallionairesCountSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(
                    call: Call<DeallionairesCount>,
                    response: Response<DeallionairesCount>
                ) {
                    homeView?.onGettingHomeDeallionairesCountFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<DeallionairesCount>,
                    response: Response<DeallionairesCount>
                ) {
                    homeView?.onGettingHomeDeallionairesCountFail(response.message())
                }

                override fun onFailure(call: Call<DeallionairesCount>, t: Throwable) {
                    homeView?.onGettingHomeDeallionairesCountFail(t.message!!)
                }
            })
        } else {
            homeView?.onGettingHomeProfileNoInternet()
        }
    }
}