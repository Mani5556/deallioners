package com.example.deallionaires.ui.mobileVerification

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.deallionaires.R
import com.example.deallionaires.model.*
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.resetPassword.ResetPasswordActivity
import com.example.deallionaires.utils.*
import com.example.deallionaires.utils.SmsBroadcastReceiver.SmsBroadcastReceiverListener
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.android.synthetic.main.activity_mobile_verification.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class MobileVerificationActivity : AppCompatActivity(), MobileVerificationView {

    private val REQ_USER_CONSENT = 200
    var smsBroadcastReceiver: SmsBroadcastReceiver? = null

    private var signUpMobileNumberVerified by PreferenceExt.appPreference(
        this,
        Constants.VERIFIED_MOBILE_NUMBER,
        false
    )

    private var profileMobileNumberVerified by PreferenceExt.appPreference(
        this,
        Constants.VERIFIED_PROFILE_MOBILE_NUMBER,
        false
    )

    var signUpMobNumVerified by PreferenceExt.appPreference(
        this,
        Constants.SIGN_UP_MOBILE_NUM_VERIFIED,
        false
    )

    private var isUserLogin by PreferenceExt.appPreference(this, Constants.IS_USER_LOGIN, false)

    private var userTokenKey by PreferenceExt.appPreference(
        this,
        Constants.USER_TOKEN_KEY,
        Constants.DEFAULT_STRING
    )

    private var userSkipLogin by PreferenceExt.appPreference(
        this,
        Constants.USER_SKIP_LOGIN,
        false
    )

    private lateinit var signUpTokenKey: String
    private var otpVerificationToken: String = ""

    private lateinit var fromActivity: String
    private lateinit var countryCode: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_verification)

        val mobileVerificationPresenter = MobileVerificationPresenter(this, this)

        val verificationText = "Enter the verification code we just sent to your mobile number : "

        val enteredMobileNumber = intent.getStringExtra(
            Constants.VERIFICATION_MOBILE_NUMBER
        )!!

        fromActivity = intent.getStringExtra(Constants.FROM_ACTIVITY)!!

        if (fromActivity == Constants.FROM_MOBILE_NUMBER) {
            signUpTokenKey = intent.getStringExtra(Constants.SIGN_UP_ACCESS_TOKEN)!!
        }

//        countryCode = if(fromActivity == Constants.FORGOT_PASSWORD){
//            ""
//        }else {
//            intent.getStringExtra(Constants.COUNTRY_CODE)!!
//        }

        countryCode = intent.getStringExtra(Constants.COUNTRY_CODE)!!

        val verificationMobileNumberText: String =
            verificationText + "+" + countryCode + " " + enteredMobileNumber.toUpperCase(Locale.ROOT)

        val spannable: Spannable = SpannableString(verificationMobileNumberText)

        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorYellow)),
            verificationText.length,
            ("$verificationText+$countryCode " + intent.getStringExtra(
                Constants.VERIFICATION_MOBILE_NUMBER
            )).length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        tvVerificationMobileNum.setText(spannable, TextView.BufferType.SPANNABLE)

        startSmsUserConsent()

        getOtpBasedOnFromActivity(mobileVerificationPresenter)

        ivMobileNumVerificationBack.setOnClickListener {
            onBackPressed()
        }

        etMobileVerifyOtpFirstDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    etMobileVerifyOtpSecondDigit.requestFocus()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        etMobileVerifyOtpSecondDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    etMobileVerifyOtpThirdDigit.requestFocus()
                } else {
                    etMobileVerifyOtpFirstDigit.requestFocus()
                    etMobileVerifyOtpFirstDigit.setSelection(1)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        etMobileVerifyOtpThirdDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    etMobileVerifyOtpFourthDigit.requestFocus()
                } else {
                    etMobileVerifyOtpSecondDigit.requestFocus()
                    etMobileVerifyOtpSecondDigit.setSelection(1)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        etMobileVerifyOtpFourthDigit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.length == 1) {
                    hideKeyboard(etMobileVerifyOtpFourthDigit)
                    etMobileVerifyOtpFourthDigit.clearFocus()
                } else {
                    etMobileVerifyOtpThirdDigit.requestFocus()
                    etMobileVerifyOtpThirdDigit.setSelection(1)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
            }
        })

        tvMobileOtpResend.paintFlags = tvMobileOtpResend.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        val userTokenKey by PreferenceExt.appPreference(
            this,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        tvMobileVerifyOtpVerify.setOnClickListener {
            if (etMobileVerifyOtpFirstDigit.text.toString()
                    .isEmpty() || etMobileVerifyOtpSecondDigit.text.toString()
                    .isEmpty() || etMobileVerifyOtpThirdDigit.text.toString()
                    .isEmpty() || etMobileVerifyOtpFourthDigit.text.toString().isEmpty()
            ) {
                toast("Please enter OTP")
            } else {
                val otp =
                    etMobileVerifyOtpFirstDigit.text.toString() +
                            etMobileVerifyOtpSecondDigit.text.toString() +
                            etMobileVerifyOtpThirdDigit.text.toString() +
                            etMobileVerifyOtpFourthDigit.text.toString()
                if (isNetworkAvailable(this)) {
                    when (fromActivity) {
                        Constants.FROM_SIGN_UP -> {
                            val otpVerification = OtpVerification(otp, "mobile_verification")
                            mobileVerificationPresenter.getVerifyMobileOtpForSignUp(
                                otpVerificationToken,
                                otpVerification
                            )
                        }
                        Constants.FORGOT_PASSWORD -> {
                            val otpVerification = OtpVerification(otp, "verification")
                            mobileVerificationPresenter.getVerifyOtpForForgotPassword(
                                otpVerificationToken,
                                otpVerification
                            )
                        }
                        Constants.FROM_PROFILE -> {
                            val mobNumChangeWithCountryCode = MobNumChangeOtpVerification(
                                intent.getStringExtra(Constants.COUNTRY_CODE)!!.toInt(),
                                intent.getStringExtra(Constants.VERIFICATION_MOBILE_NUMBER)!!
                                    .toLong(),
                                otp,
                                "verification"
                            )
                            mobileVerificationPresenter.getVerifyMobNumChangeOtp(
                                userTokenKey,
                                mobNumChangeWithCountryCode
                            )
                        }
                        Constants.FROM_MOBILE_NUMBER -> {
                            val mobNumChangeWithCountryCode = MobNumChangeOtpVerification(
                                intent.getStringExtra(Constants.COUNTRY_CODE)!!.toInt(),
                                intent.getStringExtra(Constants.VERIFICATION_MOBILE_NUMBER)!!
                                    .toLong(),
                                otp,
                                "verification"
                            )
                            mobileVerificationPresenter.getVerifyMobNumSignUpOtp(
                                signUpTokenKey,
                                mobNumChangeWithCountryCode
                            )
                        }
                    }
                } else {
                    toast(resources.getString(R.string.txt_please_check_your_internet_connection))
                }
            }

        }

        tvMobileOtpResend.setOnClickListener {
            getOtpBasedOnFromActivity(mobileVerificationPresenter)
        }
    }

    private fun startSmsUserConsent() {
        val client = SmsRetriever.getClient(this)
        client.startSmsUserConsent(null)
            .addOnSuccessListener {
//                Toast.makeText(
//                    applicationContext,
//                    "On Success",
//                    Toast.LENGTH_LONG
//                ).show()
                Log.e("Mobile Verification", "Sms Retriver Success")
            }
            .addOnFailureListener {
//                Toast.makeText(
//                    applicationContext,
//                    "On OnFailure",
//                    Toast.LENGTH_LONG
//                ).show()
                Log.e("Mobile Verification", "Sms Retriver failed")
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_USER_CONSENT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                val message: String? = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)
//                Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
                getOtpFromMessage(message)
            }
        }
    }

    private fun getOtpFromMessage(message: String?) {
        val pattern: Pattern = Pattern.compile("(|^)\\d{4}")
        val matcher: Matcher = pattern.matcher(message as CharSequence)
        if (matcher.find()) {
            etMobileVerifyOtpFirstDigit.setText(matcher.group(0)!![0].toString())
            etMobileVerifyOtpSecondDigit.setText(matcher.group(0)!![1].toString())
            etMobileVerifyOtpThirdDigit.setText(matcher.group(0)!![2].toString())
            etMobileVerifyOtpFourthDigit.setText(matcher.group(0)!![3].toString())
            etMobileVerifyOtpFourthDigit.requestFocus()
        }
    }

    private fun getOtpBasedOnFromActivity(mobileVerificationPresenter: MobileVerificationPresenter) {
        val userTokenKey by PreferenceExt.appPreference(
            this,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )
        if (isNetworkAvailable(this)) {
            when (fromActivity) {
                Constants.FORGOT_PASSWORD -> {
                    val mobileVerification = MobileVerification(
                        "+" +intent.getStringExtra(Constants.COUNTRY_CODE)!!,
                        intent.getStringExtra(Constants.VERIFICATION_MOBILE_NUMBER)!!,
                        "mobile"
                    )
                    mobileVerificationPresenter.getMobileOtpForForgotPassword(
                        userTokenKey,
                        mobileVerification
                    )
                }
                Constants.FROM_MOBILE_NUMBER -> {
                    val mobileVerification = MobileVerificationWithCountryCode(
                        intent.getStringExtra(Constants.COUNTRY_CODE)!!.toInt(),
                        intent.getStringExtra(Constants.VERIFICATION_MOBILE_NUMBER)!!.toLong(),
                        "mobile"
                    )
                    mobileVerificationPresenter.getMobileVerificationOtpForMobNumForSignUp(
                        intent.getStringExtra(Constants.SIGN_UP_ACCESS_TOKEN)!!,
                        mobileVerification
                    )
                }
                Constants.FROM_PROFILE -> {
                    val mobileVerification = MobileVerificationWithCountryCode(
                        intent.getStringExtra(Constants.COUNTRY_CODE)!!.toInt(),
                        intent.getStringExtra(Constants.VERIFICATION_MOBILE_NUMBER)!!.toLong(),
                        "mobile"
                    )
                    mobileVerificationPresenter.getMobileVerificationOtpForMobNumChange(
                        userTokenKey,
                        mobileVerification
                    )
                }
                Constants.FROM_SIGN_UP -> {
                    val mobileVerification = MobileVerificationWithCountryCode(
                        intent.getStringExtra(Constants.COUNTRY_CODE)!!.toInt(),
                        intent.getStringExtra(Constants.VERIFICATION_MOBILE_NUMBER)!!.toLong(),
                        "mobile"
                    )
                    mobileVerificationPresenter.getMobileVerificationOtpForSignUp(
                        userTokenKey,
                        mobileVerification
                    )
                }
            }
        } else {
            toast(resources.getString(R.string.txt_please_check_your_internet_connection))
        }
    }

    override fun onMobileVerificationSuccess(mobileVerificationResponse: MobileVerificationResponse) {
        toast(mobileVerificationResponse.msg!!)
        otpVerificationToken = if (mobileVerificationResponse.token != null) {
            mobileVerificationResponse.token!!
        } else {
            ""
        }
    }

    override fun onMobileVerificationFailed(message: String) {
        toast(message)
    }

    override fun onOtpVerificationSuccess(otpVerificationResponse: OtpVerificationResponse) {
        toast(otpVerificationResponse.msg!!)
        when (fromActivity) {
            Constants.FORGOT_PASSWORD -> {
                startActivity(intentFor<ResetPasswordActivity>(Constants.FORGOT_PASSWORD_ACCESS_TOKEN to otpVerificationResponse.token))
            }
            Constants.FROM_PROFILE -> {
                profileMobileNumberVerified = true
                onBackPressed()
            }
            Constants.FROM_SIGN_UP -> {
                signUpMobileNumberVerified = true
                onBackPressed()
            }
            Constants.FROM_MOBILE_NUMBER -> {
                isUserLogin = true
                userSkipLogin = false
                signUpMobNumVerified = true
                userTokenKey = otpVerificationResponse.token!!
                Log.e("User token key", otpVerificationResponse.token!!)
                val intent = Intent(this, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.putExtra(Constants.FROM_ACTIVITY, Constants.FROM_LOGIN)
                startActivity(intent)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        registerBroadcastReceiver()
    }

    private fun registerBroadcastReceiver() {
        smsBroadcastReceiver = SmsBroadcastReceiver()
        smsBroadcastReceiver!!.smsBroadcastReceiverListener =
            object : SmsBroadcastReceiverListener {
                override fun onSuccess(intent: Intent?) {
                    startActivityForResult(intent, REQ_USER_CONSENT)
                }

                override fun onFailure() {
                    Log.e("Mobile Verification", "Broadcast reciver failed")
                }
            }
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(smsBroadcastReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(smsBroadcastReceiver);
    }

    override fun onOtpVerificationFailed(message: String) {
        toast(message)
    }

    override fun onMobileVerificationNoInternet() {
        toast(resources.getString(R.string.txt_please_check_your_internet_connection))
    }
}