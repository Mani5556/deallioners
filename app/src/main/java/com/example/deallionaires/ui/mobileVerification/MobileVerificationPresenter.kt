package com.example.deallionaires.ui.mobileVerification

import android.content.Context
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class MobileVerificationPresenter(
    private val context: Context,
    private val mobileVerificationView: MobileVerificationView?
) {
    fun getMobileVerificationOtpForSignUp(userAccessKey: String, mobileVerificationWithCountryCode: MobileVerificationWithCountryCode) {
        if (isNetworkAvailable(context)) {
            APIRequests.getSignUpMobileOtp(
                userAccessKey,
                mobileVerificationWithCountryCode,
                object : NWResponseCallback<MobileVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onMobileVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onMobileVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onMobileVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getMobileVerificationOtpForMobNumChange(userAccessKey: String, mobileVerificationWithCountryCode: MobileVerificationWithCountryCode) {
        if (isNetworkAvailable(context)) {
            APIRequests.getOtpForMobNumChange(
                userAccessKey,
                mobileVerificationWithCountryCode,
                object : NWResponseCallback<MobileVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onMobileVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onMobileVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onMobileVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getMobileVerificationOtpForMobNumForSignUp(userAccessKey: String, mobileVerificationWithCountryCode: MobileVerificationWithCountryCode) {
        if (isNetworkAvailable(context)) {
            APIRequests.getOtpForMobNumSignUp(
                userAccessKey,
                mobileVerificationWithCountryCode,
                object : NWResponseCallback<MobileVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onMobileVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onMobileVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onMobileVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getVerifyMobileOtpForSignUp(userAccessKey: String, otpVerification: OtpVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getSignUpVerifyOtp(
                userAccessKey,
                otpVerification,
                object : NWResponseCallback<OtpVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onOtpVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onOtpVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onOtpVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getMobileOtpForForgotPassword(userAccessKey: String, mobileVerification: MobileVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getForgotPasswordMobileOtp(
                userAccessKey,
                mobileVerification,
                object : NWResponseCallback<MobileVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onMobileVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onMobileVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<MobileVerificationResponse?>,
                        response: Response<MobileVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onMobileVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onMobileVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getVerifyOtpForForgotPassword(userAccessKey: String, otpVerification: OtpVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getVerifyOtpForForgotPassword(
                userAccessKey,
                otpVerification,
                object : NWResponseCallback<OtpVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onOtpVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onOtpVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onOtpVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getVerifyMobNumChangeOtp(userAccessKey: String, mobNumChangeOtpVerification: MobNumChangeOtpVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getVerifyMobNumChangeOtp(
                userAccessKey,
                mobNumChangeOtpVerification,
                object : NWResponseCallback<OtpVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onOtpVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onOtpVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onOtpVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }

    fun getVerifyMobNumSignUpOtp(userAccessKey: String, mobNumChangeOtpVerification: MobNumChangeOtpVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getVerifyMobNumSignUpOtp(
                userAccessKey,
                mobNumChangeOtpVerification,
                object : NWResponseCallback<OtpVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            mobileVerificationView!!.onOtpVerificationSuccess(response.body()!!)
                        } else {
                            mobileVerificationView!!.onOtpVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        mobileVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                        mobileVerificationView!!.onOtpVerificationFailed(t.message!!)
                    }
                })
        } else {
            mobileVerificationView!!.onMobileVerificationNoInternet()
        }
    }
}