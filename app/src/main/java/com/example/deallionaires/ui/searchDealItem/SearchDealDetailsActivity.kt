package com.example.deallionaires.ui.searchDealItem

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Layout
import android.text.format.DateUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.example.deallionaires.R
import com.example.deallionaires.adapters.BadgeAdapter
import com.example.deallionaires.model.Dealsdetail
import com.example.deallionaires.model.RedeemDealResponse
import com.example.deallionaires.model.RedeemDeal
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.login.LoginActivity
import com.example.deallionaires.ui.pendingDeals.PendingDealsActivity
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.utils.*
import kotlinx.android.synthetic.main.activity_search_business_details.*
import kotlinx.android.synthetic.main.activity_search_deal_details.*
import kotlinx.android.synthetic.main.content_search_deals_item.*
import kotlinx.android.synthetic.main.content_search_deals_item.tv_description
import kotlinx.android.synthetic.main.content_search_deals_item.tv_expires
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.time.milliseconds

class SearchDealDetailsActivity : AppCompatActivity(), SearchDealDetailsView {

    private var dealsDetailResponse: Dealsdetail? = null

    private var textDescriptionIsExpanded = false
    private var textTermsAndConditionsIsExpanded = false
    var dealId: Int = 0
    var fromActivity: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_deal_details)
        setSupportActionBar(tbUserDealsItem)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        dealId = intent.getIntExtra(Constants.DEAL_ID, 0)
        fromActivity = intent.getStringExtra(Constants.FROM_ACTIVITY)

        val userTokenKey by PreferenceExt.appPreference(
            this,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        val userSkipLogin by PreferenceExt.appPreference(
            this,
            Constants.USER_SKIP_LOGIN,
            false
        )

        Log.e("Access Key", userTokenKey)

        val searchDealDetailsPresenter =
            SearchDealDetailsPresenterImpl(this@SearchDealDetailsActivity, this)
        if (isNetworkAvailable(this)) {
            searchDealDetailsPresenter.getDealDetailsResponse(
                userTokenKey,
                dealId
            )
            LoadingDialog.getInstance().show(this)
        } else {
            toast(getString(R.string.txt_please_check_your_internet_connection))
        }

        if (userSkipLogin) {
            lyt_claim_deal.visibility = View.GONE
        } else {
            lyt_claim_deal.visibility = View.VISIBLE
        }





        tvDealDetailClaimDeal.setOnClickListener {
            if (userSkipLogin) {
//                toast(getString(R.string.txt_sign_in_to_claim_deal))
                displayLoginDialog()
            } else {
                CustomDialog(this).showDialog(
                    "Do You Want to Claim The Deal?",
                    "Claim",
                    "Not Now"
                ) {
                    if (dealsDetailResponse != null) {
                        val redeemDeal = RedeemDeal(
                            "" + dealsDetailResponse!!.merchantid,
                            "" + dealId,
                            "" + dealsDetailResponse!!.offerTypeId,
                            "" + dealsDetailResponse!!.offerCategoryId,
                            dealsDetailResponse!!.loyaltyPoints,
                            dealsDetailResponse!!.regularPrice.toInt(),
                            dealsDetailResponse!!.offerPrice.toInt(),
                            dealsDetailResponse!!.discount.toInt(),
                            dealsDetailResponse!!.customerBaseOfferTypeId
                        )
                        searchDealDetailsPresenter.postRedeemDeal(userTokenKey, redeemDeal)
                        LoadingDialog.getInstance().show(this)
                    } else {
                        toast("Claim deal not success, please try again later.")
                    }
                }
            }
        }

        tv_description_view_more.setOnClickListener {
            if (!textDescriptionIsExpanded) {
                tv_description.maxLines = Int.MAX_VALUE
                tv_description_view_more.text = resources.getString(R.string.view_less)
                textDescriptionIsExpanded = true
            } else {
                tv_description.maxLines = 1
                tv_description_view_more.text = resources.getString(R.string.view_more)
                textDescriptionIsExpanded = false
            }
        }

        tv_terms_conditions_view_more.setOnClickListener {
            if (!textTermsAndConditionsIsExpanded) {
                tv_terms_conditions.maxLines = Int.MAX_VALUE
                tv_terms_conditions_view_more.text = resources.getString(R.string.view_less)
                textTermsAndConditionsIsExpanded = true
            } else {
                tv_terms_conditions.maxLines = 1
                tv_terms_conditions_view_more.text = resources.getString(R.string.view_more)
                textTermsAndConditionsIsExpanded = false
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        supportFinishAfterTransition()
    }

    private fun displayLoginDialog() {
        CustomDialog(this).showDialog(
            "Login to claim Deal",
            "Login",
            "Not Now"
        ) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onGetDealDealsSuccess(dealsdetail: Dealsdetail?) {
        if (dealsdetail != null) {
            lyt_deal_body.visibility = View.VISIBLE
            tvNoDealDetailFound.visibility = View.GONE
            dealsDetailResponse = dealsdetail

            val color = getRandomColor()
            Glide.with(this)
                .load(dealsdetail.dealImage)
                .placeholder(color)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(iv_deals_image)

            iv_deals_image.load(dealsdetail.dealImage) {isLoaded->
                if(isLoaded){
                    iv_deals_image.setOnClickListener {imageView->
                        dealsdetail.dealImage?.let {
                            goToFullImageView(it,imageView)
                        }
                    }
                }
            }


            tv_remaining.text = "Remaining : ${dealsdetail.remaining}"
            tv_redeemed.text = "Redeemed : ${dealsdetail.claimed}"
            if (dealsdetail.expiryDate != null) {
                tv_expires.visibility = View.VISIBLE
                tv_expires.text = "Exp : ${getExpiryDays(dealsdetail.expiryDate!!)}"
            } else {
                tv_expires.visibility = View.GONE
            }
            tv_deal_name.run {
                text = dealsdetail.businessName
                setOnClickListener {
                    startActivity(intentFor<SearchBusinessDetailsActivity>(Constants.MERCHANT_ID to dealsdetail.merchantid))
                }
            }
//            tvDealDetailStoreTitle.text = dealsdetail.businessName
            tv_address.text = dealsdetail.address
            tv_deallion_points.text = "${dealsdetail.rewardPoints}"
            tv_variable_points_info.visibility =
                if (dealsdetail.typeOfDeal.equals("fixed", false)) View.GONE else View.VISIBLE
            tv_offer_category.text = ": ${dealsdetail.dealCategory}"
            tv_offer_type.text = ": ${dealsdetail.dealType}"
            tv_description.text = dealsdetail.description

            tv_description_top.text = dealsdetail.title
            tv_terms_conditions.text = dealsdetail.termsAndConditions
            final_price.text = "${dealsdetail.offerPrice}$"
            mrp_price.paintFlags =
                mrp_price.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            mrp_price.text = "${dealsdetail.regularPrice}$"
            tv_discount_percentage.text = "${dealsdetail.discount.toInt()}%"
            getTextEllipsize()


            if (dealsdetail.dealClub.isNullOrEmpty()) {
                lyt_deal_club.visibility = View.GONE
            } else {
                lyt_deal_club.visibility = View.VISIBLE
                rv_deal_club_badge.run {
                    this.adapter = BadgeAdapter(
                        this@SearchDealDetailsActivity,
                        dealsdetail.dealClub.replace("D", "").toInt()
                    )
                    layoutManager = LinearLayoutManager(
                        this@SearchDealDetailsActivity,
                        LinearLayoutManager.HORIZONTAL,
                        false
                    )
                }
            }

            if (dealsdetail.userClub.isNullOrEmpty()) {
                rv_user_club_badge.visibility = View.GONE
                tv_user_club_error.visibility = View.VISIBLE
                tv_user_club_error.text =
                    dealsdetail.userClubInfo ?: "Deallionaires Club not applicable"
            } else {
                rv_user_club_badge.visibility = View.VISIBLE
                tv_user_club_error.visibility = View.GONE
                rv_user_club_badge.run {
                    this.adapter = BadgeAdapter(
                        this@SearchDealDetailsActivity,
                        dealsdetail.userClub.replace("D", "").toInt()
                    )
                    layoutManager = LinearLayoutManager(
                        this@SearchDealDetailsActivity,
                        LinearLayoutManager.HORIZONTAL,
                        false
                    )
                }
            }

            if (fromActivity == Constants.FROM_COMPLETED_DEALS_FRAGMENT || fromActivity == Constants.FROM_PENDING_DEALS_FRAGMENT) {
                lyt_claim_deal.visibility = View.GONE
            } else if (!isAfterCurrentData(dealsdetail.expiryDate)) {
                lyt_claim_deal.visibility = View.GONE
            } else if (!isBeforeCurrentDate(dealsdetail.startDate)) {
                lyt_claim_deal.visibility = View.GONE
            } else if (dealsdetail.dealClub.isNullOrEmpty() && dealsdetail.userClub.isNullOrEmpty()) {
                lyt_claim_deal.visibility = View.VISIBLE
            } else {

                if (dealsdetail.dealClub.isNullOrEmpty()) {
                    lyt_claim_deal.visibility = View.VISIBLE
                } else if (dealsdetail.userClub.isNullOrEmpty() && !dealsdetail.dealClub.isNullOrEmpty()) {
                    lyt_claim_deal.visibility = View.GONE
                } else if (!dealsdetail.userClub.isNullOrEmpty() && !dealsdetail.dealClub.isNullOrEmpty()) {

                    val userDeal = dealsdetail.userClub.split("D").first()?.toInt()
                    val clubDeal = dealsdetail.dealClub.split("D").first()?.toInt()
                    if (userDeal > clubDeal) {
                        lyt_claim_deal.visibility = View.VISIBLE
                    } else {
                        lyt_claim_deal.visibility = View.GONE
                    }
                } else {
                    lyt_claim_deal.visibility = View.VISIBLE
                }

            }
        } else {
            lyt_deal_body.visibility = View.GONE
            tvNoDealDetailFound.visibility = View.VISIBLE
        }
        LoadingDialog.getInstance().cancel()
    }

    private fun getTextEllipsize() {
        val vtoDescription: ViewTreeObserver = tv_description.viewTreeObserver
        vtoDescription.addOnGlobalLayoutListener {
            val lDescription: Layout = tv_description.layout
            val lines = lDescription.lineCount
            if (lines > 0) {
                if (lDescription.getEllipsisCount(lines - 1) > 0) {
                    tv_description_view_more.visibility = View.VISIBLE
                }
            }
        }

        val vtoTermsAndCondition: ViewTreeObserver = tv_terms_conditions.viewTreeObserver
        vtoTermsAndCondition.addOnGlobalLayoutListener {
            val lTermsAndCondition: Layout = tv_terms_conditions.layout
            val lines = lTermsAndCondition.lineCount
            if (lines > 0) {
                if (lTermsAndCondition.getEllipsisCount(lines - 1) > 0) {
                    tv_terms_conditions_view_more.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onGetDealDetailsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        toast(message)
    }

    override fun onGetDealDetailsNoInternet() {
        LoadingDialog.getInstance().cancel()
        toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun onPostRedeemDealSuccess(redeemDealResponse: RedeemDealResponse) {
        LoadingDialog.getInstance().cancel()
//        val intent = Intent(this, HomeActivity::class.java)
//        intent.putExtra("openPendingDeals", true)
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        startActivity(intent)
//        toast(redeemDealResponse.msg)

        startActivity(intentFor<PendingDealsActivity>())
        finish()
    }

    override fun onPostRedeemDealFail(message: String) {
        LoadingDialog.getInstance().cancel()
        toast(message)
    }


    fun isAfterCurrentData(expiry: String?): Boolean {
        try {
            val expiryString = expiry?.replace("[a-z,A-Z]".toRegex(), " ")
            val inputFormate: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val expDate: Date = inputFormate.parse(expiryString)
            val currentTime = inputFormate.parse(inputFormate.format(Date()))

            return if (expDate.after(currentTime)) true else false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }

    fun isBeforeCurrentDate(startString: String?): Boolean {
        try {
            val startStr = startString?.replace("[a-z,A-Z]".toRegex(), " ")
            val inputFormate: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val startDate: Date = inputFormate.parse(startStr)
            val currentTime = inputFormate.parse(inputFormate.format(Date()))
            return if (startDate.before(currentTime)) true else false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }
}
