package com.example.deallionaires.ui.home.fragments.cart

import com.example.deallionaires.model.ClaimDealResponse
import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.RedeemDealResponse

interface CartView {

    fun onGettingCartSuccess(carts: ArrayList<DealStatus>)

    fun onGettingCartFailed(message: String)

    fun cartNoInternet()

    fun onPostRedeemDealSuccess(claimDealResponse: ClaimDealResponse)

    fun onPostRedeemDealFail(message: String)

}