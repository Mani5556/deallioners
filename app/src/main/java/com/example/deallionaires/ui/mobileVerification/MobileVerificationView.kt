package com.example.deallionaires.ui.mobileVerification

import com.example.deallionaires.model.MobileVerificationResponse
import com.example.deallionaires.model.OtpVerificationResponse

interface MobileVerificationView {

    fun onMobileVerificationSuccess(mobileVerificationResponse: MobileVerificationResponse)

    fun onMobileVerificationFailed(message: String)

    fun onOtpVerificationSuccess(otpVerificationResponse: OtpVerificationResponse)

    fun onOtpVerificationFailed(message: String)

    fun onMobileVerificationNoInternet()

}