package com.example.deallionaires.ui.home.activity

import com.example.deallionaires.model.DeallionairesCount
import com.example.deallionaires.model.Profile

interface HomeView {

    fun onGettingHomeProfileSuccess(userProfile: Profile?)

    fun onGettingHomeProfileFail(message: String)

    fun onGettingHomeDeallionairesCountSuccess(deallionairesCount: DeallionairesCount)

    fun onGettingHomeDeallionairesCountFail(message: String)

    fun onGettingHomeProfileNoInternet()

}