package com.example.deallionaires.ui.login

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.basgeekball.awesomevalidation.AwesomeValidation
import com.basgeekball.awesomevalidation.ValidationStyle
import com.basgeekball.awesomevalidation.utility.RegexTemplate
import com.example.deallionaires.R
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIClient
import com.example.deallionaires.network.LoginService
import com.example.deallionaires.ui.forgotPassword.ForgotPasswordActivity
import com.example.deallionaires.ui.home.activity.HomeActivity
import com.example.deallionaires.ui.mobileVerification.MobileNumberActivity
import com.example.deallionaires.ui.signUp.SignUpActivity
import com.example.deallionaires.utils.*
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_sign_up.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit


class LoginActivity : AppCompatActivity(), LoginView {

    private var googleApiClient: GoogleSignInClient? = null
    private val RC_SIGN_IN = 7

    private lateinit var loginPresenter: LoginPresenter

    private var isUserLogin by PreferenceExt.appPreference(this, Constants.IS_USER_LOGIN, false)

    private var userTokenKey by PreferenceExt.appPreference(
        this,
        Constants.USER_TOKEN_KEY,
        Constants.DEFAULT_STRING
    )

    private var userSkipLogin by PreferenceExt.appPreference(
        this,
        Constants.USER_SKIP_LOGIN,
        false
    )

    private val clientId = Constants.INSTAGRAM_APP_ID
    private val clientSecret = Constants.INSTAGRAM_CLIENT_SECRET
    private val redirectUri = "https://com.example.deallionaires/callback"


    lateinit var myAccessToken: String

    lateinit var myShortToken: String
    lateinit var myLongToken: String
    lateinit var userId: String


    var pref: SharedPreferences? = null
    val WEB_VIEW_INTENT_CODE = 856

    private var callbackManager: CallbackManager? = null

    private val EMAIL = "email"

    private val loginValidation = AwesomeValidation(ValidationStyle.BASIC)

    private var isShown = false

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        tvSignUp.setText(Html.fromHtml("CREATE NEW <u>ACCOUNT</u>"))

//        calculateHashKey(packageName)
        loginPresenter = LoginPresenterImpl(this, this)

        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)


        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        googleApiClient = GoogleSignIn.getClient(this, gso)
//        googleApiClient = GoogleSignIn.getClient(this, gso)

        callbackManager = CallbackManager.Factory.create()


        // Callback registration
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    // App code
                    Log.e("Login", "Facebook Login Successful")
                    getUserDetails(loginResult)
                }

                override fun onCancel() {
                    // App code
                    Log.e("login", "Facebook Login Cancelled")
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Log.e(
                        "Login",
                        "Facebook Login Exception" + exception.message
                    )
                }
            })

        ll_login.setOnClickListener {
            validateLoginDetails()
        }

        ibGoogle.setOnClickListener {
            if (isNetworkAvailable(this)) {
                val signInIntent: Intent = googleApiClient!!.signInIntent
                startActivityForResult(signInIntent, RC_SIGN_IN)
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }
//            loginPresenter.loginUser(
//                "mailtwo@gmail.com",
//                "",
//                Constants.LOGIN_TYPE_GOOGLE,
//                ""
//            )
        }

        ibFacebook.setOnClickListener {
            if (isNetworkAvailable(this)) {
                LoginManager.getInstance().logInWithReadPermissions(
                    this,
                    listOf(EMAIL, "public_profile")
                )
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }
        }

        ibInstagram.setOnClickListener {
            if (isNetworkAvailable(this)) {
                openWebViewActivity()
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }
        }

        ivShowPassword.setOnClickListener {
            if (!isShown) {
                // show password
                etLoginPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                ivShowPassword.setImageDrawable(resources.getDrawable(R.drawable.ic_show_password))
                isShown = true
            } else {
                // hide password
                ivShowPassword.setImageDrawable(resources.getDrawable(R.drawable.ic_hide_password))
                etLoginPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                isShown = false
            }
        }

        tvForgotPassword.setOnClickListener {
            startActivity(intentFor<ForgotPasswordActivity>())
        }

        tvSignUp.setOnClickListener {
            startActivity(intentFor<SignUpActivity>())
        }

        tvSkip.setOnClickListener {
            startActivity(intentFor<HomeActivity>(Constants.FROM_ACTIVITY to Constants.FROM_LOGIN))
            userSkipLogin = true
            finish()
        }
    }

    //for facebook
//    private fun getUserDetails(loginResult: LoginResult?) {
//        val dataRequest = GraphRequest.newMeRequest(loginResult!!.accessToken) { jsonObject, _ ->
//            try {
//                val jsonObjectResponse = JSONObject(jsonObject.toString())
//                val profilePicData = JSONObject(jsonObjectResponse["picture"].toString())
//                val profilePicUrl = JSONObject(profilePicData.getString("data"))
////                etLoginEmailId.setText(jsonObjectResponse["email"].toString())
//                val gender = jsonObjectResponse["gender"]
//                loginPresenter.loginUser(
//                    "" + jsonObjectResponse["email"],
//                    etLoginPassword.text.toString(),
//                    Constants.LOGIN_TYPE_FACEBOOK,
//                    "" + jsonObjectResponse["id"]
//                )
//                Log.e("Facebook email", "" + jsonObjectResponse["email"])
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//        }
//        val permission_param = Bundle()
//        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)")
//        dataRequest.parameters = permission_param
//        dataRequest.executeAsync()
//    }

    private fun openWebViewActivity() {



        val intent = Intent(this, WebViewActivity::class.java)
        startActivityForResult(intent, WEB_VIEW_INTENT_CODE)
    }

    private fun validateLoginDetails() {
        hideKeyboard(tvLogin)

        loginValidation.addValidation(
            this,
            R.id.etLoginEmailId,
            RegexTemplate.NOT_EMPTY,
            R.string.txt_enter_email
        )

        if (etLoginEmailId.text.isNotEmpty()) {
            loginValidation.addValidation(
                this,
                R.id.etLoginEmailId,
                android.util.Patterns.EMAIL_ADDRESS,
                R.string.error_valid_email
            )
        }

        if (loginValidation.validate()) {
            if (isNetworkAvailable(this)) {
                loginPresenter.loginUser(
                    etLoginEmailId.text.toString(),
                    etLoginPassword.text.toString(),
                    Constants.LOGIN_TYPE_NORMAL,
                    Constants.EMPTY_STRING
                )
                LoadingDialog.getInstance().show(this)
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
                LoadingDialog.getInstance().cancel()
            }
        }
    }

    override fun onLoginSuccess(loginResponse: LoginResponse) {
        LoadingDialog.getInstance().cancel()
        if (loginResponse.errorcode == 1000) {
            toast("Please verify your mobile number for login.")
            startActivity(
                intentFor<MobileNumberActivity>(
                    Constants.FROM_ACTIVITY to Constants.FROM_LOGIN,
                    Constants.SIGN_UP_ACCESS_TOKEN to loginResponse.tokenKey
                )
            )
        } else if (loginResponse.errorcode == 0) {
            if (loginResponse.msg != "Wrong password") {
                isUserLogin = true
                userTokenKey = loginResponse.tokenKey!!
                Log.e("User token key", loginResponse.tokenKey)
                startActivity(intentFor<HomeActivity>(Constants.FROM_ACTIVITY to Constants.FROM_LOGIN))
                finish()
            } else {
                etLoginPassword.error = loginResponse.msg
            }
        } else if (loginResponse.errorcode == 1002) {
            toast("Email does not exist. Please signup to login.")
        }
        userSkipLogin = false
    }

    override fun onLoginFail(message: String) {
        LoadingDialog.getInstance().cancel()
        if (message == "Wrong password") {
            etLoginPassword.error = message
        } else {
            toast(message)
        }
        userSkipLogin = false
    }

    override fun onLoginNoInternet() {
        LoadingDialog.getInstance().cancel()
        userSkipLogin = false
        toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun socialLoginSuccess(socialLogin: SignUp) {
        LoadingDialog.getInstance().cancel()
        if (socialLogin.errorcode == 1000) {
            startActivity(
                intentFor<MobileNumberActivity>(
                    Constants.SIGN_UP_ACCESS_TOKEN to socialLogin.token,
                    Constants.FROM_ACTIVITY to Constants.FROM_SOCIAL_SIGN_UP
                )
            )
        } else if (socialLogin.errorcode == 0) {
                isUserLogin = true
                userTokenKey = socialLogin.token!!
                Log.e("User token key", userTokenKey)
                startActivity(intentFor<HomeActivity>(Constants.FROM_ACTIVITY to Constants.FROM_LOGIN))
                finish()
        }else if(socialLogin.errorcode == 1002) {
            toast(socialLogin.msg.toString())
        }else{
            toast(socialLogin.msg.toString())
        }
        userSkipLogin = false

    }
    override fun socialLoginFail(message: String) {
        LoadingDialog.getInstance().cancel()
        userSkipLogin = false
        toast(getString(R.string.txt_please_check_your_internet_connection))
    }

    override fun socialLoginNoInternet() {
        LoadingDialog.getInstance().cancel()
        toast(getString(R.string.txt_please_check_your_internet_connection))
        userSkipLogin = false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else if (requestCode == WEB_VIEW_INTENT_CODE && data != null) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    Log.e("return", "result ok")
                    val resultExtra = data.getStringExtra("code")
                    val uri: Uri? = Uri.parse(resultExtra)
                    getAccessToken(uri!!)
                }
                Activity.RESULT_CANCELED -> {
                    Log.e("return", "result canceled")
                }
                else -> {
                    Log.e("return", "bad")
                }
            }
        } else {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

//    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>?) {
//        try {
//            val account: GoogleSignInAccount = completedTask!!.getResult(ApiException::class.java)!!
////            toast(account.email.toString())
////            etLoginEmailId.setText(account.email.toString())
//            loginPresenter.loginUser(
//                account.email.toString(),
//                "",
//                Constants.LOGIN_TYPE_GOOGLE,
//                account.id.toString()
//            )
//            clearGMailSession()
//        } catch (e: ApiException) {
//            Log.w("TAG", "signInResult:failed code=" + e.statusCode)
//        }
//    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>?) {
        try {
            var dateOfBirth = "1900-1-1"
            var gender = "dummygender"

            val account: GoogleSignInAccount = completedTask!!.getResult(ApiException::class.java)!!
//            val person = Plus.PeopleApi.getCurrentPerson(googleApiClient!!.asGoogleApiClient())
//            Log.e("Login", "gender --" +person.gender)
//            Log.e("Login", "date of Birth --" +person.birthday)
//            etFirstName.setText(account.displayName)

            var familyName = ""
            if (!account.familyName.isNullOrEmpty()) {
                familyName = account.familyName!!
//                etLastName.setText(account.familyName)
            }
//            etEmailAddress.setText(account.email)
            val socialId = account.id!!

//            loginType = Constants.LOGIN_TYPE_GOOGLE

            //todo add data of birth conditions here and assign it to dateOfBirth reference
            //todo add gender conditions here and assign it to gender reference

            val loginType = "gmail"

            if (isNetworkAvailable(this)) {
                val profile = SignUpUser(
                    account.displayName!!,
                    familyName,
                    account.email!!,
                    "",
                    0,
                    "",
                    "",
                    gender,
                    dateOfBirth,
                    loginType,
                    socialId
                )
                loginPresenter.socilalogin(profile)
                LoadingDialog.getInstance().show(this)
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }

            clearGMailSession()
        } catch (e: ApiException) {
            Log.w("TAG", "signInResult:failed code=" + e.statusCode)
        }
    }

    private fun getUserDetails(loginResult: LoginResult?) {

        val dataRequest = GraphRequest.newMeRequest(
            loginResult!!.accessToken
        ) { jsonObject, _ ->
            try {
                var dateOfBirth = "1900-1-1"
                var gender = "dummygender"
                val jsonObjectResponse = JSONObject(jsonObject.toString())
//                val profilePicData = JSONObject(jsonObjectResponse["picture"].toString())
//                val profilePicUrl = JSONObject(profilePicData.getString("data"))
//                etFirstName.setText(jsonObjectResponse["name"].toString())
//                etEmailAddress.setText(jsonObjectResponse["email"].toString())
                Log.e("Facebook email", "" + jsonObjectResponse["email"])
//                loginType = Constants.LOGIN_TYPE_FACEBOOK
                val socialId = jsonObjectResponse["id"].toString()

                val loginType = "facebook"

                //todo add data of birth conditions here and assign it to dateOfBirth reference
                //todo add gender conditions here and assign it to gender reference

                if (isNetworkAvailable(this)) {
                    val profile = SignUpUser(
                        jsonObjectResponse["name"].toString(),
                        "",
                        jsonObjectResponse["email"].toString(),
                        "",
                        0,
                        "",
                        "",
                        gender,
                        dateOfBirth,
                        loginType,
                        socialId
                    )
                    loginPresenter.socilalogin(profile)
                    LoadingDialog.getInstance().show(this)
                } else {
                    toast(getString(R.string.txt_please_check_your_internet_connection))
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        val permissionParam = Bundle()
        permissionParam.putString("fields", "email,id,name,picture.width(120).height(120)")
        dataRequest.parameters = permissionParam
        dataRequest.executeAsync()
    }


    private fun clearGMailSession() {
        googleApiClient!!.signOut().addOnCompleteListener { Log.e("Tag", "Clean login session") }
    }

    private fun getAccessToken(uri: Uri) {

        if (uri.toString().startsWith(redirectUri)) {

            LoadingDialog.getInstance().show(this)
            val code: String = uri.getQueryParameter("code").toString()
            Log.e("auth code ", code)

            val builder = Retrofit.Builder()
                .baseUrl(" https://api.instagram.com/")
                .addConverterFactory(GsonConverterFactory.create())


            val retrofit = builder.build()
            val oAuthTokenService: LoginService =
                retrofit.create(
                    LoginService::class.java
                )

            val call: Call<InstaResposne> = oAuthTokenService.postAccessToken(
                clientId,
                clientSecret,
                "authorization_code",
                redirectUri,
                code
            )

            call.enqueue(object : Callback<InstaResposne> {

                override
                fun onResponse(call: Call<InstaResposne>, response: Response<InstaResposne?>) {
                    myShortToken = response.body()!!.accessToken!!
                    userId = response.body()!!.userId.toString()
                    Log.e("accessToken", myShortToken)
                    Log.e("userId", userId)
                    toast(userId)

                    getUserInfo(myShortToken,userId)
                }

                override
                fun onFailure(call: Call<InstaResposne?>?, t: Throwable?) {
                    Log.e("insta ex", t!!.message!!)
                    LoadingDialog.getInstance().cancel()
                }
            })
        } else {
            Log.e("code is", "null")
        }
    }

    private fun getUserInfo(myShortToken: String, userId: String) {

        val okHttpClient = OkHttpClient.Builder()
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttpClient.addInterceptor(loggingInterceptor)

        val builder = Retrofit.Builder()
            .baseUrl("https://graph.instagram.com")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient.build())

        val retrofit = builder.build()
        val oAuthTokenService: LoginService =
            retrofit.create(LoginService::class.java)

        val fields = "id,username";

        val call: Call<InstaUser> = oAuthTokenService.getUserData(userId,myShortToken,fields)

        call.enqueue(object : Callback<InstaUser> {
            override
            fun onResponse(call: Call<InstaUser>, response: Response<InstaUser?>) {
                LoadingDialog.getInstance().cancel()
                if(response.isSuccessful){
                    val userName = response.body()!!.userName
                    toast(userName)
                    sendSocialLoginRequestForInsta(response.body()!!.userId,response.body()!!.userName)
                }else{
                    toast(response.message())
                }
            }

            override
            fun onFailure(call: Call<InstaUser?>?, t: Throwable?) {
                LoadingDialog.getInstance().cancel()
                Log.e("insta ex", t!!.message!!)
            }
        })
    }

    private fun sendSocialLoginRequestForInsta(userId: String, userName: String) {
        try {
            var dateOfBirth = "1900-1-1"
            var gender = "dummygender"

            val socialId = userId;

            val loginType = "instagram"

            //todo add data of birth conditions here and assign it to dateOfBirth reference
            //todo add gender conditions here and assign it to gender reference

            if (isNetworkAvailable(this)) {
                val profile = SignUpUser(
                   userName,
                    "",
                   "",
                    "",
                    0,
                    "",
                    "",
                    gender,
                    dateOfBirth,
                    loginType,
                    socialId
                )
                loginPresenter.socilalogin(profile)
                LoadingDialog.getInstance().show(this)
            } else {
                toast(getString(R.string.txt_please_check_your_internet_connection))
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }


    private fun calculateHashKey(yourPackageName: String) {
        try {
            val info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d(
                    "KeyHash:",
                    Base64.encodeToString(md.digest(), Base64.DEFAULT)
                )
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
    }

}
