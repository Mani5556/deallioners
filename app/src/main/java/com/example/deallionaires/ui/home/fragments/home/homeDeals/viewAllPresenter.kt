package com.example.deallionaires.ui.home.fragments.home.homeDeals

import android.content.Context
import com.example.deallionaires.model.AllDealsBusinessList
import com.example.deallionaires.model.FavouriteAdded
import com.example.deallionaires.model.FavouriteRemoved
import com.example.deallionaires.model.SearchBusinessCategoryResponse
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.ui.searchBusiness.SearchBusinessView
import com.example.deallionaires.utils.isNetworkAvailable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Response

interface ViewAllPresenter {

    fun getAllData(userTokenKey: String, categoryName: String)

    fun addDealToFavourites(userTokenKey: String, merchantid: Int,position:Int)

    fun removeDealFromFavourites(userTokenKey: String, merchantid: Int,position:Int)

    fun removeview()

}


class ViewAllPresenterImpl(private val context: Context, private var viewAllDealsBusinessView: ViewAllDealsBusinessView?) : ViewAllPresenter {

    override fun getAllData(userTokenKey: String, categoryName: String) {
        if (isNetworkAvailable(context)) {
            APIRequests.getViewAllDealsBusiness(
                userTokenKey,
                categoryName,
                object : NWResponseCallback<AllDealsBusinessList> {
                    override fun onSuccess(
                        call: Call<AllDealsBusinessList>,
                        response: Response<AllDealsBusinessList>
                    ) {

                        viewAllDealsBusinessView?.onGetItemsSuccess(response.body()!!.allList)
                    }

                    override fun onResponseBodyNull(
                        call: Call<AllDealsBusinessList>,
                        response: Response<AllDealsBusinessList>
                    ) {
                        viewAllDealsBusinessView?.onGetItemsFail(
                           "No data found"
                        )
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<AllDealsBusinessList>,
                        response: Response<AllDealsBusinessList>
                    ) {
                        val type = object : TypeToken<AllDealsBusinessList>() {}.type
                        val errorData:AllDealsBusinessList = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        viewAllDealsBusinessView?.onGetItemsFail(
                            errorData?.msg?:"Oops some thing wet wrong"
                        )
                    }

                    override fun onFailure(
                        call: Call<AllDealsBusinessList>,
                        t: Throwable
                    ) {
                        viewAllDealsBusinessView?.onGetItemsFail(t.message!!)
                    }

                })
        } else {
            viewAllDealsBusinessView?.onGetItemsNoInternet()

        }
    }

    override fun addDealToFavourites(userTokenKey: String, merchantid: Int,position:Int) {
        if (isNetworkAvailable(context)) {
            APIRequests.getAddDealToFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteAdded?> {
                    override fun onSuccess(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        viewAllDealsBusinessView?.onAddingDealToFavouritesSuccess(response.body()!!,position)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        viewAllDealsBusinessView?.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteAdded?>,
                        response: Response<FavouriteAdded?>
                    ) {
                        viewAllDealsBusinessView?.onAddingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteAdded?>, t: Throwable) {
                        viewAllDealsBusinessView?.onAddingDealToFavouritesFail(t.message!!)
                    }
                })
        } else {
            viewAllDealsBusinessView?.onGetItemsNoInternet()
        }
    }

    override fun removeDealFromFavourites(userTokenKey: String, merchantid: Int,position: Int) {
        if (isNetworkAvailable(context)) {
            APIRequests.getRemoveDealFromFavourites(
                userTokenKey,
                merchantid.toString(),
                object : NWResponseCallback<FavouriteRemoved?> {
                    override fun onSuccess(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        viewAllDealsBusinessView?.onRemovingDealToFavouritesSuccess(response.body()!!,position)
                    }

                    override fun onResponseBodyNull(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        viewAllDealsBusinessView?.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<FavouriteRemoved?>,
                        response: Response<FavouriteRemoved?>
                    ) {
                        viewAllDealsBusinessView?.onRemovingDealToFavouritesFail(response.message())
                    }

                    override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                        viewAllDealsBusinessView?.onRemovingDealToFavouritesFail(t.message!!)
                    }

                })
        } else {
            viewAllDealsBusinessView?.onGetItemsNoInternet()
        }
    }

    override fun removeview(){
        viewAllDealsBusinessView = null
    }

}
