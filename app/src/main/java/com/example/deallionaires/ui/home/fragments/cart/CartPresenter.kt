package com.example.deallionaires.ui.home.fragments.cart

import com.example.deallionaires.model.ClaimDeal

interface CartPresenter {

    fun getUserCart(userTokenKey: String)

    fun claimToCompleteDeal(userToken: String, redeemdealid: Int, claimDeal: ClaimDeal)

    fun destroyCart()

}