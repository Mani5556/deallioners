package com.example.deallionaires.ui.notification.merchant

import android.content.Context
import com.example.deallionaires.model.MerchantNotificationModel
import com.example.deallionaires.model.Notimerchant
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class MerchantNotificationPresenter(
    val context: Context,
    private val notificationView: MerchantNotificationView?
) {
    fun getMerchantNotification(tokenKey: String) {
        if (isNetworkAvailable(context)) {
            APIRequests.getMerchantNotifications(
                tokenKey,
                object : NWResponseCallback<MerchantNotificationModel> {
                    override fun onSuccess(
                        call: Call<MerchantNotificationModel>,
                        response: Response<MerchantNotificationModel>
                    ) {
                        val merchantNotifications = ArrayList<Notimerchant>()
                        for (i in response.body()!!.notimerchant.indices) {

                            merchantNotifications.add(
                                Notimerchant(
                                    response.body()!!.notimerchant[i].id,
                                    response.body()!!.notimerchant[i].title,
                                    response.body()!!.notimerchant[i].description,
                                    response.body()!!.notimerchant[i].business_name,
                                    response.body()!!.notimerchant[i].profile_image
                                )
                            )

                        }
                        notificationView!!.onGetMerchantNotificationSuccess(merchantNotifications)
                    }

                    override fun onResponseBodyNull(
                        call: Call<MerchantNotificationModel>,
                        response: Response<MerchantNotificationModel>
                    ) {
                        notificationView!!.onGetMerchantNotificationFail(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<MerchantNotificationModel>,
                        response: Response<MerchantNotificationModel>
                    ) {
                        notificationView!!.onGetMerchantNotificationFail(response.message())
                    }

                    override fun onFailure(call: Call<MerchantNotificationModel>, t: Throwable) {
                        notificationView!!.onGetMerchantNotificationFail(t.message.toString())
                    }
                })
        } else {
            notificationView!!.onGetMerchantNotificationNoInternet()
        }
    }

    fun onMerchantNotificationFragmentDestroy() {
        notificationView == null
    }

}