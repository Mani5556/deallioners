package com.example.deallionaires.ui.home.fragments.home.homeDeals

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ethanhua.skeleton.RecyclerViewSkeletonScreen
import com.ethanhua.skeleton.Skeleton

import com.example.deallionaires.R
import com.example.deallionaires.adapters.HomeViewAllBusinessAdapter
import com.example.deallionaires.adapters.SearchBusinessItemsAdapter
import com.example.deallionaires.adapters.SearchDealsItemsAdapter
import com.example.deallionaires.model.*
import com.example.deallionaires.ui.searchBusinessItem.SearchBusinessDetailsActivity
import com.example.deallionaires.ui.searchDealItem.SearchDealDetailsActivity
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.LoadingDialog
import com.example.deallionaires.utils.PreferenceExt
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home_deals.*
import kotlinx.android.synthetic.main.fragment_search_business.*
import kotlinx.android.synthetic.main.fragment_search_business.rvHomeDeals
import kotlinx.android.synthetic.main.fragment_search_deals.*
import kotlinx.coroutines.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

/**
 * A simple [Fragment] subclass.
 */
class HomeViewAllFragment : Fragment(), ViewAllDealsBusinessView {

    private var businessItemList: java.util.ArrayList<Searchbusiness> = ArrayList<Searchbusiness>()
    private lateinit var searchDealsItemAdapter: SearchDealsItemsAdapter
    private var skeletonScreen: RecyclerViewSkeletonScreen? = null
    private lateinit var searchBusinessAdapter: HomeViewAllBusinessAdapter
    private var homeDeals: ArrayList<HomeDeals> = ArrayList<HomeDeals>()

    lateinit var activity: Activity
    lateinit var viewAllPresenter: ViewAllPresenter
    private var type: Int? = 1

    val ITEM_TYPE_MERCHANT = 1
    val ITEM_TYPE_DEAL = 2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_deals, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity()!!
        type = arguments?.getInt("type")
        val key = arguments?.getString("key")

        viewAllPresenter = ViewAllPresenterImpl(context!!, this)

        val userToken by PreferenceExt.appPreference(
            activity,
            Constants.USER_TOKEN_KEY,
            Constants.DEFAULT_STRING
        )

        if (type == ITEM_TYPE_MERCHANT) {

            searchBusinessAdapter = HomeViewAllBusinessAdapter(
                activity,
                businessItemList) { searchbusinessFromAdapter: Searchbusiness, isFavouriteClicked: Boolean, isLiked: Int, position: Int ->
                if (isFavouriteClicked) {
                    if (isLiked == 1) {
                        viewAllPresenter.removeDealFromFavourites(
                            userToken,
                            searchbusinessFromAdapter.merchantid,
                            position
                        )
                    } else {
                        viewAllPresenter.addDealToFavourites(
                            userToken,
                            searchbusinessFromAdapter.merchantid,
                            position
                        )
                    }
                } else {
                    activity.startActivity(
                        activity.intentFor<SearchBusinessDetailsActivity>(
                            Constants.MERCHANT_ID to searchbusinessFromAdapter.merchantid
                        )
                    )
                }
            }

            activity.rvHomeViewAllDeals.run {
                adapter = searchBusinessAdapter
                layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)

            }

            skeletonScreen = Skeleton.bind(activity.rvHomeViewAllDeals)
                .adapter(searchBusinessAdapter)
                .load(R.layout.deals_skeleton)
                .color(R.color.gray_20)
                .show()
        } else {
            searchDealsItemAdapter = SearchDealsItemsAdapter(
                activity,
                ArrayList<SearchDealsCategoryItems>()
            ) {
                activity.startActivity(
                    activity.intentFor<SearchDealDetailsActivity>(
                        Constants.DEAL_ID to it,
                        Constants.FROM_ACTIVITY to Constants.FROM_SEARCH_DEALS
                    )
                )
            }

            activity.rvHomeViewAllDeals.run {
                adapter = searchDealsItemAdapter
                layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
            }


            skeletonScreen = Skeleton.bind(activity.rvHomeViewAllDeals)
                .adapter(searchDealsItemAdapter)
                .load(R.layout.deals_skeleton)
                .color(R.color.shimmer_bg)
                .duration(1500)
                .show()
        }

        //getting the date based on key
        viewAllPresenter.getAllData(userToken, key!!)

    }

    override fun onAddingDealToFavouritesSuccess(favouriteAdded: FavouriteAdded, position: Int) {
        LoadingDialog.getInstance().cancel()
        context?.toast(favouriteAdded.msg?.toString()?:"Added to favorites")
        businessItemList[position]?.isLiked = 1
        searchBusinessAdapter.notifyItemChanged(position)
    }

    override fun onAddingDealToFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
    }

    override fun onRemovingDealToFavouritesSuccess(
        favouriteRemoved: FavouriteRemoved,
        position: Int
    ) {
        LoadingDialog.getInstance().cancel()
        context?.toast(favouriteRemoved.msg?.toString()?:"Removed from favorites")
        businessItemList[position]?.isLiked = 0
        searchBusinessAdapter.notifyItemChanged(position)

    }

    override fun onRemovingDealToFavouritesFail(message: String) {
        LoadingDialog.getInstance().cancel()
    }

    override fun onGetItemsSuccess(searchbusiness: JsonArray?) {
        skeletonScreen?.hide()
        CoroutineScope(Dispatchers.IO).launch {
            val list:List<*> = if(type == ITEM_TYPE_MERCHANT){
                Gson().fromJson(searchbusiness,object :TypeToken<ArrayList<Searchbusiness>>() {}.type)
            }else{
                Gson().fromJson(searchbusiness,object :TypeToken<ArrayList<SearchDealsCategoryItems>>() {}.type)
            }
            withContext(Dispatchers.Main) {
                if (type == ITEM_TYPE_MERCHANT) {
                    businessItemList = list as ArrayList<Searchbusiness>
                    searchBusinessAdapter.updateData(businessItemList)
                } else {
                    searchDealsItemAdapter.updateData(list as ArrayList<SearchDealsCategoryItems>)

                }
            }
        }
    }

    override fun onGetItemsFail(message: String) {
        LoadingDialog.getInstance().cancel()
        showErrorMessage(message)
    }

    override fun onGetItemsNoInternet() {
        skeletonScreen?.hide()
        showErrorMessage("No internet connection")
    }

    private fun showErrorMessage(message:String){
        activity.tv_error_message.text = message
        activity.tv_error_message.visibility = View.VISIBLE
        skeletonScreen?.hide()
        activity.rvHomeViewAllDeals.visibility = View.GONE
    }

    override fun onDestroy() {
        super.onDestroy()
        viewAllPresenter.removeview()
    }
}
