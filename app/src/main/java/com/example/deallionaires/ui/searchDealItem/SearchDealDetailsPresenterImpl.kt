package com.example.deallionaires.ui.searchDealItem

import android.content.Context
import android.util.Log
import com.example.deallionaires.model.DealItem
import com.example.deallionaires.model.RedeemDealResponse
import com.example.deallionaires.model.RedeemDeal
import com.example.deallionaires.network.APIRequests.getDealDetailResponse
import com.example.deallionaires.network.APIRequests.postRedeemDealRequest
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SearchDealDetailsPresenterImpl(
    private val context: Context,
    private var searchDealDetailsView: SearchDealDetailsView?
) : SearchDealDetailsPresenter {


    override fun getDealDetailsResponse(userToken: String, dealid: Int) {
        if (isNetworkAvailable(context)) {
            getDealDetailResponse(userToken, dealid, object : NWResponseCallback<DealItem> {
                override fun onSuccess(call: Call<DealItem>, response: Response<DealItem>) {
                    Log.e("success", response.body().toString())
                    searchDealDetailsView!!.onGetDealDealsSuccess(response.body()!!.dealsdetail)
                }

                override fun onResponseBodyNull(
                    call: Call<DealItem>,
                    response: Response<DealItem>
                ) {
                    Log.e("body null", response.body().toString())
                    searchDealDetailsView!!.onGetDealDetailsFail(response.message())
                }

                override fun onResponseUnsuccessful(
                    call: Call<DealItem>,
                    response: Response<DealItem>
                ) {
                    Log.e("unsuccess", response.body().toString())
                    searchDealDetailsView!!.onGetDealDetailsFail(response.message())
                }

                override fun onFailure(call: Call<DealItem>, t: Throwable) {
                    Log.e("success", t.message!!)
                    searchDealDetailsView!!.onGetDealDetailsFail(t.message!!)
                }

            })
        } else {
            searchDealDetailsView!!.onGetDealDetailsNoInternet()
        }
    }

    override fun postRedeemDeal(userToken: String, redeemDeal: RedeemDeal) {
        if (isNetworkAvailable(context)) {
            postRedeemDealRequest(userToken, redeemDeal, object : NWResponseCallback<RedeemDealResponse> {
                override fun onSuccess(call: Call<RedeemDealResponse>, response: Response<RedeemDealResponse>) {
                    searchDealDetailsView!!.onPostRedeemDealSuccess(response.body()!!)
                }

                override fun onResponseBodyNull(call: Call<RedeemDealResponse>, response: Response<RedeemDealResponse>) {
                    searchDealDetailsView!!.onGetDealDetailsFail(response.body().toString())
                }

                override fun onResponseUnsuccessful(
                    call: Call<RedeemDealResponse>,
                    response: Response<RedeemDealResponse>
                ) {
                    searchDealDetailsView!!.onGetDealDetailsFail(response.body().toString())
                }

                override fun onFailure(call: Call<RedeemDealResponse>, t: Throwable) {
                    searchDealDetailsView!!.onGetDealDetailsFail(t.message!!)
                }

            })
        } else {
            searchDealDetailsView!!.onGetDealDetailsNoInternet()
        }
    }

    override fun dealDetailsDestroy() {
        searchDealDetailsView = null
    }

}