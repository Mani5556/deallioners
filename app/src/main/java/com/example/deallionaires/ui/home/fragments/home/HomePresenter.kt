package com.example.deallionaires.ui.home.fragments.home

import android.content.Context

interface HomePresenter {

    fun getCategories(userTokenKey: String, context: Context,latitude:String,longitude:String)

    fun onHomeFragmentDestroy()

    fun addDealToFavourites(userTokenKey: String, merchantid: Int,position:Int,parentAdapterPosition:Int)
    fun removeDealFromFavourites(userTokenKey: String, merchantid: Int,position: Int,parentAdapterPosition:Int)

    fun onDetach()

}