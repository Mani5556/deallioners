package com.example.deallionaires.ui.login

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.AlertDialogLayout
import com.example.deallionaires.R
import com.example.deallionaires.utils.Constants
import com.example.deallionaires.utils.CustomDialog
import com.example.deallionaires.utils.LoadingDialog
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : AppCompatActivity() {

    private val clientId = Constants.INSTAGRAM_APP_ID
    private val redirectUri = "https://com.example.deallionaires/callback"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        LoadingDialog.getInstance().show(this@WebViewActivity)

        webv.settings.javaScriptEnabled = true
        webv.loadUrl("https://api.instagram.com/oauth/authorize?client_id=$clientId&redirect_uri=$redirectUri&scope=user_profile&response_type=code")


        webv.webViewClient = object : WebViewClient() {
            var authComplete = false
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                if (url!!.contains("?code=") && !authComplete) {
                    val uri = Uri.parse(url)
                    Log.i("", "AUTHORISED")
                    authComplete = true
                    intent.putExtra("code", uri.toString())
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else if (url.contains("error=access_denied")) {
                    Log.i("", "ACCESS_DENIED_HERE")
                    authComplete = true
                    setResult(Activity.RESULT_CANCELED, intent)
                    finish()
                }
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                LoadingDialog.getInstance().cancel()
            }

            override fun onLoadResource(view: WebView?, url: String?) {
                super.onLoadResource(view, url)
            }
        }

    }
}
