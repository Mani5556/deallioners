package com.example.deallionaires.ui.dealsStatus.pendingDeals

import com.example.deallionaires.model.DealStatus
import com.example.deallionaires.model.Deals
import com.example.deallionaires.model.Profile

interface PendingDealsView {

    fun onGettingPendingDealsSuccess(deals: ArrayList<DealStatus>)

    fun onGettingPendingDealsFail(message: String)

    fun onGettingPendingDealsNoInternet()

    fun onClaimSuccess(successMessage: String)

    fun onClaimFail(message: String)

    fun onClaimNoInternet()
}