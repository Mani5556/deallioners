package com.example.deallionaires.ui.home.fragments.dashboard

import android.content.Context
import com.example.deallionaires.model.DashboardResponse
import com.example.deallionaires.network.APIRequests.getUserDashboard
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class DashboardPresenterImpl(private var context: Context?, private var dashboardView: DashboardView?) : DashboardPresenter {

    override fun getDashboardPoints(userTokenKey: String) {
        if(isNetworkAvailable(context!!)){
            getUserDashboard(userTokenKey, object : NWResponseCallback<DashboardResponse>{
                override fun onSuccess(
                    call: Call<DashboardResponse>,
                    response: Response<DashboardResponse>
                ) {
                    dashboardView!!.displayDashboardPoints(response.body()!!.dashboard!!)
                }

                override fun onResponseBodyNull(
                    call: Call<DashboardResponse>,
                    response: Response<DashboardResponse>
                ) {
                    dashboardView!!.noDashboardPoints()
                }

                override fun onResponseUnsuccessful(
                    call: Call<DashboardResponse>,
                    response: Response<DashboardResponse>
                ) {
                    dashboardView!!.noDashboardPoints()
                }

                override fun onFailure(call: Call<DashboardResponse>, t: Throwable) {
                    dashboardView!!.noDashboardPoints()
                }
            })
        }else{
            dashboardView!!.dashboardNoInternet()
        }
    }

    override fun dashboardDestroy() {
        dashboardView = null
    }
}