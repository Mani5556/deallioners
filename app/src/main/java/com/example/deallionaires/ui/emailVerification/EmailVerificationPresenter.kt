package com.example.deallionaires.ui.emailVerification

import android.content.Context
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class EmailVerificationPresenter(
    private val context: Context,
    private var emailVerificationView: EmailVerificationView?
) {
    fun getEmailVerificationOtpForSignUp(userAccessKey: String, emailVerification: EmailVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getSignUpEmailOtp(
                userAccessKey,
                emailVerification,
                object : NWResponseCallback<EmailVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<EmailVerificationResponse?>,
                        response: Response<EmailVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            emailVerificationView!!.onEmailVerificationSuccess(response.body()!!)
                        } else {
                            emailVerificationView!!.onEmailVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<EmailVerificationResponse?>,
                        response: Response<EmailVerificationResponse?>
                    ) {
                        emailVerificationView!!.onEmailVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<EmailVerificationResponse?>,
                        response: Response<EmailVerificationResponse?>
                    ) {
                        emailVerificationView!!.onEmailVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<EmailVerificationResponse?>, t: Throwable) {
                        emailVerificationView!!.onEmailVerificationFailed(t.message!!)
                    }
                })
        } else {
            emailVerificationView!!.onEmailVerificationNoInternet()
        }
    }

    fun getEmailOtpForForgotPassword(userAccessKey: String, emailVerification: EmailVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getForgotPasswordEmailOtp(
                userAccessKey,
                emailVerification,
                object : NWResponseCallback<EmailVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<EmailVerificationResponse?>,
                        response: Response<EmailVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            emailVerificationView!!.onEmailVerificationSuccess(response.body()!!)
                        } else {
                            emailVerificationView!!.onEmailVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<EmailVerificationResponse?>,
                        response: Response<EmailVerificationResponse?>
                    ) {
                        emailVerificationView!!.onEmailVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<EmailVerificationResponse?>,
                        response: Response<EmailVerificationResponse?>
                    ) {
                        emailVerificationView!!.onEmailVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<EmailVerificationResponse?>, t: Throwable) {
                        emailVerificationView!!.onEmailVerificationFailed(t.message!!)
                    }
                })
        } else {
            emailVerificationView!!.onEmailVerificationNoInternet()
        }
    }

    fun getVerifyEmailOtp(userAccessKey: String, otpVerification: OtpVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getSignUpVerifyOtp(
                userAccessKey,
                otpVerification,
                object : NWResponseCallback<OtpVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            emailVerificationView!!.onOtpVerificationSuccess(response.body()!!)
                        } else {
                            emailVerificationView!!.onOtpVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        emailVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        emailVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                        emailVerificationView!!.onOtpVerificationFailed(t.message!!)
                    }
                })
        } else {
            emailVerificationView!!.onEmailVerificationNoInternet()
        }
    }

    fun getVerifyOtpForForgotPassword(userAccessKey: String, otpVerification: OtpVerification) {
        if (isNetworkAvailable(context)) {
            APIRequests.getVerifyOtpForForgotPassword(
                userAccessKey,
                otpVerification,
                object : NWResponseCallback<OtpVerificationResponse?> {
                    override fun onSuccess(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        if (response.body()!!.success!!) {
                            emailVerificationView!!.onOtpVerificationSuccess(response.body()!!)
                        } else {
                            emailVerificationView!!.onOtpVerificationFailed(response.body()!!.msg!!)
                        }
                    }

                    override fun onResponseBodyNull(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        emailVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<OtpVerificationResponse?>,
                        response: Response<OtpVerificationResponse?>
                    ) {
                        emailVerificationView!!.onOtpVerificationFailed(response.message())
                    }

                    override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                        emailVerificationView!!.onOtpVerificationFailed(t.message!!)
                    }
                })
        } else {
            emailVerificationView!!.onEmailVerificationNoInternet()
        }
    }
}