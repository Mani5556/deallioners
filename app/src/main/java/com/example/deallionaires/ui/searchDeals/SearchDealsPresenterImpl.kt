package com.example.deallionaires.ui.searchDeals

import android.content.Context
import android.util.Log
import com.example.deallionaires.model.*
import com.example.deallionaires.network.APIRequests.getSearchCategoriesWithSubCategories
import com.example.deallionaires.network.APIRequests.getSearchDealsCategoryResponse
import com.example.deallionaires.network.APIRequests.getSearchDealsCategorySubCategoryResponse
import com.example.deallionaires.network.APIRequests.getSearchDealsCategorySubCategorySortByResponse
import com.example.deallionaires.network.NWResponseCallback
import com.example.deallionaires.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response

class SearchDealsPresenterImpl(
    private val context: Context,
    private var searchDealsView: SearchDealsView?

) : SearchDealsPresenter {

    private var loadMoreDataCallBack: NWResponseCallback<SearchDealsCategoryResponse>? = null
    private var initialDataCallBack: NWResponseCallback<SearchDealsCategoryResponse>? = null

    override fun getUserDealsCategories(userTokenKey: String) {
        if (isNetworkAvailable(context)) {
            getSearchCategoriesWithSubCategories(
                userTokenKey,
                context,
                object : NWResponseCallback<SearchUserCategories?> {
                    override fun onSuccess(
                        call: Call<SearchUserCategories?>,
                        response: Response<SearchUserCategories?>
                    ) {
                        Log.e("User Deals", response.body().toString())
                        val searchUserCategoriesResult = ArrayList<SearchUserCategory>()
                        for (i in response.body()!!.categories.indices) {
                            searchUserCategoriesResult.add(
                                SearchUserCategory(
                                    response.body()!!.categories[i].id,
                                    response.body()!!.categories[i].categoryName,
                                    response.body()!!.categories[i].slug,
                                    response.body()!!.categories[i].bannerImage,
                                    response.body()!!.categories[i].searchUserSubcategories
                                )
                            )
                        }
                        searchDealsView?.onGetUserDealsCategoriesSuccess(searchUserCategoriesResult)
                    }

                    override fun onResponseBodyNull(
                        call: Call<SearchUserCategories?>,
                        response: Response<SearchUserCategories?>
                    ) {
                        searchDealsView?.onGetUserDealsCategoriesFail("")
                        Log.e("User Deals null", response.body().toString())
                    }

                    override fun onResponseUnsuccessful(
                        call: Call<SearchUserCategories?>,
                        response: Response<SearchUserCategories?>
                    ) {
                        searchDealsView?.onGetUserDealsCategoriesFail(response.body().toString())
                        Log.e("User Deals unsu", response.body().toString())
                    }

                    override fun onFailure(call: Call<SearchUserCategories?>, t: Throwable) {
                        Log.e("User Deals", t.message!!)
                        searchDealsView?.onGetUserDealsCategoriesFail(t.message!!)
                    }

                })
        } else {
            searchDealsView?.onGetUserDealsCategoriesNoInternet()
        }
    }

    override fun getUserDealsSubCategories(userTokenKey: String, position: Int) {
        searchDealsView?.onGetUserDealsSubCategoriesSuccess(position)
    }




    override fun getSearchDealsCategorySubCategoryItems(
        userTokenKey: String,
        categoryId: String,
        sub_categoryId: String,
        pageNumber: Int
    ) {
        if (isNetworkAvailable(context)) {
            searchDealsView?.showShimmer()

            loadMoreDataCallBack?.let {
                loadMoreDataCallBack = null
            }
            initialDataCallBack = object : NWResponseCallback<SearchDealsCategoryResponse> {
                override fun onSuccess(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    Log.e("Search success", response.body().toString())
                    searchDealsView?.onGetUserDealsItemsSuccess(response.body()!!.searchdeals)
                }

                override fun onResponseBodyNull(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    searchDealsView?.onGetUserDealsItemsFail(response.message())
                    Log.e("Search body null", response.message())
                    searchDealsView?.hideShimmer()
                }

                override fun onResponseUnsuccessful(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    searchDealsView?.onGetUserDealsItemsFail(response.message())
                    searchDealsView?.hideShimmer()
                }

                override fun onFailure(call: Call<SearchDealsCategoryResponse>, t: Throwable) {
                    searchDealsView?.onGetUserDealsItemsFail(t.message!!)
                    searchDealsView?.hideShimmer()
                }

            }


            getSearchDealsCategorySubCategoryResponse(
                userTokenKey,
                categoryId,
                sub_categoryId,
                pageNumber,
                initialDataCallBack!!
            )
        } else {
            searchDealsView?.onGetUserDealsCategoriesNoInternet()
            searchDealsView?.hideShimmer()
        }
    }

    override fun getSearchDealsCategorySubCategoryItemsLoadMore(
        userTokenKey: String,
        categoryId: String,
        sub_categoryId: String,
        pageNumber: Int
    ) {
        if (isNetworkAvailable(context)) {

            loadMoreDataCallBack?.let {
                loadMoreDataCallBack = null
            }

            loadMoreDataCallBack = getLoadMoreDataCallBack()
            getSearchDealsCategorySubCategoryResponse(
                userTokenKey, categoryId, sub_categoryId, pageNumber, loadMoreDataCallBack!!
            )
        } else {
            searchDealsView?.onGetLoadMoreFail("No NetworkConnection")
        }
    }

    override fun getSearchDealsCategorySubCategoryItemsSortBy(
        userTokenKey: String,
        categoryName: String,
        sub_category: String,
        sortOptions: String,
        pageNumber: Int
    ) {
        if (isNetworkAvailable(context)) {
            searchDealsView?.showShimmer()

            loadMoreDataCallBack?.let {
                loadMoreDataCallBack = null
            }

            initialDataCallBack = object : NWResponseCallback<SearchDealsCategoryResponse> {
                override fun onSuccess(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    Log.e("Search success", response.body().toString())
                    searchDealsView?.onGetUserDealsItemsSuccess(response.body()!!.searchdeals)
                }

                override fun onResponseBodyNull(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    searchDealsView?.onGetUserDealsItemsFail(response.message())
                    Log.e("Search body null", response.message())
                    searchDealsView?.hideShimmer()
                }

                override fun onResponseUnsuccessful(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    searchDealsView?.onGetUserDealsItemsFail(response.message())
                    searchDealsView?.hideShimmer()
                }

                override fun onFailure(call: Call<SearchDealsCategoryResponse>, t: Throwable) {
                    searchDealsView?.onGetUserDealsItemsFail(t.message!!)
                    searchDealsView?.hideShimmer()
                }

            }
            getSearchDealsCategorySubCategorySortByResponse(
                userTokenKey,
                categoryName,
                sub_category,
                sortOptions, pageNumber, initialDataCallBack!!
            )
        } else {
            searchDealsView?.onGetUserDealsCategoriesNoInternet()
            searchDealsView?.hideShimmer()
        }
    }



    override fun getSearchDealsCategorySubCategoryItemsSortByLoadMore(
        userTokenKey: String,
        categoryName: String,
        sub_category: String,
        sortOptions: String,
        pageNumber: Int
    ) {
        if (isNetworkAvailable(context)) {

            loadMoreDataCallBack?.let {
                loadMoreDataCallBack = null
            }

            loadMoreDataCallBack = getLoadMoreDataCallBack()
            getSearchDealsCategorySubCategorySortByResponse(
                userTokenKey,
                categoryName,
                sub_category,
                sortOptions,
                pageNumber,loadMoreDataCallBack!!
            )
        } else {
            searchDealsView?.onGetLoadMoreFail("No Internet Connection")
        }
    }

    override fun destroyUserDeals() {
        searchDealsView = null
    }

    override fun onDetach() {
        searchDealsView = null
    }


    fun getLoadMoreDataCallBack(): NWResponseCallback<SearchDealsCategoryResponse> {
        val callBack = object : NWResponseCallback<SearchDealsCategoryResponse> {
            override fun onSuccess(
                call: Call<SearchDealsCategoryResponse>,
                response: Response<SearchDealsCategoryResponse>
            ) {
                searchDealsView?.onGetLoadMoreDataSuccess(response.body()!!.searchdeals)
            }

            override fun onResponseBodyNull(
                call: Call<SearchDealsCategoryResponse>,
                response: Response<SearchDealsCategoryResponse>
            ) {
                searchDealsView?.onGetLoadMoreFail(response.body()!!.toString())

            }

            override fun onResponseUnsuccessful(
                call: Call<SearchDealsCategoryResponse>,
                response: Response<SearchDealsCategoryResponse>
            ) {
                searchDealsView?.onGetLoadMoreFail(response.body()!!.toString())

            }

            override fun onFailure(call: Call<SearchDealsCategoryResponse>, t: Throwable) {
                searchDealsView?.onGetLoadMoreFail("Load Again")

            }
        }

        return callBack
    }

}