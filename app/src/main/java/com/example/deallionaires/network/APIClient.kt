package com.example.deallionaires.network

import android.text.TextUtils
import android.util.Log
import com.example.deallionaires.utils.Constants
import com.google.gson.GsonBuilder
import okhttp3.Credentials
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.commons.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * API Client object to create and provide Retrofit service
 */
object APIClient {
    private const val BASE_URL = Constants.BASE_URL
    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            Log.d(
                "Deallionaires_Network",
                message
            )
        }).apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.BODY
        })
    private val builder = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(
        GsonConverterFactory.create(
            GsonBuilder().setDateFormat(Constants.DEFAULT_DATE_FORMAT).create()
        )
    )
    private var retrofit = builder.build()

    fun <S> createService(serviceClass: Class<S>): S {
        return createService(serviceClass, Constants.AUTH_USERNAME, Constants.AUTH_PASSWORD)
    }


    fun <S> createService(
        serviceClass: Class<S>, username: String?, password: String?
    ): S {
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            val authToken = Credentials.basic(username!!, password!!)
            return createService(serviceClass, authToken)
        }

        return createService(serviceClass, null, null)
    }

    private fun <S> createService(
        serviceClass: Class<S>, authToken: String
    ): S {
        if (!TextUtils.isEmpty(authToken)) {
            val interceptor = AuthenticationInterceptor(authToken)
            if (!okHttpClient.interceptors().contains(interceptor)) {
                okHttpClient.addInterceptor(UserLatLongIntercepter())
                okHttpClient.addInterceptor(interceptor)
                    .pingInterval(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(300, TimeUnit.SECONDS)
                builder.client(okHttpClient.build())
                retrofit = builder.build()
            }
        }
        return retrofit.create(serviceClass)
    }

}