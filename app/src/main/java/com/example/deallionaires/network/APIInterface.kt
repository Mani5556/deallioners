package com.example.deallionaires.network

import com.example.deallionaires.model.*
import com.example.deallionaires.ui.home.fragments.home.homeDeals.ViewAllDealsBusinessView
import com.example.deallionaires.utils.Constants
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*
import javax.security.auth.Subject


interface APIInterface {

    @Headers("Content-Type: application/json")
    @POST(Constants.USER + Constants.POST_LOGIN)
    fun loginUser(@Body param: User): Call<LoginResponse?>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST(Constants.FORGOT_PASSWORD_API)
    fun forgotPassword(
        @Header("x-access-token") accessToken: String,
        @Field("email") email: String
    ): Call<ForgotPasswordResponse?>

//    @FormUrlEncoded
//    @POST(Constants.RESET_PASSWORD)
//    fun resetPassword(
//        @Field("token") token: String,
//        @Field("password") password: String,
//        @Field("actual_token") actual_token: String
//    ): Call<ResetPasswordResponse?>

//    @Headers("Content-Type: application/json")
//    @POST(Constants.RESET_PASSWORD)
//    fun resetPassword(
//        @Header("x-access-token") accessToken: String,
//        @Body resetPassword: ResetPassword
//    ): Call<ResetPasswordResponse?>

    @GET(Constants.USER + Constants.GET)
    fun getUserDetails(@Header("x-access-token") accessToken: String): Call<ResponseProfile>

    @GET(Constants.DEALLIONAIRES_COUNT)
    fun getUserDeallionairesCount(@Header("x-access-token") accessToken: String): Call<DeallionairesCount>

    @GET(Constants.GET_MERCHANT_NOTIFICATIONS)
    fun getMerchantNotif(@Header("x-access-token") accessToken: String): Call<MerchantNotificationModel>

    @GET(Constants.GET_ADMIN_NOTIFICATIONS)
    fun getAdminNotif(@Header("x-access-token") accessToken: String): Call<AdminNotificationModel>


    @Headers("Content-Type: application/json")
    @POST(Constants.EDIT_USER)
    fun editUser(
        @Header("x-access-token") accessToken: String,
        @Body param: EditProfile
    ): Call<EditProfileResponse?>

    @Multipart
    @POST("user/image_upload")
    fun uploadProfilePic(
        @Header("x-access-token") accessToken: String,
        @Part profilePic: MultipartBody.Part
    ): Call<ImageUploadResponse?>

    @GET(Constants.GET_MY_DEALS)
    fun getUserDeals(@Header("x-access-token") accessToken: String): Call<DealStatusResponse>

    @GET(Constants.GET_COMPLETED_DEALS)
    fun getUserCompletedDeals(@Header("x-access-token") accessToken: String): Call<DealStatusResponse>

    @GET(Constants.GET_PENDING_DEALS)
    fun getUserPendingDeals(@Header("x-access-token") accessToken: String): Call<DealStatusResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.ADD_NEW_USER)
    fun registerUser(@Body param: SignUpUser): Call<SignUp?>

    @GET(Constants.LANDING + Constants.LANDING_PAGE)
    fun getUserLandingPage(@Header("x-access-token") accessToken: String,@Query("userlat")userLat:String,@Query("userlong")userLong:String): Call<JsonObject>

    @GET(Constants.LANDING + Constants.CATEGORY_LIST)
    fun getCategorylist(@Header("x-access-token") accessToken: String): Call<List<Categories>>

    @GET(Constants.LANDING + Constants.ALL_lIST)
    fun getAllDealsBusiness(
        @Header("x-access-token") accessToken: String,
        @Query("list_type") category: String
    ): Call<AllDealsBusinessList>

    @GET(Constants.BUSINESS)
    fun getSearchCategoriesSubCategories(@Header("x-access-token") accessToken: String): Call<SearchUserCategories?>

    @GET(Constants.SEARCH_DEALS)
    fun getSearchDealsCategories(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.MAIN_CATEGORY_NAME) category: String
    ): Call<SearchDealsCategoryResponse>

    @GET(Constants.SEARCH_DEALS)
    fun getSearchDealsCategoriesSubCategories(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.SUB_CATEGORY_NAME) sub_category: String,
        @Query(Constants.MAIN_CATEGORY_NAME) category: String,
        @Query(Constants.PAGE) page: Int
    ): Call<SearchDealsCategoryResponse>

    @GET(Constants.SEARCH_DEALS)
    fun getSearchDealsCategoriesSubCategoriesWithSortBy(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.SORT_OPTIONS) sortOptions: String,
        @Query(Constants.SUB_CATEGORY_NAME) sub_category: String,
        @Query(Constants.MAIN_CATEGORY_NAME) category: String,
        @Query(Constants.PAGE) page: Int
    ): Call<SearchDealsCategoryResponse>


    @GET(Constants.SEARCH_DEAL_DETAIL_ID)
    fun getDealItemDetails(
        @Header("x-access-token") accessToken: String,
        @Path(Constants.DEAL_ID) dealid: Int
    ): Call<DealItem>

    @GET(Constants.SEARCH_BUSINESS_MERCHANT_ID)
    fun getSearchBusinessItemDetails(
        @Header("x-access-token") accessToken: String,
        @Path(Constants.MERCHANT_ID) merchantid: Int
    ): Call<SearchBusinessItemDetail>

    @GET(Constants.SEARCH_BUSINESS)
    fun getSearchBusinessCategories(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.MAIN_CATEGORY_NAME) category: String
    ): Call<SearchBusinessCategoryResponse>

    @GET(Constants.SEARCH_BUSINESS)
    fun getSearchBusinessCategoriesSubCategories(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.SUB_CATEGORY_NAME) sub_category: String,
        @Query(Constants.MAIN_CATEGORY_NAME) category: String,
        @Query(Constants.PAGE) page: Int
    ): Call<SearchBusinessCategoryResponse>

    @GET(Constants.SEARCH_BUSINESS)
    fun getSearchBusinessCategoriesSubCategoriesSortBy(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.SORT_OPTIONS) sortOptions: String,
        @Query(Constants.SUB_CATEGORY_NAME) sub_category: String,
        @Query(Constants.MAIN_CATEGORY_NAME) category: String,
        @Query(Constants.PAGE) page: Int
    ): Call<SearchBusinessCategoryResponse>

    @GET(Constants.SEARCH_BUSINESS)
    fun getSearchBusinessCategoriesSubCategoriesSortByDistance(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.USER_LAT) userlat: Double,
        @Query(Constants.USER_LONG) userlong: Double,
        @Query(Constants.PAGE) page: Int
    ): Call<SearchBusinessCategoryResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.REDEEM_DEAL)
    fun postRedeemDealCall(
        @Header("x-access-token") accessToken: String,
        @Body param: RedeemDeal
    ): Call<RedeemDealResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.CLAIM_DEAL)
    fun postClaimToCompleteDealCall(
        @Header("x-access-token") accessToken: String,
        @Path(Constants.REDEEM_DEAL_ID) redeemdealid: Int,
        @Body param: ClaimDeal
    ): Call<ClaimDealResponse>

    @GET(Constants.GET_DASHBOARD)
    fun getUserDashboard(@Header("x-access-token") accessToken: String): Call<DashboardResponse>

    @GET(Constants.GET_DEALLIONAIRES_CLUB)
    fun getUserDeallionairesClub(@Header("x-access-token") accessToken: String): Call<DeallionairesClubResponse>

    @GET(Constants.GET_OFFER_VALUE_POINTS)
    fun getUserMerchantWiseDeallions(@Header("x-access-token") accessToken: String): Call<MerchantWiseDeallionsResponse>

    @GET(Constants.GET_APP_VALUE_POINTS)
    fun getUserTransactionWiseDeallions(@Header("x-access-token") accessToken: String): Call<TransactionWiseResponse>

    @GET(Constants.GET_USER_FAVOURITES)
    fun getUserFavourites(@Header("x-access-token") accessToken: String): Call<FavouritesResponse>

    @GET(Constants.GET_USER_SPECIAL_DEALS)
    fun getUserSpecialDeals(@Header("x-access-token") accessToken: String): Call<SpecialOfferResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.ADD_DEAL_TO_FAVOURITES)
    fun addDealToFavourites(
        @Header("x-access-token") accessToken: String,
        @Body merchant_id: MerchantBody
    ): Call<FavouriteAdded?>

    @Headers("Content-Type: application/json")
    @POST(Constants.REMOVE_DEAL_FROM_FAVOURITES)
    fun removeDealFromFavourites(
        @Header("x-access-token") accessToken: String,
        @Body merchant_id: MerchantBody
    ): Call<FavouriteRemoved?>

    @Headers("Content-Type: application/json")
    @POST(Constants.ADD_BUSINESS_REVIEW_RATING)
    fun addBusinessReviewRating(
        @Header("x-access-token") accessToken: String,
        @Body addReviewRating: AddReviewRating
    ): Call<ReviewAdded?>

    @Headers("Content-Type: application/json")
    @GET("business/detailbusiness/{merchantid}")
    fun getFilteredDeals(
        @Header("x-access-token") accessToken: String,
        @Path("merchantid") merchantId:Int,
        @Query("deals_category_id") filterId: Int
    ): Call<FilteredDeals?>

    @GET(Constants.SEARCH_HOME_DEALS)
    fun getSearchHomeDealsAtLocation(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.SEARCH_QUERY) searchQuery: String,
        @Query(Constants.USER_LAT) userlat: Double,
        @Query(Constants.USER_LONG) userlong: Double
    ): Call<SearchHomeDealsResponse>

    @GET(Constants.SEARCH_HOME_DEALS)
    fun getSearchHomeDeals(
        @Header("x-access-token") accessToken: String,
        @Query(Constants.SEARCH_QUERY) searchQuery: String
    ): Call<SearchHomeDealsResponse>


    @Headers("Content-Type: application/json")
    @POST(Constants.GET_FORGOT_PASSWORD_OTP)
    fun getEmailOtpForForgotPassword(
        @Body emailVerification: EmailVerification
    ): Call<EmailVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_FORGOT_PASSWORD_OTP)
    fun getMobileOtpForForgotPassword(
        @Body mobileVerification: MobileVerification
    ): Call<MobileVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_FORGOT_PASSWORD_OTP)
    fun verifyOtpForForgotPassword(
        @Header("x-access-token") accessToken: String,
        @Body otpVerification: OtpVerification
    ): Call<OtpVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_FORGOT_PASSWORD_OTP)
    fun  resetPassword(
        @Header("x-access-token") accessToken: String,
        @Body resetPassword: ResetPassword
    ): Call<ResetPasswordResponse?>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_MOB_NUM_CHANGE_OTP)
    fun  otpForMobNumChange(
        @Header("x-access-token") accessToken: String,
        @Body mobileVerificationWithCountryCode: MobileVerificationWithCountryCode
    ): Call<MobileVerificationResponse?>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_MOB_NUM_SIGN_UP_OTP)
    fun  otpForMobNumForSignUp(
        @Header("x-access-token") accessToken: String,
        @Body mobileVerificationWithCountryCode: MobileVerificationWithCountryCode
    ): Call<MobileVerificationResponse?>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_OTP)
    fun getEmailOtpForSignUp(
        @Body emailVerification: EmailVerification
    ): Call<EmailVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.HELP_CENTER_QUERY)
    fun submitHelpQuery(@Header("x-access-token") accessToken: String, @Query("subject")subject: String,@Query("description")description: String
    ): Call<HelpResponse>


    @Headers("Content-Type: application/json")
    @GET(Constants.GET_FAQS)
    fun getFaqs(@Header("x-access-token") accessToken: String): Call<FaqsResponse>


    @Headers("Content-Type: application/json")
    @POST(Constants.GET_OTP)
    fun getMobileOtpForSignUp(
        @Body mobileVerificationWithCountryCode: MobileVerificationWithCountryCode
    ): Call<MobileVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_OTP)
    fun verifyOtpForSignUp(
        @Header("x-access-token") accessToken: String,
        @Body otpVerification: OtpVerification
    ): Call<OtpVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_MOB_NUM_CHANGE_OTP)
    fun verifyMobNumOtpChange(
        @Header("x-access-token") accessToken: String,
        @Body mobNumChangeOtpVerification: MobNumChangeOtpVerification
    ): Call<OtpVerificationResponse>

    @Headers("Content-Type: application/json")
    @POST(Constants.GET_MOB_NUM_SIGN_UP_OTP)
    fun verifyMobNumOtpForSignUp(
        @Header("x-access-token") accessToken: String,
        @Body mobNumChangeOtpVerification: MobNumChangeOtpVerification
    ): Call<OtpVerificationResponse>

}