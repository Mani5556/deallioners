package com.example.deallionaires.network

import android.util.Log
import com.example.deallionaires.utils.Constants
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthenticationInterceptor(private val authToken: String) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val builder = original.newBuilder()
            .header("Authorization", authToken)

        val request = builder.build()
        return chain.proceed(request)
    }
}

//class UserLatLongIntercepter() : Interceptor {
//
//    override fun intercept(chain: Interceptor.Chain): Response {
//        val request: Request = chain.request()
//        if (request.url().queryParameterNames().contains(Constants.USER_LAT) || request.url()
//                .queryParameterNames().contains(Constants.USER_LONG)
//        ) {
//            Log.e("QuerryParams", "already location added")
//            return chain.proceed(request)
//        } else {
//            val url: HttpUrl = request.url().newBuilder()
//                .addQueryParameter(Constants.USER_LAT, Constants.USER_LATITUDE_VALUE.toString())
//                .addQueryParameter(Constants.USER_LONG, Constants.USER_LONGITUDE_VALUE.toString())
//                .build()
//            val newRequest = request.newBuilder().url(url).build()
//            return chain.proceed(newRequest)
//        }
//
//    }
//}

class UserLatLongIntercepter() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request: Request = chain.request()
        request = request.newBuilder()
            .addHeader(Constants.USER_LAT, Constants.USER_LATITUDE_VALUE.toString())
            .addHeader(Constants.USER_LONG, Constants.USER_LONGITUDE_VALUE.toString()).build()
        val newRequest = request.newBuilder().build()
        return chain.proceed(newRequest)

    }
}