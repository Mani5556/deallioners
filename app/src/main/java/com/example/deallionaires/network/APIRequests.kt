package com.example.deallionaires.network

import android.content.Context
import android.content.LocusId
import com.example.deallionaires.model.*
import com.example.deallionaires.utils.App
import com.example.deallionaires.utils.ProgressCallBacks
import com.example.deallionaires.utils.ProgressRequestBody
import com.google.gson.JsonObject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


object APIRequests {

    fun checkUserLogin(
        context: Context,
        user: User,
        callback: NWResponseCallback<LoginResponse?>
    ) {
        App.apiClient.loginUser(user).enqueue(object : Callback<LoginResponse?> {
            override fun onFailure(call: Call<LoginResponse?>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<LoginResponse?>,
                response: Response<LoginResponse?>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }
        })
    }

    fun getUserDetails(tokenKey: String, callback: NWResponseCallback<ResponseProfile>) {
        App.apiClient.getUserDetails(tokenKey).enqueue(object : Callback<ResponseProfile> {
            override fun onFailure(call: Call<ResponseProfile>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<ResponseProfile>,
                response: Response<ResponseProfile>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getUserDeallionairesCount(
        tokenKey: String,
        callback: NWResponseCallback<DeallionairesCount>
    ) {
        App.apiClient.getUserDeallionairesCount(tokenKey)
            .enqueue(object : Callback<DeallionairesCount> {
                override fun onFailure(call: Call<DeallionairesCount>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<DeallionairesCount>,
                    response: Response<DeallionairesCount>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun editUserDetails(
        tokenKey: String,
        editProfile: EditProfile,
        callback: NWResponseCallback<EditProfileResponse?>
    ) {
        App.apiClient.editUser(tokenKey, editProfile)
            .enqueue(object : Callback<EditProfileResponse?> {
                override fun onFailure(call: Call<EditProfileResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<EditProfileResponse?>,
                    response: Response<EditProfileResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }


    fun uploadProfilePic(
        tokenKey: String,
        profilePicUrl: String,
        progressCallBacks: ProgressCallBacks,
        callback: NWResponseCallback<ImageUploadResponse?>
    ) {
        val file = File(profilePicUrl)

        val progressRequestBody = ProgressRequestBody(file, MediaType.parse("image/*"),progressCallBacks)
        val filePart: MultipartBody.Part  = MultipartBody.Part.createFormData("profile_pic", file.getName(), progressRequestBody)

        App.apiClient.uploadProfilePic(tokenKey, filePart)
            .enqueue(object : Callback<ImageUploadResponse?> {
                override fun onFailure(call: Call<ImageUploadResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<ImageUploadResponse?>,
                    response: Response<ImageUploadResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getUserDeals(tokenKey: String, callback: NWResponseCallback<DealStatusResponse>) {
        App.apiClient.getUserDeals(tokenKey).enqueue(object : Callback<DealStatusResponse> {
            override fun onFailure(call: Call<DealStatusResponse>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<DealStatusResponse>,
                response: Response<DealStatusResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getUserCompetedDeals(tokenKey: String, callback: NWResponseCallback<DealStatusResponse>) {
        App.apiClient.getUserCompletedDeals(tokenKey)
            .enqueue(object : Callback<DealStatusResponse> {
                override fun onFailure(call: Call<DealStatusResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<DealStatusResponse>,
                    response: Response<DealStatusResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getUserPendingDeals(tokenKey: String, callback: NWResponseCallback<DealStatusResponse>) {
        App.apiClient.getUserPendingDeals(tokenKey).enqueue(object : Callback<DealStatusResponse> {
            override fun onFailure(call: Call<DealStatusResponse>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<DealStatusResponse>,
                response: Response<DealStatusResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getMerchantNotifications(
        tokenKey: String,
        callback: NWResponseCallback<MerchantNotificationModel>
    ) {
        App.apiClient.getMerchantNotif(tokenKey)
            .enqueue(object : Callback<MerchantNotificationModel> {
                override fun onFailure(call: Call<MerchantNotificationModel>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<MerchantNotificationModel>,
                    response: Response<MerchantNotificationModel>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }


    fun getAdminNotifications(
        tokenKey: String,
        callback: NWResponseCallback<AdminNotificationModel>
    ) {
        App.apiClient.getAdminNotif(tokenKey).enqueue(object : Callback<AdminNotificationModel> {
            override fun onFailure(call: Call<AdminNotificationModel>, t: Throwable) {


                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<AdminNotificationModel>,
                response: Response<AdminNotificationModel>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getLandingPageCategories(
        userAccessToken: String,
        context: Context,
        callback: NWResponseCallback<List<Categories>>
    ) {
        App.apiClient.getCategorylist(userAccessToken).enqueue(object : Callback<List<Categories>> {
            override fun onFailure(call: Call<List<Categories>>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(call: Call<List<Categories>>, response: Response<List<Categories>>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)

                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getUserLandingPageDeals(userAccessToken: String, context: Context,latitude:String,longitude:String, callback: NWResponseCallback<JsonObject>) {
        App.apiClient.getUserLandingPage(userAccessToken,latitude,longitude).enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }


    fun registerNewUser(
        context: Context,
        signUpUser: SignUpUser,
        callback: NWResponseCallback<SignUp?>
    ) {
        App.apiClient.registerUser(signUpUser).enqueue(object : Callback<SignUp?> {
            override fun onFailure(call: Call<SignUp?>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(call: Call<SignUp?>, response: Response<SignUp?>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getSearchCategoriesWithSubCategories(
        userAccessToken: String,
        context: Context,
        callback: NWResponseCallback<SearchUserCategories?>
    ) {
        App.apiClient.getSearchCategoriesSubCategories(userAccessToken)
            .enqueue(object : Callback<SearchUserCategories?> {
                override fun onFailure(call: Call<SearchUserCategories?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchUserCategories?>,
                    response: Response<SearchUserCategories?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchDealsCategoryResponse(
        userAccessToken: String,
        category: String,
        callback: NWResponseCallback<SearchDealsCategoryResponse>
    ) {
        App.apiClient.getSearchDealsCategories(userAccessToken, category)
            .enqueue(object : Callback<SearchDealsCategoryResponse> {
                override fun onFailure(call: Call<SearchDealsCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchBusinessCategoryResponse(
        userAccessToken: String,
        category: String,
        callback: NWResponseCallback<SearchBusinessCategoryResponse>
    ) {
        App.apiClient.getSearchBusinessCategories(userAccessToken, category)
            .enqueue(object : Callback<SearchBusinessCategoryResponse> {
                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }


    fun getSearchDealsCategorySubCategoryResponse(
        userAccessToken: String,
        category: String,
        sub_category: String,
        pageNumber:Int,
        callback: NWResponseCallback<SearchDealsCategoryResponse>
    ) {
        App.apiClient.getSearchDealsCategoriesSubCategories(userAccessToken, sub_category, category,pageNumber)
            .enqueue(object : Callback<SearchDealsCategoryResponse> {
                override fun onFailure(call: Call<SearchDealsCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchDealsCategorySubCategorySortByResponse(
        userAccessToken: String,
        category: String,
        sub_category: String,
        sortOptions: String,
        pageNumber: Int,
        callback: NWResponseCallback<SearchDealsCategoryResponse>
    ) {
        App.apiClient.getSearchDealsCategoriesSubCategoriesWithSortBy(
            userAccessToken,
            sortOptions,
            sub_category,
            category,
            pageNumber
        )
            .enqueue(object : Callback<SearchDealsCategoryResponse> {
                override fun onFailure(call: Call<SearchDealsCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchDealsCategoryResponse>,
                    response: Response<SearchDealsCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchBusinessCategorySubCategoryResponse(
        userAccessToken: String,
        category: String,
        sub_category: String,
        pageNumber: Int,
        callback: NWResponseCallback<SearchBusinessCategoryResponse>?
    ) {
        App.apiClient.getSearchBusinessCategoriesSubCategories(
            userAccessToken,
            sub_category,
            category,
            pageNumber
        )
            .enqueue(object : Callback<SearchBusinessCategoryResponse> {
                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback?.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback?.onSuccess(call, response)
                        } else {
                            callback?.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback?.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchBusinessCategorySubCategoryItemsWithSortBy(
        userAccessToken: String,
        category: String,
        sub_category: String,
        sortOptions: String,
        page:Int,
        callback: NWResponseCallback<SearchBusinessCategoryResponse>
    ) {
        App.apiClient.getSearchBusinessCategoriesSubCategoriesSortBy(
            userAccessToken,
            sortOptions,
            sub_category,
            category,
            page
        )
            .enqueue(object : Callback<SearchBusinessCategoryResponse> {
                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchBusinessCategorySubCategoryItemsWithSortByDistance(
        userAccessToken: String,
        userLat: Double,
        userLang: Double,
        pageNumber: Int,
        callback: NWResponseCallback<SearchBusinessCategoryResponse>
    ) {
        App.apiClient.getSearchBusinessCategoriesSubCategoriesSortByDistance(
            userAccessToken,
            userLat,
            userLang,
            pageNumber
        )
            .enqueue(object : Callback<SearchBusinessCategoryResponse> {
                override fun onFailure(call: Call<SearchBusinessCategoryResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchBusinessCategoryResponse>,
                    response: Response<SearchBusinessCategoryResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getDealDetailResponse(
        userAccessToken: String,
        dealId: Int,
        callback: NWResponseCallback<DealItem>
    ) {
        App.apiClient.getDealItemDetails(userAccessToken, dealId)
            .enqueue(object : Callback<DealItem> {
                override fun onFailure(call: Call<DealItem>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(call: Call<DealItem>, response: Response<DealItem>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchBusinessDetailResponse(
        userAccessToken: String,
        merchantId: Int,
        callback: NWResponseCallback<SearchBusinessItemDetail>
    ) {
        App.apiClient.getSearchBusinessItemDetails(userAccessToken, merchantId)
            .enqueue(object : Callback<SearchBusinessItemDetail> {
                override fun onFailure(call: Call<SearchBusinessItemDetail>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchBusinessItemDetail>,
                    response: Response<SearchBusinessItemDetail>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }


    fun postClaimToCompleteRequest(
        tokenKey: String,
        redeemdealid: Int,
        claimDeal: ClaimDeal,
        callback: NWResponseCallback<ClaimDealResponse>
    ) {
        App.apiClient.postClaimToCompleteDealCall(tokenKey, redeemdealid, claimDeal)
            .enqueue(object : Callback<ClaimDealResponse> {
                override fun onFailure(call: Call<ClaimDealResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<ClaimDealResponse>,
                    response: Response<ClaimDealResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun postRedeemDealRequest(
        tokenKey: String,
        redeemDeal: RedeemDeal,
        callback: NWResponseCallback<RedeemDealResponse>
    ) {
        App.apiClient.postRedeemDealCall(tokenKey, redeemDeal)
            .enqueue(object : Callback<RedeemDealResponse> {
                override fun onFailure(call: Call<RedeemDealResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<RedeemDealResponse>,
                    dealResponse: Response<RedeemDealResponse>
                ) {
                    if (dealResponse.isSuccessful) {
                        if (dealResponse.body() != null) {
                            callback.onSuccess(call, dealResponse)
                        } else {
                            callback.onResponseBodyNull(call, dealResponse)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, dealResponse)
                    }
                }

            })
    }

    fun getUserDashboard(tokenKey: String, callback: NWResponseCallback<DashboardResponse>) {
        App.apiClient.getUserDashboard(tokenKey).enqueue(object : Callback<DashboardResponse> {
            override fun onFailure(call: Call<DashboardResponse>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<DashboardResponse>,
                response: Response<DashboardResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getUserDeallionairesClub(
        tokenKey: String,
        callback: NWResponseCallback<DeallionairesClubResponse>
    ) {
        App.apiClient.getUserDeallionairesClub(tokenKey)
            .enqueue(object : Callback<DeallionairesClubResponse> {
                override fun onFailure(call: Call<DeallionairesClubResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<DeallionairesClubResponse>,
                    response: Response<DeallionairesClubResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getUserMerchantWiseDeallions(
        tokenKey: String,
        callback: NWResponseCallback<MerchantWiseDeallionsResponse>
    ) {
        App.apiClient.getUserMerchantWiseDeallions(tokenKey)
            .enqueue(object : Callback<MerchantWiseDeallionsResponse> {
                override fun onFailure(call: Call<MerchantWiseDeallionsResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<MerchantWiseDeallionsResponse>,
                    response: Response<MerchantWiseDeallionsResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getUserTransactionWiseDeallions(
        tokenKey: String,
        callback: NWResponseCallback<TransactionWiseResponse>
    ) {
        App.apiClient.getUserTransactionWiseDeallions(tokenKey)
            .enqueue(object : Callback<TransactionWiseResponse> {
                override fun onFailure(call: Call<TransactionWiseResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<TransactionWiseResponse>,
                    response: Response<TransactionWiseResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getUserFavourites(tokenKey: String, callback: NWResponseCallback<FavouritesResponse>) {
        App.apiClient.getUserFavourites(tokenKey).enqueue(object : Callback<FavouritesResponse> {
            override fun onFailure(call: Call<FavouritesResponse>, t: Throwable) {
                t.printStackTrace()
                callback.onFailure(call, t)
            }

            override fun onResponse(
                call: Call<FavouritesResponse>,
                response: Response<FavouritesResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        callback.onSuccess(call, response)
                    } else {
                        callback.onResponseBodyNull(call, response)
                    }
                } else {
                    callback.onResponseUnsuccessful(call, response)
                }
            }

        })
    }

    fun getUserSpecialOffers(tokenKey: String, callback: NWResponseCallback<SpecialOfferResponse>) {
        App.apiClient.getUserSpecialDeals(tokenKey)
            .enqueue(object : Callback<SpecialOfferResponse> {
                override fun onFailure(call: Call<SpecialOfferResponse>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SpecialOfferResponse>,
                    response: Response<SpecialOfferResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getForgotPassword(
        userAccessToken: String,
        forgotPassword: ForgotPassword,
        callback: NWResponseCallback<ForgotPasswordResponse?>
    ) {
        App.apiClient.forgotPassword(userAccessToken, forgotPassword.email)
            .enqueue(object : Callback<ForgotPasswordResponse?> {
                override fun onFailure(call: Call<ForgotPasswordResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<ForgotPasswordResponse?>,
                    response: Response<ForgotPasswordResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

//    fun getResetPassword(
//        userAccessToken: String,
//        token: String,
//        password: String,
//        actual_token: String,
//        callback: NWResponseCallback<ResetPasswordResponse?>
//    ) {
//        App.apiClient.resetPassword(token, password, actual_token)
//            .enqueue(object : Callback<ResetPasswordResponse?> {
//                override fun onFailure(call: Call<ResetPasswordResponse?>, t: Throwable) {
//                    t.printStackTrace()
//                    callback.onFailure(call, t)
//                }
//
//                override fun onResponse(
//                    call: Call<ResetPasswordResponse?>,
//                    response: Response<ResetPasswordResponse?>
//                ) {
//                    if (response.isSuccessful) {
//                        if (response.body() != null) {
//                            callback.onSuccess(call, response)
//                        } else {
//                            callback.onResponseBodyNull(call, response)
//                        }
//                    } else {
//                        callback.onResponseUnsuccessful(call, response)
//                    }
//                }
//
//            })
//    }

    fun getAddDealToFavourites(
        token: String,
        merchant_id: String,
        callback: NWResponseCallback<FavouriteAdded?>
    ) {
        App.apiClient.addDealToFavourites(token, MerchantBody(merchant_id))
            .enqueue(object : Callback<FavouriteAdded?> {
                override fun onFailure(call: Call<FavouriteAdded?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<FavouriteAdded?>,
                    response: Response<FavouriteAdded?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

       fun getAddBusinessReviewRating(
        token: String,
        addReviewRating: AddReviewRating,
        callback: NWResponseCallback<ReviewAdded?>
    ) {
        App.apiClient.addBusinessReviewRating(token, addReviewRating)
            .enqueue(object : Callback<ReviewAdded?> {
                override fun onFailure(call: Call<ReviewAdded?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<ReviewAdded?>,
                    response: Response<ReviewAdded?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getFilteredDealsList(
        token: String,
        marchantId:Int,
        filterId: Int,
        callback: NWResponseCallback<FilteredDeals?>
    ) {
        App.apiClient.getFilteredDeals(token,marchantId,filterId)
            .enqueue(object : Callback<FilteredDeals?> {
                override fun onFailure(call: Call<FilteredDeals?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<FilteredDeals?>,
                    response: Response<FilteredDeals?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }


    fun getRemoveDealFromFavourites(
        token: String,
        merchant_id: String,
        callback: NWResponseCallback<FavouriteRemoved?>
    ) {

        App.apiClient.removeDealFromFavourites(token, MerchantBody(merchant_id))
            .enqueue(object : Callback<FavouriteRemoved?> {
                override fun onFailure(call: Call<FavouriteRemoved?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<FavouriteRemoved?>,
                    response: Response<FavouriteRemoved?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchHomeDealsAtLocation(
        userAccessToken: String,
        userLang: Double,
        userLat: Double,
        searchQuery: String,
        callback: NWResponseCallback<SearchHomeDealsResponse?>
    ) {
        App.apiClient.getSearchHomeDealsAtLocation(userAccessToken, searchQuery, userLat, userLang)
            .enqueue(object : Callback<SearchHomeDealsResponse?> {
                override fun onFailure(call: Call<SearchHomeDealsResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchHomeDealsResponse?>,
                    response: Response<SearchHomeDealsResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSearchHomeDeals(
        userAccessToken: String,
        searchQuery: String,
        callback: NWResponseCallback<SearchHomeDealsResponse?>
    ) {
        App.apiClient.getSearchHomeDeals(userAccessToken, searchQuery)
            .enqueue(object : Callback<SearchHomeDealsResponse?> {
                override fun onFailure(call: Call<SearchHomeDealsResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<SearchHomeDealsResponse?>,
                    response: Response<SearchHomeDealsResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSignUpEmailOtp(
        userAccessToken: String,
        emailVerification: EmailVerification,
        callback: NWResponseCallback<EmailVerificationResponse?>
    ) {
        App.apiClient.getEmailOtpForSignUp(emailVerification)
            .enqueue(object : Callback<EmailVerificationResponse?> {
                override fun onFailure(call: Call<EmailVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<EmailVerificationResponse?>,
                    response: Response<EmailVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }
    fun submitHelpQuery(
        userAccessToken: String,
        subject: String,
        description:String,
        callback: NWResponseCallback<HelpResponse?>
    ) {
        App.apiClient.submitHelpQuery(userAccessToken,subject,description)
            .enqueue(object : Callback<HelpResponse?> {
                override fun onFailure(call: Call<HelpResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<HelpResponse?>,
                    response: Response<HelpResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getFaqs(
        userAccessToken: String,
        callback: NWResponseCallback<FaqsResponse?>
    ) {
        App.apiClient.getFaqs(userAccessToken)
            .enqueue(object : Callback<FaqsResponse?> {
                override fun onFailure(call: Call<FaqsResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<FaqsResponse?>,
                    response: Response<FaqsResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getForgotPasswordEmailOtp(
        userAccessToken: String,
        emailVerification: EmailVerification,
        callback: NWResponseCallback<EmailVerificationResponse?>
    ) {
        App.apiClient.getEmailOtpForForgotPassword(emailVerification)
            .enqueue(object : Callback<EmailVerificationResponse?> {
                override fun onFailure(call: Call<EmailVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<EmailVerificationResponse?>,
                    response: Response<EmailVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getForgotPasswordMobileOtp(
        userAccessToken: String,
        mobileVerification: MobileVerification,
        callback: NWResponseCallback<MobileVerificationResponse?>
    ) {
        App.apiClient.getMobileOtpForForgotPassword(mobileVerification)
            .enqueue(object : Callback<MobileVerificationResponse?> {
                override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<MobileVerificationResponse?>,
                    response: Response<MobileVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }


    fun getSignUpMobileOtp(
        userAccessToken: String,
        mobileVerificationWithCountryCode: MobileVerificationWithCountryCode,
        callback: NWResponseCallback<MobileVerificationResponse?>
    ) {
        App.apiClient.getMobileOtpForSignUp(mobileVerificationWithCountryCode)
            .enqueue(object : Callback<MobileVerificationResponse?> {
                override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<MobileVerificationResponse?>,
                    response: Response<MobileVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getSignUpVerifyOtp(
        userAccessToken: String,
        otpVerification: OtpVerification,
        callback: NWResponseCallback<OtpVerificationResponse?>
    ) {
        App.apiClient.verifyOtpForSignUp(userAccessToken, otpVerification)
            .enqueue(object : Callback<OtpVerificationResponse?> {
                override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<OtpVerificationResponse?>,
                    response: Response<OtpVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getVerifyOtpForForgotPassword(
        userAccessToken: String,
        otpVerification: OtpVerification,
        callback: NWResponseCallback<OtpVerificationResponse?>
    ) {
        App.apiClient.verifyOtpForForgotPassword(userAccessToken, otpVerification)
            .enqueue(object : Callback<OtpVerificationResponse?> {
                override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<OtpVerificationResponse?>,
                    response: Response<OtpVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getVerifyMobNumChangeOtp(
        userAccessToken: String,
        mobNumChangeOtpVerification: MobNumChangeOtpVerification,
        callback: NWResponseCallback<OtpVerificationResponse?>
    ) {
        App.apiClient.verifyMobNumOtpChange(userAccessToken, mobNumChangeOtpVerification)
            .enqueue(object : Callback<OtpVerificationResponse?> {
                override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<OtpVerificationResponse?>,
                    response: Response<OtpVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getVerifyMobNumSignUpOtp(
        userAccessToken: String,
        mobNumChangeOtpVerification: MobNumChangeOtpVerification,
        callback: NWResponseCallback<OtpVerificationResponse?>
    ) {
        App.apiClient.verifyMobNumOtpForSignUp(userAccessToken, mobNumChangeOtpVerification)
            .enqueue(object : Callback<OtpVerificationResponse?> {
                override fun onFailure(call: Call<OtpVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<OtpVerificationResponse?>,
                    response: Response<OtpVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getResetPassword(
        tokenKey: String,
        resetPassword: ResetPassword,
        callback: NWResponseCallback<ResetPasswordResponse?>
    ) {
        App.apiClient.resetPassword(tokenKey, resetPassword)
            .enqueue(object : Callback<ResetPasswordResponse?> {
                override fun onFailure(call: Call<ResetPasswordResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<ResetPasswordResponse?>,
                    response: Response<ResetPasswordResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getOtpForMobNumChange(
        tokenKey: String,
        mobileVerificationWithCountryCode: MobileVerificationWithCountryCode,
        callback: NWResponseCallback<MobileVerificationResponse?>
    ) {
        App.apiClient.otpForMobNumChange(tokenKey, mobileVerificationWithCountryCode)
            .enqueue(object : Callback<MobileVerificationResponse?> {
                override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<MobileVerificationResponse?>,
                    response: Response<MobileVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }

    fun getOtpForMobNumSignUp(
        tokenKey: String,
        mobileVerificationWithCountryCode: MobileVerificationWithCountryCode,
        callback: NWResponseCallback<MobileVerificationResponse?>
    ) {
        App.apiClient.otpForMobNumForSignUp(tokenKey, mobileVerificationWithCountryCode)
            .enqueue(object : Callback<MobileVerificationResponse?> {
                override fun onFailure(call: Call<MobileVerificationResponse?>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<MobileVerificationResponse?>,
                    response: Response<MobileVerificationResponse?>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }



    fun getViewAllDealsBusiness(
        userAccessToken: String,
        category: String,
        callback: NWResponseCallback<AllDealsBusinessList>
    ) {
        App.apiClient.getAllDealsBusiness(userAccessToken, category)
            .enqueue(object : Callback<AllDealsBusinessList> {
                override fun onFailure(call: Call<AllDealsBusinessList>, t: Throwable) {
                    t.printStackTrace()
                    callback.onFailure(call, t)
                }

                override fun onResponse(
                    call: Call<AllDealsBusinessList>,
                    response: Response<AllDealsBusinessList>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            callback.onSuccess(call, response)
                        } else {
                            callback.onResponseBodyNull(call, response)
                        }
                    } else {
                        callback.onResponseUnsuccessful(call, response)
                    }
                }

            })
    }
}