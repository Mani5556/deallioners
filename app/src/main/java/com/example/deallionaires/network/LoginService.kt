package com.example.deallionaires.network


import com.example.deallionaires.model.InstaResposne
import com.example.deallionaires.model.InstaUser
import retrofit2.Call
import retrofit2.http.*


interface LoginService {
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("oauth/access_token")
    fun postAccessToken(
        @Field("client_id") clientId: String?,
        @Field("client_secret") clientSecret: String?,
        @Field("grant_type") grant_type: String?,
        @Field("redirect_uri") redirect_uri: String?,
        @Field("code") code: String?
    ): Call<InstaResposne>


//    @Headers("Content-Type: application/x-www-form-urlencoded")
//    @FormUrlEncoded
    @GET("{user-id}")
    fun getUserData(@Path("user-id")userId:String,@Query("access_token") clientId: String?, @Query("fields") code: String?): Call<InstaUser>
}