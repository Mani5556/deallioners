package com.example.deallionaires.network

import retrofit2.Call
import retrofit2.Response

interface NWResponseCallback<T> {

    fun onSuccess(call: Call<T>, response: Response<T>)

    fun onResponseBodyNull(call: Call<T>, response: Response<T>)

    fun onResponseUnsuccessful(call: Call<T>, response: Response<T>)

    fun onFailure(call: Call<T>, t: Throwable)

}