package com.example.deallionaires.utils

import android.content.Context
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import java.io.IOException
import java.util.*

class LocationAddress {

    fun getAddressFromLocation(
        latitude: Double, longitude: Double,
        context: Context?, handler: Handler?
    ) {
        val thread: Thread = object : Thread() {
            override fun run() {
                val geocoder = Geocoder(context, Locale.getDefault())
                var result: String? = null
                try {
                    val addressList =
                        geocoder.getFromLocation(
                            latitude, longitude, 1
                        )
                    if (addressList != null && addressList.size > 0) {
                        val address = addressList[0]
                        val sb = StringBuilder()
                        for (i in 0 until address.maxAddressLineIndex) {
                            sb.append(address.getAddressLine(i)).append(",")
                        }
                        sb.append(address.featureName).append(", ")
                        sb.append(address.subLocality).append(", ")
                        sb.append(address.locality).append(", ")
                        sb.append(address.subAdminArea).append(", ")
                        sb.append(address.adminArea).append(", ")
                        sb.append(address.countryName).append(", ")
                        sb.append(address.postalCode)
                        result = sb.toString()
                    }
                } catch (e: IOException) {
                    Log.e("Location Address", "Unable connect to Geocoder", e)
                } finally {
                    val message = Message.obtain()
                    message.target = handler
                    if (result != null) {
                        message.what = 1
                        val bundle = Bundle()
//                        result = "Latitude: $latitude Longitude: $longitude Address: $result"
                        bundle.putString("address", result)
                        message.data = bundle
                    } else {
                        message.what = 1
                        val bundle = Bundle()
//                        result = "Latitude: $latitude Longitude: $longitude Unable to get address for this lat-long."
                        bundle.putString("address", "Unable to get address. Click again to retry")
                        message.data = bundle
                    }
                    message.sendToTarget()
                }
            }
        }
        thread.start()
    }
}