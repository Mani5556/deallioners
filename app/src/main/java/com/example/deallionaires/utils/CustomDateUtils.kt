package com.example.deallionaires.utils

import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*

class CustomDateUtils(
    val context: Context,
    val from: String,
    private val tvDateOfBirth: EditText,
    dateCallback: (Boolean, Long) -> Unit
) {

    var cal = getInstance()

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            cal.set(YEAR, year)
            cal.set(MONTH, monthOfYear)
            cal.set(DAY_OF_MONTH, dayOfMonth)
            setBirthText { displayTextView: Boolean, timeLong: Long ->
                dateCallback(displayTextView, timeLong)
            }
        }

    private fun getCurrentDate(): String {
        val format = "dd-MMM-yyyy"
        val localTime: Locale = Locale.getDefault()
        val simpleDateFormat = SimpleDateFormat(format, localTime)
        return simpleDateFormat.format(Date())
    }

    private fun setBirthText(calCallback: (Boolean, Long) -> Unit) {
        val myFormat = "dd-MMM-yyyy"  // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        tvDateOfBirth.setText(sdf.format(cal.time))
        if (sdf.format(cal.time) == getCurrentDate()) {
            calCallback(true, cal.timeInMillis)
        } else calCallback(false, cal.timeInMillis)
    }

    fun launchMaxDatePicker() {
        val datePickerDialog = DatePickerDialog(
            context,
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(YEAR),
            cal.get(MONTH),
            cal.get(DAY_OF_MONTH)
        )
        datePickerDialog.datePicker.maxDate = cal.timeInMillis
        datePickerDialog.show()
    }

}