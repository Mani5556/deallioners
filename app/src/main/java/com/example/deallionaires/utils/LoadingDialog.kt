package com.example.deallionaires.utils


import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import com.example.deallionaires.R


class LoadingDialog {
    private var dialog: Dialog? = null

    fun show(context: Context) {
        if (dialog != null && dialog!!.isShowing) {
            return
        }
        dialog = Dialog(context, R.style.LoadingDialog)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.loading_dialog_layout)
        dialog!!.window?.setBackgroundDrawable(ColorDrawable(context.resources.getColor(R.color.full_transperant)));
        dialog!!.setCancelable(false)
        dialog!!.show()
    }

    fun cancel() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }

    companion object {
        private var instance: LoadingDialog? = null

        @Synchronized
        fun getInstance(): LoadingDialog {
            if (instance == null) {
                instance = LoadingDialog()
            }
            return instance as LoadingDialog
        }
    }
}