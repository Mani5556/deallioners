package com.example.deallionaires.utils

import okhttp3.HttpUrl
import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.*

class ProgressRequestBody : RequestBody {

    val mFile: File
    val ignoreFirstNumberOfWriteToCalls : Int
    var  content_type : MediaType? =  MediaType.parse("image/*")
    lateinit var progressCallBacks: ProgressCallBacks


    constructor(mFile: File,content_type:MediaType?, progressCallBacks: ProgressCallBacks) : super(){
        this.mFile = mFile
        ignoreFirstNumberOfWriteToCalls = 0
        this.content_type = content_type
        this.progressCallBacks = progressCallBacks
    }

    var numWriteToCalls = 0
    @Throws(IOException::class)
    override fun contentLength(): Long {
        return mFile.length()
    }

    override fun contentType(): MediaType? {
        return content_type
    }

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        numWriteToCalls++

        val fileLength = mFile.length()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val `in` = FileInputStream(mFile)
        var uploaded: Long = 0

        try {
            var read: Int
            var lastProgressPercentUpdate = 0.0f
            read = `in`.read(buffer)
            while (read != -1) {

                uploaded += read.toLong()
                sink.write(buffer, 0, read)
                read = `in`.read(buffer)

                // when using HttpLoggingInterceptor it calls writeTo and passes data into a local buffer just for logging purposes.
                // the second call to write to is the progress we actually want to track
                if (numWriteToCalls > ignoreFirstNumberOfWriteToCalls ) {
                    val progress = (uploaded.toFloat() / fileLength.toFloat()) * 100f
                    //prevent publishing too many updates, which slows upload, by checking if the upload has progressed by at least 1 percent
                    if (progress - lastProgressPercentUpdate > 1 || progress == 100f) {
                        // publish progress
                        progressCallBacks?.progressUpdate(progress)
                        lastProgressPercentUpdate = progress
                    }
                }
            }
        } finally {
            `in`.close()
        }
    }


    companion object {

        private val DEFAULT_BUFFER_SIZE = 2048
    }


}

interface ProgressCallBacks{

    fun progressUpdate(precentage:Float)
    fun uploadSuccess(imageUrl: String)
    fun uploadFail(message:String)
}