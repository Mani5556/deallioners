package com.example.deallionaires.utils

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import androidx.core.app.ActivityCompat

class AppLocationService(private val context: Context) : Service(),
    LocationListener {
    private var locationManager: LocationManager? = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    var location: Location? = null
    fun getLocation(provider: String?, networkProvider: String?): Location? {
        if (locationManager!!.isProviderEnabled(provider!!)) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return null
            } else {
                locationManager!!.requestLocationUpdates(
                    provider,
                    MIN_TIME_FOR_UPDATE,
                    MIN_DISTANCE_FOR_UPDATE.toFloat(),
                    this
                )
                if (locationManager != null) {
                    location = locationManager!!.getLastKnownLocation(provider)
                    return location
                }
            }
        }else if(locationManager!!.isProviderEnabled(networkProvider!!)){
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return null
            } else {
                locationManager!!.requestLocationUpdates(
                    provider,
                    MIN_TIME_FOR_UPDATE,
                    MIN_DISTANCE_FOR_UPDATE.toFloat(),
                    this
                )
                if (locationManager != null) {
                    location = locationManager!!.getLastKnownLocation(networkProvider)
                    return location
                }
            }
        }
        return location
    }
    override fun onLocationChanged(location: Location) {}
    override fun onProviderDisabled(provider: String) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onStatusChanged(
        provider: String,
        status: Int,
        extras: Bundle
    ) {
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    companion object {
        private const val MIN_DISTANCE_FOR_UPDATE: Long = 10
        private const val MIN_TIME_FOR_UPDATE = 1000 * 60 * 2.toLong()
    }

}