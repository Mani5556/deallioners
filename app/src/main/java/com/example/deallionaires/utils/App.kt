package com.example.deallionaires.utils

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.example.deallionaires.model.UserLatLong
import com.example.deallionaires.network.APIClient
import com.example.deallionaires.network.APIInterface

class App : Application() {

    companion object {
        lateinit var instance: App
            private set
        val apiClient by lazy {
            APIClient.createService(APIInterface::class.java)
        }
    }

    override fun onCreate() {
        super.onCreate()
        var userLocationLatitude by PreferenceExt.appPreference(
            this,
            Constants.USER_LOCATION_LATITUDE,
            Constants.DEFAULT_STRING
        )

        var userLocationLongitude by PreferenceExt.appPreference(
            this,
            Constants.USER_LOCATION_LONGITUDE,
            Constants.DEFAULT_STRING
        )
        Constants.USER_LATITUDE_VALUE = if(userLocationLatitude.isNullOrBlank()) 0.0 else userLocationLatitude.toDouble()
        Constants.USER_LONGITUDE_VALUE =if(userLocationLongitude.isNullOrBlank()) 0.0 else userLocationLongitude.toDouble()
    }

}