package com.example.deallionaires.utils

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PercentageLinearLayoutManager(val context: Context,orientation:Int,val ratio:Float): LinearLayoutManager(context,orientation,false){
    override fun attachView(child: View, index: Int, lp: RecyclerView.LayoutParams?) {
        super.attachView(child, index, lp)
    }

    override fun generateLayoutParams(lp: ViewGroup.LayoutParams?): RecyclerView.LayoutParams {
        return getPercentageParams(super.generateLayoutParams(lp))
    }

    override fun generateLayoutParams(c: Context?, attrs: AttributeSet?): RecyclerView.LayoutParams {
        return  getPercentageParams(super.generateLayoutParams(c, attrs))
    }

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return getPercentageParams(super.generateDefaultLayoutParams())
    }

    private fun getPercentageParams(layoutParams: RecyclerView.LayoutParams):RecyclerView.LayoutParams = layoutParams.apply {
        width = (getHorizontalSpace() * ratio).toInt()
    }


    private fun getHorizontalSpace(): Int {
        return width - paddingRight - paddingLeft
    }
    override fun canScrollHorizontally(): Boolean {
        return true
    }
}