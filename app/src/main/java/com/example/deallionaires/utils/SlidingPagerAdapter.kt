package com.example.deallionaires.utils

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.example.deallionaires.R
import kotlinx.android.synthetic.main.item_img_slider.view.*
import java.util.*
import kotlin.collections.ArrayList

class SlidingPagerAdapter(val context:Activity,val images:ArrayList<String>):PagerAdapter(){
    private var inflater: LayoutInflater? = null
//    private val images = arrayOf(R.drawable.anton, R.drawable.frankjpg, R.drawable.redcharlie, R.drawable.westboundary)

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater!!.inflate(R.layout.item_img_slider, null)
        Glide.with(context).load(images[position]).error(getRandomColor()).into(view.iv_slider)

        view.iv_slider.load(images[position]) {isLoaded->
            if(isLoaded){
                view.iv_slider.setOnClickListener {imageView->
                    images[position]?.let {
                        context.goToFullImageView(it,imageView)
                    }
                }
            }
        }


        val vp = container as ViewPager
        vp.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }

}