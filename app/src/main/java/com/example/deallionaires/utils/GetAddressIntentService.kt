package com.example.deallionaires.utils

import android.app.IntentService
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.ResultReceiver
import android.util.Log
import java.util.*

class GetAddressIntentService :
    IntentService(IDENTIFIER) {
    private var addressResultReceiver: ResultReceiver? = null

    //handle the address request
    override fun onHandleIntent(intent: Intent?) {
        var msg = ""
        if (intent != null) {
            addressResultReceiver =
                intent.getParcelableExtra("add_receiver")
        }
        if (addressResultReceiver == null) {
            Log.e(
                "GetAddressIntentService",
                "No receiver, not processing the request further"
            )
            return
        }
        var location: Location? = null
        if (intent != null) {
            location = intent.getParcelableExtra("add_location")
        }

        //send no location error to results receiver
        if (location == null) {
            msg = "No location, can't go further without location"
            sendResultsToReceiver(0, msg)
            return
        }
        //call GeoCoder getFromLocation to get address
        //returns list of addresses, take first one and send info to result receiver
        val geocoder = Geocoder(this, Locale.getDefault())
        var addresses: List<Address>? = null
        try {
            addresses = geocoder.getFromLocation(
                location.latitude,
                location.longitude,
                1
            )
        } catch (ioException: Exception) {
            Log.e("", "Error in getting address for the location")
        }
        if (addresses == null || addresses.size == 0) {
            msg = "No address found for the location"
            sendResultsToReceiver(1, msg)
        } else {
            val address = addresses[0]
            val addressDetails = address.featureName +
                    "," + address.locality +
                    "," + address.subAdminArea +
                    "," + address.adminArea +
                    "," + address.countryName +
                    "," + address.postalCode
            sendResultsToReceiver(2, addressDetails)
        }
    }

    //to send results to receiver in the source activity
    private fun sendResultsToReceiver(resultCode: Int, message: String) {
        val bundle = Bundle()
        bundle.putString("address_result", message)
        addressResultReceiver!!.send(resultCode, bundle)
    }

    companion object {
        private const val IDENTIFIER = "GetAddressIntentService"
    }
}