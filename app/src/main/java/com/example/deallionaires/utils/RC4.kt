package com.example.deallionaires.utils

import kotlin.experimental.and
import kotlin.experimental.xor

class RC4 (key: ByteArray?) {

    private val state = ByteArray(256)
    private var x: Int
    private var y: Int

    constructor(key: String) : this(key.toByteArray()) {}

    fun rc4(data: String?): ByteArray? {
        if (data == null) {
            return null
        }
        val tmp = data.toByteArray()
        this.rc4(tmp)
        return tmp
    }

    private fun rc4(buf: ByteArray?): ByteArray? {
        var xorIndex: Int
        var tmp: Byte
        if (buf == null) {
            return null
        }
        val result = ByteArray(buf.size)
        for (i in buf.indices) {
            x = x + 1 and 0xff
            y = (state[x] and 0xff.toByte()) + y and 0xff
            tmp = state[x]
            state[x] = state[y]
            state[y] = tmp
            xorIndex = (state[x] and 0xff.toByte()) + (state[y] and 0xff.toByte()) and 0xff
            result[i] = (buf[i] xor state[xorIndex]) as Byte
        }
        return result
    }

    init {
        for (i in 0..255) {
            state[i] = i.toByte()
        }
        x = 0
        y = 0
        var index1 = 0
        var index2 = 0
        var tmp: Byte
        if (key == null || key.isEmpty()) {
            throw NullPointerException()
        }
        for (i in 0..255) {
            index2 = (key[index1] and 0xff.toByte()) + (state[i] and 0xff.toByte()) + index2 and 0xff
            tmp = state[i]
            state[i] = state[index2]
            state[index2] = tmp
            index1 = (index1 + 1) % key.size
        }
    }
}