package com.example.deallionaires.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.provider.MediaStore
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import com.example.deallionaires.R
import com.example.deallionaires.model.HomeDeals
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*
import kotlin.reflect.KProperty


object Constants {

    val TRANSITION_NAME: String = "transitionName"
    const val SELECTED_CATEGORY_POSITION: String = "selected_category_position"
    const val APP_SHARED_PREFS_NAME = "Deallionaires App Preferences"

    const val IS_USER_LOGIN = "is_user_login"

    //    const val API_KEY = "AIzaSyAl_PB1x_bg7AU9hmcrqNPxE3_bNd2Omqg"
    const val API_KEY = "AIzaSyD7XJ6WJYHg8NgX-KkN2Wh5OI1N4p7d3ao"

    const val DEFAULT_STRING = ""
    const val EMPTY_STRING = ""

    const val DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

    //FOR DEALLIONAIRES NETWORK CALLS
//    const val BASE_URL = "http://34.93.168.103/api/"
//    const val BASE_URL = "http://13.232.173.143:3000/api/" //main url
//    const val BASE_URL = "http://13.235.23.120:3000/api/" //Deallionaires
//    const val BASE_URL = "http://13.232.29.76:3000/api/" //Deallionaires Demo
    const val BASE_URL = "http://13.232.173.143:3000/api/" //Deallionaires UAT
    const val AUTH_USERNAME = "Deallionaires"
    const val AUTH_PASSWORD = "Deallionaires"
    const val USER = "user/"
    const val DEALLIONAIRES_COUNT = "dashboard/logocount"
    const val FORGOT_PASSWORD_API = "user/forgot"
    const val FORGOT_PASSWORD = "forgot_password"
    const val RESET_PASSWORD = "user/reset"
    const val EDIT_USER = "user/edit"
    const val LANDING = "landing/"

    //    const val LANDING_PAGE = "landingpage"
    const val CATEGORY_LIST = "subcategory_list"
    const val ALL_lIST = "landingpage_list"
    const val LANDING_PAGE = "landingpage_customized"
    const val GET = "get"
    const val POST_LOGIN = "login"
    const val ADD_NEW_USER = "user/add"
    const val BUSINESS = "business/categories/categorieswithsubcat"
    const val SEARCH_DEALS = "deals/vd"
    const val SEARCH_BUSINESS = "business/searchbusiness"
    const val SEARCH_HOME_DEALS = "dashboard/search"
    const val SEARCH_DEAL_DETAIL_ID = "deals/dealsdetail/{dealid}"
    const val SEARCH_BUSINESS_MERCHANT_ID = "business/detailbusiness/{merchantid}"
    const val CLAIM_DEAL = "deals/claimdeal/{redeemId}?"
    const val REDEEM_DEAL = "deals/redeemdeal"
    const val GET_OTP = "user/otp"
    const val HELP_CENTER_QUERY = "user/help_center"
    const val GET_FAQS = "faq/faq_listing"
    const val GET_FORGOT_PASSWORD_OTP = "user/forgotpassword"
    const val GET_MOB_NUM_CHANGE_OTP = "user/mobileno_change"
    const val GET_MOB_NUM_SIGN_UP_OTP = "user/mobile_verification"
    const val GET_DASHBOARD = "dashboard/view"
    const val GET_DEALLIONAIRES_CLUB = "user/deallionairesclub"
    const val GET_OFFER_VALUE_POINTS = "dashboard/offervaluepoints"

    //    const val GET_APP_VALUE_POINTS = "dashboard/applevelpoints"
    const val GET_APP_VALUE_POINTS = "dashboard/transactionlevelpoints"
    const val GET_MY_DEALS = "deals/redeemdetail?"
    const val GET_COMPLETED_DEALS = "deals/claimeddeals"
    const val GET_PENDING_DEALS = "deals/redeemdetail"
    const val GET_MERCHANT_NOTIFICATIONS = "notifications/notimerchant"
    const val GET_ADMIN_NOTIFICATIONS = "notifications/notiadmin"
    const val GET_USER_SPECIAL_DEALS = "dashboard/special_offers"
    const val ADD_DEAL_TO_FAVOURITES = "dashboard/favourite"
    const val ADD_BUSINESS_REVIEW_RATING = "business/addreview_rating"
    const val REMOVE_DEAL_FROM_FAVOURITES = "dashboard/favourite/deletefavourite"
    const val GET_USER_FAVOURITES = "dashboard/favourite"

    //FOR NETWORK QUERIES
    const val MAIN_CATEGORY_NAME = "category_id"
    const val PAGE = "page"
    const val SLUG = "slug"
    const val SUB_CATEGORY_NAME = "subcategory_id"
    const val SORT_OPTIONS = "sortOptions"
    const val SEARCH_QUERY = "searchQuery"
    const val USER_LAT = "userlat"
    const val USER_LONG = "userlong"

    const val ID = "id"
    const val PROFILE_IMAGE = "profile_image"
    const val FIRST_NAME = "first_name"
    const val LAST_NAME = "last_name"
    const val EMAIL = "email"
    const val MOBILE_NUMBER = "mobile_number"
    const val ADDRESS = "address"
    const val COUNTRY = "country"
    const val CITY = "city"
    const val STATE = "state"
    const val GENDER = "gender"
    const val DATE_OF_BIRTH = "date_of_birth"
    const val SIGN_UP_TYPE = "signup_type"
    const val LOGIN_TYPE_NORMAL = "normal"
    const val LOGIN_TYPE_FACEBOOK = "facebook"
    const val LOGIN_TYPE_GOOGLE = "gmail"
    const val LOGIN_TYPE_INSTAGRAM = "Instagram"

    const val USER_TOKEN_KEY = "user_token_key"
    const val USER_LOCATION = "user_location"

    const val DEAL_ID = "dealid"
    const val REDEEM_DEAL_ID = "redeemId"
    const val MERCHANT_ID = "merchantid"

    //    const val INSTAGRAM_APP_ID = "1086191008414520"
    const val INSTAGRAM_APP_ID = "599893400901795"

    //    const val INSTAGRAM_CLIENT_SECRET = "352bdaca26ed5dce1c5d577b530e597b"
    const val INSTAGRAM_CLIENT_SECRET = "c2640ab784eafa700009fa477f658206"


    const val FROM_ACTIVITY = "from_activity"
    const val USER_SKIP_LOGIN = "skip_login"
    const val ACTIVITY = "activity"
    const val FROM_SEARCH_BUSINESS_ACTIVITY = "from_search_business_activity"
    const val FROM_HOME_FRAGMENT = "from_home_fragment"
    const val FROM_DASHBOARD_FRAGMENT = "from_dashboard_fragment"
    const val FROM_CART_FRAGMENT = "from_cart_fragment"
    const val FROM_FAVOURITES_FRAGMENT = "from_favourites_fragment"
    const val FROM_DEALLIONAIRES_FRAGMENT = "from_deallionaires_fragment"
    const val FROM_SEARCH_HOME_FRAGMENT = "from_search_home_fragment"
    const val FROM_SEARCH_DEALS = "from_search_deals"
    const val FROM_SPECIAL_DEALS = "from_special_deals"
    const val FROM_SPECIAL_DEALS_FRAGMENT = "from_special_deals_fragment"
    const val FROM_DEAL_ITEM_FRAGMENT = "from_deal_item"
    const val FROM_DASHBOARD = "from_dashboard"
    const val FROM_PROFILE = "from_profile"
    const val FROM_LOGIN = "from_login"
    const val FROM_SPLASH_SCREEN = "from_splash_screen"
    const val FROM_SIGN_UP = "from_sign_up"
    const val FROM_SOCIAL_SIGN_UP = "from_social_sign_up"
    const val FROM_MOBILE_NUMBER = "from_mob_num"

    const val HOME_DEALS = "Deals"
    const val HOME_CATEGORY_DEALS = "Home Category Deals"
    const val CART = "Cart"
    const val DEALLIONS_CLUB = "Deallions Club"
    const val DEALLIONS_EARNED = "Deallionaires Earned"
    const val HOME_SEARCH = "Home Search"
    const val COMPLETED_DEALS = "Completed Deals"
    const val FROM_COMPLETED_DEALS_FRAGMENT = "completed_deals_fragment"
    const val FROM_PENDING_DEALS_FRAGMENT = "pending_deals_fragment"
    const val FAVOURITES = "Favourites"

    const val FORGOT_PASSWORD_ACCESS_TOKEN = "forgot_access_token"
    const val SIGN_UP_ACCESS_TOKEN = "sign_up_access_token"
    const val SIGN_UP_MOBILE_NUM_VERIFIED = "sign_up_mob_num_verified"
    const val SIGN_UP_FACEBOOK_MAIL_ID = "sign_up_fb_email_id"
    const val SIGN_UP_GOOGLE_MAIL_ID = "sign_up_google_mail_id"

    const val LOCATION_CODE = 123
    const val LOCATION = "Location"
    const val DEFAULT_LAT_LONG = "def_lat_long"

    const val USER_PROFILE_NAME = "default_name"

    const val HOME_DEALS_ARRAY_LIST_STRING = "home_deals_list_string"
    const val HOME_DEAL_ID = "home_deal_id"
    const val USER_LAT_LONG = "latlong"
    const val USER_LOCATION_LATITUDE = "latitude"
    const val USER_LOCATION_LONGITUDE = "longitude"
    const val DEALLIONS_SECRET_KEY = "530m9754serianodealli"

    const val VERIFICATION_EMAIL = "verification_email"
    const val VERIFIED_EMAIL = "verified_email"
    const val VERIFICATION_MOBILE_NUMBER = "verification_mob_num"
    const val VERIFIED_MOBILE_NUMBER = "verified_mob_num"
    const val VERIFIED_PROFILE_MOBILE_NUMBER = "verified_profile_mob_num"
    const val COUNTRY_CODE = "country_code"
    const val IS_SOCIAL_LOGIN_CLICKED = "social_login_clicked"

    const val APP_GALLERY_PATH = "/Deallionaries/Images"
    const val ACTION_LOCATION_UPDATE_RECEIVER = "action_location_update_receiver"
    const val ACTION_PENDING_DEALS_UPDATED = "ACTION_PENDING_DEALS_UPDATED"


    //user latitude value and longitude will update from home page
    var USER_LATITUDE_VALUE = 0.0
    var USER_LONGITUDE_VALUE = 0.0


    /**
     * this method is used to get string from array list
     * @param arrayList contains list of objects
     */
    fun getStringFromArrayList(arrayList: ArrayList<Any>): String {
        return Gson().toJson(
            arrayList,
            object : TypeToken<ArrayList<Any?>?>() {}.type
        )
    }

    /**
     * this method is used to get array list from string
     * @param listString contains list string
     */
    fun getArrayListFromHomeDealsString(listString: String): ArrayList<HomeDeals> {
        return fromHomeDealsJson(
            listString,
            object : TypeToken<ArrayList<HomeDeals?>?>() {}.type
        )!!
    }

    private fun fromHomeDealsJson(jsonString: String?, type: Type?): ArrayList<HomeDeals>? {
        return Gson().fromJson<ArrayList<HomeDeals>>(jsonString, type)
    }

}

/**
 * This method is used for to check is network is connected or not
 */
fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
    return if (connectivityManager is ConnectivityManager) {
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        networkInfo?.isConnected ?: false
    } else false
}

/**
 * used to hide keyboard
 * @param view contains id of views
 */
fun Context.hideKeyboard(view: View) {
    val inputMethodManager =
        getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

/**
 * This class is contain the logout dialog implementation.
 */
class CustomDialog(context: Context) {

    val logoutDialog = LayoutInflater.from(context).inflate(R.layout.dialog_logout, null)
    private val dialogBuilder: AlertDialog.Builder =
        AlertDialog.Builder(context).setView(logoutDialog)
    private val no: TextView by lazy { logoutDialog.findViewById<TextView>(R.id.tvLogoutNo) }
    private val yes: TextView by lazy { logoutDialog.findViewById<TextView>(R.id.tvLogoutYes) }
    private val tvLogoutMsg: TextView by lazy { logoutDialog.findViewById<TextView>(R.id.tvLgoutConfirmation) }
    fun showDialog(
        diaContent: String,
        diaPositive: String,
        diaNegative: String,
        positiveResponse: () -> Unit
    ) {
        val dialog = dialogBuilder.setCancelable(true).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        tvLogoutMsg.text = diaContent
        no.text = diaNegative
        yes.text = diaPositive
        dialog.show()
        no.setOnClickListener { dialog.dismiss() }
        yes.setOnClickListener {
            positiveResponse()
            dialog.dismiss()
        }
    }
}

fun <T> getStringFromModel(input: T) = ModelToStringConverter(input)

fun <T> getModelFromString(inputStr: String, inputModel: Class<T>) =
    StringToModelConverter(inputStr, inputModel)

class ModelToStringConverter<T>(private val input: T) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return convertValue(input)
    }

    private fun convertValue(input: T): String {
        val srtGson = Gson()
        return srtGson.toJson(input)
    }
}

fun getRealPathFromURI(context: Context, contentURI: Uri): String? {
    var result: String? = ""
    try {
        val cursor: Cursor? =
            context.getContentResolver().query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return result
}

fun getRandomColor(): Int {
    val rnd = Random()
    return when (rnd.nextInt(3)) {
        1 -> {
            R.color.colorTextFavourites
        }
        2 -> {
            R.color.colorHomeCategory
        }
        else -> {
            R.color.colorBusinessDealCategory
        }
    }
}

@SuppressLint("SimpleDateFormat")
fun getExpiryDays(exp: String): String {
    val expDate = exp.replace(("[A-Za-z]").toRegex(), " ").trim()
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    val expTime = simpleDateFormat.parse(expDate).time
    val newDateFormate = SimpleDateFormat("dd-MMM-yyyy")
    return newDateFormate.format(expTime)
//    val currentTime = System.currentTimeMillis()
//    val relativeTimeStr = DateUtils.getRelativeTimeSpanString(
//        expTime,
//        currentTime, DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE
//    )
//    return relativeTimeStr.toString()
}

fun getDpSize(intSize: Int, context: Context) = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    intSize.toFloat(),
    context.resources.displayMetrics
).toInt()

/**
 * this function return the expiry date in dd-mm-yyyy format
 */
fun getExpiry(expiry: String?): String? {

    if (expiry == null) {
        return expiry
    }

    if (expiry?.contains("T")) {
        return "Club Exp:${expiry.split("T").first()}"
    } else {
        val outputFormater = SimpleDateFormat("dd-MMM-yyyy")
        val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val date = outputFormater.format(inputFormat.parse(expiry))
        return "Club Exp:${date}"
    }
}


 public fun addCommasToNumericString(digits: String): String? {
    var result = ""
    val len = digits.length
    var nDigits = 0
    for (i in len - 1 downTo 0) {
        result = digits[i].toString() + result
        nDigits++
        if (nDigits % 3 == 0 && i > 0) {
            result = ",$result"
        }
    }
    return result
}

class StringToModelConverter<T>(private val input: String, val type: Class<T>) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        val modelGson = Gson()
        return modelGson.fromJson(input, type)
    }


}




enum class NetworkState(val value: Int) {
    IN_PROGRESS(1),
    COMPLETED(2),
    ERROR(3),
    NO_DATA(4),
    NO_NETWORK(5)
}


