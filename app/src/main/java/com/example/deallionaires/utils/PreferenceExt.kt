package com.example.deallionaires.utils

import android.content.Context
import android.content.SharedPreferences
import kotlin.reflect.KProperty

/**
 * This file contains shared preference implementation using delegates
 */
object PreferenceExt {
    fun <T> appPreference(context: Context, name: String, default: T) =
        AppPreference(context, name, default)

    /**
     * This class is used to create app shared preferences using delegates.
     * By using delegates we can easily save and retrieve values from shared preference.
     */
    class AppPreference<T>(val context: Context, val name: String, private val defaultValue: T) {
        private val appPrefs: SharedPreferences by lazy { context.getSharedPreferences(Constants.APP_SHARED_PREFS_NAME, Context.MODE_PRIVATE) }
        operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
            return getPrefValue(name, defaultValue)
        }

        operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
            setPrefValue(name, value)
        }

        fun clearData() {
            appPrefs.edit().clear().apply()
        }

        /**
         * This method is used to save values in shared preferences.
         */
        private fun setPrefValue(name: String, value: T) = with(appPrefs.edit()) {
            when (value) {
                is Boolean -> putBoolean(name, value)
                is String -> putString(name, value)
                is Int -> putInt(name, value)
                is Float -> putFloat(name, value)
                is Long -> putLong(name, value)
                else -> throw IllegalArgumentException("This type of preference can't save")
            }.apply()
        }

        /**
         * This method is used to get values from shared preference.
         */
        private fun getPrefValue(name: String, defaultValue: T): T = with(appPrefs) {
            val res: Any = when (defaultValue) {
                is Boolean -> getBoolean(name, defaultValue)
                is String -> getString(name, defaultValue)!!
                is Int -> getInt(name, defaultValue)
                is Float -> getFloat(name, defaultValue)
                is Long -> getLong(name, defaultValue)
                else -> throw IllegalArgumentException("This type of preference can't get")
            }
            return res as T
        }

    }
}