package com.example.deallionaires.utils

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.example.deallionaires.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.map_dialog.*
import kotlinx.android.synthetic.main.map_dialog.view.*

class CustomMapDialog : DialogFragment() {

    companion object {
        private const val TAG = "LocationPickerDialog"
        private const val EXTRA_LAT = "lat"
        private const val EXTRA_LNG = "lng"
        private const val EXTRA_BUSINESS_NAME = "businessName"
        private const val EXTRA_ADDRESS = "address"
        private const val EXTRA_PHONE = "phone"

        private const val DEFAULT_ZOOM = 15f

        fun newInstance(lat: Double? = null, lng: Double? = null,businessName:String?,address:String?,phone:String?): CustomMapDialog {
            val dialog = CustomMapDialog()
            val args = Bundle().apply {
                lat?.let { putDouble(EXTRA_LAT, it) }
                lng?.let { putDouble(EXTRA_LNG, it) }
                businessName?.let { putString(EXTRA_BUSINESS_NAME, it) }
                address?.let { putString(EXTRA_ADDRESS, it) }
                phone?.let { putString(EXTRA_PHONE, it) }
            }
            dialog.arguments = args
            return dialog
        }
    }

    var customView: View? = null
    private var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null

    private val okButton: Button by lazy {
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
    }
    private val cancelButton: Button by lazy {
        (dialog as AlertDialog).getButton(AlertDialog.BUTTON_NEGATIVE)
    }



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return customView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // StackOverflowError
        // customView = layoutInflater.inflate(R.layout.dialog_edit_text, null)
        customView = activity!!.layoutInflater.inflate(R.layout.map_dialog, null)
        customView?.findViewById<ImageView>(R.id.iv_cancel)?.setOnClickListener { dismiss() }

        val builder = AlertDialog.Builder(context!!)
            .setView(customView)
//            .setPositiveButton(android.R.string.ok, null)
//            .setNegativeButton(android.R.string.cancel, null)
        val dialog = builder.create()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent);
        return dialog
    }

    override fun onStart() {
        super.onStart()

        okButton.setOnClickListener {
//            onOk?.invoke()
            dismiss()
        }

        cancelButton.setOnClickListener {
//            onCancel?.invoke()

        }

        okButton.isEnabled = false
        cancelButton.isEnabled = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tv_title.text =  arguments?.getString(EXTRA_BUSINESS_NAME)?:""
        tv_address.text =  arguments?.getString(EXTRA_ADDRESS)?:""
        tv_contact.text =  arguments?.getString(EXTRA_PHONE)?:""

//         if onCreateView didn't return view
//         java.lang.IllegalStateException: Fragment does not have a view
        mapFragment = childFragmentManager.findFragmentByTag("map") as SupportMapFragment?
        if (mapFragment == null) {
            Log.d(TAG, "SupportMapFragment.newInstance")
            mapFragment = SupportMapFragment.newInstance()
            childFragmentManager.beginTransaction().replace(R.id.map, mapFragment!!, "map").commit()
        }


        mapFragment?.let { mapFragment ->
            mapFragment.getMapAsync { map ->
                googleMap = map

                map.setOnMapLoadedCallback {

                    val lat = arguments?.getDouble(EXTRA_LAT)
                    val lng = arguments?.getDouble(EXTRA_LNG)


                    if (lat != null && lng != null) {
                        val latLng = LatLng(lat, lng)

                        map.addMarker(
                            MarkerOptions()
                                .position(latLng)
                        )
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM))
                        map.animateCamera(CameraUpdateFactory.newLatLng(latLng))
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
                    }
                }
            }
        }
    }
}