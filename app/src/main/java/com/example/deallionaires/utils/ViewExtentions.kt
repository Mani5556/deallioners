package com.example.deallionaires.utils

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.core.app.ActivityOptionsCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.example.deallionaires.R
import com.example.deallionaires.ui.profile.FullPhotoActivity
import com.facebook.internal.NativeProtocol.IMAGE_URL_KEY
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Scope

//This extension is used for to get and set edit text data
var EditText.data
    get() = this.text.toString()
    set(value) = this.setText(value)

//This extension is used for to get current date and time
val Date.currentDate: String
    get() = run {
        val format = "dd/MM/yyyy HH:mm:ss"
        val localTime: Locale = Locale.getDefault()
        val formatter = SimpleDateFormat(format, localTime)
        return formatter.format(this)
    }

fun EditText.getIntValue(): Int {
    return if (this.text.isNullOrBlank()) {
        0
    } else {
        this.text.toString().trim().toInt()
    }
}

fun EditText.getLongValue(): Long {
    return if (this.text.isNullOrBlank()) {
        0
    } else {
        this.text.toString().trim().toLong()
    }
}

fun EditText.getDoubleValue(): Double {
    return if (this.text.isNullOrBlank()) {
        0.0
    } else {
        this.text.toString().trim().toDouble()
    }
}


fun Activity.goToFullImageView(url: String?, imageView: View) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageView, imageView.transitionName).toBundle()
        Intent(this, FullPhotoActivity::class.java)
            .putExtra(IMAGE_URL_KEY, url)
            .putExtra(Constants.TRANSITION_NAME,imageView.transitionName)
            .let { startActivity(it, options ?: null) }
    } else {
        startActivity(intentFor<FullPhotoActivity>(IMAGE_URL_KEY to url))
    }

}

fun ImageView.load(url: String?,canShowIconPlaceholder:Boolean = false, onLoadingFinished: (isLoaded:Boolean) -> Unit = {}) {
    val listener = object : RequestListener<Drawable> {
        override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
            onLoadingFinished(false)
            return false
        }

        override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            onLoadingFinished(true)
            return false
        }


    }
    Glide.with(this.context)
        .load(url)
        .apply( RequestOptions.placeholderOf(if(canShowIconPlaceholder) R.drawable.deallionaires_logo else getRandomColor()))
        .listener(listener)
        .into(this)
}

