package com.example.deallionaires.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseProfile {
    @SerializedName("profile")
    @Expose
    var profile: List<Profile>? = null
}