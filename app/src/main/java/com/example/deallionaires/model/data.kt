package com.example.deallionaires.model

import com.example.deallionaires.utils.Constants
import com.google.gson.JsonArray
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.jetbrains.annotations.NotNull

data class Categories(
    @SerializedName("id")
    @Expose
    var id: String,
    @SerializedName("category_name")
    @Expose
    var category_name: String,
    @SerializedName("slug")
    @Expose
    var slug: String,
    @SerializedName("banner_image")
    @Expose
    var banner_image: String,
    @SerializedName("sub_category")
    @Expose
    var subCategorries: List<String> = ArrayList<String>()
)

data class Banner(
    @SerializedName("id")
    @Expose
    var id: String,
    @SerializedName("category_name")
    @Expose
    var category_name: String,
    @SerializedName("slug")
    @Expose
    var slug: String,
    @SerializedName("banner_image")
    @Expose
    var banner_image: String
)

data class SignUp(

    @SerializedName("success")
    @Expose
    val success: Boolean,

    @SerializedName("token")
    @Expose
    val token: String?,


    @SerializedName("errorcode")
    @Expose
    val errorcode: Int,

    @SerializedName("msg")
    @Expose
    val msg: String?
)

//data class ClaimDeal(
//    @SerializedName("verificationcode")
//    @Expose
//    var verificationcode: String,
//    @SerializedName("dealid")
//    @Expose
//
//
//    var dealid: String,
//    @SerializedName("merchantid")
//    @Expose
//    var merchantid: String,
//    @SerializedName("paidprice")
//    @Expose
//    var paidprice: String,
//    @SerializedName("savingamount")
//    @Expose
//    var savingamount: String,
//    @SerializedName("useloyaltypoint")
//    @Expose
//    var useloyaltypoint: String
//)

data class ClaimDeal(
    @SerializedName("verification_code")
    @Expose
    var verificationcode: String,

    @SerializedName("transaction_amount")
    @Expose
    var transactionAmount: String,
    @SerializedName("savingamount")
    @Expose
    var savingamount: String,

    @SerializedName("deal_id")
    @Expose
    var dealId: Int,
    @SerializedName("merchant_id")
    @Expose
    var merchantid: Int,


    @SerializedName("used_points")
    @Expose
    var usedPoints: String

)

data class ClaimDealResponse(
    @SerializedName("msg")
    @Expose
    val msg: String
)

class RedeemDeal(
    @SerializedName("merchantid")
    @Expose
    var merchantid: String,

    @SerializedName("dealid")
    @Expose
    var dealid: String,

    @SerializedName("offer_type_id")
    @Expose
    var offerTypeId: String,

    @SerializedName("offer_category_id")
    @Expose
    var offerCategoryId: String,

    @SerializedName("loyalty_points")
    @Expose
    var loyaltyPoints: Int,

    @SerializedName("regular_price")
    @Expose
    var regularPrice: Int,

    @SerializedName("offer_price")
    @Expose
    var offerPrice: Int,

    @SerializedName("discount")
    @Expose
    var discount: Int,

    @SerializedName("customer_base_offer_type_id")
    @Expose
    var customerBaseOfferTypeId: Int
)

data class RedeemDealResponse(
    @SerializedName("msg")
    @Expose
    val msg: String
)

data class Login(
    @SerializedName("tokenkey")
    @Expose
    val tokenKey: String?,

    @SerializedName("msg")
    @Expose
    val msg: String?
)

data class SubCategory(
    var id: String,
    var name: String
)

data class Deals(
    @SerializedName("id")
    @NotNull
    var id: Int,

    @SerializedName("regular_price")
    @NotNull
    var regular_price: Float,

    @SerializedName("offer_price")
    @NotNull
    var offer_price: Float,

    @SerializedName("title")
    @NotNull
    var title: String,

    @SerializedName("description")
    @NotNull
    var description: String,

    @SerializedName("deal_image")
    @NotNull
    var deal_image: String,

    @SerializedName("merchantsimage")
    @NotNull
    var merchantsimage: String
)


data class UserDeals(
    var id: String,
    var image: String,
    var title: String,
    var originalPrice: String,
    var offerPrice: String
)

data class HomeDeals(
    var id: Int,
    var regular_price: Float,
    var offer_price: Float,
    var title: String,
    var description: String,
    var deal_image: String,
    var merchantsimage: String,
    var categoryName: String
)

data class NotificationModel(
    var id: String,
    var notificationId: String,
    var notificationTitle: String,
    var notificationSubTitle: String,
    var notificationContent: String
)

data class Favourite(
    var id: String,
    var image: String,
    var title: String,
    var description: String,
    var count: Int
)

data class Cart(
    var id: String,
    var image: String,
    var title: String,
    var description: String,
    var expireOn: String,
    var transactionId: String,
    var status: String,
    var count: Int
)

data class User(
    var email: String,
    var password: String,
    var login_type: String,
    var social_id: String
)

data class UserProfile(
    @SerializedName(Constants.ID)
    @NotNull
    var id: Int,
    @SerializedName(Constants.PROFILE_IMAGE)
    var profile_image: String,
    @SerializedName(Constants.FIRST_NAME)
    var first_name: String,
    @SerializedName(Constants.LAST_NAME)
    var last_name: String,
    @SerializedName(Constants.EMAIL)
    var email: String,
    @SerializedName(Constants.MOBILE_NUMBER)
    var mobile_number: String,
    @SerializedName(Constants.ADDRESS)
    var address: String,
    @SerializedName(Constants.COUNTRY)
    var country: String,
    @SerializedName(Constants.CITY)
    var city: String,
    @SerializedName(Constants.STATE)
    var state: String,
    @SerializedName(Constants.GENDER)
    var gender: String,
    @SerializedName(Constants.DATE_OF_BIRTH)
    var date_of_birth: String,
    @SerializedName(Constants.SIGN_UP_TYPE)
    var signup_type: String
) {
    constructor(
        first_name: String,
        last_name: String,
        email: String,
        mobile_number: String,
        address: String,
        gender: String,
        date_of_birth: String,
        signup_type: String
    ) : this(
        0,
        "profile_image",
        first_name,
        last_name,
        email,
        mobile_number,
        address,
        "country",
        "city", "state",
        gender,
        date_of_birth,
        signup_type
    )
}

data class SearchUserCategories(
    @SerializedName("success")
    @Expose
    var success: Boolean,

    @SerializedName("categories")
    @Expose
    var categories: List<SearchUserCategory>
)


data class SearchUserCategory(
    @SerializedName("id")
    @Expose
    var id: String,
    @SerializedName("category_name")
    @Expose
    var categoryName: String,

    @SerializedName("slug")
    @Expose
    var slug: String,

    @SerializedName("banner_image")
    @Expose
    var bannerImage: String,

    @SerializedName("subcategories")
    @Expose
    var searchUserSubcategories: List<SearchUserSubcategory>
)

data class SearchUserSubcategory(
    @SerializedName("id")
    @Expose
    var id: String,

    @SerializedName("subcategory_name")
    @Expose
    var subcategoryName: String,

    @SerializedName("banner_image")
    @Expose
    var bannerImage: String,

    @SerializedName("slug")
    @Expose
    var slug: String
)

data class SearchDealsCategoryResponse(
    @SerializedName("deals_result")
    @Expose
    var searchdeals: List<SearchDealsCategoryItems>? = null
)

class SearchDealsCategoryItems(
    @SerializedName("subcategory_name")
    @Expose
//    var subcategoryName: List<String>,
    var subcategoryName: String,

    @SerializedName("status")
    @Expose
    var status: String,

    @SerializedName("catslug")
    @Expose
    var catslug: String,

    @SerializedName("dealid")
    @Expose
    var dealid: Int,

    @SerializedName("title")
    @Expose
    var title: String,

    @SerializedName("slug")
    @Expose
    var slug: String,

    @SerializedName("offer_category")
    @Expose
    var offerCategory: String,

    @SerializedName("deal_image")
    @Expose
    var dealImage: String,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String,

    @SerializedName("description")
    @Expose
    var description: String,

    @SerializedName("terms_and_conditions")
    @Expose
    var termsAndConditions: String,

    @SerializedName("business_name")
    @Expose
    var business_name: String,

    @SerializedName("address")
    @Expose
    var address: String,

    @SerializedName("latitude")
    @Expose
    var latitude: String,

    @SerializedName("longitude")
    @Expose
    var longitude: String,

    @SerializedName("regular_price")
    @Expose
    var regularPrice: Float,

    @SerializedName("offer_price")
    @Expose
    var offerPrice: Float,

    @SerializedName("discount")
    @Expose
    var discount: Float,

    @SerializedName("saving_amount")
    @Expose
    var savingAmount: Float,

    @SerializedName("expiry_date")
    @Expose
    var expiryDate: String,

    @SerializedName("redeemed_deals")
    @Expose
    var redeemed_deals: String,

    @SerializedName("remaining_deals")
    @Expose
    var remainingDeals: String,

    @SerializedName("total_left")
    @Expose
    var total_left: String,

    @SerializedName("deal_club")
    @Expose
    var deal_club: String,
    @SerializedName("merchant_id")
    @Expose
    var marchentId: String,
    @SerializedName("loyalty_points")
    @Expose
    var loyaltyPoints: Int,
    @SerializedName("type_of_deals")
    @Expose
    var typeOfDeal: String?,

    @SerializedName("is_applicable")
    @Expose
    var isApplicable: Int = 1
)

class DealItem(
    @SerializedName("dealsdetail")
    @Expose
    var dealsdetail: Dealsdetail
)

class Dealsdetail(
    @SerializedName("merchantid")
    @Expose
    var merchantid: Int,

    @SerializedName("categoriescategory_name")
    @Expose
    var categoriescategoryName: String,

    @SerializedName("business_name")
    @Expose
    var businessName: String,

    @SerializedName("title")
    @Expose
    var title: String,

    @SerializedName("description")
    @Expose
    var description: String,

    @SerializedName("terms_and_conditions")
    @Expose
    var termsAndConditions: String,

    @SerializedName("address")
    @Expose
    var address: String,

    @SerializedName("deal_category")
    @Expose
    var dealCategory: String,

    @SerializedName("deal_type")
    @Expose
    var dealType: String,

    @SerializedName("regular_price")
    @Expose
    var regularPrice: Float,

    @SerializedName("offer_price")
    @Expose
    var offerPrice: Float,

    @SerializedName("discount")
    @Expose
    var discount: Float,

    @SerializedName("saving_amount")
    @Expose
    var savingAmount: Float,

    @SerializedName("deal_image")
    @Expose
    var dealImage: String,

    @SerializedName("deal_club")
    @Expose
    var dealClub: String,

    @SerializedName(" deallionaires_club_info")
    @Expose
    var userClubInfo: String?,

    @SerializedName("offer_type_id")
    @Expose
    var offerTypeId: Int,

    @SerializedName("offer_category_id")
    @Expose
    var offerCategoryId: Int,

    @SerializedName("loyalty_points")
    @Expose
    var loyaltyPoints: Int,

    @SerializedName("reward_points")
    @Expose
    var rewardPoints: Int,

    @SerializedName("customer_base_offer_type_id")
    @Expose
    var customerBaseOfferTypeId: Int,

    @SerializedName("deallionaires_club")
    @Expose
    var userClub: String,

    @SerializedName("expiry_date")
    @Expose
    var expiryDate: String?,

    @SerializedName("redeemed_deals")
    @Expose
    var claimed: String,

    @SerializedName("remaining_deals")
    @Expose
    var remaining: String,

    @SerializedName("start_date")
    @Expose
    var startDate: String,

    @SerializedName("deal_club_name")
    @Expose
    var deal_club_name: String,
    @SerializedName("user_club_name")
    @Expose
    var user_club_name: String,

    @SerializedName("type_of_deals")
    @Expose
    var typeOfDeal: String
)

data class FilteredDeals(
    @SerializedName("status_code")
    @Expose
    val statusCode: Int,
    @SerializedName("msg")
    @Expose
    val msg: FilterData
)

class FilterData(
    @SerializedName("viewdeals")
    @Expose
    val viewdeals: List<SearchDealsCategoryItems>
)

data class SearchBusinessCategoryResponse(
//    @SerializedName("searchbusiness")
    @SerializedName("result_business")
    @Expose
    var searchbusiness: List<Searchbusiness>,
    @SerializedName("msg")
    @Expose
    var msg: String
)

data class Searchbusiness(
    @SerializedName("merchantid")
    @Expose
    var merchantid: Int,

    @SerializedName("status")
    @Expose
    var status: String,

    @SerializedName("slug")
    @Expose
    var slug: String,

    @SerializedName("subcategory_name")
    @Expose
    var subcategoryName: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String,

    @SerializedName("address")
    @Expose
    var address: String,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String,

    @SerializedName("business_description")
    @Expose
    var businessDescription: String,

    @SerializedName("followers")
    @Expose
    var followers: Int,

    @SerializedName("rating")
    @Expose
    var rating: Float,

    @SerializedName("isLiked")
    @Expose
    var isLiked: Int,

    @SerializedName("rating_count")
    @Expose
    var ratingCount: Int? = 0,

    @SerializedName("club_expiry")
    @Expose
    var expiry_date: String? = null,

    @SerializedName("deallionaires_club")
    @Expose
    var deallionairsclub: String? = null
)

data class SearchBusinessItemDetail(
    @SerializedName("viewbusinessname")
    @Expose
    var viewbusinessname: List<Viewbusinessname>,

    @SerializedName("viewcategoryname")
    @Expose
    var viewcategoryname: List<Viewcategoryname>,

    @SerializedName("viewsubcategoryname")
    @Expose
    var viewsubcategoryname: List<String>,

    @SerializedName("viewbusinessrating")
    @Expose
    var viewbusinessrating: List<Viewbusinessrating>,

    @SerializedName("viewimages")
    @Expose
    var viewimages: List<String>,

    @SerializedName("viewdeals")
    @Expose
    var viewdeals: List<SearchDealsCategoryItems>,

    @SerializedName("viewbranch")
    @Expose
    var viewbranch: List<Searchbusiness>,

    @SerializedName("viewreviewrating")
    @Expose
    var viewreviewrating: List<Viewreviewrating>,

    @SerializedName("viewcontact")
    @Expose
    var viewcontact: List<Viewcontact>,

    @SerializedName("followers")
    @Expose
    var followers: Int?,

    @SerializedName("loginloyaltypoints")
    @Expose
    var loginLoyaltyPoints: Int?,

    @SerializedName("loginclubexpirydate")
    @Expose
    var loginclubexpirydate: String?,

    @SerializedName("isLiked")
    @Expose
    var isLiked: Int?,

    @SerializedName("dealsspecialoffers")
    @Expose
    var dealsspecialoffers: Dealsspecialoffers?
)

data class Dealsspecialoffers(
    @SerializedName("title")
    @Expose
    var title: String?,

    @SerializedName("dealsspecialdata")
    @Expose
    var dealsspecialdata: List<Any>?
)


data class Viewbranch(
    @SerializedName("id")
    @Expose
    var id: Int,

    @SerializedName("business_name")
    @Expose
    var businessName: String,

    @SerializedName("address")
    @Expose
    var address: String,

    @SerializedName("business_contact_no")
    @Expose
    var businessContactNo: String?,
    @SerializedName("club_expiry")
    @Expose
    var clubExpiry: String?,
    @SerializedName("category_name")
    @Expose
    var categoryname: String?,
    @SerializedName("review_rating")
    @Expose
    var reviewRating: Float?,
    @SerializedName("followers")
    @Expose
    var followers: Int?
)

data class Viewbusinessname(
    @SerializedName("latitude")
    @Expose
    var latitude: String?,

    @SerializedName("longitude")
    @Expose
    var longitude: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("city")
    @Expose
    var city: String,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("business_description")
    @Expose
    var businessDescription: String?,

    @SerializedName("club_expiry")
    @Expose
    var clubExpiry: String?,
    @SerializedName("deallionaires_club")
    @Expose
    var deallionairesClub: String?,
    @SerializedName("loyalty_points")
    @Expose
    var loyaltyPoints: String?,
    @SerializedName("mobile_number")
    @Expose
    var mobileNumbers: List<String?>
)

data class Viewbusinessrating(
    @SerializedName("rating")
    @Expose
    var rating: Float?
)

data class Viewcategoryname(
    @SerializedName("category_name")
    @Expose
    var categoryName: String?
)

data class Viewcontact(
    @SerializedName("latitude")
    @Expose
    var latitude: String?,

    @SerializedName("longitude")
    @Expose
    var longitude: String?
)

data class Viewdeal(
    @SerializedName("dealid")
    @Expose
    var dealid: Int,

    @SerializedName("deal_image")
    @Expose
    var dealImage: String,

    @SerializedName("title")
    @Expose
    var title: String,

    @SerializedName("description")
    @Expose
    var description: String,

    @SerializedName("regular_price")
    @Expose
    var regularPrice: Float,

    @SerializedName("offer_price")
    @Expose
    var offerPrice: Float,

    @SerializedName("discount")
    @Expose
    var discount: Int,

    @SerializedName("saving_amount")
    @Expose
    var savingAmount: Int,

    @SerializedName("slug")
    @Expose
    var slug: String,

    @SerializedName("expiry_date")
    @Expose
    var expiryDate: String? = null,
    @SerializedName("redeemed")
    @Expose
    var redeemed: Int? = null,
    @SerializedName("remaining")
    @Expose
    var remaining: Int? = null
)

data class Viewreviewrating(
    @SerializedName("review")
    @Expose
    var review: String,

    @SerializedName("rating")
    @Expose
    var rating: String,

    @SerializedName("merchant_reply")
    @Expose
    var merchantReply: String?,

    @SerializedName("created_at")
    @Expose
    var createdAt: String,

    @SerializedName("first_name")
    @Expose
    var firstName: String,

    @SerializedName("last_name")
    @Expose
    var lastName: String,
    @SerializedName("merchant_reply_date")
    @Expose
    var merchentReplyDate: String
)

data class Viewsubcategoryname(
    @SerializedName("subcategory_name")
    @Expose
    var subcategoryName: String?
)

class InstaResposne(
    @SerializedName("access_token")
    @Expose
    var accessToken: String?,

    @SerializedName("user_id")
    @Expose
    var userId: Long?
)

class InstaUser(
    @SerializedName("id")
    val userId: String,

    @SerializedName("username")
    val userName: String
)


class InstaLongLiveResposne(

    @SerializedName("access_token")
    @Expose
    var accessToken: String?,

    @SerializedName("token_type")
    @Expose
    var token_type: String?,

    @SerializedName("expires_in")
    @Expose
    var expiresIn: Long?
)

class InstaBody(
    @SerializedName("client_id")
    @Expose
    var client_id: String?,

    @SerializedName("client_secret")
    @Expose
    var client_secret: String?,

    @SerializedName("grant_type")
    @Expose
    var grant_type: String?,

    @SerializedName("redirect_uri")
    @Expose
    var redirect_uri: String?,

    @SerializedName("code")
    @Expose
    var code: String?
)


class DashboardResponse(
    @SerializedName("dashboard")
    @Expose
    var dashboard: Dashboard?
)

class Dashboard(
    @SerializedName("My_deals")
    @Expose
    var myDeals: Int,

    @SerializedName("favorites")
    @Expose
    var favorites: Int?,

    @SerializedName("Loyalty_points")
    @Expose
    var loyaltyPoints: Int,

    @SerializedName("business_count")
    @Expose
    var businessCount: Int
)

data class DeallionairesClubResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,

    @SerializedName("result")
    @Expose
    var deallionairesClub: List<DeallionairesClub>?
)

data class DeallionairesClub(
    @SerializedName("merchantid")
    @Expose
    var merchantid: Int,

    @SerializedName("isLiked")
    @Expose
    var isLiked: Int,

    @SerializedName("subcategory_name")
    @Expose
    var subcategoryName: String?,

    @SerializedName("slug")
    @Expose
    var slug: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String,

    @SerializedName("business_description")
    @Expose
    var businessDescription: String?,

    @SerializedName("club_expiry")
    @Expose
    var clubExpiry: String?,

    @SerializedName("category_name")
    @Expose
    var categoryName: String?,
    @SerializedName("rating")
    @Expose
    var rating: Float?,

    @SerializedName("rating_count")
    @Expose
    var ratingCount: Int?,
    @SerializedName("follower_count")
    @Expose
    var followerCount: Int?,

    @SerializedName("deallionaires_club")
    @Expose
    var deallionairesClub: String?
)

data class MerchantWiseDeallionsResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,

    @SerializedName("applevelpoints")
    @Expose
    var applevelpoints: AppLevelPoints?,

    @SerializedName("offervaluepoints")
    @Expose
    var offervaluepoints: List<Offervaluepoint>?
)

data class AllDealsBusinessList(
    @SerializedName("success")
    @Expose
    var success: Boolean?,

    @SerializedName("list_result")
    @Expose
    var allList: JsonArray?,

    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class AppLevelPoints(
    @SerializedName("first_name")
    @Expose
    var first_name: String?,

    @SerializedName("last_name")
    @Expose
    var last_name: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("app_level_loyalty_points")
    @Expose
    var app_level_loyalty_points: Int?
)

data class Offervaluepoint(
    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("merchant_id")
    @Expose
    var merchantId: Int?,

    @SerializedName("is_app_level_point_allow")
    @Expose
    var isAppLevelPointAllow: String?,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("total_deallions")
    @Expose
    var totalDeallions: Int?,

    @SerializedName("totalpoints")
    @Expose
    var totalPoints: String?,

    @SerializedName("merchant_level_points")
    @Expose
    var marchentLevelPoints: String?,

    @SerializedName("category_name")
    @Expose
    var categoryName: String?
)

data class TransactionWiseResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean,
    @SerializedName("transactionvaluepoints")
    @Expose
    var transactionvaluepoints: List<Transactionvaluepoints>?
)

data class Transactionvaluepoints(

    @SerializedName("id")
    @Expose
    var id: Int?,

    @SerializedName("business_name")
    @Expose
    var business_name: String?,

    @SerializedName("merchants_id")
    @Expose
    var merchant_id: Int?,

    @SerializedName("deallions_offered")
    @Expose
    var deallions_offered: Int?,

    @SerializedName("profile_image")
    @Expose
    var profile_image: String?,

    @SerializedName("city")
    @Expose
    var city: String?,

    @SerializedName("latitude")
    @Expose
    var latitude: String?,

    @SerializedName("longitude")
    @Expose
    var longitude: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("category_name")
    @Expose
    var category_name: String?,


    @SerializedName("transactiondetails")
    @Expose
    var transactionDetails: List<TransactionDetail>?

)

data class TransactionDetail(
    @SerializedName("loyalty_points")
    @Expose
    var loyaltyPoints: Int?,
    @SerializedName("transaction_type")
    @Expose
    var transactionType: String?,
    @SerializedName("transaction_date")
    @Expose
    var transactionDate: String?
)

data class DealStatus(

    @SerializedName("redeemdealid")
    @Expose
    val redeemdealid: Int?,
    @SerializedName("merchantid")
    @Expose
    val merchantid: Int?,
    @SerializedName("dealid")
    @Expose
    val dealid: Int?,
    @SerializedName("business_name")
    @Expose
    val business_name: String?,
    @SerializedName("city")
    @Expose
    val city: String?,
    @SerializedName("address")
    @Expose
    val address: String?,
    @SerializedName("latitude")
    @Expose
    val latitude: String?,
    @SerializedName("longitude")
    @Expose
    val longitude: String?,
    @SerializedName("profile_image")
    @Expose
    val profile_image: String?,
    @SerializedName("title")
    @Expose
    val title: String?,
    @SerializedName("expiry_date")
    @Expose
    val expiry_date: String?,
    @SerializedName("deallions_offered")
    @Expose
    val deallions_offered: Int?,
    @SerializedName("total_deallions")
    @Expose
    val total_deallions: Long?,
    @SerializedName("regular_price")
    @Expose
    val regular_price: Int?,
    @SerializedName("offer_price")
    @Expose
    val offer_price: Int?,
    @SerializedName("transaction_id")
    @Expose
    val transaction_id: String?,
    @SerializedName("sys_ref_code")
    @Expose
    val sys_ref_code: String?,
    @SerializedName("verification_code")
    @Expose
    val verification_code: String?,
    @SerializedName("purchase_amount")
    @Expose
    val purchase_amount: Float?,
    @SerializedName("saving_amount")
    @Expose
    val saving_amount: Int?,
    @SerializedName("status")
    @Expose
    val status: Int?,
    @SerializedName("offer_category_id")
    @Expose
    val offer_category_id: Int?,
    @SerializedName("offer_type_id")
    @Expose
    val offer_type_id: Int?,
    @SerializedName("customer_base_offer_type_id")
    @Expose
    val customer_base_offer_type_id: Int?,
    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("deallions_used")
    @Expose
    val deallionsUsed: Int?,
    @SerializedName("deallions_earned")
    @Expose
    val deallionsEarned: Int?
)

data class DealStatusResponse(
    @SerializedName("getreedemdetail")
    @Expose
    val getreedemdetail: List<DealStatus>
)

data class FavouritesResponse(
    @SerializedName("getfavouritedetail")
    @Expose
    var getfavouritedetail: List<Getfavouritedetail>?
)

data class Getfavouritedetail(
    @SerializedName("favouriteid")
    @Expose
    var favouriteid: Int?,

    @SerializedName("merchant_id")
    @Expose
    var merchentId: Int?,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("business_description")
    @Expose
    var businessDescription: String?,

    @SerializedName("category_name")
    @Expose
    var category_name: String?,

    @SerializedName("subcategory_name")
    @Expose
    var subcategory_name: String?,

    @SerializedName("city")
    @Expose
    var city: String?,
    @SerializedName("address")
    @Expose
    var address: String?,
    @SerializedName("latitude")
    @Expose
    var latitude: String?,
    @SerializedName("longitude")
    @Expose
    var longitude: String?,

    @SerializedName("follower_count")
    @Expose
    var followerCount: Int?,

    @SerializedName("rating")
    @Expose
    var rating: Float?,

    @SerializedName("rating_count")
    @Expose
    var ratingCount: Int?,

    @SerializedName("club_expiry")
    @Expose
    var expiry_date: String?,

    @SerializedName("deallionaires_club")
    @Expose
    var deallionairesClub: String?


)

data class MerchantNotificationModel(
    @SerializedName("notimerchant")
    @Expose
    val notimerchant: List<Notimerchant>
)

data class Notimerchant(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("title")
    @Expose
    val title: String,
    @SerializedName("description")
    @Expose
    val description: String,
    @SerializedName("business_name")
    @Expose
    val business_name: String,
    @SerializedName("profile_image")
    @Expose
    val profile_image: String
)

data class AdminNotificationModel(

    @SerializedName("notiadmin") val notiadmin: List<Notiadmin>
)

data class Notiadmin(

    @SerializedName("id") val id: Int,
    @SerializedName("description") val description: String,
    @SerializedName("created_at") val created_at: String,
    @SerializedName("profile_image") val profile_image: String
)

data class LoginResponse(

    @SerializedName("success")
    @Expose
    val success: Boolean?,

    @SerializedName("tokenkey")
    @Expose
    val tokenKey: String?,

    @SerializedName("errorcode")
    @Expose
    val errorcode: Int?,

    @SerializedName("msg")
    @Expose
    val msg: String?
)

data class SignUpUser(
    var first_name: String,
    var last_name: String,
    var email: String,
    var mobile_number: String,
    var countrycode: Int,
    var password: String,
    var address: String,
    var gender: String,
    var date_of_birth: String,
    var signup_type: String,
    var social_id: String
)


data class ForgotPasswordResponse(
    @SerializedName("msg")
    @Expose
    val msg: String,

    @SerializedName("token")
    @Expose
    val token: String?
)

data class ResetPasswordResponse(
    @SerializedName("msg")
    @Expose
    val msg: String
)

//data class ResetPassword(
//    val new_password: String,
//    val confirm_password: String,
//    val input_type: String
//)

data class ResetPassword(
    val password: String,
    val input_type: String
)

data class ForgotPassword(
    var email: String
)

data class SpecialOfferResponse(
    @SerializedName("specialoffers")
    @Expose
    var specialoffers: List<Specialoffer>
)

data class Specialoffer(
    @SerializedName("id")
    @Expose
    var id: Int?,

    @SerializedName("title")
    @Expose
    var title: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("dealid")
    @Expose
    var dealid: Int?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("regular_price")
    @Expose
    var regularPrice: Int?,

    @SerializedName("offer_price")
    @Expose
    var offerPrice: Int?,

    @SerializedName("discount")
    @Expose
    var discount: Int?,

    @SerializedName("saving_amount")
    @Expose
    var savingAmount: Int?,

    @SerializedName("deal_image")
    @Expose
    var dealImage: String?,
    @SerializedName("profile_image")
    @Expose
    var profileImage: String,

    @SerializedName("address")
    @Expose
    var address: String,
    @SerializedName("terms_and_conditions")
    @Expose
    var termsAndConditions: String,
    @SerializedName("redeemed_deals")
    @Expose
    var redeemed_deals: String,

    @SerializedName("total_left")
    @Expose
    var total_left: String
)

data class MerchantBody(
    var merchant_id: String
)

data class FavouriteAdded(
    @SerializedName("msg")
    @Expose
    var msg: String?,

    @SerializedName("favid")
    @Expose
    var favid: String?
)

data class FavouriteRemoved(
    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class HelpResponse(

    @SerializedName("success")
    @Expose
    var success: Boolean,
    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class ReviewAdded(
    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class EditProfile(
    @SerializedName("profile_image")
    @Expose
    var profile_image: String?,

    @SerializedName("first_name")
    @Expose
    var first_name: String,

    @SerializedName("last_name")
    @Expose
    var last_name: String,

    @SerializedName("mobile_number")
    @Expose
    var mobile_number: String,

    @SerializedName("address")
    @Expose
    var address: String,

    @SerializedName("country")
    @Expose
    var country: String,

    @SerializedName("city")
    @Expose
    var city: String,

    @SerializedName("state")
    @Expose
    var state: String,

    @SerializedName("gender")
    @Expose
    var gender: String,

    @SerializedName("date_of_birth")
    @Expose
    var date_of_birth: String
)

data class EditProfileResponse(
    @SerializedName("msg")
    @Expose
    var msg: String
)

data class ImageUploadResponse(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("image_link")
    val imageLink: String,
    @SerializedName("msg")
    val msg: String


)

data class Business(
    @SerializedName("merchantid")
    @Expose
    var merchantid: Int?,

    @SerializedName("slug")
    @Expose
    var slug: String?,

    @SerializedName("subcategory_name")
    @Expose
    var subcategoryName: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("profile_image")
    @Expose
    var profileImage: String?,

    @SerializedName("business_description")
    @Expose
    var businessDescription: String?,

    @SerializedName("distance")
    @Expose
    var distance: Float?
)

data class Deal(
    @SerializedName("subcategory_name")
    @Expose
    var subcategoryName: String?,

    @SerializedName("status")
    @Expose
    var status: String?,

    @SerializedName("catslug")
    @Expose
    var catslug: String?,

    @SerializedName("dealid")
    @Expose
    var dealid: Int?,

    @SerializedName("title")
    @Expose
    var title: String?,

    @SerializedName("slug")
    @Expose
    var slug: String?,

    @SerializedName("deal_image")
    @Expose
    var dealImage: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("terms_and_conditions")
    @Expose
    var termsAndConditions: String?,

    @SerializedName("business_name")
    @Expose
    var businessName: String?,

    @SerializedName("regular_price")
    @Expose
    var regularPrice: Int?,

    @SerializedName("offer_price")
    @Expose
    var offerPrice: Int?,

    @SerializedName("discount")
    @Expose
    var discount: Int?,

    @SerializedName("saving_amount")
    @Expose
    var savingAmount: Int?,

    @SerializedName("expiry_date")
    @Expose
    var expiryDate: String?,

    @SerializedName("distance")
    @Expose
    var distance: Float?
)

data class SearchHomeDealsResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,

    @SerializedName("business")
    @Expose
    var business: List<Business>?,

    @SerializedName("deals")
    @Expose
    var deals: List<Deal>?,

    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class SearchDealsLocal(

    var searchHomeType: String?,

    @SerializedName("merchantid")
    @Expose
    var merchantid: Int?,

    @SerializedName("slug")
    @Expose
    var slug: String?,

    @SerializedName("subcategory_name")
    @Expose
    var subcategory_name: String?,

    @SerializedName("business_name")
    @Expose
    var business_name: String?,

    @SerializedName("address")
    @Expose
    var address: String?,

    @SerializedName("profile_image")
    @Expose
    var profile_image: String?,

    @SerializedName("business_description")
    @Expose
    var business_description: String?,

    @SerializedName("distance")
    @Expose
    var distance: Float?,

    @SerializedName("status")
    @Expose
    var status: String?,

    @SerializedName("catslug")
    @Expose
    var catslug: String?,

    @SerializedName("dealid")
    @Expose
    var dealid: Int?,

    @SerializedName("terms_and_conditions")
    @Expose
    var terms_and_conditions: String?,

    @SerializedName("regular_price")
    @Expose
    var regular_price: Int?,

    @SerializedName("offer_price")
    @Expose
    var offer_price: Int?,

    @SerializedName("discount")
    @Expose
    var discount: Int?,

    @SerializedName("saving_amount")
    @Expose
    var saving_amount: Int?,

    @SerializedName("expiry_date")
    @Expose
    var expiry_date: String?
)

data class UserLatLong(
    val latitude: Double?,
    val longitude: Double?
)

data class DeallionairesCount(
    @SerializedName("sum")
    @Expose
    var sum: String?
)

data class AddReviewRating(
    val to_user_id: String,
    val review: String,
    val rating: String
)

data class EmailVerification(
    val email: String?,
    val input_type: String?
)

data class MobileVerification(
    val countrycode: String?,
    val mobile: String?,
    val input_type: String?
)

data class EmailVerificationResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,
    @SerializedName("token")
    @Expose
    var token: String?,
    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class MobileVerificationWithCountryCode(
    val countrycode: Int?,
    val mobile: Long?,
    val input_type: String?
)

data class MobileVerificationResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,
    @SerializedName("msg")
    @Expose
    var msg: String?,
    @SerializedName("token")
    @Expose
    var token: String?
)

data class MobNumChangeOtpVerification(
    val countrycode: Int?,
    val mobile: Long?,
    val otp: String?,
    val input_type: String?
)

data class OtpVerification(
    val otp: String?,
    val input_type: String?
)

data class OtpVerificationResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,
    @SerializedName("token")
    @Expose
    var token: String?,
    @SerializedName("msg")
    @Expose
    var msg: String?
)

data class FaqsResponse(
    @SerializedName("success")
    @Expose
    var success: Boolean?,
    @SerializedName("faq_results")
    @Expose
    var faqsResult: List<FaqCategory>?
)

data class FaqCategory(
    @SerializedName("category_id")
    @Expose
    var categoryId: Int?,

    @SerializedName("category_name")
    @Expose
    var categoryName: String?,

    @SerializedName("faq_list")
    @Expose
    var faqsList: List<Faqs>?,

    var isExpanded: Boolean = false

)

data class Faqs(
    @SerializedName("question")
    @Expose
    var question: String?,
    @SerializedName("answer")
    @Expose
    var answer: String?,
    var isExpanded: Boolean = false

)
