package com.example.deallionaires.model

import android.text.TextUtils
import com.example.deallionaires.network.AuthenticationInterceptor
import okhttp3.Credentials
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ServiceGenerator {
    private lateinit var retrofit: Retrofit
    const val BASE_URL = "https://api.instagram.com/"
    private val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private val builder: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())

    const val API_BASE_URL = "https://api.instagram.com/"

    fun <S> createInstagramService(
        serviceClass: Class<S>?, clientId: String?, clientSecret: String?
    ): S {
        if (!TextUtils.isEmpty(clientId)
            && !TextUtils.isEmpty(clientSecret)
        ) {
            val authToken: String = Credentials.basic(clientId!!, clientSecret!!)
            return createService(serviceClass, authToken)
        }
        return createService(serviceClass, null, null)
    }

    fun <S> createService(
        serviceClass: Class<S>?, authToken: String?
    ): S {
        if (!TextUtils.isEmpty(authToken)) {
            val interceptor = AuthenticationInterceptor(authToken!!)
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor)
                builder.client(httpClient.build())
                retrofit = builder.build()
            }
        }
        return retrofit.create(serviceClass!!)
    }

    fun <S> createService(
        serviceClass: Class<S>?, clientId: String?, clientSecret: String?
    ): S {
        if (!TextUtils.isEmpty(clientId)
            && !TextUtils.isEmpty(clientSecret)
        ) {
            val authToken: String = Credentials.basic(clientId!!, clientSecret!!)
            return createService(serviceClass, authToken)
        }
        return createService(serviceClass, null, null)
    }
}