package com.example.deallionaires.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap


class HomeData(
    @SerializedName("allheadings")
    @Expose
    var allHeadings: List<String>? = null ,

    @SerializedName("final_array")
    @Expose
    var finalData: LinkedHashMap<String,List<Any>>? = null


)
class HomeResponse {

    @SerializedName("banner")
    @Expose
    var banner: List<Banner>? = null

    @SerializedName("categories")
    @Expose
    var categories: List<Categories>? = null

    @SerializedName("deals")
    @Expose
    var homeDeals: List<Deals>? = ArrayList()

    @SerializedName("hotels")
    @Expose
    var hotelDeals: List<Deals>? = null

    @SerializedName("special_offer")
    @Expose
    var specialOfferDeals: List<Deals>? = null

    @SerializedName("beautyandfitness")
    @Expose
    var beautyAndFitnessDeals: List<Deals>? = null

    @SerializedName("electronics")
    @Expose
    var electronicsDeals: List<Deals>? = null

    @SerializedName("activities")
    @Expose
    var activitiesDeals: List<Deals>? = null

    @SerializedName("kids")
    @Expose
    var kidsDeals: List<Deals>? = null

    @SerializedName("onlinebusiness")
    @Expose
    var onlineBusinessDeals: List<Deals>? = null

    @SerializedName("regularservies")
    @Expose
    var regularServicesDeals: List<Deals>? = null

}