package com.example.deallionaires.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Profile {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("profile_image")
    @Expose
    var profileImage: String? = null

    @SerializedName("first_name")
    @Expose
    var firstName: String? = null

    @SerializedName("last_name")
    @Expose
    var lastName: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("countrycode")
    @Expose
    var countrycode: Int? = null

    @SerializedName("mobile_number")
    @Expose
    var mobileNumber: String? = null

    @SerializedName("address")
    @Expose
    var address: String? = null

    @SerializedName("country")
    @Expose
    var country: String? = null

    @SerializedName("city")
    @Expose
    var city: String? = null

    @SerializedName("state")
    @Expose
    var state: String? = null

    @SerializedName("gender")
    @Expose
    var gender: String? = null

    @SerializedName("date_of_birth")
    @Expose
    var dateOfBirth: String? = null

}